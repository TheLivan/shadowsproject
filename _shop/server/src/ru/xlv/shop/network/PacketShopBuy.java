package ru.xlv.shop.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.shop.XlvsShopMod;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.handle.result.ShopBuyResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketShopBuy implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private ShopBuyResult shopBuyResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int shopItemId = bbis.readInt();
        int costType = bbis.readInt();
        if(costType >= 0 && costType < ShopItemCost.Type.values().length) {
            if (REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
                XlvsShopMod.INSTANCE.getShopHandler().buy(entityPlayer.getCommandSenderName(), shopItemId, ShopItemCost.Type.values()[costType]).thenAccept(shopBuyResult1 -> {
                    shopBuyResult = shopBuyResult1;
                    packetCallbackSender.send();
                });
            } else {
                packetCallbackSender.send();
            }
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if(shopBuyResult != null) {
            bbos.writeBoolean(shopBuyResult == ShopBuyResult.SUCCESS);
            bbos.writeUTF(shopBuyResult.getResponseMessage());
        } else {
            bbos.writeBoolean(false);
            bbos.writeUTF(XlvsCore.INSTANCE.getLocalization().responseTooManyRequests);
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
