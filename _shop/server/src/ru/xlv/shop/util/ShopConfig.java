package ru.xlv.shop.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.ConfigComment;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.common.ShopItemType;
import ru.xlv.shop.handle.item.ShopItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class ShopConfig {

    @Configurable
    private final List<ShopItem> shopItemList = new ArrayList<>();

    public void load() {
        ShopItemStackConfig shopItemStackConfig = new ShopItemStackConfig();
        shopItemStackConfig.load();
        //...
        for (ShopItem shopItem : shopItemList) {
            for (ShopItem item : shopItemList) {
                if(shopItem != item && shopItem.getId() == item.getId()) {
                    throw new RuntimeException("A duplicate product id found: " + shopItem.getId());
                }
            }
            if(shopItem.getCosts().isEmpty()) {
                throw new RuntimeException("Costs not found: " + shopItem);
            }
            if(shopItem.getCategories().isEmpty()) {
                throw new RuntimeException("Categories not found: " + shopItem);
            }
        }
        shopItemList.addAll(shopItemStackConfig.getList()
                .stream()
                .map(shopItemStackModel -> {
                    List<ShopItemCost> collect = shopItemStackModel.costs.stream()
                            .map(shopItemCostModel -> new ShopItemCost(shopItemCostModel.type, shopItemCostModel.amount))
                            .collect(Collectors.toList());
                    return new ShopItemStack(
                            shopItemStackModel.shopItemType,
                            shopItemStackModel.id,
                            shopItemStackModel.name,
                            shopItemStackModel.description,
                            shopItemStackModel.categories,
                            collect,
                            shopItemStackModel.unlocalizedName,
                            shopItemStackModel.amount,
                            shopItemStackModel.metadata,
                            shopItemStackModel.nbtString
                    );
                })
                .collect(Collectors.toList())
        );
        System.out.println("ShopItems loaded: " + shopItemList.size());
    }

    public static void main(String[] args) {
        ShopConfig shopConfig = new ShopConfig();
        ShopItemStackConfig shopItemStackConfig = new ShopItemStackConfig();
        shopItemStackConfig.getList().add(new ShopItemStackModel(ShopItemType.ITEM, 0, "test", "test",
                Arrays.asList(ShopItemCategory.ARMOR, ShopItemCategory.BOOSTER),
                Arrays.asList(new ShopItemCostModel(ShopItemCost.Type.CREDITS, 123), new ShopItemCostModel(ShopItemCost.Type.PLATINUM, 123)),
                "incarn09", 1, 0, null
        ));
        shopItemStackConfig.save();
        shopConfig.load();
    }

    @Getter
    @Configurable
    public static class ShopItemStackConfig implements IConfigGson {
        private final List<ShopItemStackModel> list = new ArrayList<>();
        private transient final File configFile = new File("config/shop/item_registry.json");
    }

    @Configurable
    @RequiredArgsConstructor
    public static class ShopItemStackModel {
        @ConfigComment("Тип товара. Варианты: ITEM")
        private final ShopItemType shopItemType;
        @ConfigComment("Уникальный id.")
        private final int id;
        private final String name;
        private final String description;
        @ConfigComment("Категории, в которых товар будет отображаться. Варианты: MAIN, NEW, SELL_OUT, SPECIAL, KIT, WEAPON, ARMOR, CONSUMABLE, BOOSTER, VISUAL_EFFECT.")
        private final List<ShopItemCategory> categories;
        @ConfigComment("Стоимости товара. Варианты: CREDITS, PLATINUM")
        private final List<ShopItemCostModel> costs;
        @ConfigComment("Информацию по актуальным unlocalized names можно запросить у тех состава.")
        private final String unlocalizedName;
        @ConfigComment("Количество предметов на продаже.")
        private final int amount;
        @ConfigComment("Указывать 0, если не используется.")
        private final int metadata;
        @ConfigComment("Заполняется в качестве json-строки.")
        private final String nbtString;
    }

    @Configurable
    @RequiredArgsConstructor
    public static class ShopItemCostModel {
        private final ShopItemCost.Type type;
        private final int amount;
    }
}
