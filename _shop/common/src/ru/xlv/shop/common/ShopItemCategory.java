package ru.xlv.shop.common;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.network.IPacketComposable;

import java.util.List;

public enum ShopItemCategory implements IPacketComposable {

    MAIN,
    NEW,
    SELL_OUT,
    SPECIAL,
    KIT,
    WEAPON,
    ARMOR,
    CONSUMABLE,
    BOOSTER,
    VISUAL_EFFECT;

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(ordinal());
    }
}
