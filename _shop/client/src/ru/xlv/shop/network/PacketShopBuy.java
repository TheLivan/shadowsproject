package ru.xlv.shop.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.shop.common.ShopItemCost;

import javax.annotation.Nullable;
import java.io.IOException;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PacketShopBuy implements IPacketCallbackEffective<PacketShopBuy.Result> {

    private final Result result = new Result();

    private ShopItemCost.Type type;
    private int shopItemId;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(shopItemId);
        bbos.writeInt(type.ordinal());
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        result.responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
    }
}
