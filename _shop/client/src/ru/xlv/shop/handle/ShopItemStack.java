package ru.xlv.shop.handle;

import lombok.Getter;
import net.minecraft.item.ItemStack;
import ru.xlv.shop.common.ShopItem;
import ru.xlv.shop.common.ShopItemCategory;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.common.ShopItemType;

import java.util.List;

@Getter
public class ShopItemStack extends ShopItem {

    private final ItemStack itemStack;

    public ShopItemStack(ShopItemType shopItemType, int id, String name, String description, List<ShopItemCategory> categories, List<ShopItemCost> costs, ItemStack itemStack) {
        super(shopItemType, id, name, description, categories, costs);
        this.itemStack = itemStack;
    }
}
