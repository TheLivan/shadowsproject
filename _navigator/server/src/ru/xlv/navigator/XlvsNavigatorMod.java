package ru.xlv.navigator;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.navigator.handle.EventListener;
import ru.xlv.navigator.handle.MarkerManager;
import ru.xlv.navigator.handle.PortalManager;

import static ru.xlv.navigator.XlvsNavigatorMod.MODID;

@Mod(
        name = "XlvsNavigatorMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsNavigatorMod {

    static final String MODID = "xlvsnavigator";

    @Mod.Instance(MODID)
    public static XlvsNavigatorMod INSTANCE;

    @Getter
    private PortalManager portalManager;
    @Getter
    private MarkerManager markerManager;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        portalManager = new PortalManager();
        portalManager.init();
        MinecraftForge.EVENT_BUS.register(new EventListener());
    }
}
