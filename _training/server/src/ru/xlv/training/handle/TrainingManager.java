package ru.xlv.training.handle;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.KeyValueStore;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.training.common.trainer.ITrainer;
import ru.xlv.training.network.PacketTrainingSync;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

@RequiredArgsConstructor
public class TrainingManager {

    private KeyValueStore.Key trainingStateKey;

    private final WorldPosition spawnTrainingWorldPosition, spawnWorldPosition;

    private final Queue<ITrainer> trainers = new LinkedList<>();

    private final Map<String, Queue<ITrainer>> playerTrainers = new HashMap<>();

    @Nullable
    public ITrainer getActiveTrainer(String username) {
        synchronized (getPlayerTrainers()) {
            Queue<ITrainer> iTrainers = getPlayerTrainers().get(username);
            return iTrainers != null ? iTrainers.peek() : null;
        }
    }

    public void addTrainer(ITrainer trainer) {
        trainers.add(trainer);
    }

    public void startTraining(FlexPlayer flexPlayer) {
        flexPlayer
                .notNull()
                .apply(serverPlayer -> serverPlayer.getKeyValueStore().get(getKey(serverPlayer)))
                .notNull()
                .test((flexPlayer1, object) -> object instanceof Boolean)
                .test((flexPlayer1, object) -> ((boolean) object))
                .accept((flexPlayer1, object) -> initTraining(flexPlayer1))
                .accept((flexPlayer1, object) -> flexPlayer1.movePlayer(spawnTrainingWorldPosition));
    }

    public void stopTraining(FlexPlayer flexPlayer) {
        flexPlayer.
                notNull()
                .accept(serverPlayer -> serverPlayer.getKeyValueStore().set(getKey(serverPlayer), true))
                .accept(serverPlayer -> getPlayerTrainers().remove(serverPlayer.getPlayerName()))
                .movePlayer(spawnWorldPosition);
    }

    public void resetTraining(FlexPlayer flexPlayer) {
        initTraining(flexPlayer);
    }

    private void initTraining(FlexPlayer flexPlayer) {
        Queue<ITrainer> queue = new LinkedList<>();
        synchronized (trainers) {
            trainers.forEach(iTrainer -> queue.add(iTrainer.clone()));
        }
        flexPlayer = flexPlayer
                .notNull()
                .accept(serverPlayer -> getPlayerTrainers().put(serverPlayer.getPlayerName(), queue));
        sync(flexPlayer);
    }

    protected void sync(FlexPlayer flexPlayer) {
        flexPlayer
                .notNull()
                .apply(serverPlayer -> getPlayerTrainers().get(serverPlayer.getPlayerName()))
                .accept((flexPlayer1, iTrainers) -> flexPlayer1.sendPacket((entityPlayer, bbos) -> new PacketTrainingSync(iTrainers.peek())));
    }

    private KeyValueStore.Key getKey(ServerPlayer serverPlayer) {
        if (trainingStateKey == null) {
            trainingStateKey = serverPlayer.getKeyValueStore().genKey();
        }
        return trainingStateKey;
    }

    public synchronized Map<String, Queue<ITrainer>> getPlayerTrainers() {
        return playerTrainers;
    }

    public synchronized Queue<ITrainer> getTrainers() {
        return trainers;
    }
}
