package ru.xlv.training.handle;

import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.util.Utils;
import ru.xlv.training.common.trainer.*;

import java.util.Queue;

@RequiredArgsConstructor
public class TrainingHandler {

    private final TrainingManager trainingManager;

    public void interactMarker(EntityPlayer entityPlayer, int dim, double x, double y, double z, double radius) {
        ITrainer iTrainer = peekHead(entityPlayer.getCommandSenderName());
        if(iTrainer instanceof MarkerTrainer && Utils.isPlayerInside(entityPlayer, dim, x, y, z, radius)) {
            //todo logic
            pollHead(entityPlayer.getCommandSenderName());
            trainingManager.sync(FlexPlayer.of(entityPlayer.getCommandSenderName()));
        }
    }

    public void useItem(String username, ItemStack itemStack) {
        ITrainer iTrainer = peekHead(username);
        if (iTrainer instanceof ItemStackTrainer && ItemStack.areItemStacksEqual(itemStack, ((ItemStackTrainer) iTrainer).getItemStack())) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void equipItem(String username, Item item) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof EquipItemTrainer && ((EquipItemTrainer) iTrainer).getItem() == item) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void keyPressed(String username, int keyCode) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof KeyTrainer && ((KeyTrainer) iTrainer).getKeyCode() == keyCode) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void skillUse(String username, int skillId) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof SkillUseTrainer && ((SkillUseTrainer) iTrainer).getSkillId() == skillId) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void skillLearn(String username, int skillId) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof SkillLearnTrainer && ((SkillLearnTrainer) iTrainer).getSkillId() == skillId) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void skillSelect(String username, int skillId) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof SkillSelectTrainer && ((SkillSelectTrainer) iTrainer).getSkillId() == skillId) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    public void interactNpc(String username, String entityName) {
        ITrainer iTrainer = peekHead(username);
        if(iTrainer instanceof NpcInteractTrainer && ((NpcInteractTrainer) iTrainer).getNpcName().equals(entityName)) {
            //todo logic
            pollHead(username);
            trainingManager.sync(FlexPlayer.of(username));
        }
    }

    private ITrainer peekHead(String username) {
        Queue<ITrainer> iTrainers = trainingManager.getPlayerTrainers().get(username);
        return iTrainers != null ? iTrainers.peek() : null;
    }

    private void pollHead(String username) {
        Queue<ITrainer> iTrainers = trainingManager.getPlayerTrainers().get(username);
        if (iTrainers != null) {
            iTrainers.poll();
        }
    }
}
