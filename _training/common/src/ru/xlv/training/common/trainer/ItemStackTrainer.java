package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.ItemStack;

@Getter
@RequiredArgsConstructor
public class ItemStackTrainer implements ITrainer {

    private final ItemStack itemStack;
    private final String description;

    @Override
    public ITrainer clone() {
        return new ItemStackTrainer(itemStack, description);
    }
}
