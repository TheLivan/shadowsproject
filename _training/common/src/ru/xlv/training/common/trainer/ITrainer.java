package ru.xlv.training.common.trainer;

public interface ITrainer extends Cloneable {

    String getDescription();

    ITrainer clone();
}
