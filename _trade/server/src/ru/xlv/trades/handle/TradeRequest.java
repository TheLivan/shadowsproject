package ru.xlv.trades.handle;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.trade.common.SimpleTradeRequest;

@Setter
@Getter
public class TradeRequest extends SimpleTradeRequest<ServerPlayer> {

    public TradeRequest(ServerPlayer initiator, ServerPlayer target) {
        super(initiator, target);
    }
}
