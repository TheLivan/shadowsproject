package ru.xlv.trades.handle;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.player.ServerPlayer;

@Setter
@Getter
public class TradeInvite {

    public static final long REMOVAL_TIME_MILLS = 5000L;

    private final ServerPlayer initiator, target;
    private final long creationTimeMills;

    TradeInvite(ServerPlayer initiator, ServerPlayer target) {
        this.initiator = initiator;
        this.target = target;
        creationTimeMills = System.currentTimeMillis();
    }
}
