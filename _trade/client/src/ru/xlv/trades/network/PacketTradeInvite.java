package ru.xlv.trades.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketTradeInvite implements IPacketCallbackEffective<PacketTradeInvite.Result> {

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
    }

    private final Result result = new Result();

    private String targetName;

    public PacketTradeInvite(String targetName) {
        this.targetName = targetName;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(targetName);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean success = bbis.readBoolean();
        String responseMessage = bbis.readUTF();
        result.success = success;
        result.responseMessage = responseMessage;
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
