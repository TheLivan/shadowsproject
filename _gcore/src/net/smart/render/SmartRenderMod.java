// ==================================================================
// This file is part of Smart Render.
//
// Smart Render is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Smart Render is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Smart Render. If not, see <http://www.gnu.org/licenses/>.
// ==================================================================

package net.smart.render;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.smart.render.statistics.SmartStatisticsContext;

@Mod(modid = "SmartRender", name = "Smart Render", version = "2.1")
public class SmartRenderMod
{
	private static boolean addRenderer = true;

	private boolean hasRenderer = false;

	public static void doNotAddRenderer()
	{
		addRenderer = false;
	}

	@EventHandler
	@SuppressWarnings("unused")
	public void init(FMLInitializationEvent event)
	{
		if(!FMLCommonHandler.instance().getSide().isClient())
			return;

//		hasRenderer = Loader.isModLoaded("RenderPlayerAPI");
//
//		if(hasRenderer)
//		{
//			Class<?> type = Reflect.LoadClass(SmartRenderMod.class, new Name("net.smart.render.playerapi.SmartRender"), true);
//			Method method = Reflect.GetMethod(type, new Name("register"));
//			Reflect.Invoke(method, null);
//		}
//
//		if(!hasRenderer && addRenderer)
//			SmartRenderContext.registerRenderers(null);

		FMLCommonHandler.instance().bus().register(this);
	}

	@SubscribeEvent
	@SuppressWarnings({ "static-method", "unused" })
	public void tickStart(ClientTickEvent event)
	{
		SmartStatisticsContext.onTickInGame();
	}
}