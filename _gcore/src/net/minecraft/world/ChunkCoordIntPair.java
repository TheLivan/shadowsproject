package net.minecraft.world;

public class ChunkCoordIntPair
{
    /** The X position of this Chunk Coordinate Pair */
    public final int chunkXPos;
    /** The Z position of this Chunk Coordinate Pair */
    public final int chunkZPos;
    private int cachedHashCode = 0;

    public ChunkCoordIntPair(int p_i1947_1_, int p_i1947_2_)
    {
        this.chunkXPos = p_i1947_1_;
        this.chunkZPos = p_i1947_2_;
    }

    /**
     * converts a chunk coordinate pair to an integer (suitable for hashing)
     */
    public static long chunkXZ2Int(int p_77272_0_, int p_77272_1_)
    {
        return (long)p_77272_0_ & 4294967295L | ((long)p_77272_1_ & 4294967295L) << 32;
    }

    public int hashCode()
    {
        if (this.cachedHashCode == 0)
        {
            int var1 = 1664525 * this.chunkXPos + 1013904223;
            int var2 = 1664525 * (this.chunkZPos ^ -559038737) + 1013904223;
            this.cachedHashCode = var1 ^ var2;
        }

        return this.cachedHashCode;
    }

    public boolean equals(Object parObject)
    {
        if (this == parObject)
        {
            return true;
        }
        else if (!(parObject instanceof ChunkCoordIntPair))
        {
            return false;
        }
        else
        {
            ChunkCoordIntPair chunkcoordintpair = (ChunkCoordIntPair)parObject;
            return this.chunkXPos == chunkcoordintpair.chunkXPos && this.chunkZPos == chunkcoordintpair.chunkZPos;
        }
    }

    public int getCenterXPos()
    {
        return (this.chunkXPos << 4) + 8;
    }

    public int getCenterZPosition()
    {
        return (this.chunkZPos << 4) + 8;
    }

    public ChunkPosition func_151349_a(int p_151349_1_)
    {
        return new ChunkPosition(this.getCenterXPos(), p_151349_1_, this.getCenterZPosition());
    }

    public String toString()
    {
        return "[" + this.chunkXPos + ", " + this.chunkZPos + "]";
    }
}