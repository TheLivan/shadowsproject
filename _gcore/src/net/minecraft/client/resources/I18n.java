package net.minecraft.client.resources;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Map;

@SideOnly(Side.CLIENT)
public class I18n
{
    private static Locale i18nLocale;

    static void setLocale(Locale p_135051_0_)
    {
        i18nLocale = p_135051_0_;
    }

    /**
     * format(a, b) is equivalent to String.format(translate(a), b). Args: translationKey, params...
     */
    public static String format(String par0Str, Object ... par1ArrayOfObj)
    {
        return i18nLocale.formatMessage(par0Str, par1ArrayOfObj);
    }

    public static Map getLocaleProperties()
    {
        return i18nLocale.field_135032_a;
    }
}