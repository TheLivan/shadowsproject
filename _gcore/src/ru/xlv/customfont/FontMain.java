package ru.xlv.customfont;

public class FontMain {

    private static final FontContainer defaultFont;
    static {
        defaultFont = FontType.DEFAULT.getFontContainer();
    }

    public static FontContainer getDefaultFont() {
        return defaultFont;
    }
}
