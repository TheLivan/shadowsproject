package ru.xlv.core.resource;

public enum ResourceLoadingState {
    WAIT,
    ASYNC,
    SYNC,
    DONE,
    LOADER_NOT_FOUND
}
