package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;

@Getter
@RequiredArgsConstructor
public class EventRenderArmorMainMenu extends Event {

    private final EntityPlayer entityPlayer;
    private final RenderPlayer renderPlayer;
    private final String body, head, bracers, pants, boots;

}
