package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.util.MovingObjectPosition;

@AllArgsConstructor
@Getter
public class EventAttackObjectMouseOver extends Event {

    private final MovingObjectPosition movingObjectPosition;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
