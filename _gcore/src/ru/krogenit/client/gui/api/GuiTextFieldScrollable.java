package ru.krogenit.client.gui.api;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.xlv.customfont.FontType;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

public class GuiTextFieldScrollable extends GuiTextFieldAdvanced {
    public boolean numbersOnly = false;
    private final float posX;
    private final float posY;
    private final boolean wrapLine = true;
    private final GuiScroll scroll = new GuiScroll();
    private int cursorPosition;
    private static final IntBuffer intBuffer = BufferUtils.createIntBuffer(16);

    public GuiTextFieldScrollable(FontType fontType, float x, float y, float width, float height, float fontScale, int textColor, int maxLineWidth) {
        this(fontType, x, y, width, height, fontScale);
        this.enabledColor = textColor;
        setMaxStringLength(maxLineWidth);
    }

    public GuiTextFieldScrollable(FontType fontType, float x, float y, float width, float height, float fontScale) {
        super(fontType, x, y, width, height, fontScale);
        this.posX = x;
        this.posY = y;
        this.scroll.setScrollViewHeight(this.height);
        this.setMaxStringLength(Integer.MAX_VALUE);
        this.canEdit = true;
    }

    @Override
    public boolean textboxKeyTyped(char c, int i) {
        if (this.isFocused() && this.canEdit) {
            String originalText = this.getText();
            this.setText(originalText);

            if (c == 13 || c == 10) {
                this.setText(originalText.substring(0, this.cursorPosition) + c + originalText.substring(this.cursorPosition));
            }

            this.setCursorPositionZero();
            this.moveCursorBy(this.cursorPosition);
            boolean bo = super.textboxKeyTyped(c, i);
            String newText = this.getText();

            if (i != 211) {
                this.cursorPosition += newText.length() - originalText.length();
            }

            if (i == 203 && this.cursorPosition > 0) {
                --this.cursorPosition;
            }

            if (i == 205 && this.cursorPosition < newText.length()) {
                ++this.cursorPosition;
            }

            return bo;
        } else {
            return false;
        }
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        mouseX = Mouse.getEventX();
        mouseY = mc.displayHeight - Mouse.getEventY() - 1;
        if (this.canEdit) {
            boolean flag = mouseX >= this.xPosition && mouseX < this.xPosition + this.width && mouseY >= this.yPosition && mouseY < this.yPosition + this.height;

            if (this.canLoseFocus) {
                this.setFocused(flag);
            }

            if (this.isFocused && mouseButton == 0) {
                float l = mouseX - this.xPosition;

                if (this.enableBackgroundDrawing) {
                    l -= 4;
                }

                String s = this.fontRenderer.trimStringToWidth(this.text.substring(this.lineScrollOffset), this.getWidth());
                this.setCursorPosition(this.fontRenderer.trimStringToWidth(s, l).length() + this.lineScrollOffset);
            }
        }

        if (this.scroll.mouseClicked(mouseX, mouseY, mouseButton)) {
        } else if (mouseButton == 0 && this.canEdit) {
            float x = mouseX - this.xPosition;
            float y = mouseY - this.yPosition - ScaleGui.get(4) + scroll.getScrollAnim();
            this.cursorPosition = 0;
            int charCount = 0;
            float searchY = 0;
            float maxSize = this.width - ScaleGui.get(14);
            float fontHeight = getFontHeight();

            for (String s : this.getLines()) {
                StringBuilder line = new StringBuilder();

                for (char c : s.toCharArray()) {
                    this.cursorPosition = charCount;
                    float lineWidth = fontContainer.width(line.toString() + c) * fontScale;
                    if (lineWidth > maxSize && this.wrapLine) {
                        searchY += fontHeight;
                        line = new StringBuilder();

                        if (y < searchY) {
                            break;
                        }
                    }

                    if (y > searchY && y <= searchY + fontHeight && x <= fontContainer.width(line.toString() + c) * fontScale) {
                        return;
                    }

                    ++charCount;
                    line.append(c);
                }

                this.cursorPosition = charCount;
                searchY += fontHeight;
                ++charCount;

                if (y < searchY) {
                    break;
                }
            }

            if (y >= searchY) {
                this.cursorPosition = this.getText().length();
            }
        }
    }

    private List<String> getLines() {
        List<String> list = new ArrayList<>();
        StringBuilder line = new StringBuilder();

        for (char c : this.getText().toCharArray()) {
            if (c != 13 && c != 10) {
                line.append(c);
            } else {
                list.add(line.toString());
                line = new StringBuilder();
            }
        }

        list.add(line.toString());
        return list;
    }

    public void drawTextBox(int mouseX, int mouseY) {
        drawBackground();
        int color = enabledColor;
        boolean flag = this.isFocused() && this.cursorCounter / 6 % 2 == 0;
        float startLine = 0;
        float maxLine = this.height + startLine;
        List<String> lines = this.getLines();
        int charCount = 0;
        float lineY = -scroll.getScrollAnim();
        float maxSize = this.width - ScaleGui.get(14);
        int k2;


        boolean scissor = GL11.glGetBoolean(GL11.GL_SCISSOR_TEST);
        if(!scissor) {
            GL11.glEnable(GL11.GL_SCISSOR_TEST);
            GL11.glScissor((int) xPosition, (int) (mc.displayHeight - yPosition - height), (int)width,(int)height);
        } else {
            GL11.glGetInteger(GL11.GL_SCISSOR_BOX, (IntBuffer) intBuffer.position(0));
            int sx = intBuffer.get(0);
            int sy = intBuffer.get(1);
            int swidth = intBuffer.get(2);
            int sheight = intBuffer.get(3);
            int syy = (int) (mc.displayHeight - yPosition - height);
            GL11.glScissor((int) Math.max(sx, xPosition), Math.max(syy, sy), (int)Math.min(swidth, width),(int)Math.min(sheight, height));
        }

        for (k2 = 0; k2 < lines.size(); ++k2) {
            String wholeLine = lines.get(k2);
            String line = "";
            char[] xx = wholeLine.toCharArray();
            float yy = xx.length;

            float offset = ScaleGui.get(4);
            float offsetY = ScaleGui.get(12f);
            float underscoreOffsetY = ScaleGui.get(8f);
            float fontHeight = getFontHeight();
            for (int var16 = 0; var16 < yy; ++var16) {
                char c = xx[var16];

                if (fontContainer.width(line + c) * fontScale > maxSize && this.wrapLine) {
                    if (lineY >= startLine - fontHeight && lineY < maxLine) {
                        GuiDrawUtils.drawStringNoScale(fontContainer, line, this.xPosition + offset, this.yPosition + offsetY + lineY, fontScale, color);
                    }

                    line = "";
                    lineY+=fontHeight;
                }

                if (flag && charCount == this.cursorPosition && fontHeight >= startLine && fontHeight < maxLine && this.canEdit) {
                    float xx1 = this.xPosition + fontContainer.width(line) * fontScale + offset;
                    float yy1 = this.yPosition + offset + lineY;

                    if (this.getText().length() == this.cursorPosition) {
                        GuiDrawUtils.drawStringNoScale(fontContainer, "_", xx1, yy1 + underscoreOffsetY, fontScale, color);
                    } else {
                        GL11.glDisable(GL11.GL_TEXTURE_2D);
                        GuiDrawUtils.drawRect(xx1, yy1 - 1, 1, getFontHeight(), 1f, 1f, 1f, 1f);
                        GL11.glEnable(GL11.GL_TEXTURE_2D);
                    }
                }

                ++charCount;
                line = line + c;
            }

            if (lineY >= startLine - fontHeight && lineY < startLine + height + fontHeight) {
                GuiDrawUtils.drawStringNoScale(fontContainer, line, this.xPosition + offset, this.yPosition + offsetY + lineY, fontScale, color);

                if (flag && charCount == this.cursorPosition && this.canEdit) {
                    float var20 = this.xPosition + fontContainer.width(line) * fontScale + offset;
                    yy = this.yPosition + offset + lineY;

                    if (this.getText().length() == this.cursorPosition) {
                        GuiDrawUtils.drawStringNoScale(fontContainer, "_", var20, yy + underscoreOffsetY, fontScale, color);
                    } else {
                        GL11.glDisable(GL11.GL_TEXTURE_2D);
                        GuiDrawUtils.drawRect(var20, yy - 1, 1, getFontHeight(), 1f, 1f, 1f, 1f);
                        GL11.glEnable(GL11.GL_TEXTURE_2D);
                    }
                }
            }

            lineY += fontHeight;
            ++charCount;
        }

        if(!scissor) {
            GL11.glDisable(GL11.GL_SCISSOR_TEST);
        } else {
            GL11.glScissor(intBuffer.get(0), intBuffer.get(1), intBuffer.get(2), intBuffer.get(3));
        }

        k2 = Mouse.getDWheel();

        if (k2 != 0 && this.isFocused()) {
            scroll.mouseScroll(k2);
        }

        if (Mouse.isButtonDown(0)) {
            scroll.mouseClickMove(mouseX, mouseY);
        } else {
            scroll.mouseMovedOrUp();
        }

        scroll.setScrollTotalHeight(lineY + scroll.getScrollAnim() + ScaleGui.get(4f));
        scroll.drawScroll(this.xPosition + width - ScaleGui.get(10f), this.yPosition + ScaleGui.get(4f), this.height - ScaleGui.get(24), this.yPosition + this.height - ScaleGui.get(8f));
    }
}
