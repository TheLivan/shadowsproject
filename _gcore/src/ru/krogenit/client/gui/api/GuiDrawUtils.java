package ru.krogenit.client.gui.api;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import net.smart.moving.render.ModelPlayer;
import net.smart.render.SmartRenderModel;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.client.gui.item.ItemStat;
import ru.krogenit.client.gui.item.ItemStatSmall;
import ru.krogenit.client.gui.item.ItemTag;
import ru.krogenit.client.gui.item.WeaponStat;
import ru.krogenit.shaders.GL;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.utils.AnimationHelper;
import ru.krogenit.utils.Utils;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.customfont.StringCache;

import java.util.ArrayList;
import java.util.List;

import static net.minecraftforge.client.IItemRenderer.ItemRenderType.INVENTORY;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.glBlendFuncSeparate;

public class GuiDrawUtils {
    private static final ResourceLocation textureGridTransparent = new ResourceLocation("escmenu", "textures/grid_tr.png");
    private static final ResourceLocation textureGridMask = new ResourceLocation("escmenu", "textures/grid_rect_black.png");

    private static final Tessellator tessellator = Tessellator.instance;
    private static final Minecraft mc = Minecraft.getMinecraft();
    private static final RenderItem renderItem = RenderItem.getInstance();
    private static float animBackground;
    private static float breathAnimation;

    public static void draw(float x, float y, float width, float height) {
        drawRect(ScaleGui.get(x), ScaleGui.get(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void draw(ResourceLocation textureLocation, float x, float y, float width, float height) {
        drawRect(textureLocation, ScaleGui.get(x), ScaleGui.get(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void drawCentered(float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(ScaleGui.get(x) - width / 2f, ScaleGui.get(y) - height / 2f, width, height);
    }

    public static void drawCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.get(x) - width / 2f, ScaleGui.get(y) - height / 2f, width, height);
    }

    public static void drawCenter(float x, float y, float width, float height, float r, float g, float b, float a) {
        drawRect(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height), r, g, b, a);
    }

    public static void drawCenter(float x, float y, float width, float height) {
        drawRect(ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void drawCenter(ResourceLocation textureLocation, float x, float y, float width, float height) {
        drawRect(textureLocation, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void drawCenterXBot(float x, float y, float width, float height) {
        drawRect(ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void drawCenterXBot(ResourceLocation textureLocation, float x, float y, float width, float height) {
        drawRect(textureLocation, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(width), ScaleGui.get(height));
    }

    public static void drawCenterXBotCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getCenterX(x) - width / 2f, ScaleGui.getBot(y) - height / 2f, width, height);
    }

    public static void drawCenterCentered(float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(ScaleGui.getCenterX(x) - width / 2f, ScaleGui.getCenterY(y) - height / 2f, width, height);
    }

    public static void drawCenterCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getCenterX(x) - width / 2f, ScaleGui.getCenterY(y) - height / 2f, width, height);
    }

    public static void drawRightCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getRight(x) - width / 2f, ScaleGui.get(y) - height / 2f, width, height);
    }

    public static void drawCenterRightCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getRight(x) - width / 2f, ScaleGui.getCenterY(y) - height / 2f, width, height);
    }

    public static void drawBotCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.get(x) - width / 2f, ScaleGui.getBot(y) - height / 2f, width, height);
    }

    public static void drawRightBotCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getRight(x) - width / 2f, ScaleGui.getBot(y) - height / 2f, width, height);
    }

    public static void drawCenterXCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getCenterX(x) - width / 2f, ScaleGui.get(y) - height / 2f, width, height);
    }

    public static void drawCenterYCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.get(x) - width / 2f, ScaleGui.getCenterY(y) - height / 2f, width, height);
    }

    public static void drawCenterYRightCentered(ResourceLocation textureLocation, float x, float y, float width, float height) {
        width = ScaleGui.get(width);
        height = ScaleGui.get(height);
        drawRect(textureLocation, ScaleGui.getRight(x) - width / 2f, ScaleGui.getCenterY(y) - height / 2f, width, height);
    }

    public static void renderCuttedRectBack(float x1, float y1, float x2, float y2, float cutSize, float halfY1, float halfY2, float r, float g, float b, float a, float r1, float g1, float b1, float a1) {
        GL11.glShadeModel(GL11.GL_SMOOTH);
        tessellator.startDrawing(GL_TRIANGLES);
        float colorScale = 1.2f;
        float halfColorR = (r1 - r) / colorScale;
        float halfColorG = (g1 - g) / colorScale;
        float halfColorB = (b1 - b) / colorScale;
        float halfColorA = (a1 - a) / colorScale;
        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x2, y1, 0);
        tessellator.addVertex(x1 + cutSize, y1, 0);
        tessellator.setColorRGBA_F(halfColorR, halfColorG, halfColorB, halfColorA);
        tessellator.addVertex(x1, y1 + cutSize, 0);

        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x2, y1, 0);
        tessellator.setColorRGBA_F(halfColorR, halfColorG, halfColorB, halfColorA);
        tessellator.addVertex(x1, y1 + cutSize, 0);
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertex(x1, y1 + halfY1, 0);

        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x2, y1, 0);
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x2, y1 + halfY2, 0);

        tessellator.addVertex(x2, y1 + halfY2, 0);
        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x2, y2 - cutSize, 0);

        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x2 - cutSize, y2, 0);
        tessellator.addVertex(x2, y2 - cutSize, 0);

        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x1, y2, 0);
        tessellator.addVertex(x2 - cutSize, y2, 0);

        tessellator.draw();
        GL11.glShadeModel(GL_FLAT);
    }

    public static void renderCuttedRect(float x1, float y1, float x2, float y2, float cutSize, float halfY1, float halfY2, float r, float g, float b, float a, float r1, float g1, float b1, float a1) {
        GL11.glShadeModel(GL11.GL_SMOOTH);
        tessellator.startDrawing(GL_TRIANGLES);
        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x1, y1, 0);
        tessellator.addVertex(x2, y1 + cutSize, 0);
        tessellator.addVertex(x2 - cutSize, y1, 0);

        tessellator.addVertex(x1, y1, 0);
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertex(x2, y1 + halfY2, 0);
        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x2, y1 + cutSize, 0);

        tessellator.addVertex(x1, y1, 0);
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x2, y1 + halfY2, 0);

        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x2, y2, 0);
        tessellator.addVertex(x2, y1 + halfY2, 0);

        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x1 + cutSize, y2, 0);
        tessellator.addVertex(x2, y2, 0);

        tessellator.addVertex(x1, y1 + halfY1, 0);
        tessellator.addVertex(x1, y2 - cutSize, 0);
        tessellator.addVertex(x1 + cutSize, y2, 0);
        tessellator.draw();
        GL11.glShadeModel(GL_FLAT);
    }


    public static void renderTooltipItem(float x1, float y1, float x2, float y2, float cutSize, Vector4f gradientColor) {
        glDisable(GL_TEXTURE_2D);
        float half = ScaleGui.get(250f);
        float half1 = ScaleGui.get(250f);
        float half2 = ScaleGui.get(350f);
        float bg = 18 / 255f;
        renderCuttedRectBack(x1, y1, x2, y2, cutSize, half, half, bg, bg, bg, gradientColor.w, bg, bg, bg, gradientColor.w);
        renderCuttedRectBack(x1, y1, x2, y2, cutSize, half1, half2, 0f, 0f, 0f, 0f, gradientColor.x, gradientColor.y, gradientColor.z, gradientColor.w);
        glEnable(GL_TEXTURE_2D);
    }

    public static void renderBuildConfirmPopup(float posX, float posY, float posX2, float posY2, float cutSize, float r, float g, float b) {
        glDisable(GL_TEXTURE_2D);
        posX = ScaleGui.getCenterX(posX);
        posX2 = ScaleGui.getCenterX(posX2);
        posY = ScaleGui.getCenterY(posY);
        posY2 = ScaleGui.getCenterY(posY2);
        cutSize = ScaleGui.get(cutSize);
        glShadeModel(GL_SMOOTH);
        float halfY1 = posY + (posY2 - posY) / 1.3f;
        tessellator.startDrawing(GL_TRIANGLES);

        tessellator.setColorRGBA_F(r, g, b, 1.2f);
        tessellator.addVertex(posX2, posY, 0);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 1.2f);
        tessellator.addVertex(posX2, halfY1, 0);

        tessellator.setColorRGBA_F(r, g, b, 1.2f);
        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 1.2f);
        tessellator.addVertex(posX2, halfY1, 0);
        tessellator.setColorRGBA_F(r, g, b, 1.2f);
        tessellator.addVertex(posX + cutSize, posY, 0);

        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 1.2f);
        tessellator.addVertex(posX, halfY1, 0);
        tessellator.addVertex(posX2, halfY1, 0);

        tessellator.addVertex(posX2, halfY1, 0);
        tessellator.addVertex(posX, halfY1, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, halfY1, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, halfY1, 0);
        tessellator.addVertex(posX, posY2, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);

        tessellator.draw();
        glShadeModel(GL_FLAT);
        glEnable(GL_TEXTURE_2D);
    }

    public static void renderConfirmPopup(float posX, float posY, float posX2, float posY2, float cutSize) {
        glDisable(GL_TEXTURE_2D);
        posX = ScaleGui.getCenterX(posX);
        posX2 = ScaleGui.getCenterX(posX2);
        posY = ScaleGui.getCenterY(posY);
        posY2 = ScaleGui.getCenterY(posY2);
        cutSize = ScaleGui.get(cutSize);
        glShadeModel(GL_SMOOTH);
        float halfY1 = posY + (posY2 - posY) / 1.3f;
        tessellator.startDrawing(GL_TRIANGLES);

        float colorX1 = 60 / 255f;
        float colorY1 = 14 / 255f;
        float colorZ1 = 19 / 255f;
        float colorA1 = 1.2f;
        float colorXZ2 = 18 / 255f;

        tessellator.setColorRGBA_F(colorX1, colorY1, colorZ1, colorA1);
        tessellator.addVertex(posX2, posY, 0);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);
        tessellator.addVertex(posX2, halfY1, 0);

        tessellator.setColorRGBA_F(colorX1, colorY1, colorZ1, colorA1);
        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);

        tessellator.addVertex(posX2, halfY1, 0);
        tessellator.setColorRGBA_F(colorX1, colorY1, colorZ1, colorA1);
        tessellator.addVertex(posX + cutSize, posY, 0);

        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);
        tessellator.addVertex(posX, halfY1, 0);
        tessellator.addVertex(posX2, halfY1, 0);

        tessellator.addVertex(posX2, halfY1, 0);
        tessellator.addVertex(posX, halfY1, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, halfY1, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, halfY1, 0);
        tessellator.setColorRGBA_F(colorXZ2, colorZ1, colorXZ2, colorA1);
        tessellator.addVertex(posX, posY2, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);

        tessellator.draw();
        glShadeModel(GL_FLAT);
        glEnable(GL_TEXTURE_2D);
    }

    public static void renderToolTipSkill(float posX, float posY, float posX2, float posY2, float cutSize) {
        float cutX = cutSize;
        float cutY = cutSize;
        GL11.glShadeModel(GL11.GL_SMOOTH);
        tessellator.startDrawing(GL_TRIANGLES);
        float botColor = 0.0f;
        float upColor = 0.10f;
        float halfY1 = (posY2 - posY) / 3f;
        float halfY2 = (posY2 - posY) / 2.5f;
        tessellator.setColorRGBA_F(upColor, upColor, upColor, 0.95f);
        tessellator.addVertex(posX, posY, 0);
        tessellator.addVertex(posX2, posY + cutY, 0);
        tessellator.addVertex(posX2 - cutX, posY, 0);

        tessellator.addVertex(posX, posY, 0);
        tessellator.setColorRGBA_F(botColor, botColor, botColor, 0.95f);
        tessellator.addVertex(posX2, posY + halfY2, 0);
        tessellator.setColorRGBA_F(upColor, upColor, upColor, 0.95f);
        tessellator.addVertex(posX2, posY + cutY, 0);

        tessellator.addVertex(posX, posY, 0);
        tessellator.setColorRGBA_F(botColor, botColor, botColor, 0.95f);
        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.addVertex(posX2, posY + halfY2, 0);

        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.addVertex(posX2, posY2, 0);
        tessellator.addVertex(posX2, posY + halfY2, 0);

        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.addVertex(posX + cutX, posY2, 0);
        tessellator.addVertex(posX2, posY2, 0);

        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.addVertex(posX, posY2 - cutY, 0);
        tessellator.addVertex(posX + cutX, posY2, 0);
        tessellator.draw();
        GL11.glShadeModel(GL_FLAT);
    }

    public static void renderTooltipBuild(float posX, float posY, float posX2, float posY2, float cutSize) {
        glDisable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);
        float halfY1 = (posY2 - posY) / 3.7f;
        float halfY2 = (posY2 - posY) / 2.4f;
        tessellator.startDrawing(GL_TRIANGLES);

        tessellator.setColorRGBA_F(65 / 255f, 65 / 255f, 65 / 255f, 0.95f);
        tessellator.addVertex(posX2, posY, 0);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX2, posY + halfY2, 0);

        tessellator.setColorRGBA_F(65 / 255f, 65 / 255f, 65 / 255f, 0.95f);
        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX2, posY + halfY2, 0);
        tessellator.setColorRGBA_F(65 / 255f, 65 / 255f, 65 / 255f, 0.95f);
        tessellator.addVertex(posX + cutSize, posY, 0);

        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.addVertex(posX2, posY + halfY2, 0);

        tessellator.addVertex(posX2, posY + halfY2, 0);
        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);

        tessellator.addVertex(posX, posY + halfY1, 0);
        tessellator.setColorRGBA_F(18 / 255f, 19 / 255f, 18 / 255f, 0.95f);
        tessellator.addVertex(posX, posY2, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);

        tessellator.draw();
        glShadeModel(GL_FLAT);
        glEnable(GL_TEXTURE_2D);
    }

    public static void renderToolTipSkillType(float posX, float posY, float posX2, float posY2, float cutSize) {
        tessellator.startDrawing(GL_TRIANGLES);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);
        tessellator.addVertex(posX2, posY, 0);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.addVertex(posX2, posY2 - cutSize, 0);
        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.addVertex(posX + cutSize, posY, 0);
        tessellator.addVertex(posX, posY + cutSize, 0);
        tessellator.addVertex(posX, posY2, 0);
        tessellator.addVertex(posX2 - cutSize, posY2, 0);
        tessellator.draw();
    }

    public static void drawString(FontType fontType, String string, float x, float y, float scale, int color) {
        drawStringNoScale(fontType.getFontContainer(), string, ScaleGui.get(x), ScaleGui.get(y), ScaleGui.get(scale), color);
    }

    public static void drawCenteredString(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.get(x) - fontContainer.width(string) * scale / 2f, ScaleGui.get(y), scale, color);
    }

    public static void drawStringNoXYScale(FontType fontType, String string, float x, float y, float scale, int color) {
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontType.getFontContainer(), string, x, y, scale, color);
    }

    public static void drawRightStringNoXYScale(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, x - fontContainer.width(string) * scale, y, scale, color);
    }

    public static void drawCenteredStringNoXYScale(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, x - fontContainer.width(string) * scale / 2f, y, scale, color);
    }

    public static void drawCenteredStringCenterX(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.getCenterX(x) - fontContainer.width(string) * scale / 2f, ScaleGui.get(y), scale, color);
    }

    public static void drawCenteredStringCenter(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.getCenterX(x) - fontContainer.width(string) * scale / 2f, ScaleGui.getCenterY(y), scale, color);
    }

    public static void drawStringCenter(FontType fontType, String string, float x, float y, float scale, int color) {
        drawStringNoScale(fontType.getFontContainer(), string, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(scale), color);
    }

    public static void drawStringCenterX(FontType fontType, String string, float x, float y, float scale, int color) {
        drawStringNoScale(fontType.getFontContainer(), string, ScaleGui.getCenterX(x), ScaleGui.get(y), ScaleGui.get(scale), color);
    }

    public static void drawStringCenterYRight(FontType fontType, String string, float x, float y, float scale, int color) {
        drawStringNoScale(fontType.getFontContainer(), string, ScaleGui.getRight(x), ScaleGui.getCenterY(y), ScaleGui.get(scale), color);
    }

    public static void drawRightStringCenter(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.getCenterX(x) - fontContainer.width(string) * scale, ScaleGui.getCenterY(y), scale, color);
    }

    public static void drawRightString(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.get(x) - fontContainer.width(string) * scale, ScaleGui.get(y), scale, color);
    }

    public static void drawStringCenterXBot(FontType fontType, String string, float x, float y, float scale, int color) {
        drawStringNoScale(fontType.getFontContainer(), string, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(scale), color);
    }

    public static void drawRightStringCenterXBot(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.getCenterX(x) - fontContainer.width(string) * scale, ScaleGui.getBot(y), scale, color);
    }

    public static void drawRightStringRightBot(FontType fontType, String string, float x, float y, float scale, int color) {
        FontContainer fontContainer = fontType.getFontContainer();
        scale = ScaleGui.get(scale);
        drawStringNoScale(fontContainer, string, ScaleGui.getRight(x) - fontContainer.width(string) * scale, ScaleGui.getBot(y), scale, color);
    }

    public static float drawSplittedStringCenter(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color, EnumStringRenderType renderType) {
        return drawSplittedString(font, text, ScaleGui.getCenterX(x), ScaleGui.getCenterY(y), ScaleGui.get(scale), width, heightLimit, color, renderType);
    }

    public static float drawSplittedStringRightBot(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color, EnumStringRenderType renderType) {
        return drawSplittedString(font, text, ScaleGui.getRight(x), ScaleGui.getBot(y), ScaleGui.get(scale), width, heightLimit, color, renderType);
    }

    public static float drawSplittedStringCenterXBot(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color) {
        return drawSplittedString(font, text, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(scale), width, heightLimit, color, EnumStringRenderType.DEFAULT);
    }

    public static float drawSplittedRightStringCenterXBot(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color) {
        return drawSplittedString(font, text, ScaleGui.getCenterX(x), ScaleGui.getBot(y), ScaleGui.get(scale), width, heightLimit, color, EnumStringRenderType.RIGHT);
    }

    public static float drawSplittedStringNoScale(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color, EnumStringRenderType type) {
        return drawSplittedString(font, text, x, y, ScaleGui.get(scale), width, heightLimit, color, type);
    }

    private static final List<String> tempList = new ArrayList<>();
    private static final List<String> tempSplitted = new ArrayList<>();

    public static float drawSplittedString(FontType font, String text, float x, float y, float scale, float width, float heightLimit, int color, EnumStringRenderType type) {
        if (text == null) return 0;
        FontContainer fontContainer = font.getFontContainer();
        StringCache textFont = fontContainer.getTextFont();
        text = text.replaceAll(String.valueOf((char) 160), " ");
        tempList.clear();

        String defaultColor = "";
        String preColor = defaultColor;

        int offset = 0;
        while (text.contains("\n")) {
            int index = text.indexOf("\n");
            String temp = text.substring(offset, index);
            int lastColorIndex = temp.lastIndexOf("§");
            tempList.add(preColor + temp);
            if (lastColorIndex != -1) {
                preColor = temp.substring(lastColorIndex, lastColorIndex + 2);
            }
            text = preColor + text.replaceFirst("\n", "");
            offset = index;
        }
        tempList.add(preColor + text.substring(offset));

        tempSplitted.clear();
        preColor = defaultColor;
        for (String s : tempList) {
            if (s.length() == 0) {
                tempSplitted.add("");
            } else {
                String string = s;
                while (string.length() > 0) {
                    String temp = textFont.trimStringToWidthSaveWords(string, width / scale, false);
                    tempSplitted.add(preColor + temp);
                    int lastColorIndex = temp.lastIndexOf("§");
                    if (lastColorIndex != -1) {
                        preColor = temp.substring(lastColorIndex, lastColorIndex + 2);
                    }
                    string = string.replace(temp, "");
                }
            }
        }

        int height = 0;
        for (String s : tempSplitted) {
            drawStringNoScale(fontContainer, s, x - (type == EnumStringRenderType.DEFAULT ? 0 : type == EnumStringRenderType.RIGHT ?
                    fontContainer.width(s) * scale : fontContainer.width(s) * scale / 2f), y + height, scale, color);
            height += fontContainer.height() / 1.25f * scale;
            if (heightLimit != -1 && height >= heightLimit) {
                return height;
            }
        }

        return height;
    }

    public static void drawStringNoScale(FontContainer fontContainer, String string, float x, float y, float scale, int color) {
        GL11.glPushMatrix();
        GL11.glTranslatef(x, y, 0);
        GL11.glScalef(scale, scale, 1.0f);
        fontContainer.drawString(string, 0, 0, color);
        GL11.glPopMatrix();
    }

    public static void clearMaskBuffer(float x, float y, float width, float height) {
        glDisable(GL_TEXTURE_2D);
        glColor4f(1f, 1f, 1f, 1f);
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_ZERO, GL_ZERO);
        drawRect(x, y, width, height);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);
    }

    public static void drawLoadingBarMaskingEffect(ResourceLocation texture, float x, float y, float width, float height, float maskWidth, float maskHeight) {
        x = ScaleGui.getCenterX(x);
        y = ScaleGui.getCenterY(y);
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ZERO);
        drawRect(texture, x, y, ScaleGui.get(width), ScaleGui.get(height));
        glDisable(GL_TEXTURE_2D);
        glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
        drawRect(x, y, ScaleGui.get(maskWidth), ScaleGui.get(maskHeight));
        glEnable(GL_TEXTURE_2D);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void drawMaskingButtonEffect(ResourceLocation texture, float x, float y, float width, float height, float maskX, float maskY, float maskWidth, float maskHeight, float angle) {
        glPushMatrix();
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ZERO);
        drawRect(texture, x, y, width, height);
        glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
        float toZeroX = maskX + maskWidth / 2f;
        float toZeroY = maskY + maskHeight / 2f;
        glTranslatef(toZeroX, toZeroY, 0f);
        glRotatef(angle, 0, 0, 1);
        glDisable(GL_TEXTURE_2D);
        drawRect(maskX - toZeroX, maskY - toZeroY, maskWidth, maskHeight);
        glEnable(GL_TEXTURE_2D);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPopMatrix();
    }

    public static void drawRectWithMask(ResourceLocation texture, float x, float y, float width, float height, float maskX, float maskY, float maskWidth, float maskHeight) {
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ZERO);
        drawRect(texture, x, y, width, height);
        if (KrogenitShaders.forwardPBRDirectionalShaderOld.isActive())
            KrogenitShaders.forwardPBRDirectionalShaderOld.setUseTexture(false);
        else glDisable(GL_TEXTURE_2D);
        glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
        drawRect(maskX, maskY, maskWidth, maskHeight);
        if (KrogenitShaders.forwardPBRDirectionalShaderOld.isActive())
            KrogenitShaders.forwardPBRDirectionalShaderOld.setUseTexture(true);
        else glEnable(GL_TEXTURE_2D);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void drawMaskingBackgroundEffect(float x, float y, float width, float height) {
        animBackground += AnimationHelper.getAnimationSpeed() * 0.0075f;
        if (animBackground > 1.25f) {
            animBackground -= 1.5f;
        }
        float effectWidth = ScaleGui.screenWidth / 0.96676f;
        float effectHeight = ScaleGui.screenHeight / 0.4073f;
        float effectX = animBackground * (width) - effectWidth / 2f;
        float effectY = height - animBackground * (height) - effectHeight / 2f;
        glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ONE_MINUS_DST_ALPHA);
        Utils.bindTexture(textureGridTransparent, true, GL_REPEAT);
        float umax = 1.0f / (ScaleGui.DEFAULT_WIDTH / (float) mc.displayWidth);
        float vmax = 1.0f / (ScaleGui.DEFAULT_HEIGHT / (float) mc.displayHeight);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(0.5f, 1f, 1f, 0.5f);
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, vmax);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, umax, vmax);
        tessellator.addVertexWithUV(x + width, y, 0.0, umax, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
        glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_SRC_COLOR);
        drawRect(textureGridMask, effectX, effectY, effectWidth, effectHeight, 0.5f, 1f, 1f, 0.5f);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void drawPlayer(AbstractClientPlayer entityPlayer, float x, float y, float z, float scale, float rotateX, float r, float b, float g, Vector3f lightPosition, Vector3f lightColor, Event event) {
        Render render;
        if(entityPlayer != null) render = RenderManager.instance.getEntityRenderObject(entityPlayer);
        else render = RenderManager.instance.getEntityClassRenderObject(EntityPlayer.class);

        if(render instanceof RenderPlayer) {
            RenderPlayer renderPlayer = (RenderPlayer) render;
            if(entityPlayer != null) Minecraft.getMinecraft().getTextureManager().bindTexture(entityPlayer.getLocationSkin());
            else Minecraft.getMinecraft().getTextureManager().bindTexture(AbstractClientPlayer.locationStevePng);

            ModelBiped modelBipedMain = renderPlayer.modelBipedMain;
            if(modelBipedMain instanceof ModelPlayer) {
                ModelPlayer modelPlayer = (ModelPlayer) modelBipedMain;
                SmartRenderModel renderModel = modelPlayer.getRenderModel();
                renderModel.actualRotation = 0f;
                renderModel.isInventory = true;
            }

            glDepthMask(true);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_ALPHA_TEST);
            glPushMatrix();
            glTranslatef(x, y, z);
            glScalef(-scale, scale, scale);
            KrogenitShaders.forwardPBRDirectionalShader.enable();
            KrogenitShaders.forwardPBRDirectionalShader.setDirectionLight(true);
            KrogenitShaders.forwardPBRDirectionalShader.setModelView();
            KrogenitShaders.forwardPBRDirectionalShader.setLightPos(lightPosition.x, lightPosition.y, lightPosition.z);
            KrogenitShaders.forwardPBRDirectionalShader.setLightColor(lightColor.x, lightColor.y, lightColor.z);
            KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
            KrogenitShaders.forwardPBRDirectionalShaderOld.setDirectionLight(true);
            KrogenitShaders.forwardPBRDirectionalShaderOld.setModelView();
            KrogenitShaders.forwardPBRDirectionalShaderOld.setLightPos(lightPosition.x, lightPosition.y, lightPosition.z);
            KrogenitShaders.forwardPBRDirectionalShaderOld.setLightColor(lightColor.x, lightColor.y, lightColor.z);
            GL.color(r, g, b);
            glRotatef(rotateX, 0.0F, 1.0F, 0.0F);
            renderPlayer.modelBipedMain.isChild = false;
            float prevOnground = renderPlayer.modelBipedMain.onGround;
            renderPlayer.modelBipedMain.onGround = 0f;
            renderPlayer.modelBipedMain.render(entityPlayer, 0f, 0.0f, breathAnimation, 0, 0, 0.0625F);
            renderPlayer.modelBipedMain.onGround = prevOnground;
            breathAnimation += AnimationHelper.getAnimationSpeed() * 0.5f;
            MinecraftForge.EVENT_BUS.post(event);
            KrogenitShaders.forwardPBRDirectionalShader.enable();
            GL.color(1f, 1f, 1f);
            KrogenitShaders.forwardPBRDirectionalShader.disable();
            glDepthMask(false);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_ALPHA_TEST);
            glPopMatrix();
            KrogenitShaders.finishCurrentShader();
        }
    }

    public static float drawWeaponPopup(float x, float y, float width, float height, String name, String secondName, ItemTag[] tags, String description, ItemStat[] itemStats, WeaponStat[] weaponStats, ItemStatSmall[] itemStatSmalls) {

        return 0f;
    }

    public static void renderItem(ItemStack itemStack, float itemScale, float offsetX, float offsetY, float x, float y, float width, float height, float rotation) {
        glPushMatrix();
        float minSize = width < height ? width : height;
        float itemSize = 0;
        float dx = 0;
        float dy = 0;
        if (width > height) {
            dx = (height - width) / 2f;
        } else {
            dy = (width - height) / 2f;
        }

        IItemRenderer customRenderer = MinecraftForgeClient.getItemRenderer(itemStack, INVENTORY);
        if (customRenderer != null) {
            itemSize = minSize / 16f * itemScale;
            renderItem.renderWithColor = false;
        } else {
            float add = height / 10f;
            offsetX += add;
            offsetY += add;
            itemSize = minSize / 20f * itemScale;
            GL11.glEnable(GL11.GL_LIGHTING);
        }

        glTranslatef(x, y, 0f);
        float width1 = width / 2f;
        float height1 = height / 2f;
        glTranslatef(width1, height1, 0);
        glRotatef(rotation, 0, 0, 1);
        glTranslatef(-width1, -height1, 0);
        glTranslatef(offsetX - dx, offsetY - dy,0);

        glScalef(itemSize, itemSize, 1f);
        glDepthMask(true);
        glEnable(GL_DEPTH_TEST);
        renderItem.renderItemAndEffectIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, 0, 0);
        renderItem.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, 0, 0, null, false);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_ALPHA_TEST);
        glEnable(GL_BLEND);
        glAlphaFunc(GL_GREATER, 0.001f);
        glDepthMask(false);
        glDisable(GL11.GL_LIGHTING);

        if (customRenderer != null) {
            GL11.glColor4f(1f, 1f, 1f, 1f);
            renderItem.renderWithColor = true;
        }

        glPopMatrix();
    }

    public static void drawGradient(double x, double y, double width, double height, float r, float g, float b, float a, float r1, float g1, float b1, float a1) {
        glShadeModel(GL_SMOOTH);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(r1, g1, b1, a1);
        tessellator.addVertex(x, y + height, 0.0);
        tessellator.addVertex(x + width, y + height, 0.0);
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertex(x + width, y, 0.0);
        tessellator.addVertex(x, y, 0.0);
        tessellator.draw();
        glShadeModel(GL_FLAT);
    }

    public static void drawRectXY(ResourceLocation textureLocation, double x1, double y1, double x2, double y2) {
        Utils.bindTexture(textureLocation);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x1, y2, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x2, y2, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x2, y1, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x1, y1, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRectXY(double x1, double y1, double x2, double y2) {
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x1, y2, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x2, y2, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x2, y1, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x1, y1, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRect(double x, double y, double width, double height, float r, float g, float b, float a) {
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRect(double x, double y, double width, double height) {
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRect(ResourceLocation textureLocation, double x, double y, double width, double height, float r, float g, float b, float a) {
        Utils.bindTexture(textureLocation);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRect(ResourceLocation textureLocation, double x, double y, double width, double height) {
        Utils.bindTexture(textureLocation);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRectCentered(ResourceLocation textureLocation, double x, double y, double width, double height) {
        x -= width / 2f;
        y -= height / 2f;
        Utils.bindTexture(textureLocation);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRect(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3, float r, float g, float b, float a) {
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(r, g, b, a);
        tessellator.addVertexWithUV(x, y, 0.0, 0.0, 1.0);
        tessellator.addVertexWithUV(x1, y1, 0.0, 1.0, 1.0);
        tessellator.addVertexWithUV(x2, y2, 0.0, 1.0, 0.0);
        tessellator.addVertexWithUV(x3, y3, 0.0, 0.0, 0.0);
        tessellator.draw();
    }

    public static void drawRecLines(float x, float y, float width, float height) {
        GL11.glBegin(GL11.GL_LINE_STRIP);
        GL11.glVertex2f(x, y);
        GL11.glVertex2f(x + width, y);
        GL11.glVertex2f(x + width, y + height);
        GL11.glVertex2f(x, y + height);
        GL11.glVertex2f(x, y);
        GL11.glEnd();
    }

    public static void drawPreAlpha(float x, float y, float width, float height, float fontScale) {
        float rectX1 = ScaleGui.getCenterX(x);
        float rectY1 = ScaleGui.getCenterY(y);
        float rectX2 = ScaleGui.getCenterX(x + width);
        float rectY2 = ScaleGui.getCenterY(y + height);
        float halfY1 = (rectY2 - rectY1) / 2f;
        float halfY2 = (rectY2 - rectY1) / 2f;
        glDisable(GL_TEXTURE_2D);
        GuiDrawUtils.renderCuttedRect(rectX1, rectY1, rectX2, rectY2, ScaleGui.get(height / 3f), halfY1, halfY2, 0.9f, 0.5f, 0.2f, 0.5f, 0.9f, 0.5f, 0.2f, 0.5f);
        glEnable(GL_TEXTURE_2D);
        drawSplittedStringCenter(FontType.FUTURA_PT_MEDIUM, "Внимание! Это Pre-Alpha версия игры, предназначенная для тестирования. " +
                "Она не отображает финального качества продукта. Спасибо за понимание и поддержку!", x + height / 3f, y + height / 5f, fontScale, ScaleGui.get(width / 1.1f), -1, 0x000000, EnumStringRenderType.DEFAULT);
    }
}
