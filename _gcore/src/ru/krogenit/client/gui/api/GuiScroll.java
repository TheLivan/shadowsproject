package ru.krogenit.client.gui.api;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.utils.AnimationHelper;

public class GuiScroll {

    private static final ResourceLocation textureInvArrowTop = new ResourceLocation("inventory", "arrow_top.png");
    private static final ResourceLocation textureInvScrollBody = new ResourceLocation("inventory", "scroll_body.png");
    private static final ResourceLocation textureInvArrowBot = new ResourceLocation("inventory", "arrow_bottom.png");

    @Getter
    private float scrollAnim;
    private float scroll, lastScroll;
    @Setter
    private float scrollTotalHeight, scrollViewHeight, scrollTextureHeight;
    private float scrollX, scrollY, scrollWidth, scrollHeight;
    private boolean mouseScrolling;
    private float mouseStartY;

    public void drawScrollScaled(float x, float y, float height, float bottomY) {
        float diff = scrollTotalHeight - scrollViewHeight;
        if(scroll < 0) {
            scroll = 0;
        } else if(diff > 0 && scroll > diff) {
            scroll = diff;
        }

        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        x = ScaleGui.getCenterX(x);
        y = ScaleGui.getCenterY(y);
        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(textureInvArrowTop, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = ScaleGui.get(height);
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(textureInvScrollBody, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
        y = ScaleGui.getCenterY(bottomY);
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(textureInvArrowBot, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
    }

    public void drawScroll(float x, float y, float height, float bottomY) {
        float diff = scrollTotalHeight - scrollViewHeight;
        if(scroll < 0) {
            scroll = 0;
        } else if(diff > 0 && scroll > diff) {
            scroll = diff;
        }

        if(scrollAnim < scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, 0.25f, 0.1f);
        } else if(scrollAnim > scroll) {
            scrollAnim = AnimationHelper.updateSlowEndAnim(scrollAnim, scroll, -0.25f, -0.1f);
        }

        float iconWidth = ScaleGui.get(7);
        float iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(textureInvArrowTop, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
        y += iconHeight + ScaleGui.get(3);
        scrollTextureHeight = height;
        float scrollValue = scrollViewHeight / scrollTotalHeight;
        float scrollYValue = scrollTextureHeight / scrollTotalHeight;
        if(scrollValue > 1) scrollValue = 1f;
        if(scrollYValue > 1) scrollYValue = 1f;
        iconHeight = scrollTextureHeight * scrollValue;
        y += scrollAnim * scrollYValue;
        scrollX = x;
        scrollY = y;
        scrollWidth = iconWidth;
        scrollHeight = iconHeight;
        GuiDrawUtils.drawRect(textureInvScrollBody, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
        y = bottomY;
        iconHeight = ScaleGui.get(4);
        GuiDrawUtils.drawRect(textureInvArrowBot, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);
    }

    public boolean mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if(mouseButton == 0) {
            if(mouseX > scrollX && mouseY > scrollY && mouseX < scrollX + scrollWidth && mouseY < scrollY + scrollHeight) {
                mouseStartY = mouseY;
                mouseScrolling = true;
                lastScroll = scroll;
                return true;
            }
        }

        return false;
    }

    public boolean mouseClickMove(int mouseX, int mouseY) {
        if(mouseScrolling) {
            float heightDiff = scrollTotalHeight - scrollViewHeight;
            if(heightDiff > 0) {
                float diff = scrollTextureHeight / scrollTotalHeight;
                scroll = lastScroll + (mouseY - mouseStartY) / diff;

                if (scroll < 0) {
                    scroll = 0;
                } else if (scroll > scrollTotalHeight - scrollViewHeight) {
                    scroll = scrollTotalHeight - scrollViewHeight;
                }

                return true;
            }
        }

        return false;
    }

    public void mouseMovedOrUp() {
        mouseScrolling = false;
    }

    public void mouseScroll(int d) {
        float diff = scrollTotalHeight - scrollViewHeight;
        if(diff > 0) {
            scroll -= d / 2f;
            if(scroll < 0) {
                scroll = 0;
            } else if(scroll > diff) {
                scroll = diff;
            }
        }
    }
}
