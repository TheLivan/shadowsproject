package ru.krogenit.client.gui.api;

public enum EnumStringRenderType {
    DEFAULT, CENTERED, RIGHT;
}
