package ru.krogenit.client.gui.api;

import net.minecraft.client.gui.GuiButton;

public interface IGuiWithPopup {
    void popupAction(GuiButton button);
    void setPopup(GuiPopup popup);
}
