package ru.krogenit.client.gui.api;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector3f;
import ru.xlv.customfont.FontType;

public class GuiPopup extends AbstractGuiScreenAdvanced {

    protected static final ResourceLocation textureTreeButtonAccept = new ResourceLocation("skilltree", "textures/button_accept.png");
    protected static final ResourceLocation textureTreeButtonCancel = new ResourceLocation("skilltree", "textures/button_cancel.png");
    protected static final ResourceLocation textureTreeButtonHover = new ResourceLocation("skilltree", "textures/button_hover.png");
    protected static final ResourceLocation textureTreeButtonMask = new ResourceLocation("skilltree", "textures/button_mask.png");
    protected static final ResourceLocation textureTreeBorderConfirm = new ResourceLocation("skilltree", "textures/border_confirm.png");

    public static final Vector3f green = new Vector3f(27/255f, 48/255f, 23/255f);
    public static final Vector3f red = new Vector3f(60/255f, 14/255f, 19/255f);

    protected final IGuiWithPopup guiWithPopup;
    protected final String header;
    protected final String info;
    protected final Vector3f color;
    protected float x, y;

    public GuiPopup(IGuiWithPopup guiWithPopup, String header, String info, Vector3f color, float x, float y) {
        this.guiWithPopup = guiWithPopup;
        this.header = header;
        this.info = info;
        this.color = color;
        this.x = x;
        this.y = y;
        initGui();
    }

    public GuiPopup(IGuiWithPopup guiWithPopup, String header, String info, Vector3f color) {
        this(guiWithPopup, header, info, color, 960, 540);
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        createButtons();
    }

    protected void createButtons() {
        float buttonWidth = 171;
        float buttonHeight = 39;
        float x = this.x;
        float y = -24 + this.y;
        y += buttonHeight * 1.35f;
        GuiButtonAnimated button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОК");
        button.setTexture(textureTreeButtonAccept);
        button.setTextureHover(textureTreeButtonHover);
        button.setMaskTexture(textureTreeButtonMask);
        buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        guiWithPopup.setPopup(null);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        GuiDrawUtils.renderBuildConfirmPopup(x - 317, y - 220, x + 317, y + 90, ScaleGui.get(27f), color.x, color.y, color.z);
        drawButtons(mouseX, mouseY, partialTick);
        float x = this.x;
        float y = this.y - 164;
        float fs = 1.3f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, header, x, y, fs, 0xffffff);

        float iconWidth = 502;
        float iconHeight = 1;
        y += 25;
        GuiDrawUtils.drawCenterCentered(textureTreeBorderConfirm, x, y, iconWidth, iconHeight);
        y += 22;
        fs = 0.85f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, info, x, y, fs, 0xffffff);
        y += 30;
        GuiDrawUtils.drawCenterCentered(textureTreeBorderConfirm, x, y, iconWidth, iconHeight);
    }
}
