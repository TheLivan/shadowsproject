package ru.krogenit.dds_loader;

import lombok.Getter;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.utils.Utils;
import ru.xlv.core.resource.AbstractResource;
import ru.xlv.core.resource.ResourceLoadingState;

public class TextureDDS extends AbstractResource {

    @Getter private int textureId;
    @Getter private final DDSFile ddsFile;

    public TextureDDS(ResourceLocation resourceLocation) {
        super(resourceLocation);
        this.ddsFile = new DDSFile();
    }

    @Override
    public void loadFromFile() {
        ddsFile.loadFile(Utils.getInputStreamFromZip(resourceLocation));
    }

    @Override
    public void loadToMemory() {
        TextureLoaderDDS.createTexture(ddsFile, textureId = GL11.glGenTextures());
    }

    @Override
    public void unload() {
        GL11.glDeleteTextures(textureId);
    }

    @Override
    public void setFrom(AbstractResource resource) {
        TextureDDS that = (TextureDDS) resource;
        textureId = that.getTextureId();
    }

    public boolean isLoaded() {
        return loadingState == ResourceLoadingState.DONE;
    }
}
