package ru.krogenit.shaders;

public enum EnumShaderLightingType {
    NONE, DIRECTIONAL, ALL;
}
