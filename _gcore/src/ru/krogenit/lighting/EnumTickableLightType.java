package ru.krogenit.lighting;

public enum EnumTickableLightType {
    None, FadesOut, FadesInAndOut;
}
