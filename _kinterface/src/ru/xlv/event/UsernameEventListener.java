package ru.xlv.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.RenderLivingEvent;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.group.XlvsGroupMod;

import java.util.List;
import java.util.Set;

public class UsernameEventListener {

    private Set<String> friendNames;
    private List<String> groupNames;

    private EntityLivingBase lastTargetEntity;

    private long updateNamesTimer;
    private long updateTargetTimer;

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(RenderLivingEvent.Specials.Pre event) {
        if (event.entity instanceof EntityPlayer && event.entity != Minecraft.getMinecraft().thePlayer) {
            if(updateNamesTimer < System.currentTimeMillis()) {
                updateNamesTimer = System.currentTimeMillis() + 1000L;
                friendNames = XlvsFriendMod.INSTANCE.getFriendHandler().getAllFriendNames();
                if (XlvsGroupMod.INSTANCE.getGroup() != null) {
                    groupNames = XlvsGroupMod.INSTANCE.getGroup().getPlayers();
                }
            }
            if(updateTargetTimer < System.currentTimeMillis()) {
                updateTargetTimer = System.currentTimeMillis() + 200L;
                EntityLivingBase closestEntityLookedAt = Utils.getClosestEntityLookedAt(4, 1);
                if (closestEntityLookedAt instanceof EntityPlayer) {
                    lastTargetEntity = closestEntityLookedAt;
                } else {
                    lastTargetEntity = null;
                }
            }
            int color = 0xffffff;
            int distance = 16;
            if (groupNames != null && groupNames.contains(event.entity.getCommandSenderName())) {
                color = 0x44ff00;
                distance = 128;
                lastTargetEntity = event.entity;
            } else if (friendNames != null && friendNames.contains(event.entity.getCommandSenderName())) {
                color = 0x41f2e9;
                distance = 128;
                lastTargetEntity = event.entity;
            }
            if(lastTargetEntity instanceof EntityPlayer) {
                ClientPlayer clientPlayer = ClientPlayerManager.INSTANCE.getPlayer((EntityPlayer) event.entity);
                String name = lastTargetEntity.getCommandSenderName();
                if (clientPlayer != null) {
                    name = clientPlayer.getPlayerName();
                    renderTag(event.entity, "<" + clientPlayer.getCharacterType().getDisplayName() + ">", event.x, event.y - .15D, event.z, distance, .6F, color);
                }
                renderTag(lastTargetEntity, name, event.x, event.y + .1D, event.z, distance, .9F, color);
            }
            event.setCanceled(true);
        }
    }

    private static void renderTag(Entity entity, String tag, double x, double y, double z, double range, float scale, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        RenderManager renderManager = RenderManager.instance;
        if(Minecraft.isGuiEnabled() && entity != renderManager.livingPlayer && !entity.isInvisibleToPlayer(mc.thePlayer) && entity.riddenByEntity == null){
            double distance = entity.getDistanceSqToEntity(renderManager.livingPlayer);
            if(distance <= range * range){
                FontContainer fontContainer = FontType.DEFAULT.getFontContainer();
                IPBR shader = KrogenitShaders.getCurrentPBRShader(true);

                float f = 1.6F;
                float f1 = 0.016666668F * f;
                GL11.glPushMatrix();
                GL11.glTranslatef((float)x + 0.0F, (float)y + entity.height + 0.5F, (float)z);
                GL11.glNormal3f(0.0F, 1.0F, 0.0F);
                GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
                GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
                GL11.glScalef(-f1, -f1, f1);
                shader.useLighting(false);
                shader.setLightMapping(false);
                GL11.glDepthMask(false);
                GL11.glDisable(GL11.GL_DEPTH_TEST);
                GL11.glEnable(GL11.GL_BLEND);
                OpenGlHelper.glBlendFunc(770, 771, 1, 0);
                shader.setUseTexture(false);
                float sl = fontContainer.width(tag) / 2f * scale;

                Tessellator t = Tessellator.instance;
                t.startDrawingQuads();
                t.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
                t.addVertex(-sl - 1, -2.0D * scale, 0.0D);
                t.addVertex(-sl - 1, 9.0D * scale, 0.0D);
                t.addVertex(sl + 1, 9.0D * scale, 0.0D);
                t.addVertex(sl + 1, -2.0D * scale, 0.0D);
                t.draw();
                shader.setUseTexture(true);
                GL11.glDepthMask(true);
                GuiDrawUtils.drawStringNoScale(fontContainer, tag, -sl, 0, scale, color);
                GL11.glEnable(GL11.GL_DEPTH_TEST);
                shader.useLighting(true);
                GL11.glDisable(GL11.GL_BLEND);
                GL11.glPopMatrix();
            }
        }
    }
}
