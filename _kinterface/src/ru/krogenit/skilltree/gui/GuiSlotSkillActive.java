package ru.krogenit.skilltree.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.skilltree.CoreSkillTree;
import ru.krogenit.skilltree.type.EnumSlotSkillActiveType;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

public class GuiSlotSkillActive extends GuiButton {

    private GuiSkill skill;
    private EnumSlotSkillActiveType type;
    private ResourceLocation skillTexture;
    private final float minAspect;

    private boolean hovered;

    public GuiSlotSkillActive(int id, float xPos, float yPos, float width, float height, EnumSlotSkillActiveType type, float minAspect) {
        super(id, xPos, yPos, width, height, "");
        this.type = type;
        this.minAspect = minAspect;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        boolean hovered = isHovered(mouseX, mouseY);
        if(hovered && !this.hovered) {
            SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
        }
        this.hovered = hovered;
        if(type == EnumSlotSkillActiveType.LOCKED) {
            Utils.bindTexture(TextureRegister.textureSkillActiveLock);
        } else {
            Utils.bindTexture(TextureRegister.textureSkillActiveEmpty);
        }
        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

        if(skillTexture != null) {
            Utils.bindTexture(skillTexture);
            GuiDrawUtils.drawRect(this.xPosition + height / 50f, this.yPosition + height / 50f, this.width / 1.16f, this.height / 1.19f);
        }

        if(type != EnumSlotSkillActiveType.LOCKED) {
            Utils.bindTexture(TextureRegister.textureSkillActiveEmptyBorder);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
        }

        drawText(mc);
    }

    protected void drawText(Minecraft mc) {
        float ratio = mc.displayWidth / (float) mc.displayHeight;
        float scaleValue = ratio < minAspect ? mc.displayHeight / (1.0f + (minAspect - ratio)) : mc.displayHeight;
        float textScale = scaleValue / (1080f / 1f);
        float offX = scaleValue / 50f;
        float offY = scaleValue / 30f;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrBold, "" + (id+1), this.xPosition + offX + this.width / 2.0f,
                this.yPosition + offY + (this.height) / 2.0f - FontType.HelveticaNeueCyrBold.getFontContainer().height() / 2.0f * textScale, textScale, 0x121212);
    }

    private boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
    }

    public EnumSlotSkillActiveType getType() {
        return type;
    }

    public void setType(EnumSlotSkillActiveType type) {
        this.type = type;
    }

    public void setSkillTexture(String skillTexture) {
        if(skillTexture != null) this.skillTexture = new ResourceLocation(CoreSkillTree.MODID, "textures/learned/" + skillTexture + "_hud_256x256.png");
        else this.skillTexture = null;
    }

    public ResourceLocation getSkillTexture() {
        return skillTexture;
    }

    public void setSkill(GuiSkill skill) {
        this.skill = skill;
    }

    public GuiSkill getSkill() {
        return skill;
    }
}
