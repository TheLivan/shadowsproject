package ru.krogenit;

import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.escmenu.CoreEscMenu;
import ru.krogenit.hud.HudMainMod;
import ru.krogenit.npc.CoreNpcInterface;
import ru.krogenit.pda.CorePda;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.settings.CoreSettings;
import ru.krogenit.skilltree.CoreSkillTree;
import ru.krogenit.skilltree.type.EnumSkillStatType;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.resource.preload.PreLoadableResource;
import ru.xlv.core.resource.preload.PreLoadableTextureWrapFormat;

@PreLoadableResource
public class TextureRegister {

    public static final ResourceLocation textureESCButton = new ResourceLocation(CoreEscMenu.MODID, "textures/button.png");
    public static final ResourceLocation textureESCButtonHover = new ResourceLocation(CoreEscMenu.MODID, "textures/button_hover.png");
    public static final ResourceLocation textureESCButtonMask = new ResourceLocation(CoreEscMenu.MODID, "textures/button_mask.png");
    public static final ResourceLocation textureESCBorderTop = new ResourceLocation(CoreEscMenu.MODID, "textures/border_top.png");
    public static final ResourceLocation textureESCBorderBot = new ResourceLocation(CoreEscMenu.MODID, "textures/border_bottom.png");
    public static final ResourceLocation textureESCLogo = new ResourceLocation(CoreEscMenu.MODID, "textures/logo.png");
    @PreLoadableTextureWrapFormat(GL11.GL_REPEAT)
    public static final ResourceLocation textureESCGridTr = new ResourceLocation(CoreEscMenu.MODID, "textures/grid_tr.png");
    public static final ResourceLocation textureESCGridRectBlack = new ResourceLocation(CoreEscMenu.MODID, "textures/grid_rect_black.png");

    public static final ResourceLocation textureSettingsCaseBorder = new ResourceLocation(CoreSettings.MODID, "textures/case_border.png");
    public static final ResourceLocation textureSettingsSlider = new ResourceLocation(CoreSettings.MODID, "textures/slider.png");
    public static final ResourceLocation textureSettingsButton = new ResourceLocation(CoreSettings.MODID, "textures/button.png");
    public static final ResourceLocation textureSettingsBorderTop = new ResourceLocation(CoreSettings.MODID, "textures/border_top.png");
    public static final ResourceLocation textureSettingsBorderBot = new ResourceLocation(CoreSettings.MODID, "textures/border_bottom.png");

    public static final ResourceLocation textureInvBorderBot = new ResourceLocation("inventory", "corpse/border_bottom.png");
    public static final ResourceLocation textureInvBorderTop = new ResourceLocation("inventory", "corpse/border_top.png");
    public static final ResourceLocation textureInvBorderLeft = new ResourceLocation("inventory", "border_left.png");
    public static final ResourceLocation textureInvEquipmentLine = new ResourceLocation("inventory", "element_player_equipment_line.png");
    public static final ResourceLocation textureInvBorderForm = new ResourceLocation("inventory", "border_form.png");
    public static final ResourceLocation textureInvArrowTop = new ResourceLocation("inventory", "arrow_top.png");
    public static final ResourceLocation textureInvScrollBody = new ResourceLocation("inventory", "scroll_body.png");
    public static final ResourceLocation textureInvArrowBot = new ResourceLocation("inventory", "arrow_bottom.png");
    public static final ResourceLocation textureInvElementBio = new ResourceLocation("inventory", "element_biometrical.png");
    public static final ResourceLocation textureInvElementPower = new ResourceLocation("inventory", "element_power.png");
    public static final ResourceLocation textureInvElementLifeSystem = new ResourceLocation("inventory", "element_life_system.png");
    public static final ResourceLocation textureInvElementGrid = new ResourceLocation("inventory", "element_grid.png");
    public static final ResourceLocation textureInvToolTipBorder = new ResourceLocation("inventory", "tooltip_border.png");
    public static final ResourceLocation textureInvSlotSpecBg = new ResourceLocation("inventory", "slot_special_bg.png");
    public static final ResourceLocation textureInvSlotSpecGradient = new ResourceLocation("inventory", "slot_special_gradient.png");
    public static final ResourceLocation textureInvSlotSpecFrame = new ResourceLocation("inventory", "slot_special_frame.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_HEAD = new ResourceLocation("inventory", "slot_head.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_BODY = new ResourceLocation("inventory", "slot_body.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_ARMS = new ResourceLocation("inventory", "slot_arms.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_LEGS = new ResourceLocation("inventory", "slot_legs.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_BOOTS = new ResourceLocation("inventory", "slot_boots.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_BACKPACK = new ResourceLocation("inventory", "slot_backpack.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_MELEE = new ResourceLocation("inventory", "slot_melee.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_PISTOL = new ResourceLocation("inventory", "slot_pistol.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_MAIN = new ResourceLocation("inventory", "slot_main.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_WPN_BG = new ResourceLocation("inventory", "slot_special_weapon_bg.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_WPN_GRADIENT = new ResourceLocation("inventory", "slot_special_weapon_gradient.png");
    public static final ResourceLocation TEXTURE_INV_SLOT_WPN_FRAME = new ResourceLocation("inventory", "slot_special_weapon_frame.png");
    public static final ResourceLocation textureInvPlayerFloor = new ResourceLocation("inventory", "element_player_floor.png");
    public static final ResourceLocation textureInvPlayerFloor1 = new ResourceLocation("inventory", "element_player_floor1.png");
    public static final ResourceLocation textureInvPlayerLight = new ResourceLocation("inventory", "element_player_light.png");
    public static final ResourceLocation textureInvPlayerFloor2 = new ResourceLocation("inventory", "element_player_floor2.png");
    public static final ResourceLocation textureInvCorpseAva = new ResourceLocation("inventory", "corpse/ava.png");
    public static final ResourceLocation textureInvIconCircleStatEnergy = new ResourceLocation("inventory", "icon_circle_stat_energy.png");

    public static final ResourceLocation textureTreeBorderConfirm = new ResourceLocation(CoreSkillTree.MODID, "textures/border_confirm.png");
    public static final ResourceLocation textureTreeButtonAccept = new ResourceLocation(CoreSkillTree.MODID, "textures/button_accept.png");
    public static final ResourceLocation textureTreeButtonHover = new ResourceLocation(CoreSkillTree.MODID, "textures/button_hover.png");
    public static final ResourceLocation textureTreeButtonMask = new ResourceLocation(CoreSkillTree.MODID, "textures/button_mask.png");
    public static final ResourceLocation textureTreeButtonCancel = new ResourceLocation(CoreSkillTree.MODID, "textures/button_cancel.png");
    public static final ResourceLocation textureTreeIconResCombat = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_res1.png");
    public static final ResourceLocation textureTreeIconResResearch = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_res2.png");
    public static final ResourceLocation textureTreeIconResSurvive = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_res3.png");
    public static final ResourceLocation textureSkillActiveLearned = new ResourceLocation("skilltree", "textures/skill_active_learned.png");
    public static final ResourceLocation textureSkillPassive = new ResourceLocation("skilltree", "textures/icon_skill_passive.png");
    public static final ResourceLocation textureSkillIconLeftMouse = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_mouse_left.png");
    public static final ResourceLocation textureSkillIconStatHp = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_hp.png");
    public static final ResourceLocation textureSkillIconStatMana = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_mana.png");
    public static final ResourceLocation textureSkillIconStatStamina = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_energy.png");
    public static final ResourceLocation textureSkillIconStatArmor = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_armor.png");
    public static final ResourceLocation textureSkillIconStatCutDefence = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_defence.png");
    public static final ResourceLocation textureSkillIconSkillMask = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_mask.png");
    public static final ResourceLocation textureSkillIconSkillLock = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_lock.png");
    public static final ResourceLocation textureSkillIconSkillEmpty = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_empty.png");
    public static final ResourceLocation textureSkillIconSkillActive = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_active.png");
    public static final ResourceLocation textureSkillIconSkillArmor = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_armor.png");
    public static final ResourceLocation textureSkillIconSkillStatMana = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_mana.png");
    public static final ResourceLocation textureSkillIconSkillStatTime = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_stat_time.png");
    public static final ResourceLocation textureSkillIconLearned = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_learned.png");
    public static final ResourceLocation textureSkillHexHoverMask = new ResourceLocation(CoreSkillTree.MODID, "textures/skill_hex_hover_mask.png");
    public static final ResourceLocation textureSkillHexHighlight = new ResourceLocation(CoreSkillTree.MODID, "textures/skill_hex_highlight.png");
    public static final ResourceLocation textureSkillActiveHoverMask = new ResourceLocation(CoreSkillTree.MODID, "textures/skill_hover_mask.png");
    public static final ResourceLocation textureSkillActiveHighlight = new ResourceLocation(CoreSkillTree.MODID, "textures/skill_active_highlight.png");
    public static final ResourceLocation textureSkillPssiveBorderLearned = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_passive_border_learned.png");
    public static final ResourceLocation textureSkillPssiveBorder = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_passive_border.png");
    public static final ResourceLocation textureSkillActiveBorderLearned = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_border_rect_learned.png");
    public static final ResourceLocation textureSkillActiveBorder = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_border_rect.png");
    public static final ResourceLocation textureSkillButtonLeftClose = new ResourceLocation(CoreSkillTree.MODID, "textures/button_left_stats_close.png");
    public static final ResourceLocation textureSkillButtonLeft = new ResourceLocation(CoreSkillTree.MODID, "textures/button_left_stats.png");
    public static final ResourceLocation textureSkillButtonPlus = new ResourceLocation(CoreSkillTree.MODID, "textures/button_plus.png");
    public static final ResourceLocation textureSkillButtonPlusHover = new ResourceLocation(CoreSkillTree.MODID, "textures/button_plus_hover.png");
    public static final ResourceLocation textureSkillButtonMinus = new ResourceLocation(CoreSkillTree.MODID, "textures/button_minus.png");
    public static final ResourceLocation textureSkillButtonMinusHover = new ResourceLocation(CoreSkillTree.MODID, "textures/button_minus_hover.png");
    public static final ResourceLocation textureSkillBorderLeft = new ResourceLocation(CoreSkillTree.MODID, "textures/border_left.png");
    public static final ResourceLocation textureSkillPassiveSkillsBg = new ResourceLocation(CoreSkillTree.MODID, "textures/bg_passive_skills.png");
    public static final ResourceLocation textureSkillScrollBody = new ResourceLocation(CoreSkillTree.MODID, "textures/scroll_body.png");
    public static final ResourceLocation textureSkillOverlay = new ResourceLocation(CoreSkillTree.MODID, "textures/skill_tree_overlay.png");
    public static final ResourceLocation textureSkillBorderTop = new ResourceLocation(CoreSkillTree.MODID, "textures/border_top.png");
    public static final ResourceLocation textureSkillBorderRight = new ResourceLocation(CoreSkillTree.MODID, "textures/border_right.png");
    public static final ResourceLocation textureSkillBorderBot = new ResourceLocation(CoreSkillTree.MODID, "textures/border_bottom.png");
    public static final ResourceLocation textureSkillActiveLock = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_active_lock.png");
    public static final ResourceLocation textureSkillActiveEmpty = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_active_empty.png");
    public static final ResourceLocation textureSkillActiveEmptyBorder = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_active_empty_border.png");
    public static final ResourceLocation textureSkillPssiveLock = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_passive_lock.png");
    public static final ResourceLocation textureSkillPssiveEmpty = new ResourceLocation(CoreSkillTree.MODID, "textures/icon_skill_passive_empty.png");

    public static final ResourceLocation textureInvIconCredit = new ResourceLocation("inventory", "icon_credit.png");
    public static final ResourceLocation textureInvIconPlatina = new ResourceLocation("inventory", "icon_platina.png");
    public static final ResourceLocation textureInvIconWeight = new ResourceLocation("inventory", "icon_weight.png");
    public static final ResourceLocation textureInvIconGold = new ResourceLocation("inventory", "icon_currency_gold.png");
    public static final ResourceLocation textureInvIconSilver = new ResourceLocation("inventory", "icon_currency_silver.png");
    public static final ResourceLocation textureInvIconBronze = new ResourceLocation("inventory", "icon_currency_bronze.png");
    public static final ResourceLocation textureInvIconHpGaus = new ResourceLocation("inventory", "icon_hp_gaus.png");
    public static final ResourceLocation textureInvIconManaGaus = new ResourceLocation("inventory", "icon_mana_gaus.png");
    public static final ResourceLocation textureInvIconStaminaGaus = new ResourceLocation("inventory", "icon_energy_gaus.png");
    public static final ResourceLocation textureInvIconHp = new ResourceLocation("inventory", "icon_hp.png");
    public static final ResourceLocation textureInvIconMana = new ResourceLocation("inventory", "icon_mana.png");
    public static final ResourceLocation textureInvIconStamina = new ResourceLocation("inventory", "icon_energy.png");
    public static final ResourceLocation textureInvIconArmor = new ResourceLocation("inventory", "icon_armor.png");
    public static final ResourceLocation textureInvIconCutDefence = new ResourceLocation("inventory", "icon_cut_defence.png");
    public static final ResourceLocation textureInvIconArmorPopup = new ResourceLocation("inventory", "icon_armor_popup.png");
    public static final ResourceLocation textureInvIconCutDefencePopup = new ResourceLocation("inventory", "icon_defence_popup.png");
    public static final ResourceLocation textureInvIconEnergy = new ResourceLocation("inventory", "icon_stat_energy.png");
    public static final ResourceLocation textureInvIconHeat = new ResourceLocation("inventory", "icon_stat_heat.png");
    public static final ResourceLocation textureInvIconFire = new ResourceLocation("inventory", "icon_stat_fire.png");
    public static final ResourceLocation textureInvIconElectricity = new ResourceLocation("inventory", "icon_stat_electricity.png");
    public static final ResourceLocation textureInvIconToxins = new ResourceLocation("inventory", "icon_stat_toxins.png");
    public static final ResourceLocation textureInvIconRadiation = new ResourceLocation("inventory", "icon_stat_radiation.png");
    public static final ResourceLocation textureInvIconEfficiency = new ResourceLocation("inventory", "icon_efficiency.png");
    public static final ResourceLocation textureInvIconResilience = new ResourceLocation("inventory", "icon_resilience.png");
    public static final ResourceLocation textureInvIconExplosion = new ResourceLocation("inventory", "icon_stat_explosion.png");

    public static final ResourceLocation textureNPCButtonDialog = new ResourceLocation(CoreNpcInterface.MODID, "trade/button_dialog.png");
    public static final ResourceLocation textureNPCButtonMask = new ResourceLocation(CoreNpcInterface.MODID, "trade/button_mask.png");
    public static final ResourceLocation textureNPCButtonTrade = new ResourceLocation(CoreNpcInterface.MODID, "trade/button_trade.png");
    public static final ResourceLocation textureNPCButtonSave = new ResourceLocation(CoreNpcInterface.MODID, "trade/button_save.png");
    public static final ResourceLocation textureNPCButtonLocked = new ResourceLocation(CoreNpcInterface.MODID, "trade/button_locked.png");
    public static final ResourceLocation textureNPCDialogBorderBot = new ResourceLocation(CoreNpcInterface.MODID, "dialog/border_bottom.png");
    public static final ResourceLocation textureNPCDialogBorderTop = new ResourceLocation(CoreNpcInterface.MODID, "dialog/border_top.png");
    public static final ResourceLocation textureNPCTradeArrowsRight = new ResourceLocation("npcinterface", "trade/arrows_right.png");
    public static final ResourceLocation textureNPCTradeArrowsLeft = new ResourceLocation("npcinterface", "trade/arrows_left.png");
    public static final ResourceLocation textureNPCTradeArrowLeft = new ResourceLocation(CoreNpcInterface.MODID, "trade/arrow_left.png");
    public static final ResourceLocation textureNPCTradeArrowRight = new ResourceLocation(CoreNpcInterface.MODID, "trade/arrow_right.png");
    public static final ResourceLocation textureNPCTradeDevider = new ResourceLocation("npcinterface", "trade/devider.png");
    public static final ResourceLocation textureNPCBorderBotIconsCover = new ResourceLocation(CoreNpcInterface.MODID, "trade/border_bottom_icons_cover.png");
    public static final ResourceLocation textureNPCDeviderLine = new ResourceLocation(CoreNpcInterface.MODID, "dialog/devider_line.png");

    public static final ResourceLocation textureDeathScreenElementDevider = new ResourceLocation("deathscreen", "element_devider.png");
    public static final ResourceLocation textureDeathScreenElementDeviderSmall = new ResourceLocation("deathscreen", "element_devider_small.png");
    public static final ResourceLocation textureDeathScreenButtonCollapse = new ResourceLocation("deathscreen", "button_collapse.png");
    public static final ResourceLocation textureDeathScreenButtonExpand = new ResourceLocation("deathscreen", "button_expand.png");
    public static final ResourceLocation textureDeathScreenButtonDeathScreen = new ResourceLocation("deathscreen", "button.png");
    public static final ResourceLocation textureDeathScreenButtonDeathScreenHover = new ResourceLocation("deathscreen", "button_hover.png");
    public static final ResourceLocation textureDeathScreenIconTravel = new ResourceLocation("deathscreen", "icon_travel.png");
    public static final ResourceLocation textureDeathScreenIconLocations = new ResourceLocation("deathscreen", "icon_locations.png");
    public static final ResourceLocation textureDeathScreenIconPlayers = new ResourceLocation("deathscreen", "icon_players.png");
    public static final ResourceLocation textureDeathScreenIconMonsters = new ResourceLocation("deathscreen", "icon_monsters.png");
    public static final ResourceLocation textureDeathScreenIconBoss = new ResourceLocation("deathscreen", "icon_boss.png");
    public static final ResourceLocation textureDeathScreenIconMoney = new ResourceLocation("deathscreen", "icon_money.png");
    public static final ResourceLocation textureDeathScreenIconMissions = new ResourceLocation("deathscreen", "icon_missions.png");
    public static final ResourceLocation textureDeathScreenIconCombat = new ResourceLocation("deathscreen", "icon_combat.png");
    public static final ResourceLocation textureDeathScreenIconResearch = new ResourceLocation("deathscreen", "icon_research.png");
    public static final ResourceLocation textureDeathScreenIconSurvive = new ResourceLocation("deathscreen", "icon_survive.png");
    public static final ResourceLocation textureDeathScreenIconBullet = new ResourceLocation("deathscreen", "icon_bullet.png");

    public static final ResourceLocation textureHUDChatBg = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementChatBg.png");
    public static final ResourceLocation textureHUDChatScrollBg = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementChatSkrollBg.png");
    public static final ResourceLocation textureHUDIconArrow = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconArrow.png");
    public static final ResourceLocation textureHUDIconArrowDown = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconArrowDown.png");
    public static final ResourceLocation textureHUDChatButton = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementChatButton.png");
    public static final ResourceLocation textureHUDChatInput = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementChatInput.png");
    public static final ResourceLocation textureHUDChatSent = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconChatSent.png");
    public static final ResourceLocation textureHUDElementLeft = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementLeft.png");
    public static final ResourceLocation textureHUDElementMana = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementMana.png");
    public static final ResourceLocation textureHUDIconMana = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconMana.png");
    public static final ResourceLocation textureHUDSkillFrame = new ResourceLocation(HudMainMod.MODID, "textures/gui/skillFrame.png");
    public static final ResourceLocation textureHUDSkillRecharging = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconSkillRecharging.png");
    public static final ResourceLocation textureHUDSkillReady = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconSkillReady.png");
    public static final ResourceLocation textureHUDElementRight = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementRight.png");
    public static final ResourceLocation textureHUDElementHp = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementHp.png");
    public static final ResourceLocation textureHUDIconHp = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconHp.png");
    public static final ResourceLocation textureHUDIconBullets = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconBullets.png");
    public static final ResourceLocation textureHUDSlotItems = new ResourceLocation(HudMainMod.MODID, "textures/gui/slotItems.png");
    public static final ResourceLocation textureHUDSlotActive = new ResourceLocation(HudMainMod.MODID, "textures/gui/slotActive.png");
    public static final ResourceLocation textureHUDSlotNumbers = new ResourceLocation(HudMainMod.MODID, "textures/gui/slotNumbers.png");
    public static final ResourceLocation textureHUDElementStamina = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementStamina.png");
    public static final ResourceLocation textureHUDIconStamina = new ResourceLocation(HudMainMod.MODID, "textures/gui/iconStamina.png");
    public static final ResourceLocation textureHUDElementStaminaBg = new ResourceLocation(HudMainMod.MODID, "textures/gui/elementStaminaBg.png");

    public static final ResourceLocation textureModifyBorderTop = new ResourceLocation("modify", "border_top.png");
    public static final ResourceLocation textureModifyBorderBot = new ResourceLocation("modify", "border_bottom.png");
    public static final ResourceLocation textureModifyRows = new ResourceLocation("modify", "rows.png");
    public static final ResourceLocation textureModifyBars = new ResourceLocation("modify", "bars.png");
    public static final ResourceLocation textureModifyIconAll = new ResourceLocation("modify", "icon_all.png");
    public static final ResourceLocation textureModifyIconWeapons = new ResourceLocation("modify", "icon_weapons.png");
    public static final ResourceLocation textureModifyIconPistols = new ResourceLocation("modify", "icon_pistols.png");
    public static final ResourceLocation textureModifyIconKnifes = new ResourceLocation("modify", "icon_knifes.png");
    public static final ResourceLocation textureModifySlotGun = new ResourceLocation("modify", "slot_gun.png");
    public static final ResourceLocation textureModifySlotSight = new ResourceLocation("modify", "slot_pricel.png");
    public static final ResourceLocation textureModifySlotTrigger = new ResourceLocation("modify", "slot_trigger.png");
    public static final ResourceLocation textureModifySlotSilencer = new ResourceLocation("modify", "slot_silencer.png");
    public static final ResourceLocation textureModifySlotButt = new ResourceLocation("modify", "slot_butt.png");
    public static final ResourceLocation textureModifySlotMagazine = new ResourceLocation("modify", "slot_magazine.png");
    public static final ResourceLocation textureModifySlotCase = new ResourceLocation("modify", "slot_case.png");
    public static final ResourceLocation textureModifySlotAmmo = new ResourceLocation("modify", "slot_ammo.png");
    public static final ResourceLocation textureModifySlot = new ResourceLocation("modify", "slot_modify.png");
    public static final ResourceLocation textureModifyStatAccuracy = new ResourceLocation("modify", "icon_accuracy.png");
    public static final ResourceLocation textureModifyStatClip = new ResourceLocation("modify", "icon_clip.png");
    public static final ResourceLocation textureModifyStatDurability = new ResourceLocation("modify", "icon_durability.png");
    public static final ResourceLocation textureModifyStatKnockBack = new ResourceLocation("modify", "icon_knockback.png");
    public static final ResourceLocation textureModifyThermalDamage = new ResourceLocation("modify", "icon_thermal_damage.png");
    public static final ResourceLocation textureModifyAccuracy = new ResourceLocation("modify", "icon_accuracy.png");
    public static final ResourceLocation textureModifyClip = new ResourceLocation("modify", "icon_clip.png");
    public static final ResourceLocation textureModifyDurability = new ResourceLocation("modify", "icon_durability.png");
    public static final ResourceLocation textureModifyKnockBack = new ResourceLocation("modify", "icon_knockback.png");

    public static final ResourceLocation texturePDAIconCircle = new ResourceLocation(CorePda.MODID, "textures/icon_circle.png");
    public static final ResourceLocation texturePDAButtonHelp = new ResourceLocation(CorePda.MODID, "textures/button_help.png");
    public static final ResourceLocation texturePDAButtonHelpHover = new ResourceLocation(CorePda.MODID, "textures/button_help_hover.png");
    public static final ResourceLocation texturePDAButtonCollapse = new ResourceLocation(CorePda.MODID, "textures/button_collapse.png");
    public static final ResourceLocation texturePDAButtonCollapseHover = new ResourceLocation(CorePda.MODID, "textures/button_collapse_hover.png");
    public static final ResourceLocation texturePDAButtonClose = new ResourceLocation(CorePda.MODID, "textures/button_close.png");
    public static final ResourceLocation texturePDAButtonCloseHover = new ResourceLocation(CorePda.MODID, "textures/button_close_hover.png");
    public static final ResourceLocation texturePDAButtonSubInfo = new ResourceLocation(CorePda.MODID, "textures/button_sub_info.png");
    public static final ResourceLocation texturePDAButtonSub = new ResourceLocation(CorePda.MODID, "textures/button_sub.png");
    public static final ResourceLocation texturePDAButtonSubHover = new ResourceLocation(CorePda.MODID, "textures/button_sub_hover.png");
    public static final ResourceLocation texturePDAButtonSubActive = new ResourceLocation(CorePda.MODID, "textures/button_sub_active.png");
    public static final ResourceLocation texturePDAButtonSubInactive = new ResourceLocation(CorePda.MODID, "textures/button_sub_inactive.png");
    public static final ResourceLocation texturePDAButtonSubInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_sub_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonAuction = new ResourceLocation(CorePda.MODID, "textures/button_auction.png");
    public static final ResourceLocation texturePDAButtonAuctionHover = new ResourceLocation(CorePda.MODID, "textures/button_auction_hover.png");
    public static final ResourceLocation texturePDAButtonAuctionActive = new ResourceLocation(CorePda.MODID, "textures/button_auction_active.png");
    public static final ResourceLocation texturePDAButtonAuctionInactive = new ResourceLocation(CorePda.MODID, "textures/button_auction_inactive.png");
    public static final ResourceLocation texturePDAButtonAuctionInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_auction_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonGroup = new ResourceLocation(CorePda.MODID, "textures/button_group.png");
    public static final ResourceLocation texturePDAButtonGroupHover = new ResourceLocation(CorePda.MODID, "textures/button_group_hover.png");
    public static final ResourceLocation texturePDAButtonGroupActive = new ResourceLocation(CorePda.MODID, "textures/button_group_active.png");
    public static final ResourceLocation texturePDAButtonGroupInactive = new ResourceLocation(CorePda.MODID, "textures/button_group_inactive.png");
    public static final ResourceLocation texturePDAButtonGroupInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_group_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonMail = new ResourceLocation(CorePda.MODID, "textures/button_mail.png");
    public static final ResourceLocation texturePDAButtonMailHover = new ResourceLocation(CorePda.MODID, "textures/button_mail_hover.png");
    public static final ResourceLocation texturePDAButtonMailActive = new ResourceLocation(CorePda.MODID, "textures/button_mail_active.png");
    public static final ResourceLocation texturePDAButtonMailInactive = new ResourceLocation(CorePda.MODID, "textures/button_mail_inactive.png");
    public static final ResourceLocation texturePDAButtonMailInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_mail_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonMap = new ResourceLocation(CorePda.MODID, "textures/button_map.png");
    public static final ResourceLocation texturePDAButtonMapHover = new ResourceLocation(CorePda.MODID, "textures/button_map_hover.png");
    public static final ResourceLocation texturePDAButtonMapActive = new ResourceLocation(CorePda.MODID, "textures/button_map_active.png");
    public static final ResourceLocation texturePDAButtonMapInactive = new ResourceLocation(CorePda.MODID, "textures/button_map_inactive.png");
    public static final ResourceLocation texturePDAButtonMapInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_map_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonMissions = new ResourceLocation(CorePda.MODID, "textures/button_missions.png");
    public static final ResourceLocation texturePDAButtonMissionsHover = new ResourceLocation(CorePda.MODID, "textures/button_missions_hover.png");
    public static final ResourceLocation texturePDAButtonMissionsActive = new ResourceLocation(CorePda.MODID, "textures/button_missions_active.png");
    public static final ResourceLocation texturePDAButtonMissionsInactive = new ResourceLocation(CorePda.MODID, "textures/button_missions_inactive.png");
    public static final ResourceLocation texturePDAButtonMissionsInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_missions_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonProfile = new ResourceLocation(CorePda.MODID, "textures/button_profile.png");
    public static final ResourceLocation texturePDAButtonProfileHover = new ResourceLocation(CorePda.MODID, "textures/button_profile_hover.png");
    public static final ResourceLocation texturePDAButtonProfileActive = new ResourceLocation(CorePda.MODID, "textures/button_profile_active.png");
    public static final ResourceLocation texturePDAButtonProfileInactive = new ResourceLocation(CorePda.MODID, "textures/button_profile_inactive.png");
    public static final ResourceLocation texturePDAButtonProfileInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_profile_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonSociety = new ResourceLocation(CorePda.MODID, "textures/button_society.png");
    public static final ResourceLocation texturePDAButtonSocietyHover = new ResourceLocation(CorePda.MODID, "textures/button_society_hover.png");
    public static final ResourceLocation texturePDAButtonSocietyActive = new ResourceLocation(CorePda.MODID, "textures/button_society_active.png");
    public static final ResourceLocation texturePDAButtonSocietyInactive = new ResourceLocation(CorePda.MODID, "textures/button_society_inactive.png");
    public static final ResourceLocation texturePDAButtonSocietyInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_society_inactive_hover.png");
    public static final ResourceLocation texturePDAButtonWiki = new ResourceLocation(CorePda.MODID, "textures/button_wiki.png");
    public static final ResourceLocation texturePDAButtonWikiHover = new ResourceLocation(CorePda.MODID, "textures/button_wiki_hover.png");
    public static final ResourceLocation texturePDAButtonWikiActive = new ResourceLocation(CorePda.MODID, "textures/button_wiki_active.png");
    public static final ResourceLocation texturePDAButtonWikiInactive = new ResourceLocation(CorePda.MODID, "textures/button_wiki_inactive.png");
    public static final ResourceLocation texturePDAButtonWikiInactiveHover = new ResourceLocation(CorePda.MODID, "textures/button_wiki_inactive_hover.png");
    public static final ResourceLocation texturePDAIconIdPlayer = new ResourceLocation(CorePda.MODID, "textures/icon_id_player.png");
    public static final ResourceLocation texturePDAElementLoadingBorder = new ResourceLocation(CorePda.MODID, "textures/element_loading_border.png");
    public static final ResourceLocation texturePDAElementLoadingBar = new ResourceLocation(CorePda.MODID, "textures/element_loading_bar.png");
    public static final ResourceLocation texturePDAIconServerStatusMisc = new ResourceLocation(CorePda.MODID, "textures/icon_server_status_misc.png");
    public static final ResourceLocation texturePDABorderBot = new ResourceLocation(CorePda.MODID, "textures/border_bottom.png");
    public static final ResourceLocation texturePDABorderTop = new ResourceLocation(CorePda.MODID, "textures/border_top.png");
    public static final ResourceLocation texturePDABorderLeft = new ResourceLocation(CorePda.MODID, "textures/border_left.png");
    public static final ResourceLocation texturePDABorderRight = new ResourceLocation(CorePda.MODID, "textures/border_right.png");

    public static final ResourceLocation texturePDAAuctionIconDone = new ResourceLocation(CorePda.MODID, "textures/auction/done_icon.png");
    public static final ResourceLocation texturePDAAuctionServerImage = new ResourceLocation(CorePda.MODID, "textures/auction/server_image.png");
    public static final ResourceLocation texturePDAAuctionItem = new ResourceLocation(CorePda.MODID, "textures/auction/item_image.png");

    public static final ResourceLocation texturePDAProfileAva = new ResourceLocation(CorePda.MODID, "textures/profile/avatar.png");
    public static final ResourceLocation texturePDAProfileSlotAchieve = new ResourceLocation(CorePda.MODID, "textures/profile/slot_achievement.png");
    public static final ResourceLocation texturePDAProfileBorderDevider = new ResourceLocation(CorePda.MODID, "textures/profile/border_devider.png");
    public static final ResourceLocation texturePDAProfileBorderTop = new ResourceLocation(CorePda.MODID, "textures/profile/border_top.png");
    public static final ResourceLocation texturePDAProfileFaceBio = new ResourceLocation(CorePda.MODID, "textures/profile/icon_face_bio.png");

    public static final ResourceLocation texturePDAAchieveBronze = new ResourceLocation(CorePda.MODID, "textures/achievements/achievement_bronze.png");
    public static final ResourceLocation texturePDAAchieveSilver = new ResourceLocation(CorePda.MODID, "textures/achievements/achievement_silver.png");
    public static final ResourceLocation texturePDAAchieveGold = new ResourceLocation(CorePda.MODID, "textures/achievements/achievement_gold.png");
    public static final ResourceLocation texturePDAAchieveBarMask = new ResourceLocation(CorePda.MODID, "textures/achievements/progress_bar_mask.png");

    public static final ResourceLocation texturePDACommServerImage = new ResourceLocation(CorePda.MODID, "textures/community/server_image.png");

    public static final ResourceLocation texturePDAGroupIconLeader = new ResourceLocation(CorePda.MODID, "textures/group/icon_leader.png");
    public static final ResourceLocation texturePDAGroupClassStormtrooper = new ResourceLocation(CorePda.MODID, "textures/group/class_stormtrooper.png");
    public static final ResourceLocation texturePDAGroupClassTank = new ResourceLocation(CorePda.MODID, "textures/group/class_tank.png");
    public static final ResourceLocation texturePDAGroupClassMedic = new ResourceLocation(CorePda.MODID, "textures/group/class_medic.png");
    public static final ResourceLocation texturePDAGroupIconMedic = new ResourceLocation(CorePda.MODID, "textures/group/icon_medic.png");
    public static final ResourceLocation texturePDAGroupIconStormtrooper = new ResourceLocation(CorePda.MODID, "textures/group/icon_stormtrooper.png");
    public static final ResourceLocation texturePDAGroupIconTank = new ResourceLocation(CorePda.MODID, "textures/group/icon_tank.png");

    public static final ResourceLocation texturePDAMailServerImage = new ResourceLocation(CorePda.MODID, "textures/mail/server_image.png");

    public static final ResourceLocation texturePDAMapPinNPC = new ResourceLocation(CorePda.MODID, "textures/map/pin_npc.png");
    public static final ResourceLocation texturePDAMapIconMoney = new ResourceLocation(CorePda.MODID, "textures/map/icon_money.png");
    public static final ResourceLocation texturePDAMapIconPlayer = new ResourceLocation(CorePda.MODID, "textures/map/icon_player.png");
    public static final ResourceLocation texturePDAMapPinQuest = new ResourceLocation(CorePda.MODID, "textures/map/pin_quest.png");
    public static final ResourceLocation texturePDAMapBorder = new ResourceLocation(CorePda.MODID, "textures/map/border.png");
    public static final ResourceLocation texturePDAMapLeftMouse = new ResourceLocation(CorePda.MODID, "textures/map/left_mouse.png");
    public static final ResourceLocation texturePDAMapIconKill = new ResourceLocation(CorePda.MODID, "textures/map/icon_kill.png");
    public static final ResourceLocation texturePDAMapIconQuestion = new ResourceLocation(CorePda.MODID, "textures/map/icon_question.png");
    public static final ResourceLocation texturePDAMapFilterQuestion = new ResourceLocation(CorePda.MODID, "textures/map/filter_question.png");
    public static final ResourceLocation texturePDAMapFilterQuestionActive = new ResourceLocation(CorePda.MODID, "textures/map/filter_question_active.png");
    public static final ResourceLocation texturePDAMapFilterMoney = new ResourceLocation(CorePda.MODID, "textures/map/filter_money.png");
    public static final ResourceLocation texturePDAMapFilterMoneyActive = new ResourceLocation(CorePda.MODID, "textures/map/filter_money_active.png");
    public static final ResourceLocation texturePDAMapFilterAmmo = new ResourceLocation(CorePda.MODID, "textures/map/filter_ammo.png");
    public static final ResourceLocation texturePDAMapFilterAmmoActive = new ResourceLocation(CorePda.MODID, "textures/map/filter_ammo_active.png");
    public static final ResourceLocation texturePDAMapFilterKill = new ResourceLocation(CorePda.MODID, "textures/map/filter_kill.png");
    public static final ResourceLocation texturePDAMapFilterKillActive = new ResourceLocation(CorePda.MODID, "textures/map/filter_kill_active.png");
    public static final ResourceLocation texturePDAMapButtonScaleMinus = new ResourceLocation(CorePda.MODID, "textures/map/button_scale_minus.png");
    public static final ResourceLocation texturePDAMapButtonScaleMinusHover = new ResourceLocation(CorePda.MODID, "textures/map/button_scale_minus_hover.png");
    public static final ResourceLocation texturePDAMapButtonScalePlus = new ResourceLocation(CorePda.MODID, "textures/map/button_scale_plus.png");
    public static final ResourceLocation texturePDAMapButtonScalePlusHover = new ResourceLocation(CorePda.MODID, "textures/map/button_scale_plus_hover.png");
    public static final ResourceLocation texturePDAMapButtonPlayer = new ResourceLocation(CorePda.MODID, "textures/map/button_player.png");
    public static final ResourceLocation texturePDAMapButtonPlayerHover = new ResourceLocation(CorePda.MODID, "textures/map/button_player_hover.png");
    public static final ResourceLocation texturePDAMap = new ResourceLocation(CorePda.MODID, "textures/map/map.png");
    public static final ResourceLocation texturePDAMapBorders = new ResourceLocation(CorePda.MODID, "textures/map/map_borders.png");
    public static final ResourceLocation texturePDAMapCompass = new ResourceLocation(CorePda.MODID, "textures/map/compass.png");

    public static final ResourceLocation texturePDAMissionsBattleBg = new ResourceLocation(CorePda.MODID, "textures/missions/bg_battle.png");
    public static final ResourceLocation texturePDAMissionsResearchBg = new ResourceLocation(CorePda.MODID, "textures/missions/bg_research.png");
    public static final ResourceLocation texturePDAMissionsSurviveBg = new ResourceLocation(CorePda.MODID, "textures/missions/bg_survive.png");
    public static final ResourceLocation texturePDAMissionsDevider = new ResourceLocation(CorePda.MODID, "textures/missions/devider.png");
    public static final ResourceLocation texturePDAMissionsIconBattle = new ResourceLocation(CorePda.MODID, "textures/missions/icon_battle.png");
    public static final ResourceLocation texturePDAMissionsIconResearch = new ResourceLocation(CorePda.MODID, "textures/missions/icon_research.png");
    public static final ResourceLocation texturePDAMissionsIconSurvive = new ResourceLocation(CorePda.MODID, "textures/missions/icon_survive.png");

    public static final ResourceLocation texturePDAWikiItem1 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_1.png");
    public static final ResourceLocation texturePDAWikiItem2 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_2.png");
    public static final ResourceLocation texturePDAWikiItem3 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_3.png");
    public static final ResourceLocation texturePDAWikiItem4 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_4.png");
    public static final ResourceLocation texturePDAWikiItem5 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_5.png");
    public static final ResourceLocation texturePDAWikiItem6 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_6.png");
    public static final ResourceLocation texturePDAWikiItem7 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_7.png");
    public static final ResourceLocation texturePDAWikiItem8 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_8.png");
    public static final ResourceLocation texturePDAWikiItem9 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_9.png");
    public static final ResourceLocation texturePDAWikiItem10 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_10.png");
    public static final ResourceLocation texturePDAWikiItem11 = new ResourceLocation(CorePda.MODID, "textures/wiki/item_11.png");
    public static final ResourceLocation texturePDAWikiButtonHome = new ResourceLocation(CorePda.MODID, "textures/wiki/button_home.png");
    public static final ResourceLocation texturePDAWikiButtonHomeHover = new ResourceLocation(CorePda.MODID, "textures/wiki/button_home_hover.png");
    public static final ResourceLocation texturePDAWikiButtonBack = new ResourceLocation(CorePda.MODID, "textures/wiki/button_back.png");
    public static final ResourceLocation texturePDAWikiButtonBackHover = new ResourceLocation(CorePda.MODID, "textures/wiki/button_back_hover.png");
    public static final ResourceLocation texturePDAWikiButtonLeft = new ResourceLocation(CorePda.MODID, "textures/wiki/button_left.png");
    public static final ResourceLocation texturePDAWikiButtonLeftHover = new ResourceLocation(CorePda.MODID, "textures/wiki/button_left_hover.png");
    public static final ResourceLocation texturePDAWikiButtonRight = new ResourceLocation(CorePda.MODID, "textures/wiki/button_right.png");
    public static final ResourceLocation texturePDAWikiButtonRightHover = new ResourceLocation(CorePda.MODID, "textures/wiki/button_right_hover.png");

    public static final ResourceLocation texturePDAWinAchieveBg = new ResourceLocation(CorePda.MODID, "textures/windows/achievements/bg.png");
    public static final ResourceLocation texturePDAWinFriendsBg = new ResourceLocation(CorePda.MODID, "textures/windows/friends/bg.png");
    public static final ResourceLocation texturePDAWinPlayersDevider = new ResourceLocation(CorePda.MODID, "textures/windows/players/devider_window.png");
    public static final ResourceLocation texturePDAWinFriendsIconAccept = new ResourceLocation(CorePda.MODID, "textures/windows/friends/icon_accept.png");
    public static final ResourceLocation texturePDAWinFriendsIconDecline = new ResourceLocation(CorePda.MODID, "textures/windows/friends/icon_decline.png");
    public static final ResourceLocation texturePDAWinPlayersBg = new ResourceLocation(CorePda.MODID, "textures/windows/players/bg.png");

    public static final ResourceLocation texturePDAWinMailBg = new ResourceLocation(CorePda.MODID, "textures/windows/mail/bg.png");

    public static final ResourceLocation textureShopBackground = new ResourceLocation("shop" , "background.png");
    public static final ResourceLocation textureShopButtonBalance = new ResourceLocation("shop", "button_balance.png");
    public static final ResourceLocation textureShopIconSearch = new ResourceLocation("shop", "icon_search.png");
    public static final ResourceLocation textureShopButtonItem = new ResourceLocation("shop", "button_item.png");
    public static final ResourceLocation textureShopBadgeLtd = new ResourceLocation("shop", "badge_ltd.png");
    public static final ResourceLocation textureShopBadgeNew = new ResourceLocation("shop", "badge_new.png");
    public static final ResourceLocation textureShopBadgeTop = new ResourceLocation("shop", "badge_top.png");
    public static final ResourceLocation textureShopItemDevider = new ResourceLocation("shop", "item_devider.png");
    public static final ResourceLocation textureShopMainBanner = new ResourceLocation("shop", "main/banner.png");
    public static final ResourceLocation textureShopMainSlider = new ResourceLocation("shop", "main/slider.png");
    public static final ResourceLocation textureShopMainSliderActive = new ResourceLocation("shop", "main/slider_active.png");
    public static final ResourceLocation textureShopDevider = new ResourceLocation("shop", "devider.png");
    public static final ResourceLocation textureShopButtonArmor = new ResourceLocation("shop", "button_armor.png");
    public static final ResourceLocation textureShopButtonArmorHover = new ResourceLocation("shop", "button_armor_hover.png");
    public static final ResourceLocation textureShopButtonArmorActive = new ResourceLocation("shop", "button_armor_active.png");
    public static final ResourceLocation textureShopButtonCase = new ResourceLocation("shop", "button_case.png");
    public static final ResourceLocation textureShopButtonCaseHover = new ResourceLocation("shop", "button_case_hover.png");
    public static final ResourceLocation textureShopButtonCaseActive = new ResourceLocation("shop", "button_case_active.png");
    public static final ResourceLocation textureShopButtonExperience = new ResourceLocation("shop", "button_experience.png");
    public static final ResourceLocation textureShopButtonExperienceHover = new ResourceLocation("shop", "button_experience_hover.png");
    public static final ResourceLocation textureShopButtonExperienceActive = new ResourceLocation("shop", "button_experience_active.png");
    public static final ResourceLocation textureShopButtonKit = new ResourceLocation("shop", "button_kit.png");
    public static final ResourceLocation textureShopButtonKitHover = new ResourceLocation("shop", "button_kit_hover.png");
    public static final ResourceLocation textureShopButtonKitActive = new ResourceLocation("shop", "button_kit_active.png");
    public static final ResourceLocation textureShopButtonMain = new ResourceLocation("shop", "button_main.png");
    public static final ResourceLocation textureShopButtonMainHover = new ResourceLocation("shop", "button_main_hover.png");
    public static final ResourceLocation textureShopButtonMainActive = new ResourceLocation("shop", "button_main_active.png");
    public static final ResourceLocation textureShopButtonMisc = new ResourceLocation("shop", "button_misc.png");
    public static final ResourceLocation textureShopButtonMiscHover = new ResourceLocation("shop", "button_misc_hover.png");
    public static final ResourceLocation textureShopButtonMiscActive = new ResourceLocation("shop", "button_misc_active.png");
    public static final ResourceLocation textureShopButtonNew = new ResourceLocation("shop", "button_new.png");
    public static final ResourceLocation textureShopButtonNewHover = new ResourceLocation("shop", "button_new_hover.png");
    public static final ResourceLocation textureShopButtonNewActive = new ResourceLocation("shop", "button_new_active.png");
    public static final ResourceLocation textureShopButtonSale = new ResourceLocation("shop", "button_sale.png");
    public static final ResourceLocation textureShopButtonSaleHover = new ResourceLocation("shop", "button_sale_hover.png");
    public static final ResourceLocation textureShopButtonSaleActive = new ResourceLocation("shop", "button_sale_active.png");
    public static final ResourceLocation textureShopButtonUnique = new ResourceLocation("shop", "button_unique.png");
    public static final ResourceLocation textureShopButtonUniqueHover = new ResourceLocation("shop", "button_unique_hover.png");
    public static final ResourceLocation textureShopButtonUniqueActive = new ResourceLocation("shop", "button_unique_active.png");
    public static final ResourceLocation textureShopButtonVisual = new ResourceLocation("shop", "button_visual.png");
    public static final ResourceLocation textureShopButtonVisualHover = new ResourceLocation("shop", "button_visual_hover.png");
    public static final ResourceLocation textureShopButtonVisualActive = new ResourceLocation("shop", "button_visual_active.png");
    public static final ResourceLocation textureShopButtonWeapons = new ResourceLocation("shop", "button_weapons.png");
    public static final ResourceLocation textureShopButtonWeaponsHover = new ResourceLocation("shop", "button_weapons_hover.png");
    public static final ResourceLocation textureShopButtonWeaponsActive = new ResourceLocation("shop", "button_weapons_active.png");

    public static ResourceLocation getSkillStatTexture(EnumSkillStatType statType) {
        switch (statType) {
            case TIME: return textureSkillIconSkillStatTime;
            default: return textureSkillIconSkillStatMana;
        }
    }

    public static ResourceLocation[] getPDAButtonTexture(EnumPdaLeftMenuSection leftMenuSection) {
        switch (leftMenuSection) {
            case PROFILE: return new ResourceLocation[] {texturePDAButtonProfile, texturePDAButtonProfileHover, texturePDAButtonProfileActive, texturePDAButtonProfileInactive, texturePDAButtonProfileInactiveHover};
            case AUCTION: return new ResourceLocation[] {texturePDAButtonAuction, texturePDAButtonAuctionHover, texturePDAButtonAuctionActive, texturePDAButtonAuctionInactive, texturePDAButtonAuctionInactiveHover};
            case COMMUNITY: return new ResourceLocation[] {texturePDAButtonSociety, texturePDAButtonSocietyHover, texturePDAButtonSocietyActive, texturePDAButtonSocietyInactive, texturePDAButtonSocietyInactiveHover};
            case GROUP: return new ResourceLocation[] {texturePDAButtonGroup, texturePDAButtonGroupHover, texturePDAButtonGroupActive, texturePDAButtonGroupInactive, texturePDAButtonGroupInactiveHover};
            case MAIL: return new ResourceLocation[] {texturePDAButtonMail, texturePDAButtonMailHover, texturePDAButtonMailActive, texturePDAButtonMailInactive, texturePDAButtonMailInactiveHover};
            case MAP: return new ResourceLocation[] {texturePDAButtonMap, texturePDAButtonMapHover, texturePDAButtonMapActive, texturePDAButtonMapInactive, texturePDAButtonMapInactiveHover};
            case MISSIONS: return new ResourceLocation[] {texturePDAButtonMissions, texturePDAButtonMissionsHover, texturePDAButtonMissionsActive, texturePDAButtonMissionsInactive, texturePDAButtonMissionsInactiveHover};
            case WIKI: return new ResourceLocation[] {texturePDAButtonWiki, texturePDAButtonWikiHover, texturePDAButtonWikiActive, texturePDAButtonWikiInactive, texturePDAButtonWikiInactiveHover};
            case SHOP_ARMOR: return new ResourceLocation[] {textureShopButtonArmor, textureShopButtonArmorHover, textureShopButtonArmorActive};
            case SHOP_CASE: return new ResourceLocation[] {textureShopButtonCase, textureShopButtonCaseHover, textureShopButtonCaseActive};
            case SHOP_EXPERIENCE: return new ResourceLocation[] {textureShopButtonExperience, textureShopButtonExperienceHover, textureShopButtonExperienceActive};
            case SHOP_KITS: return new ResourceLocation[] {textureShopButtonKit, textureShopButtonKitHover, textureShopButtonKitActive};
            case SHOP_MAIN: return new ResourceLocation[] {textureShopButtonMain, textureShopButtonMainHover, textureShopButtonMainActive};
            case SHOP_MISC: return new ResourceLocation[] {textureShopButtonMisc, textureShopButtonMiscHover, textureShopButtonMiscActive};
            case SHOP_NEW: return new ResourceLocation[] {textureShopButtonNew, textureShopButtonNewHover, textureShopButtonNewActive};
            case SHOP_SALE: return new ResourceLocation[] {textureShopButtonSale, textureShopButtonSaleHover, textureShopButtonSaleActive};
            case SHOP_UNIQUE: return new ResourceLocation[] {textureShopButtonUnique, textureShopButtonUniqueHover, textureShopButtonUniqueActive};
            case SHOP_VISUAL: return new ResourceLocation[] {textureShopButtonVisual, textureShopButtonVisualHover, textureShopButtonVisualActive};
            case SHOP_WEAPONS: return new ResourceLocation[] {textureShopButtonWeapons, textureShopButtonWeaponsHover, textureShopButtonWeaponsActive};

            default: return new ResourceLocation[] {texturePDAButtonProfile};
        }
    }

    public static ResourceLocation getClassBackgroundTexture(CharacterType characterType) {
        switch (characterType) {
            case TANK: return texturePDAGroupClassTank;
            case ASSASSIN: return texturePDAGroupClassStormtrooper;
            default: return texturePDAGroupClassMedic;
        }
    }

    public static ResourceLocation getClassIcon(CharacterType characterType) {
        switch (characterType) {
            case TANK: return texturePDAGroupIconTank;
            case ASSASSIN: return texturePDAGroupIconStormtrooper;
            default: return texturePDAGroupIconMedic;
        }
    }

}
