package ru.krogenit.npc.quest;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import noppes.npcs.controllers.Quest;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.util.ColorUtils;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiQuestElement extends GuiButtonAdvanced {

    private final Quest quest;
    private final List<GuiButtonAnimated> buttons = new ArrayList<>();
    private final GuiNPCQuests guiNPCQuests;
    private float rewardElementsXOffset;

    public GuiQuestElement(int id, float x, float y, float width, float height, Quest quest, GuiNPCQuests guiNPCQuests) {
        super(id, x, y, width, height, quest.title);
        this.guiNPCQuests = guiNPCQuests;
        this.quest = quest;
        float buttonWidth = ScaleGui.get(171);
        float buttonHeight = ScaleGui.get(40);
        GuiButtonAnimated guiButtonAnimated = new GuiButtonAnimated(0, xPosition + ScaleGui.get(867), yPosition + ScaleGui.get(121), buttonWidth, buttonHeight, "ПРИНЯТЬ");
        guiButtonAnimated.setTexture(TextureRegister.textureTreeButtonAccept);
        guiButtonAnimated.setTextureHover(TextureRegister.textureTreeButtonHover);
        guiButtonAnimated.setMaskTexture(TextureRegister.textureTreeButtonMask);
        this.buttons.add(guiButtonAnimated);
        guiButtonAnimated = new GuiButtonAnimated(1, xPosition + ScaleGui.get(1086), yPosition + ScaleGui.get(121), buttonWidth, buttonHeight, "ПОДРОБНЕЕ");
        guiButtonAnimated.setTexture(TextureRegister.textureTreeButtonCancel);
        guiButtonAnimated.setTextureHover(TextureRegister.textureTreeButtonHover);
        guiButtonAnimated.setMaskTexture(TextureRegister.textureTreeButtonMask);
        this.buttons.add(guiButtonAnimated);
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        GL11.glDisable(GL11.GL_TEXTURE_2D);

        if(isHovered) {
            GL11.glShadeModel(GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0.0f);
            tessellator.addVertex(xPosition, yPosition + height,0f);
            tessellator.addVertex(xPosition + width, yPosition + height, 0);
            tessellator.setColorRGBA_F(65f / 255f, 166f / 255f, 42f / 255f, 0.2f);
            tessellator.addVertex(xPosition + width, yPosition, 0f);
            tessellator.addVertex(xPosition, yPosition, 0f);
            tessellator.draw();
            GL11.glShadeModel(GL_FLAT);
        }

        if(isHovered) GL11.glColor4f(1f, 1f, 1f, 1.0f);
        else GL11.glColor4f(0.4f, 0.4f, 0.4f, 1.0f);
        GL11.glBegin(GL11.GL_LINE_STRIP);
        GL11.glVertex2f(xPosition, yPosition);
        GL11.glVertex2f(xPosition + width, yPosition);
        GL11.glVertex2f(xPosition + width, yPosition + height);
        GL11.glVertex2f(xPosition, yPosition + height);
        GL11.glVertex2f(xPosition, yPosition);
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);

        float x = xPosition + ScaleGui.get(17);
        float y = yPosition + ScaleGui.get(96);
        float width = ScaleGui.get(1240);
        float height = ScaleGui.get(2);
        GuiDrawUtils.drawRect(TextureRegister.textureNPCDeviderLine, x, y, width, height);
        x = xPosition + ScaleGui.get(18);
        y = yPosition + ScaleGui.get(29);
        float fs = 60 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "УНИЧТОЖЕНИЕ БОССА НИКИТОСА", x, y, fs, 0xffffff);
        x = xPosition + ScaleGui.get(692);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "НАГРАДА", x, y, fs, 0xffffff);
        fs = 30 / 32f;
        x = xPosition + ScaleGui.get(18);
        y += ScaleGui.get(30);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "БОЕВАЯ МИССИЯ", x, y, fs, 0xffffff);
        String desc = "Война со стражами, довольно выгодна для сколачивания первичного капитала. Начинать её лучше неподалёку от места\u0003где можно продать нафармленное. Иметь скорострельный мультитул и с десяток свободных ячеек экзокостюма рекомендуется.\n" +
                "Не доводить дело до 4й волны стражей тоже, можете не пережить. Имеется баг с третьей волной стражей, нельзя уничтожать\n" +
                "стража с лазером гранатами, он странным образом остаётся в качестве наблюдателя и сбежать от него сложно, как рентг...";
        fs = 26 / 32f;
        y = yPosition + ScaleGui.get(126);
        GuiDrawUtils.drawSplittedStringNoScale(FontType.HelveticaNeueCyrLight, desc, x, y, fs, ScaleGui.get(790), -1, 0xffffff, EnumStringRenderType.DEFAULT);

        for(GuiButtonAnimated button : buttons) {
            button.drawButton(mc, mouseX, mouseY);
        }

        renderItem(mouseX, mouseY);
        renderMoney();
        renderExperiences();
    }

    private void renderExperiences() {
        float x = rewardElementsXOffset + xPosition + ScaleGui.get(867);
        float y = yPosition + ScaleGui.get(54);
        float iconWidth = ScaleGui.get(26);
        float iconHeight = ScaleGui.get(26);
        float textYOffset = ScaleGui.get(7);
        float textXOffset = ScaleGui.get(12);
        GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResCombat, x, y, iconWidth, iconHeight);
        String s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.BATTLE);
        float fs = 1.75f;
        x += iconWidth;
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x + textXOffset, y + textYOffset, fs, 0x972c38);
        x += FontType.Marske.getFontContainer().width(s) * ScaleGui.get(fs) + iconWidth;
        glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResResearch, x, y, iconWidth, iconHeight);
        x += iconWidth;
        s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.EXPLORE);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x + textXOffset, y + textYOffset, fs, 0x2f85aa);
        x += FontType.Marske.getFontContainer().width(s) * ScaleGui.get(fs) + iconWidth;
        glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawRect(TextureRegister.textureTreeIconResSurvive, x, y, iconWidth, iconHeight);
        x += iconWidth;
        s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.SURVIVE);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, s, x + textXOffset, y + textYOffset, fs, 0x468039);
    }

    private void renderMoney() {
        float creditWidth = ScaleGui.get(24);
        String platina = "120";
        String credits = "555";
        float fs = 54 / 32f;
        float slcredits = FontType.Marske.getFontContainer().width(credits) * ScaleGui.get(fs);
        float x = rewardElementsXOffset + xPosition + ScaleGui.get(868);
        float y = yPosition + ScaleGui.get(22);
        GuiDrawUtils.drawRect(TextureRegister.textureInvIconCredit, x, y, creditWidth, creditWidth);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, credits, x + creditWidth + ScaleGui.get(10), y + ScaleGui.get(5), fs, 0xDFDEDE);
        GuiDrawUtils.drawRect(TextureRegister.textureInvIconPlatina, x + creditWidth + ScaleGui.get(25) + slcredits, y, creditWidth, creditWidth);
        GuiDrawUtils.drawStringNoXYScale(FontType.Marske, platina, x + creditWidth + creditWidth + ScaleGui.get(35) + slcredits, y + ScaleGui.get(5), fs, 0xDFDEDE);
    }

    private void renderItem(int mouseX, int mouseY) {
        ItemStack itemStack = new ItemStack(Items.diamond);
        float x = xPosition + ScaleGui.get(867);
        float y = yPosition + ScaleGui.get(20);
        float slotSize = ScaleGui.get(60);
        boolean invHovered = mouseX >= x && mouseX < x + slotSize && mouseY >= y && mouseY < y + slotSize;
        if(itemStack != null) {
            GuiDrawUtils.drawRect(TextureRegister.textureInvSlotSpecBg, x, y, slotSize, slotSize, 1f,1f,1f,0.9f);
            Vector3f color = ColorUtils.getColorByRarity(itemStack.getItem());
            if(invHovered) GuiDrawUtils.drawRect(TextureRegister.textureInvSlotSpecGradient, x, y, slotSize, slotSize, color.x, color.y, color.z,1f);
            else GuiDrawUtils.drawRect(TextureRegister.textureInvSlotSpecGradient, x, y, slotSize, slotSize, color.x, color.y, color.z,0.5f);
            GuiDrawUtils.drawRect(TextureRegister.textureInvSlotSpecFrame, x, y, slotSize, slotSize);
            GuiDrawUtils.renderItem(itemStack, 1.0f, slotSize / 10f, slotSize / 10f, x, y, slotSize, slotSize, 0f);
            rewardElementsXOffset = slotSize + ScaleGui.get(15);
        }
    }

    @Override
    public void addYPosition(float y) {
        super.addYPosition(y);
        for(GuiButtonAnimated buttonAnimated : buttons) {
            buttonAnimated.addYPosition(y);
        }
    }

    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, AxisAlignedBB axisAlignedBB) {
        for(GuiButtonAnimated button : buttons) {
            if(axisAlignedBB.intersectsWith(AxisAlignedBB.getBoundingBox(button.xPosition, button.yPosition, 0,
                    button.xPosition + button.width, button.yPosition + button.height, 0))) {
                if (button.mousePressed(mc, mouseX, mouseY)) {
                    this.actionPerformed(button);
                }
            }
        }
        return super.mousePressed(mc, mouseX, mouseY);
    }

    private void actionPerformed(GuiButtonAnimated button) {
        if(button.id == 0) {
            guiNPCQuests.showConfirmPopup(this);
        } else if(button.id == 1) {
            guiNPCQuests.showInfo(this);
        }
    }

    public Quest getQuest() {
        return quest;
    }
}
