package ru.krogenit.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.krogenit.client.gui.item.ItemStat;
import ru.krogenit.client.gui.item.ItemStatSmall;
import ru.krogenit.client.gui.item.ItemTag;
import ru.krogenit.client.gui.item.WeaponStat;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GuiWeaponPopup {
    private String name;
    private String secondName;
    private ItemTag[] tags;
    private String description;
    private ItemStat[] itemStats;
    private WeaponStat[] weaponStats;
    private ItemStatSmall[] itemStatSmalls;
    private float height;
}
