package ru.krogenit.animator;

import ru.krogenit.client.gui.api.GuiTextFieldAdvanced;
import ru.krogenit.guns.render.Vector3fConfigurable;
import ru.xlv.customfont.FontType;

public class GuiTextFieldWithVector extends GuiTextFieldAdvanced {

    private final Vector3fConfigurable vector;
    private final int type;

    public GuiTextFieldWithVector(FontType fontType, float x, float y, float width, float height, float fontScale, Vector3fConfigurable vector, int type) {
        super(fontType, x, y, width, height, fontScale);
        this.vector = vector;
        this.type = type;
    }

    @Override
    public boolean textboxKeyTyped(char p_146201_1_, int p_146201_2_) {
        boolean value = super.textboxKeyTyped(p_146201_1_, p_146201_2_);
        try {
            switch (type) {
                case 0: vector.x = Float.parseFloat(getText()); break;
                case 1: vector.y = Float.parseFloat(getText()); break;
                case 2: vector.z = Float.parseFloat(getText()); break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return value;
    }
}
