package ru.krogenit;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.key.KeyAnimationGui;
import ru.krogenit.key.KeyOpenModify;
import ru.krogenit.key.KeyOpenPda;
import ru.krogenit.skilltree.key.KeyOpenSkillTree;

import java.util.ArrayList;
import java.util.List;

public class KeyBindingsInterface {
    private final List<AbstractKey> keys = new ArrayList<>();

    public KeyBindingsInterface init() {
        registerKey(new KeyOpenSkillTree(new KeyBinding("skilltree.key.open", Keyboard.KEY_H, "key.categories.skilltree")));
        registerKey(new KeyOpenModify(new KeyBinding("modify1.key.open", Keyboard.KEY_J, "key.categories.modify1")));
        registerKey(new KeyOpenPda(new KeyBinding("pda.key.open", Keyboard.KEY_TAB, "key.categories.pda")));
        registerKey(new KeyAnimationGui(new KeyBinding("guns.key.animation", Keyboard.KEY_NUMPAD9, "key.categories.guns").setOnlyForCreative(true)));
        return this;
    }

    private void registerKey(AbstractKey key) {
        ClientRegistry.registerKeyBinding(key.getKeyBinding());
        this.keys.add(key);
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(InputEvent.KeyInputEvent event) {
        for(AbstractKey key : keys) {
            KeyBinding bind = key.getKeyBinding();
            if (bind.isPressed()) {
                key.keyDown();
            } else if (!Keyboard.isKeyDown(key.getKeyBinding().getKeyCode())) {
                key.keyUp();
            }
        }
    }
}
