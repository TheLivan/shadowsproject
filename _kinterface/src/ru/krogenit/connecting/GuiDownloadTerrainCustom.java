package ru.krogenit.connecting;

import net.minecraft.client.gui.GuiDownloadTerrain;
import net.minecraft.client.network.NetHandlerPlayClient;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.Utils;
import ru.xlv.core.gui.GuiScreenDelegate;

public class GuiDownloadTerrainCustom extends GuiDownloadTerrain {
    public GuiDownloadTerrainCustom(NetHandlerPlayClient p_i45023_1_) {
        super(p_i45023_1_);
    }

    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        Utils.bindTexture(GuiScreenDelegate.background, false);
        GuiDrawUtils.drawRect(0, 0, ScaleGui.screenWidth, ScaleGui.screenHeight, 1f, 1f, 1f, 1f);
    }
}
