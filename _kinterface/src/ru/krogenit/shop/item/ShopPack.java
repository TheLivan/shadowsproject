package ru.krogenit.shop.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;
import ru.krogenit.pda.EnumPdaLeftMenuSection;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public class ShopPack {
    private final String name;
    private final ShopItem[] shopItems;
    private final EnumPdaLeftMenuSection category;

    public ItemStack[] getItemStacks() {
        List<ItemStack> itemStacks = new ArrayList<>();
        for(ShopItem shopItem : shopItems) {
            itemStacks.add(shopItem.getItemStack());
        }

        return itemStacks.toArray(new ItemStack[0]);
    }
}
