package ru.krogenit.pda.gui.pages.wiki;

public enum EnumWikiSection {
    ALL("ВЫБЕРИТЕ РАЗДЕЛ"), LOCATIONS("ЛОКАЦИИ"), CREATURES("СУЩЕСТВА"), ARMOR("БРОНЯ"), WEAPON("ОРУЖИЕ"), MECHANICS("ИГРОВАЯ МЕХАНИКА");

    private String localized;

    EnumWikiSection(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
