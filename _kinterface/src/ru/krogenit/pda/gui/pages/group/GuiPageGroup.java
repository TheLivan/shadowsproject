package ru.krogenit.pda.gui.pages.group;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.gui.GuiPda;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.core.player.ClientPlayerManager;
import ru.xlv.group.common.Group;
import ru.xlv.group.network.PacketGroupChangeLeader;
import ru.xlv.group.network.PacketGroupKick;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.List;

public class GuiPageGroup extends AbstractGuiScreenAdvanced {

    private final GuiPda pda;
    private boolean isLeader;
    private final List<GuiSlotGroupMember> slots = new ArrayList<>();
    private final Group group;

    public GuiPageGroup(float minAspect, GuiPda pda, Group group) {
        super(minAspect);
        this.pda = pda;
        this.group = group;
        this.isLeader = group.getLeader().equals(mc.thePlayer.getDisplayName());
        createGui();
    }

    private void createGui() {
        buttonList.clear();

        GuiButtonAnimated b = new GuiButtonAnimated(0, 0,0,0,0, isLeader ? "РАСПУСТИТЬ ГРУППУ" : "ПОКИНУТЬ ГРУППУ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        for(String member : group.getPlayers()) {
            if(member.equals(mc.thePlayer.getCommandSenderName())) {
                ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
                addGuiSlotPlayer(member.equals(group.getLeader()), isLeader && !member.equals(mc.thePlayer.getDisplayName()), mc.thePlayer.getDisplayName(), clientMainPlayer.getCharacterType(), clientMainPlayer.getLvl());
            } else {
                ClientPlayer clientPlayer = ClientPlayerManager.INSTANCE.getPlayerByName(member);
                if(clientPlayer != null) {
                   addGuiSlotPlayer(member.equals(group.getLeader()), isLeader && !member.equals(mc.thePlayer.getDisplayName()), member, clientPlayer.getCharacterType(), clientPlayer.getLevel());
                }
            }
        }
    }

    private void addGuiSlotPlayer(boolean isLeader, boolean groupManagement, String userName, CharacterType characterType, int lvl) {
        GuiSlotGroupMember s = new GuiSlotGroupMember(1, ScaleGui.getCenterX(1800),0,0,0, groupManagement, pda, this, userName, characterType, lvl, "Unknown location");
        s.setLeader(isLeader);
        s.setGradientColor(new Vector3f(mc.theWorld.rand.nextFloat(), mc.theWorld.rand.nextFloat(), mc.theWorld.rand.nextFloat()));
        s.setAvatar(TextureRegister.texturePDAProfileAva);
        buttonList.add(s);
        slots.add(s);
    }


    @Override
    public void initGui() {
        super.initGui();

        float buttonWidth = 231;
        float buttonHeight = 40;
        float x = ScaleGui.getCenterX(1160, buttonWidth);
        float y = ScaleGui.getCenterY(883, buttonHeight);
        GuiButtonAnimated b = (GuiButtonAnimated) buttonList.get(0);
        b.setXPosition(x); b.setYPosition(y); b.setWidth(ScaleGui.get(buttonWidth)); b.setHeight(ScaleGui.get(buttonHeight));

        buttonWidth = 269;
        buttonHeight = 643;
        x = 746;
        y = 503;

        for (GuiSlotGroupMember s : slots) {
            s.xBase = ScaleGui.getCenterX(x, buttonWidth);
            s.yBase = s.yPosition = ScaleGui.getCenterY(y, buttonHeight);
            s.setWidth(ScaleGui.get(buttonWidth));
            s.setHeight(ScaleGui.get(buttonHeight));

            x += 7 + buttonWidth;
        }
    }


    private void updateSlots() {
        GuiButtonAnimated b = (GuiButtonAnimated) buttonList.get(0);
        b.displayString = isLeader ? "РАСПУСТИТЬ ГРУППУ" : "ПОКИНУТЬ ГРУППУ";

        for (GuiSlotGroupMember s : slots) {
            String playerName = s.getPlayerName();
            s.updateManagementButtons(isLeader && !playerName.equals(mc.thePlayer.getDisplayName()));
            s.setLeader(playerName.equals(group.getLeader()));
        }
    }

    public void removePlayerFromGroup(GuiSlotGroupMember member) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupKick(member.getPlayerName())).thenAcceptSync(s -> {
            //TODO: обработка респонс меседжа
            member.setRemoving();
        });
    }

    public void setGroupLeader(GuiSlotGroupMember member) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupChangeLeader(member.getPlayerName())).thenAcceptSync(s -> {
            //TODO: обработка респонс меседжа
            isLeader = false;
            member.setLeader(false);
            for(GuiSlotGroupMember member1 : slots) {
                if(member1.getPlayerName().equals(member.getPlayerName())) {
                    member1.setLeader(true);
                    break;
                }
            }
            updateSlots();
        });
    }

    private void removeSlot(GuiSlotGroupMember slot) {
        slots.remove(slot);
        buttonList.remove(slot);

        float buttonWidth = 269;
        float buttonHeight = 643;
        float x = 746;
        float y = 503;

        for (GuiSlotGroupMember s : slots) {
            s.xBase = ScaleGui.getCenterX(x, buttonWidth);
            s.yBase = ScaleGui.getCenterY(y, buttonHeight);
            x += 7 + buttonWidth;
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0) {
            if(isLeader) {
                for(GuiSlotGroupMember slot : slots) {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupKick(slot.getPlayerName())).thenAcceptSync(s -> {

                    });
                }

                pda.dissolveGroup();
            } else {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupKick(mc.thePlayer.getDisplayName())).thenAcceptSync(s -> {
                    pda.leaveFromGroup();
                });
            }
        }
    }


    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        float prevAnim = 0f;
        GuiSlotGroupMember toRemove = null;
        for(GuiSlotGroupMember slot : slots) {
            if(prevAnim <= ScaleGui.get(150f)) {
                slot.updateAnimation();
                prevAnim = Math.abs(slot.xBase - slot.xPosition);
            }

            if(slot.isRemoving() && slot.yPosition - ScaleGui.get(100f) < slot.yBase) {
                toRemove = slot;
            }
        }

        if(toRemove != null) {
            removeSlot(toRemove);
        }

        drawButtons(mouseX, mouseY, partialTick);
    }

    public GuiPda getPda() {
        return pda;
    }
}
