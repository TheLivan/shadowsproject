package ru.krogenit.pda.gui.pages.group;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.network.PacketFriendInviteRequest;
import ru.xlv.friend.network.PacketFriendRemove;

import java.util.ArrayList;
import java.util.List;

public class GuiSlotGroupMember extends GuiButtonAdvanced {

    private final List<GuiButtonAnimated> buttons = new ArrayList<>();
    private final String playerName, playerClass, playerLocation;
    private final int playerLvl;
    private boolean isLeader;
    private ResourceLocation avatar;
    private final ResourceLocation classIcon;
    private Vector3f gradientColor;
    private final GuiPageGroup group;
    private final GuiPda pda;
    public float xBase, yBase;
    private boolean isRemoving;

    public GuiSlotGroupMember(int id, float x, float y, float width, float height, boolean groupManagement, GuiPda pda, GuiPageGroup group, String playerName, CharacterType characterType, int level, String playerLocation) {
        super(id, x, y, width, height, "");
        this.pda = pda;
        this.group = group;
        this.xBase = x;
        this.yBase = y;
        this.playerName = playerName;
        this.playerLocation = playerLocation;
        this.playerClass = characterType.getDisplayName().toUpperCase();
        this.texture = TextureRegister.getClassBackgroundTexture(characterType);
        this.classIcon = TextureRegister.getClassIcon(characterType);
        this.playerLvl = level;
        createButtons(groupManagement);
    }

    private void createButtons(boolean groupManagement) {
        buttons.clear();

        if(!playerName.equals(mc.thePlayer.getDisplayName())) {
            GuiButtonAnimated b = new GuiButtonAnimated(0, 0, 0, 0, 0, "МЕСТОПОЛОЖЕНИЕ");
            b.setTexture(TextureRegister.textureESCButton);
            b.setTextureHover(TextureRegister.textureESCButtonHover);
            b.setMaskTexture(TextureRegister.textureESCButtonMask);
            this.buttons.add(b);
            b = new GuiButtonAnimated(1, 0, 0, 0, 0, "НАПИСАТЬ");
            b.setTexture(TextureRegister.textureESCButton);
            b.setTextureHover(TextureRegister.textureESCButtonHover);
            b.setMaskTexture(TextureRegister.textureESCButtonMask);
            this.buttons.add(b);
            b = new GuiButtonAnimated(2, 0, 0, 0, 0, XlvsFriendMod.INSTANCE.getFriendHandler().isFriend(playerName) ? "ИЗ ДРУЗЕЙ" : "В ДРУЗЬЯ");
            b.setTexture(TextureRegister.textureESCButton);
            b.setTextureHover(TextureRegister.textureESCButtonHover);
            b.setMaskTexture(TextureRegister.textureESCButtonMask);
            this.buttons.add(b);
            b = new GuiButtonAnimated(3, 0, 0, 0, 0, "ПРОФИЛЬ");
            b.setTexture(TextureRegister.textureESCButton);
            b.setTextureHover(TextureRegister.textureESCButtonHover);
            b.setMaskTexture(TextureRegister.textureESCButtonMask);
            this.buttons.add(b);

            if (groupManagement) {
                b = new GuiButtonAnimated(4, 0, 0, 0, 0, "СДЕЛАТЬ ЛИДЕРОМ");
                b.setTexture(TextureRegister.textureESCButton);
                b.setTextureHover(TextureRegister.textureESCButtonHover);
                b.setMaskTexture(TextureRegister.textureESCButtonMask);
                this.buttons.add(b);
                b = new GuiButtonAnimated(5, 0, 0, 0, 0, "ВЫГНАТЬ ИЗ ГРУППЫ");
                b.setTexture(TextureRegister.textureESCButton);
                b.setTextureHover(TextureRegister.textureESCButtonHover);
                b.setMaskTexture(TextureRegister.textureESCButtonMask);
                this.buttons.add(b);
            }
        }
    }

    public void updateManagementButtons(boolean groupManagement) {
        if (groupManagement) {
            if (buttons.size() < 6) {
                GuiButtonAnimated b = new GuiButtonAnimated(4, 0, 0, 0, 0, "СДЕЛАТЬ ЛИДЕРОМ");
                b.setTexture(TextureRegister.textureESCButton);
                b.setTextureHover(TextureRegister.textureESCButtonHover);
                b.setMaskTexture(TextureRegister.textureESCButtonMask);
                this.buttons.add(b);
                b = new GuiButtonAnimated(5, 0, 0, 0, 0, "ВЫГНАТЬ ИЗ ГРУППЫ");
                b.setTexture(TextureRegister.textureESCButton);
                b.setTextureHover(TextureRegister.textureESCButtonHover);
                b.setMaskTexture(TextureRegister.textureESCButtonMask);
                this.buttons.add(b);
            }
        } else {
            if (buttons.size() > 4) {
                this.buttons.remove(5);
                this.buttons.remove(4);
            }
        }
    }

    private void updateButtonsPositions(float x, float y) {
        float buttonWidth = width / (269 / 230f);
        float buttonHeight = height / (643 / 39f);
        float yOffset = height / (643 / 16f);
        x = x + width / 2f - buttonWidth / 2f;
        y = y + height - height / (643 / 40f) - buttonHeight / 2f;
        for (GuiButtonAnimated b : buttons) {
            b.setXPosition(x);
            b.setYPosition(y);
            b.setWidth(buttonWidth);
            b.setHeight(buttonHeight);
            y -= yOffset + buttonHeight;
        }
    }

    private void actionPerformed(int id) {
        switch (id) {
            case 0:
                pda.showPlayerLocation(playerName);
                break;
            case 1:
                pda.writeMessageToPlayer(playerName);
                break;
            case 2:
                if(XlvsFriendMod.INSTANCE.getFriendHandler().isFriend(playerName)) {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendRemove(playerName)).thenAcceptSync(s -> {
                        //TODO: обработка респонс меседжа;
                        pda.setPopup(new GuiPopup(pda, "ИГРОК УСПЕШНО УДАЛЕН ИЗ ДРУЗЕЙ", s, GuiPopup.green));
                    });
                } else {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendInviteRequest(playerName)).thenAcceptSync(s -> {
                        if(s.isSuccess()) {
                            pda.setPopup(new GuiPopup(pda, "ЗАЯВКА УСПЕШНО ОТПРАВЛЕНА", s.getResponseMessage(), GuiPopup.green));
                        } else {
                            pda.setPopup(new GuiPopup(pda, "ПРОИЗОШЛА ОШИБКА", s.getResponseMessage(), GuiPopup.red));
                        }
                        //TODO: обработка респонс меседжа;
                    });
                }
                break;
            case 3:
                pda.showProfile(playerName);
                break;
            case 4:
                group.setGroupLeader(this);
                break;
            case 5:
                group.removePlayerFromGroup(this);
                break;
        }
    }

    public void setRemoving() {
        isRemoving = true;
        yBase -= height * 1.25f;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            GuiDrawUtils.drawRect(texture, xPosition, yPosition, width, height, 1f, 1f, 1f, 1f);
            boolean isHovered = isHovered(mouseX, mouseY);

            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0f);
            tessellator.addVertex(xPosition, yPosition + height, 0f);
            tessellator.addVertex(xPosition + width, yPosition + height, 0);
            if (isHovered) tessellator.setColorRGBA_F(gradientColor.x, gradientColor.y, gradientColor.z, 0.4f);
            else tessellator.setColorRGBA_F(gradientColor.x, gradientColor.y, gradientColor.z, 0.2f);
            tessellator.addVertex(xPosition + width, yPosition, 0f);
            tessellator.addVertex(xPosition, yPosition, 0f);
            tessellator.draw();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_FLAT);

            float iconSize = width / (269 / 59f);
            float offset = width / (643 / 20f);
            float x = xPosition + offset;
            float y = yPosition + offset;
            GuiDrawUtils.drawRect(avatar, x, y, iconSize, iconSize, 1f, 1f, 1f, 1f);
            y += height / (643 / 9f) + iconSize;
            GuiDrawUtils.drawRect(classIcon, x, y, iconSize, iconSize, 1f, 1f, 1f, 1f);
            if (isLeader) {
                y += height / (643 / 9f) + iconSize;
                GuiDrawUtils.drawRect(TextureRegister.texturePDAGroupIconLeader, x, y, iconSize, iconSize, 1f, 1f, 1f, 1f);
            }

            drawText();

            if (isHovered) {
                for (GuiButtonAnimated b : buttons) {
                    b.drawButton(mc, mouseX, mouseY);
                }
            }
        }
    }

    @Override
    protected void drawText() {
        float textScale = 38 / 32f;
        float xOff = width / (269 / 78f);
        float yOff = height / (643 / 15f);
        float x = xPosition + xOff;
        float y = yPosition + yOff;

        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, playerName, x, y, textScale, 0xffffff);
        y += ScaleGui.get(23f);
        textScale = 24 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, playerClass, x, y, textScale, 0xffffff);
        y += ScaleGui.get(12f);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "LVL " + playerLvl, x, y, textScale, 0xffffff);
        y += ScaleGui.get(12f);
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, playerLocation, x, y, textScale, 0xffffff);
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        if (this.enabled && this.visible && isHovered(mouseX, mouseY)) {
            for (GuiButtonAnimated b : buttons) {
                if (b.mousePressed(mc, mouseX, mouseY)) {
                    actionPerformed(b.id);
                    return true;
                }
            }
        }

        return false;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setLeader(boolean leader) {
        this.isLeader = leader;
    }

    public void setGradientColor(Vector3f gradientColor) {
        this.gradientColor = gradientColor;
    }

    public void setAvatar(ResourceLocation avatar) {
        this.avatar = avatar;
    }

    public void updateAnimation() {
        if (xPosition < xBase) {
            xPosition = AnimationHelper.updateSlowEndAnim(xPosition, xBase, 0.1f, 0.0001f);
        } else {
            xPosition = AnimationHelper.updateSlowEndAnim(xPosition, xBase, -0.1f, -0.0001f);
        }

        if (isRemoving) {
            yPosition = AnimationHelper.updateSlowEndAnim(yPosition, yBase, -0.1f, -0.0001f);
        }

        updateButtonsPositions(xPosition, yPosition);
    }

    public boolean isRemoving() {
        return isRemoving;
    }
}