package ru.krogenit.pda.gui.pages.community;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Setter
@Getter
public class GuiPageCommunity extends AbstractGuiScreenAdvanced {

    private float firstPoint, secondPoint, thirdPoint, fs1, fs2, fs3;

    private String location;
    private GuiPda pda;

    public GuiPageCommunity(float minAspect, GuiPda pda) {
        super(minAspect);
        setLocation("ГУСТОЙ ЛЕС");
        this.pda = pda;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 231;
        float buttonHeight = 40;
        float x = ScaleGui.getCenterX(869, buttonWidth);
        float y = ScaleGui.getCenterY(888, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ДРУЗЬЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1158, buttonWidth);
        b = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЗАЯВКИ В ДРУЗЬЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1446, buttonWidth);
        b = new GuiButtonAnimated(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИГРОКИ СЕРВЕРА");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showFriends();
        } else if(b.id == 3) {
            pda.showIncomingFriendRequests();
        } else if(b.id == 4) {
            pda.showServerPlayers();
        }
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if(fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if(secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                    if (fs2 > 0.9f) {
                        thirdPoint = AnimationHelper.updateSlowEndAnim(thirdPoint, 1f, 0.1f, 0.0001f);
                        if(thirdPoint > 0.9f) {
                            fs3 = AnimationHelper.updateSlowEndAnim(fs3, 1f, 0.1f, 0.0001f);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDACommServerImage, 1160, 331, 1096, 299 * firstPoint);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 502, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 640, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 738, 1096 * fs1, 2);

        float x = 1665;
        float y = 236;
        float fs = 72 / 32f;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, "NEW ERA", x, y, fs * fs1, 0xffffff);
        y+=44;
        fs = 18 / 32f;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrBlack, "V 1.0204 BETA", x, y, fs * fs1, 0xffffff);
        y+=60;
        fs = 48 / 32f;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium, "BUILD", x, y, fs * secondPoint, 0xffffff);
        y+=32;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium, "CRAFT", x, y, fs * fs2, 0xffffff);
        y+=32;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium, "EXPLORE", x, y, fs * thirdPoint, 0xffffff);
        y+=32;
        GuiDrawUtils.drawRightStringCenter(FontType.HelveticaNeueCyrMedium, "CONQUEST", x, y, fs * fs3, 0xffffff);

        x = 612;
        y = 547;
        fs = 27.5f / 32f;
        String s = "В салоне из HDD-магнитолы раздавался «Electrostatic» группы Astral Projection. Макс слушал электронную музыку все время – дома, " +
                "на работе, в разъездах. И настолько уже к ней привык, что не мог даже думать нормально без чего-нибудь трансового в фоне.\n\n" +
                "В Москве было одно место, где Макс особенно любил бывать. В ночном клубе «Satisfaction», 24 часа в сутки царил полумрак, играла " +
                "музыка ведущих электронщиков планеты, а народ колбасился, забывая обо всем. После вечеринок в клубе ломило все тело, и нередко болела голова, " +
                "но Макс возвращался туда снова и снова. Принимал дозу легкого наркотика, выходил в самый центр и сливался с музыкой. В этот момент для него не существовало ничего – ни работы, ни людей, ни компьютеров.";
        try {
            GuiDrawUtils.drawSplittedStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fs * fs1, ScaleGui.get(1100),-1, 0xffffff, EnumStringRenderType.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }


        x = 629;
        y = 699;
        float iconWidth = 37;
        float stringMinusYOffset = 14;
        float stringPositiveYOffset = 10;
        float stringXOffset = 27;
        fs = 1.2f * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        s = "СЕРВЕР: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ONLINE", x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ПИНГ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, (pda.getPing() + " МС"), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        x += 240;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        fs = 1.2f * fs2;
        LocalDateTime time = LocalDateTime.now();
        String data = DateTimeFormatter.ofPattern("dd.MM.yyyy").format(time);
        s = "ДАТА: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, data, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs , y - stringMinusYOffset, fs, 0x1BC3EC);
        String timeString = DateTimeFormatter.ofPattern("HH:mm:ss").format(time);
        s = "ВРЕМЯ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, timeString, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        x += 240;
        fs = 1.2f * fs3;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * thirdPoint, iconWidth * thirdPoint);
        s = "ЛОКАЦИЯ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + location, x + stringXOffset  + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ИГРОКОВ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + pda.getPlayersCount(), x + stringXOffset  + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        drawButtons(mouseX, mouseY, partialTick);
    }
}
