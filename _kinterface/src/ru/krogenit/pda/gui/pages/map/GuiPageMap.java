package ru.krogenit.pda.gui.pages.map;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.glScissor;

@Getter
@Setter
public class GuiPageMap extends AbstractGuiScreenAdvanced {

    private final float MAX_SCALE = 5.0f, MIN_SCALE = 0.025f;

    private float firstPoint, secondPoint, fs1, fs2;
    private GuiPda pda;
    private String location;
    private int localX, localZ;
    private List<GuiMapPin> pins;
    private float scale, scaleAnim;
    private Vector2f mouseLastClick = new Vector2f(0, 0);
    private Vector2f mapPos = new Vector2f(0, 0);
    private Vector2f mapPosAnim = new Vector2f(0, 0);
    private List<EnumPinType> filters;
    private AxisAlignedBB scissorAABB;
    private boolean movingByMouse;

    public GuiPageMap(float minAspect, GuiPda pda) {
        super(minAspect);
        this.pda = pda;
        this.pins = new ArrayList<>();
        this.scale = 1.0f;
        this.filters = new ArrayList<>();
        this.mapPos.x = (float) -mc.thePlayer.posX;
        this.mapPos.y = (float) -mc.thePlayer.posZ;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 160;
        float buttonHeight = 39;
        float x = 1450;
        float y = 200;
        GuiButtonAnimated b = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЗАДАЧИ");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);
        x += 179;
        b = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth),  ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "КАРТА МИРА");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);

        buttonWidth = 38;
        buttonHeight = 38;
        x = 630;
        y = 889;
        float xOffset = 48;
        GuiButtonMapFilter t = new GuiButtonMapFilter(2, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        t.setTexture(TextureRegister.texturePDAMapFilterQuestion);
        t.setTextureHover(TextureRegister.texturePDAMapFilterQuestionActive);
        buttonList.add(t);
        x += xOffset;
        t = new GuiButtonMapFilter(3, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        t.setTexture(TextureRegister.texturePDAMapFilterMoney);
        t.setTextureHover(TextureRegister.texturePDAMapFilterMoneyActive);
        buttonList.add(t);
        x += xOffset;
        t = new GuiButtonMapFilter(4, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        t.setTexture(TextureRegister.texturePDAMapFilterAmmo);
        t.setTextureHover(TextureRegister.texturePDAMapFilterAmmoActive);
        buttonList.add(t);
        x += xOffset;
        t = new GuiButtonMapFilter(5, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight));
        t.setTexture(TextureRegister.texturePDAMapFilterKill);
        t.setTextureHover(TextureRegister.texturePDAMapFilterKillActive);
        buttonList.add(t);

        x = 1595;
        buttonWidth = 38;
        buttonHeight = 38;
        GuiButtonAdvanced b1 = new GuiButtonAdvanced(6, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        b1.setTexture(TextureRegister.texturePDAMapButtonScaleMinus);
        b1.setTextureHover(TextureRegister.texturePDAMapButtonScaleMinusHover);
        buttonList.add(b1);
        x+=xOffset;
        b1 = new GuiButtonAdvanced(7, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        b1.setTexture(TextureRegister.texturePDAMapButtonScalePlus);
        b1.setTextureHover(TextureRegister.texturePDAMapButtonScalePlusHover);
        buttonList.add(b1);
        x+=xOffset;
        b1 = new GuiButtonAdvanced(8, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "");
        b1.setTexture(TextureRegister.texturePDAMapButtonPlayer);
        b1.setTextureHover(TextureRegister.texturePDAMapButtonPlayerHover);
        buttonList.add(b1);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
//        if(b.id == 0) {
//
//        } else if(b.id == 1) {
//
//        } else
        if(b.id == 2) {
            GuiButtonMapFilter f = (GuiButtonMapFilter) b;
            if(f.isPressed()) filters.remove(EnumPinType.QUEST_KILL);
            else filters.add(EnumPinType.QUEST_KILL);
            f.setPressed(!f.isPressed());
        } else if(b.id == 3) {
            GuiButtonMapFilter f = (GuiButtonMapFilter) b;
            if(f.isPressed()) filters.remove(EnumPinType.NPC_TRADER);
            else filters.add(EnumPinType.NPC_TRADER);
            f.setPressed(!f.isPressed());
        } else if(b.id == 4) {
            GuiButtonMapFilter f = (GuiButtonMapFilter) b;
            if(f.isPressed()) filters.remove(EnumPinType.AMMO);
            else filters.add(EnumPinType.AMMO);
            f.setPressed(!f.isPressed());
        } else if(b.id == 5) {
            GuiButtonMapFilter f = (GuiButtonMapFilter) b;
            if(f.isPressed()) filters.remove(EnumPinType.PLAYER);
            else filters.add(EnumPinType.PLAYER);
            f.setPressed(!f.isPressed());
        } else if(b.id == 6) {
            this.scale -= 0.25f * scale;
            if(scale < MIN_SCALE) {
                scale = MIN_SCALE;
            }
        } else if(b.id == 7) {
            this.scale += 0.25f * scale;
            if(scale > MAX_SCALE) {
                scale = MAX_SCALE;
            }
        } else if(b.id == 8) {
            mapPos.x = (float) -mc.thePlayer.posX;
            mapPos.y = (float) -mc.thePlayer.posZ;
        }
    }

    protected void pinPressed(GuiMapPin pin) {

    }

    public void addPin(MapPin pin) {
        Vector2f position = pin.getPosition();
        GuiMapPin guiMapPin = null;
        float pinWidth = 39;
        float pinHeight = 61;
        float x = position.x;
        float y = position.y;
        switch (pin.getPinType()) {
            case NPC_TRADER:
                guiMapPin = new GuiMapPinNpc(x, y, pinWidth, pinHeight, (NpcMapPin) pin);
                break;
            case PLAYER:
                pinWidth = 25;
                pinHeight = 25;
                guiMapPin = new GuiMapPinPlayer(x, y, pinWidth, pinHeight, (PlayerMapPin) pin);
                break;
            case QUEST_KILL:
                guiMapPin = new GuiMapPinQuestKill(x, y, pinWidth, pinHeight, (QuestMapPin) pin);
                break;
        }

        pins.add(guiMapPin);
        pins.sort((o1, o2) -> Float.compare(o1.getPin().getPosition().y, o2.getPin().getPosition().y));
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
            if(fs1 > 0.9f) {
                secondPoint = AnimationHelper.updateSlowEndAnim(secondPoint, 1f, 0.1f, 0.0001f);
                if(secondPoint > 0.9f) {
                    fs2 = AnimationHelper.updateSlowEndAnim(fs2, 1f, 0.1f, 0.0001f);
                }
            }
        }

        if(mapPosAnim.x < mapPos.x) {
            mapPosAnim.x = AnimationHelper.updateSlowEndAnim(mapPosAnim.x, mapPos.x, 0.1f, 0.001f);
        } else {
            mapPosAnim.x = AnimationHelper.updateSlowEndAnim(mapPosAnim.x, mapPos.x, -0.1f, -0.001f);
        }

        if(mapPosAnim.y < mapPos.y) {
            mapPosAnim.y = AnimationHelper.updateSlowEndAnim(mapPosAnim.y, mapPos.y, 0.1f, 0.001f);
        } else {
            mapPosAnim.y = AnimationHelper.updateSlowEndAnim(mapPosAnim.y, mapPos.y, -0.1f, -0.001f);
        }

        if(scaleAnim < scale) {
            scaleAnim = AnimationHelper.updateSlowEndAnim(scaleAnim, scale, 0.1f, 0.001f);
        } else {
            scaleAnim = AnimationHelper.updateSlowEndAnim(scaleAnim, scale, -0.1f, -0.001f);
        }
    }

    private void drawMap(int mouseX, int mouseY) {
        float centerX = 1159;
        float centerY = 544;
        float x = ScaleGui.getCenterX(611, 0);
        float y = ScaleGui.getCenterY(250, 0);
        float width = ScaleGui.getCenterX(1708, 0) - x;
        float height = ScaleGui.getCenterY(820, 0) - y;
        if(x < pda.scissorX) {
            x = pda.scissorX;
            width = pda.scissorWidth;
        }
        glScissor((int)x, (int)y,(int) width , (int)height);
        scissorAABB = AxisAlignedBB.getBoundingBox(pda.scissorX, this.height - y - height, -100, pda.scissorX + pda.scissorWidth, this.height - y,100);

        x = 1160;
        y = 545;
        float mapWidth = 2191 * 4f;
        float mapHeight = 1140 * 4f;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMap, centerX + mapPosAnim.x * scaleAnim, centerY + mapPosAnim.y * scaleAnim, mapWidth * scaleAnim, mapHeight * scaleAnim * firstPoint);

        for(GuiMapPin pin : pins) {
            if(pin.getPinType() == EnumPinType.PLAYER) {
                pin.xPosition = ScaleGui.getCenterX(centerX + (pin.getPin().getPosition().x + mapPosAnim.x) * scaleAnim, pin.width);
                pin.yPosition = ScaleGui.getCenterY(centerY + (pin.getPin().getPosition().y + mapPosAnim.y) * scaleAnim, pin.height);
            } else {
                pin.xPosition = ScaleGui.getCenterX(centerX + (pin.getPin().getPosition().x + mapPosAnim.x) * scaleAnim, pin.width);
                pin.yPosition = ScaleGui.getCenterY(centerY + (pin.getPin().getPosition().y + mapPosAnim.y) * scaleAnim, pin.height * 2f);
            }

            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(pin.xPosition, pin.yPosition, 0, pin.xPosition + pin.width, pin.yPosition + pin.height, 0))) {
                if(filters.size() == 0 || filters.contains(pin.getPinType())) {
                    pin.drawButton(mouseX, mouseY);
                }
            }
        }

        for(GuiMapPin pin : pins) {
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(pin.xPosition, pin.yPosition, 0, pin.xPosition + pin.width, pin.yPosition + pin.height, 0))) {
                if(filters.size() == 0 || filters.contains(pin.getPinType())) {
                    pin.drawPopup(mouseX, mouseY);
                }
            }
        }

        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMapBorders, x, y, 2123/2f * fs1, 1072/2f * fs1);
        x = 1642;
        y = 325;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAMapCompass, x, y, 61 * secondPoint, 60 * secondPoint);

        float fs = 22 / 32f * fs2;
        x = 646;
        y = 298;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ЛОКАЛЬНЫЕ КООРДИНАТЫ", x, y, fs, 0xffffff);
        y+=19;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "X: " + MathHelper.floor_double((mouseX - ScaleGui.getCenterX(centerX, 0)) / scaleAnim - mapPosAnim.x), x, y, fs, 0xffffff);
        y+=12;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "Z: " + MathHelper.floor_double((mouseY - ScaleGui.getCenterY(centerY, 0)) / scaleAnim - mapPosAnim.y), x, y, fs, 0xffffff);
        y = 791;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "ТЕКУЩАЯ ЛОКАЦИЯ: " + location, x, y, fs, 0xffffff);

        glScissor(pda.scissorX, pda.scissorY, pda.scissorWidth, pda.scissorHeight);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f,1f);

        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 238, 1096 * fs1, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 850, 1096 * fs1, 2);

        float x = 630;
        float y = 201;
        float iconWidth = 37;
        float stringXOffset = 27;
        float stringMinusYOffset = 14;
        float stringPositiveYOffset = 10;
        float fs = 1.15f  * fs1;
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ТЕКУЩАЯ ЛОКАЦИЯ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x + stringXOffset, y - 2, fs, 0xffffff);
        float stringOffsetX = FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, location, x + stringXOffset + stringOffsetX, y - 2, fs, 0x1BC3EC);
        stringOffsetX += FontType.HelveticaNeueCyrLight.getFontContainer().width(location) * fs;
        if(x + stringOffsetX > 630)
            x += stringOffsetX + 60;
        else x += 460;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * secondPoint, iconWidth * secondPoint);
        fs = 1.15f  * fs2;
        s = "X: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + MathHelper.floor_double(mc.thePlayer.posX), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "Z: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + MathHelper.floor_double(mc.thePlayer.posZ), x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        drawMap(mouseX, mouseY);
        drawButtons(mouseX, mouseY, partialTick);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (mouseButton == 0) {
            for (GuiButton guibutton : this.buttonList) {
                if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                    this.selectedButton = guibutton;
                    guibutton.playClickSound(this.mc.getSoundHandler());
                    this.actionPerformed(guibutton);
                    return;
                }
            }

            for(GuiMapPin pin : pins) {
                if(filters.size() == 0 || filters.contains(pin.getPinType())) {
                    if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(pin.xPosition, pin.yPosition, 0, pin.xPosition + pin.width, pin.yPosition + pin.height, 0)) && pin.mousePressed(mc, mouseX, mouseY)) {
                        pin.playClickSound(mc.getSoundHandler());
                        this.pinPressed(pin);
                    }
                }
            }

            if(!movingByMouse) {
                movingByMouse = true;
                mouseLastClick.x = mouseX;
                mouseLastClick.y = mouseY;
            }
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        if(movingByMouse) {
            mapPos.x += (mouseX - mouseLastClick.x) / ScaleGui.scaleValue * ScaleGui.DEFAULT_HEIGHT / scaleAnim;
            mapPos.y += (mouseY - mouseLastClick.y) / ScaleGui.scaleValue * ScaleGui.DEFAULT_HEIGHT / scaleAnim;
            mouseLastClick.x = mouseX;
            mouseLastClick.y = mouseY;
        }
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        if(movingByMouse) {
            movingByMouse = false;
        }
    }

    @Override
    public void scrollInput(int mouseX, int mouseY, int d) {
        if (d != 0) {
            scale += d / 300f * ((scale + MIN_SCALE) / MAX_SCALE);
            if(scale > MAX_SCALE) {
                scale = MAX_SCALE;
            } else if(scale < MIN_SCALE) {
                scale = MIN_SCALE;
            }
        }
    }
}
