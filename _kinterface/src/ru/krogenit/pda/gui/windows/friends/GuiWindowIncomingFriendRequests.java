package ru.krogenit.pda.gui.windows.friends;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiPopup;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWinButtonColumn;
import ru.krogenit.pda.gui.windows.GuiWinButtonWithSubs;
import ru.krogenit.pda.gui.windows.players.GuiButtonPlayer;
import ru.krogenit.pda.gui.windows.players.GuiWindowPlayers;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.player.ClientPlayer;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.friend.common.FriendRelation;
import ru.xlv.friend.network.PacketFriendInviteRequest;
import ru.xlv.friend.network.PacketFriendPlayerListGet;
import ru.xlv.friend.network.PacketFriendRemove;

import java.util.List;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowIncomingFriendRequests extends GuiWindowPlayers {

    protected int incomingFriendRequests;
    protected int outgoingFriendRequests;

    public GuiWindowIncomingFriendRequests(GuiPda pda, float width, float height) {
        super(pda, width, height, false);
        setHeader("ВХОДЯЩИЕ ЗАЯВКИ");
        setLeftCornerDesc("FILE №81022012_CODE_REQ");
        setBackgroundTexture(TextureRegister.texturePDAWinAchieveBg);
        this.windowType = EnumWindowType.INCOMING_FRIEND_REQUESTS;
    }

    @Override
    public void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(458);
        float y = ScaleGui.get(71);
        GuiButtonAnimated b = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИСХОДЯЩИЕ");
        b.setTexture(TextureRegister.textureTreeButtonAccept);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);
    }

    @Override
    protected void initColumnButtons() {
        float x = ScaleGui.get(93);
        float y = ScaleGui.get(140);
        float buttonWidth = 283;
        float buttonHeight = 30;
        GuiWinButtonColumn b = new GuiWinButtonColumn(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "NICK", this);
        buttonList.add(b);
        buttonWidth = 37;
        x += ScaleGui.get(293);
        b = new GuiWinButtonColumn(5, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "CL", this);
        buttonList.add(b);
        x += ScaleGui.get(47);
        b = new GuiWinButtonColumn(6, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "LVL", this);
        buttonList.add(b);
    }

    @Override
    protected void onFriendsSynced() {
        super.onFriendsSynced();
        outgoingFriendRequests = friendHandler.getAllOutgoingInvites().size();
        incomingFriendRequests = friendHandler.getAllIncomingInvites().size();
    }

    @Override
    protected void getNewInfo(int offset) {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendPlayerListGet(PacketFriendPlayerListGet.FilterType.INCOMING_INVITE, offset)).thenAcceptSync(clientPlayers -> {
            if(clientPlayers.size() > 0) {
                for (ClientPlayer clientPlayer : clientPlayers) {
                    GuiButtonIncomingRequest request = (GuiButtonIncomingRequest) playerByName.get(clientPlayer.getPlayerName());
                    if (request != null) {
                        request.setCharacterType(clientPlayer.getCharacterType());
                        request.setLevel(clientPlayer.getLevel());
                    }
                }
            }
        });

        requestedElements += 20;
    }

    @Override
    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(996);
        float buttonHeight = ScaleGui.get(37);

        int id = 1;
        List<FriendRelation> allIncomingInvites = friendHandler.getAllIncomingInvites();
        for(FriendRelation friendRelation : allIncomingInvites) {
            String playerName = friendRelation.getInitiator();
            GuiButtonIncomingRequest s = new GuiButtonIncomingRequest(id, 0, 0, buttonWidth, buttonHeight, this, playerName, CharacterType.MEDIC, 0);
            players.add(s);
            playerByName.put(playerName, s);
            id++;
        }
    }

    @Override
    protected void updateValuesPosition() {
        float x = ScaleGui.get(44);
        float y = ScaleGui.get(171);
        float startY = y;
        float yOffset = ScaleGui.get(42);
        int id = 0;
        for (GuiButtonPlayer p : filteredResults) {
            p.id = id; p.setXPosition(x); p.setYPosition(y);
            y += yOffset;
            id++;
        }

        guiScroll.setScrollViewHeight(ScaleGui.get(678));
        guiScroll.setScrollTotalHeight(y - startY);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showOutgoingFriendRequests(); return;
        }
        super.actionPerformed(b);
    }

    @Override
    public void subAction(GuiWinButtonWithSubs button, int id) {
        GuiButtonIncomingRequest b = (GuiButtonIncomingRequest) button;
        if(id == 0) {
            //accept request
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendInviteRequest(b.getPlayerName())).thenAcceptSync(s -> {
                friendHandler.addFriend(b.getPlayerName());
                pda.setPopup(new GuiPopup(pda, "ИГРОК ДОБАВЛЕН В ДРУЗЬЯ", "", GuiPopup.green));
                incomingFriendRequests--;
                refreshValues();
            });
        } else if(id == 1) {
            //decline
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketFriendRemove(b.getPlayerName())).thenAcceptSync(s -> {
                friendHandler.removeRequest(b.getPlayerName());
                incomingFriendRequests--;
                refreshValues();
            });
        }
    }

    @Override
    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(128), ScaleGui.get(573), ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ВХОДЯЩИЕ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + incomingFriendRequests, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ИСХОДЯЩИЕ: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, outgoingFriendRequests + "", x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        fs = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "№", windowXAnim + ScaleGui.get(57), windowYAnim + ScaleGui.get(154.5f), fs, 0x666666);
    }

    @Override
    protected void drawScroll() {
        guiScroll.drawScroll(windowXAnim + ScaleGui.get(636),
                windowYAnim + ScaleGui.get(149),
                ScaleGui.get(644)  * secondPoint * collapseAnim,
                windowYAnim + ScaleGui.get(803));
    }

    protected void drawPlayers(int mouseX, int mouseY) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowXAnim);
        float height = ScaleGui.get(678);
        float height1 = ScaleGui.get(windowHeight - 14);
        scissorHeight = (int) (height * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight - ScaleGui.get(27)) + height1 * (1.0f - firstPoint) + height1 * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        scissorAABB = AxisAlignedBB.getBoundingBox(pda.scissorX, windowYAnim + ScaleGui.get(137), -100, scissorX + scissorWidth, windowYAnim + ScaleGui.get(848), 100);

        int id = 0;
        for(GuiButtonPlayer b : filteredResults) {
            b.addXPosition(windowXAnim);
            b.addYPosition(windowYAnim - guiScroll.getScrollAnim());
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(b.xPosition, b.yPosition, 0, b.xPosition + b.width, b.yPosition + b.height, 0))) {
                b.drawButton(mouseX, mouseY);
                if(id + 1 == requestedElements) {
                    getNewInfo(requestedElements);
                }
            }

            id++;
        }

        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        if(!pda.isWindowOpened(EnumWindowType.OUTGOING_FRIEND_REQUESTS))
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.COMMUNITY_REQUESTS, false);
    }
}
