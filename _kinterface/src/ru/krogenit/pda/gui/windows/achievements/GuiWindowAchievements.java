package ru.krogenit.pda.gui.windows.achievements;

import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.CorePda;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.pages.profile.GuiPageProfile;
import ru.krogenit.pda.gui.pages.profile.GuiSlotAchieveProfile;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWindow;
import ru.xlv.customfont.FontType;

import java.util.*;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowAchievements extends GuiWindow {

    private final List<EnumAchievementSection> achievementHeaders = new ArrayList<>();
    private final List<GuiSlotAchieve> achievements = new ArrayList<>();
    private final Map<EnumAchievementSection, Integer> achievedInSection = new HashMap<>();
    private float achieveOffsetY;
    private final GuiPageProfile profile;
    private final GuiSlotAchieveProfile slot;

    public GuiWindowAchievements(GuiPda pda, float width, float height, GuiPageProfile profile, GuiSlotAchieveProfile slot) {
        super(EnumWindowType.ACHIEVEMENTS, pda, width, height);
        setHeader("ДОСТИЖЕНИЯ ПЕРСОНАЖА");
        setLeftCornerDesc("FILE №89000142_CODE_ACH");
        setBackgroundTexture(TextureRegister.texturePDAWinAchieveBg);
        this.profile = profile;
        this.slot = slot;
    }

    @Override
    public void initGui() {
        super.initGui();
        achievements.clear();
        achievementHeaders.clear();
        achievedInSection.clear();

        float width = 183;
        float height = 211;
        float x = - halfWidth + ScaleGui.get(137, width);
        float startX = x;
        float maxX = halfWidth - ScaleGui.get(109);
        float y = - halfHeight + ScaleGui.get(201, height);
        float yStart = y;
        int i = 0;
        achieveOffsetY = ScaleGui.get(254);
        List<EnumAchievementSection> keys = new ArrayList<>(achievesStatic.keySet());
        keys.sort(Comparator.comparingInt(Enum::ordinal));

        for(EnumAchievementSection section : keys) {
            List<Achievement> al = achievesStatic.get(section);
            for(Achievement a : al) {
                GuiSlotAchieve s = new GuiSlotAchieve(a, x, y, ScaleGui.get(width), ScaleGui.get(height));
                achievements.add(s);

                if(i % 3 == 0) achievementHeaders.add(section);
                x += ScaleGui.get(193);

                if(x > maxX) {
                    x = startX;
                    y += achieveOffsetY;
                }

                i++;

                Integer achieved = achievedInSection.get(section);
                if(achieved == null) {
                    achieved = 0;
                }

                achieved += a.isAchieved() ? 1 : 0;
                achievedInSection.put(section, achieved);
            }
        }

        guiScroll.setScrollTotalHeight(y - yStart);
        guiScroll.setScrollViewHeight(ScaleGui.get(719));
    }

    private void selectAchieve(GuiSlotAchieve a) {
        if(profile != null && slot != null && a.getAchievement().isAchieved()) {
            profile.setAchievementToSlot(slot, a);
            pda.closeWindow(this);
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.PROFILE_ACHIEVEMENTS, false);
        }
    }

    @Override
    protected void closeWindow() {
        super.closeWindow();
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.PROFILE_ACHIEVEMENTS, false);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        float iconWidth = 7;
        float iconHeight = 4 * secondPoint * collapseAnim;
        guiScroll.drawScroll(windowCenterXAnim + scaledWidth / 2f - ScaleGui.get(47, iconWidth),
                windowCenterYAnim - scaledHeight / 2f + ScaleGui.get(73, iconHeight),
                ScaleGui.get(715)  * secondPoint * collapseAnim,
                windowCenterYAnim + scaledHeight / 2f - ScaleGui.get(79, iconHeight));

        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowCenterXAnim - halfWidth);
        scissorHeight = (int) (ScaleGui.get(745) * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight - ScaleGui.get(60)) + ScaleGui.get(745) * (1.0f - firstPoint) + ScaleGui.get(745) * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        scissorAABB = AxisAlignedBB.getBoundingBox(pda.scissorX, windowCenterYAnim + halfHeight - scissorY - scissorHeight, -100, scissorX + scissorWidth, windowCenterYAnim + halfHeight - scissorY, 100);
        for (GuiSlotAchieve achieve : achievements) {
            achieve.addXPosition(windowCenterXAnim);
            achieve.addYPosition(windowCenterYAnim - guiScroll.getScrollAnim());
            achieve.drawButton(mouseX, mouseY);
        }
        float x = windowCenterXAnim;
        float y = windowCenterYAnim - halfHeight + ScaleGui.get(79) - guiScroll.getScrollAnim();
        float fs = 30 / 32f;
        for (EnumAchievementSection s : achievementHeaders) {
            int achieved = achievedInSection.get(s);
            int achievesInSection = achievesStatic.get(s).size();

            GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, s.getName(), x, y, fs, 0xffffff);
            float offsetX = FontType.HelveticaNeueCyrLight.getFontContainer().width(s.getName()) * ScaleGui.get(fs) / 2f + ScaleGui.get(5);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "(" + achieved + "/" + achievesInSection + ")", x + offsetX, y, fs, 0x02A4C7);
            y += achieveOffsetY;
        }
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        if(mouseButton == 0) {
            for(GuiSlotAchieve a : achievements) {
                if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(a.xPosition, a.yPosition, 0, a.xPosition + a.width, a.yPosition + a.height, 0)) && a.mousePressed(mc, mouseX, mouseY)) {
                    selectAchieve(a);
                    return true;
                }
            }
        }

        return super.mouseClickedWindow(mouseX, mouseY, mouseButton);
    }

    static Map<EnumAchievementSection, List<Achievement>> achievesStatic = new HashMap<>();

    static {
        List<Achievement> al = new ArrayList<>();
        Achievement a = new Achievement(EnumAchievementType.BRONZE, "НЕ УМИРАТЬ 1 ДЕНЬ", true, 1, 1, true, new ResourceLocation(CorePda.MODID, "textures/achievements/radiation_bronze_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.SILVER, "НЕ УМИРАТЬ 5 ДНЕЙ", true, 2, 5, false, new ResourceLocation(CorePda.MODID, "textures/achievements/radiation_silver_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.GOLD, "НЕ УМИРАТЬ 25 ДНЕЙ", true, 2, 25, false, new ResourceLocation(CorePda.MODID, "textures/achievements/radiation_gold_icon.png"));
        al.add(a);
        achievesStatic.put(EnumAchievementSection.FIRST, al);
        al = new ArrayList<>();
        a = new Achievement(EnumAchievementType.BRONZE, "100 ЖИВОТНЫХ", true, 100, 100, true, new ResourceLocation(CorePda.MODID, "textures/achievements/knife_bronze_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.SILVER, "1 000 ЖИВОТНЫХ", true, 1000, 1000, true, new ResourceLocation(CorePda.MODID, "textures/achievements/knife_silver_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.GOLD, "5 000 ЖИВОТНЫХ", true, 2500, 5000, false, new ResourceLocation(CorePda.MODID, "textures/achievements/knife_golden_icon.png"));
        al.add(a);
        achievesStatic.put(EnumAchievementSection.SECOND, al);
        al = new ArrayList<>();
        a = new Achievement(EnumAchievementType.BRONZE, "1 000 РЕСУРСОВ", true, 100, 100, true, new ResourceLocation(CorePda.MODID, "textures/achievements/pickaxe_bronze_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.SILVER, "5 000 РЕСУРСОВ", true, 5000, 5000, true, new ResourceLocation(CorePda.MODID, "textures/achievements/pickaxe_silver_icon.png"));
        al.add(a);
        a = new Achievement(EnumAchievementType.GOLD, "10 000 РЕСУРСОВ", true, 10000, 10000, true, new ResourceLocation(CorePda.MODID, "textures/achievements/pickaxe_golden_icon.png"));
        al.add(a);
        achievesStatic.put(EnumAchievementSection.THIRD, al);
    }
}
