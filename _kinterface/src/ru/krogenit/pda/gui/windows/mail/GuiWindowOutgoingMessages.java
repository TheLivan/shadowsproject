package ru.krogenit.pda.gui.windows.mail;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.input.Keyboard;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiPopup;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWinButtonColumn;
import ru.krogenit.pda.gui.windows.GuiWinButtonWithSubs;
import ru.xlv.core.XlvsCore;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.PostHandler;
import ru.xlv.post.network.PacketPostDelete;
import ru.xlv.post.network.PacketPostSync;

public class GuiWindowOutgoingMessages extends GuiWindowIncomingMessages {

    private final PostHandler postHandler = XlvsPostMod.INSTANCE.getPostHandler();

    public GuiWindowOutgoingMessages(GuiPda pda) {
        super(pda);
        windowType = EnumWindowType.OUTGOING_MESSAGES;
        setHeader("ИСХОДЯЩИЕ ПИСЬМА");
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSync()).thenAcceptSync(result -> {
            if (result.isSuccess()) {
                postHandler.syncPost(result.getPostObjectList());
                refreshPoints();
            }
        });
    }

    protected void initColumnButtons() {
        float x = ScaleGui.get(93);
        float y = ScaleGui.get(220);
        float buttonWidth = 194 - 38;
        float buttonHeight = 30;
        GuiWinButtonColumn b = new GuiWinButtonColumn(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Получатель", this);
        buttonList.add(b);
        buttonWidth = 218;
        x += ScaleGui.get(204 - 38);
        b = new GuiWinButtonColumn(5, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Тема", this);
        buttonList.add(b);
        buttonWidth = 139;
        x += ScaleGui.get(228);
        b = new GuiWinButtonColumn(6, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "Дата", this);
        buttonList.add(b);
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(326);
        float y = ScaleGui.get(70);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАПИСАТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += ScaleGui.get(178);
        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВХОДЯЩИЕ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    protected void initPlayers() {
        float buttonWidth = ScaleGui.get(619);
        float buttonHeight = ScaleGui.get(37);

        int id = 1;
        for(PostObject post : postHandler.getOutgoingPost()) {
            GuiButtonMail mail = new GuiButtonMail(id, 0, 0, buttonWidth, buttonHeight, this,
                    post.getRecipient(), post.getTitle(), post.getCreationTimeMills(), post.getText(), post.getUuid());
            mails.add(mail);
            id++;
        }

        incoming = postHandler.getIncomingPost().size();
        outgoing = postHandler.getOutgoingPost().size();
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
            return;
        } else if(b.id == 3) {
            pda.showOrCreateWindow(EnumWindowType.INCOMING_MESSAGES, new GuiWindowIncomingMessages(pda));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_INBOX, true);
            return;
        }
        super.actionPerformed(b);
    }

    @Override
    public void subAction(GuiWinButtonWithSubs button, int id) {
        GuiButtonMail mail = (GuiButtonMail) button;
        if(id == -1) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_READ, new GuiWindowReadMessage(pda,mail.getTarget(), mail.getSubject(),mail.getDate(), mail.getText(), true));
        } else if(id == 0) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostDelete(mail.getUuid())).thenAcceptSync(result -> {
                if(result.isSuccess()) {
                    mails.remove(button);
                    filteredResults.remove(button);
                    updateValuesPosition();
                } else {
                    pda.setPopup(new GuiPopup(pda, "НЕ УДАЛОСЬ УДАЛИТЬ СООБЩЕНИЕ", result.getResponseMessage(), GuiPopup.red));
                }
            });
        }
    }

    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(128), ScaleGui.get(620), ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(208), ScaleGui.get(620), ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "ИСХОДЯЩИЕ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, "" + outgoing, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ВХОДЯШИЕ: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, incoming + "", x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);

        fs = 28 / 32f;
        GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "№", windowXAnim + ScaleGui.get(57), windowYAnim + ScaleGui.get(234.5f), fs, 0x666666);
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_OUTBOX, false);
    }

}
