package ru.krogenit.pda.gui.windows.mail;

import lombok.Getter;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.gui.windows.ButtonContent;
import ru.krogenit.pda.gui.windows.GuiWinButtonWithSubs;
import ru.krogenit.pda.gui.windows.IGuiWindowWithSubActions;
import ru.krogenit.util.TimeUtils;
import ru.xlv.customfont.FontType;
import ru.xlv.customfont.StringCache;

import java.util.UUID;

@Getter
public class GuiButtonMail extends GuiWinButtonWithSubs {

    private final String target, subject, text;
    private final long date;
    private final UUID uuid;

    public GuiButtonMail(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow, String target,
                         String subject, long date, String text, UUID uuid) {
        super(id, x, y, width, height, guiWindow);
        this.uuid = uuid;
        this.target = target;
        this.subject = subject;
        this.date = date;
        this.text = text;
        addContent(ButtonContent.newBuilder().setString("" + id).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        StringCache textFont = FontType.HelveticaNeueCyrLight.getFontContainer().getTextFont();
        String s = textFont.trimStringToWidth(target, ScaleGui.get(126) / ScaleGui.get(28 / 32f), false);
        if(!s.equals(target)) s += "...";
        addContent(ButtonContent.newBuilder().setString(s).setWidth(156).build());
        s = textFont.trimStringToWidth(subject, ScaleGui.get(188) / ScaleGui.get(28 / 32f), false);
        if(!s.equals(subject)) s += "...";
        addContent(ButtonContent.newBuilder().setString(s).setWidth(218).build());
        addContent(ButtonContent.newBuilder().setString(TimeUtils.formatTimeMills(date)).setWidth(138).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.texturePDAWinFriendsIconDecline).setId(0).setWidth(38).setTextureOffset(6).build());
    }
}
