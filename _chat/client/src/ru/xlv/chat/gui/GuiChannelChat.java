package ru.xlv.chat.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiChat;
import org.lwjgl.input.Mouse;
import ru.krogenit.client.gui.api.GuiTextFieldChat;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.chat.network.PacketChatMessage;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.gui.button.ButtonRect;

public class GuiChannelChat extends GuiChat {

    public static ChatChannelType chatType = ChatChannelType.LOCATION;

    public GuiChannelChat(String p_i1024_1_) {
        super(p_i1024_1_);
    }

    @Override
    public void func_146403_a(String p_146403_1_) {
        this.mc.ingameGUI.getChatGUI().addToSentMessages(p_146403_1_);
        if (net.minecraftforge.client.ClientCommandHandler.instance.executeCommand(mc.thePlayer, p_146403_1_) != 0) return;
        if (p_146403_1_.startsWith("/")) {
            this.mc.thePlayer.sendChatMessage(p_146403_1_);
            return;
        }
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketChatMessage(p_146403_1_, chatType.ordinal()));
    }

    @Override
    public void initGui() {
        super.initGui();
        boolean creative = mc.thePlayer.capabilities.isCreativeMode;

        if(!creative) {
            ScaleGui.update();
            float ratio = mc.displayWidth / (float) mc.displayHeight;
            float minAspect = 16 / 9f;
            float aspectDiff = (minAspect - ratio) * 450f;
            float inputWidth = 540f;
            float inputHeight = 33f;
            float fontScale = 1.5f;
            this.inputField = new GuiTextFieldChat(150 + (ratio < minAspect ? aspectDiff : 0), 746, inputWidth, inputHeight, fontScale);
            this.inputField.setMaxStringLength(100);
            this.inputField.setEnableBackgroundDrawing(false);
            this.inputField.setFocused(true);
            this.inputField.setText(this.defaultInputFieldText);
            this.inputField.setCanLoseFocus(false);
            //        this.inputField.setText("А/ЪЪ/ВАЪ/ВХАЪ/ХВ/АЪ/ЪП/АПХ/ЪАВЪ/АЪВАХ/ЪВАХЪ/ВХАЪ/ВХА/ЪЪВАХЪВАЪ/АВ");
        }

        updateButtons();
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        switchChatType(ChatChannelType.values()[-1 - guiButton.id]);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        if(!mc.thePlayer.capabilities.isCreativeMode) {
            mouseX = Mouse.getEventX();
            mouseY = mc.displayHeight - Mouse.getEventY() - 1;
            float x = 95f;
            float y = 476;
            float width = 125;
            float height = 33;
            for (int i = 0; i < ChatChannelType.values().length; i++) {
                if (mouseX >= ScaleGui.getCenterX(x) && mouseY >= ScaleGui.getCenterY(y) &&
                        mouseX <= ScaleGui.getCenterX(x) + ScaleGui.get(width) && mouseY < ScaleGui.getCenterY(y) + ScaleGui.get(height)) {
                    switchChatType(ChatChannelType.values()[i]);
                }
                x += width + 5;
                width -= 10;
            }
        }
    }

    private void updateButtons() {
        if(mc.thePlayer.capabilities.isCreativeMode) {
            buttonList.clear();
            for (int i = 0; i < ChatChannelType.values().length; i++) {
                ChatChannelType value = ChatChannelType.values()[i];
                buttonList.add(new ButtonRect(-1 - value.ordinal(), 4, height - 27, 107, 12, value.getDisplayString()));
            }
        }
    }

    private void switchChatType(ChatChannelType chatType) {
        GuiChannelChat.chatType = chatType;
    }
}
