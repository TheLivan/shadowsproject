package ru.xlv.chat.network;

import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.chat.XlvsChatMod;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public class PacketChatMessage implements IPacketInOnServer, IPacketOutServer {

    private String message;
    private int chatTypeOrdinal;

    public PacketChatMessage() {
    }

    public PacketChatMessage(String message, int chatTypeOrdinal) {
        this.message = message;
        this.chatTypeOrdinal = chatTypeOrdinal;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(chatTypeOrdinal);
        bbos.writeUTF(message);
    }

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int ordinal = bbis.readInt();
        if(ordinal >= 0 && ordinal < ChatChannelType.values().length) {
            ChatChannelType chatChannelType = ChatChannelType.values()[ordinal];
            String message = bbis.readUTF();
            XlvsChatMod.INSTANCE.getChatHandler().handleMessage(entityPlayer, message, chatChannelType);
        }
    }
}
