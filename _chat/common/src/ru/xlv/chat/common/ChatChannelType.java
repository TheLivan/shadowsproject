package ru.xlv.chat.common;

public enum ChatChannelType {

    LOCATION("Общий"),
    AROUND("Локальный"),
    TRADE("Торговый"),
    FRIEND("Друзья"),
    GROUP("Клан");

    private static final String TEMPLATE = "[%s] %s: %s";

    private final String displayString;

    ChatChannelType(String displayString) {
        this.displayString = displayString;
    }

    public String getDisplayString() {
        return displayString;
    }

    public String formatMessage(String username, String message) {
        return String.format(TEMPLATE, displayString, username, message);
    }
}
