package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlayerAttributesGet implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(2000L);

    private ServerPlayer serverPlayer;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if (REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
            serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(bbis.readUTF());
            packetCallbackSender.send();
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        if(serverPlayer != null) {
            bbos.writeInt(CharacterAttributeType.values().length);
            for (CharacterAttributeType characterAttributeType : CharacterAttributeType.values()) {
                bbos.writeInt(characterAttributeType.ordinal());
                bbos.writeDouble(serverPlayer.getSelectedCharacter().getAttributeCompleteValue(characterAttributeType));
                bbos.writeDouble(serverPlayer.getSelectedCharacter().getAttribute(characterAttributeType).getMaxValue());
            }
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
