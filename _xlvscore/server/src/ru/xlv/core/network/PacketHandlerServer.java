package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.*;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.flex.FlexPlayerList;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PacketHandlerServer extends PacketHandler {

//    private static final RequestController<EntityPlayer> MAIN_REQUEST_CONTROLLER = new RequestController.Limited<>(10);

    private final Map<Class<? extends IPacket>, PacketData> packetMap = new HashMap<>();

    public PacketHandlerServer(PacketRegistry packetRegistry) {
        super(packetRegistry);
        for (Class<? extends IPacket> aClass : packetRegistry.getClassRegistry().keySet()) {
            PacketData packetData = new PacketData();
            {
                AsyncPacket annotation = aClass.getAnnotation(AsyncPacket.class);
                if (annotation != null) {
                    packetData.isAsync = true;
                }
            }
            {
                ControllablePacket annotation = aClass.getAnnotation(ControllablePacket.class);
                if (annotation != null) {
                    packetData.callWriteAnyway = annotation.callWriteAnyway();
                    packetData.requestLimit = annotation.limit();
                    packetData.requestPeriod = annotation.period();
                }
            }
            if(packetData.requestPeriod != -1) {
                packetData.requestController = new RequestController.Periodic<>(packetData.requestPeriod);
            } else if(packetData.requestLimit != -1) {
                packetData.requestController = new RequestController.Limited<>(packetData.requestLimit);
            }
            packetMap.put(aClass, packetData);
        }
    }

    @Override
    protected void onServerPacketReceived(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
//        if(!MAIN_REQUEST_CONTROLLER.tryRequest(entityPlayer)) {
//            logger.warning("The player " + entityPlayer.getCommandSenderName() + " sent too many packets on the moment.");
//            return;
//        }
        int pid = bbis.readInt();
        IPacket packet = getPacketById(pid);
        if (packet != null) {
            PacketData packetData = packetMap.get(packet.getClass());
            if(packetData != null && packetData.isAsync) {
                handlePacket(packet, packetData, pid, entityPlayer, bbis);
            } else {
                XlvsCore.INSTANCE.runUsingMainThread(() -> handlePacket(packet, packetData, pid, entityPlayer, bbis));
            }
        }
    }

    private void handlePacket(IPacket packet, PacketData packetData, int pid, EntityPlayer entityPlayer, ByteBufInputStream byteBufInputStream) {
        if(packetData != null) {
            RequestController<EntityPlayer> requestController = packetData.requestController;
            if (requestController != null && !requestController.tryRequest(entityPlayer)) {
                return;
            }
        }
        try {
            if (packet instanceof IPacketCallback) {
                processPacketCallbackOnServer((IPacketCallback) packet, pid, packetData != null && packetData.callWriteAnyway, entityPlayer, byteBufInputStream);
            } else if (packet instanceof IPacketIn) {
                ((IPacketIn) packet).read(byteBufInputStream);
            } else if (packet instanceof IPacketInOnServer) {
                ((IPacketInOnServer) packet).read(entityPlayer, byteBufInputStream);
            }
        } catch (Exception throwable) {
            logger.warning("An error has occurred during executing a packet " + pid + "#" + packet + " .Sender: " + entityPlayer.getCommandSenderName());
            throwable.printStackTrace();
        }
    }

    private void processPacketCallbackOnServer(IPacketCallback packet, int pid, boolean callWriteOnly, EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        int callbackId = bbis.readInt();
        if(!callWriteOnly) {
            if (packet instanceof IPacketCallbackOnServer) {
                ((IPacketCallbackOnServer) packet).read(entityPlayer, bbis, () -> {
                    if (((IPacketCallbackOnServer) packet).handleCallback()) {
                        try {
                            sendPacketCallbackToClient(packet, pid, callbackId, entityPlayer);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                packet.read(bbis);
            }
        }
        if(!(packet instanceof IPacketCallbackOnServer) || !((IPacketCallbackOnServer) packet).handleCallback()) {
            sendPacketCallbackToClient(packet, pid, callbackId, entityPlayer);
        }
    }

    private void sendPacketCallbackToClient(IPacketCallback packet, int pid, int callbackId, EntityPlayer entityPlayer) throws IOException {
        ByteBufOutputStream bbos = new ByteBufOutputStream(Unpooled.buffer());
        bbos.writeInt(pid);
        bbos.writeInt(callbackId);
        if(packet instanceof IPacketCallbackOnServer) {
            ((IPacketCallbackOnServer) packet).write(entityPlayer, bbos);
        } else {
            packet.write(bbos);
        }
        sendPacketToPlayer(entityPlayer, bbos);
    }

    public void sendPacketToAll(@Nonnull IPacketOutServer packetOut) {
        XlvsCore.INSTANCE.getPlayerManager().getOnlinePlayers().forEach(serverPlayer -> sendPacketToPlayer(serverPlayer.getEntityPlayer(), packetOut));
    }

    public void sendPacketToAllExcept(@Nonnull EntityPlayer entityPlayer, @Nonnull IPacketOutServer packetOut) {
        XlvsCore.INSTANCE.getPlayerManager().getPlayerList()
                .stream()
                .filter(ServerPlayer::isOnline)
                .filter(serverPlayer1 -> serverPlayer1.getEntityPlayer() != entityPlayer)
                .forEach(serverPlayer1 -> sendPacketToPlayer(serverPlayer1.getEntityPlayer(), packetOut));
    }

    public void sendPacketToAllExcept(@Nonnull ServerPlayer serverPlayer, @Nonnull IPacketOutServer packet) {
        FlexPlayerList.of(XlvsCore.INSTANCE.getPlayerManager().getPlayerList())
                .filter(ServerPlayer::isOnline)
                .filter(serverPlayer1 -> serverPlayer1 != serverPlayer)
                .sendPacket(packet);
    }

    public void sendPacketToAllAroundExcept(@Nonnull Entity entity, double radius, @Nonnull IPacketOutServer packet) {
        FlexPlayerList.of(XlvsCore.INSTANCE.getPlayerManager().getPlayerList())
                .filter(ServerPlayer::isOnline)
                .filter(serverPlayer -> serverPlayer.getEntityPlayer() != null)
                .filter(serverPlayer -> serverPlayer.getEntityPlayer() != entity)
                .filter(serverPlayer -> serverPlayer.getEntityPlayer().getDistanceToEntity(entity) < radius)
                .sendPacket(packet);
    }

    public void sendPacketToAllAround(double x, double y, double z, double radius, @Nonnull IPacketOutServer packetOut) {
        XlvsCore.INSTANCE.getPlayerManager().getPlayersAround(x, y, z, radius).forEach(serverPlayer -> sendPacketToPlayer(serverPlayer.getEntityPlayer(), packetOut));
    }

    public void sendPacketToAllAround(@Nonnull Entity entity, double radius, @Nonnull IPacketOutServer packetOut) {
        XlvsCore.INSTANCE.getPlayerManager().getPlayersAround(entity.posX, entity.posY, entity.posZ, radius).forEach(serverPlayer -> sendPacketToPlayer(serverPlayer.getEntityPlayer(), packetOut));
    }

    private static class PacketData {
        private RequestController<EntityPlayer> requestController;
        private boolean isAsync;
        private long requestPeriod;
        private int requestLimit;
        private boolean callWriteAnyway;
    }
}
