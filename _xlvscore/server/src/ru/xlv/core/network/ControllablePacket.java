package ru.xlv.core.network;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ControllablePacket {
    long period() default -1;
    int limit() default -1;
    boolean callWriteAnyway() default false;
}
