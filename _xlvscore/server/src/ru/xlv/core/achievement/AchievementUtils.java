package ru.xlv.core.achievement;

import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.util.List;

public class AchievementUtils {

    public static void writeAchievement(Achievement achievement, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(achievement.getAchievementType().getName());
        byteBufOutputStream.writeUTF(achievement.getAchievementType().getDescription());
        byteBufOutputStream.writeUTF(achievement.getAchievementType().getImageURL());
        if(achievement instanceof AchievementBase) {
            byteBufOutputStream.writeInt(1);
            byteBufOutputStream.writeInt(achievement.isAchieved() ? 1 : 0);
        } else if(achievement instanceof AchievementProgressive) {
            byteBufOutputStream.writeInt(((AchievementProgressive) achievement).getMaxProgress());
            byteBufOutputStream.writeInt(((AchievementProgressive) achievement).getProgress());
        }
    }

    public static void writeAchievementList(List<Achievement> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (Achievement achievement : list) {
            writeAchievement(achievement, byteBufOutputStream);
        }
    }
}
