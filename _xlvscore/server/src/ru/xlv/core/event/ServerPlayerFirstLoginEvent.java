package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.Character;

@Getter
@RequiredArgsConstructor
public class ServerPlayerFirstLoginEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final Character character;
}
