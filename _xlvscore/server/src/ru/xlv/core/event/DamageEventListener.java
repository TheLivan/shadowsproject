package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.command.CommandHandler;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.item.IAttributeProvider;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeCategory;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.DamageSources;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.DamageSourceAttr;
import ru.xlv.core.util.DamageUtils;
import ru.xlv.mochar.player.character.Character;

public class DamageEventListener {

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void event(LivingHurtEvent event) {
        ServerPlayer attackerPlayer = null, targetPlayer = null;
        if(event.source.getSourceOfDamage() instanceof EntityPlayer) {
            attackerPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) event.source.getSourceOfDamage());
        }
        if(event.entityLiving instanceof EntityPlayer) {
            targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) event.entityLiving);
        }
        CommandHandler.handleDebugDamageMessage("start", attackerPlayer, targetPlayer, event.source, event.ammount, new TObjectDoubleHashMap<>());
        //events
        if(!(event.source instanceof DamageSourceAttr) || !((DamageSourceAttr) event.source).ignoreEvents()) {
            if (targetPlayer != null) {
                EntityDamagePlayerEvent.Pre event1 = new EntityDamagePlayerEvent.Pre(targetPlayer, event.entityLiving, event.ammount);
                if (XlvsCoreCommon.EVENT_BUS.post(event1)) {
                    event.setCanceled(true);
                    return;
                }
                event.ammount = event1.getAmount();
            }
            if (attackerPlayer != null) {
                PlayerDamageEntityEvent.Pre event1 = new PlayerDamageEntityEvent.Pre(attackerPlayer, event.entityLiving, event.ammount);
                if (XlvsCoreCommon.EVENT_BUS.post(event1)) {
                    event.setCanceled(true);
                    return;
                }
                event.ammount = event1.getAmount();
                if (targetPlayer != null) {
                    PlayerDamagePlayerEvent.Pre event2 = new PlayerDamagePlayerEvent.Pre(attackerPlayer, targetPlayer, event.source, event.ammount);
                    if (XlvsCoreCommon.EVENT_BUS.post(event2)) {
                        event.setCanceled(true);
                        return;
                    }
                    event.ammount = event2.getAmount();
                }
            }
        }
        float damage = event.ammount;
        TObjectDoubleMap<CharacterAttributeType> damageMap = new TObjectDoubleHashMap<>();
        TObjectDoubleMap<CharacterAttributeType> damageMap1 = DamageUtils.getAttrDamageMap(event.source);
        if(damageMap1 != null) {
            damageMap.putAll(damageMap1);
        }
        CommandHandler.handleDebugDamageMessage("pre", attackerPlayer, targetPlayer, event.source, damage, damageMap);
        double penetration = CharacterAttributeType.PENETRATION.getDefaultValue();
        if(attackerPlayer != null) {
            EntityPlayer entityPlayer = (EntityPlayer) event.source.getSourceOfDamage();
            Character character = attackerPlayer.getSelectedCharacter();
            if (character != null) {
                //fill damage map
                for (CharacterAttribute characterAttribute : attackerPlayer.getSelectedCharacter().getCharacterAttributes()) {
                    if(characterAttribute.getType().getCategory() == CharacterAttributeCategory.DAMAGE) {
                        double attributeCompleteValue = attackerPlayer.getSelectedCharacter().getAttributeCompleteValue(characterAttribute.getType());
                        damageMap.adjustOrPutValue(characterAttribute.getType(), attributeCompleteValue, attributeCompleteValue);
                    }
                }

                if(entityPlayer.getHeldItem() != null) {
                    Item curItem = entityPlayer.getHeldItem().getItem();
                    if(curItem instanceof ItemSword) {
                        damageMap.adjustOrPutValue(CharacterAttributeType.CUT_DAMAGE, damage, damage);
                    } else if(curItem instanceof IAttributeProvider) {
                        if (((IAttributeProvider) curItem).getCharacterAttributeBoostMap() != null) {
                            for (CharacterAttributeType characterAttributeType : ((IAttributeProvider) curItem).getCharacterAttributeBoostMap().keySet()) {
                                double value = ((IAttributeProvider) curItem).getCharacterAttributeValue(entityPlayer.getHeldItem(), characterAttributeType);
//                                    double value = ((IAttributeProvider) curItem).getModCharacterAttributeMap().get(characterAttributeType);
                                if(characterAttributeType.getCategory() == CharacterAttributeCategory.DAMAGE) {
                                    damageMap.adjustOrPutValue(characterAttributeType, value, value);
                                }
                            }
                            CharacterAttributeMod characterAttributeMod = ((IAttributeProvider) curItem).getCharacterAttributeBoostMap().get(CharacterAttributeType.PENETRATION);
                            if (characterAttributeMod != null) {
                                penetration = characterAttributeMod.getValueAdd();
                            }
                        }
                    }
                }
            }
        } else if(event.source == DamageSource.inFire || event.source == DamageSource.onFire) {
            damageMap.adjustOrPutValue(CharacterAttributeType.FIRE_DAMAGE, damage, damage);
        } else if(event.source == DamageSources.RADIATION) {
            damageMap.adjustOrPutValue(CharacterAttributeType.RADIATION_DAMAGE, damage, damage);
        }
        damageMap.remove(CharacterAttributeType.PENETRATION);
        damageMap.remove(CharacterAttributeType.DAMAGE_MOD);
        //mapping recalc damage by protection
        CommandHandler.handleDebugDamageMessage("middle", attackerPlayer, targetPlayer, event.source, damage, damageMap);
        if(targetPlayer != null) {
            double baseDamageProtection = targetPlayer.getSelectedCharacter().getAttributeCompleteValueWithArmor(CharacterAttributeType.DAMAGE_BASE_PROTECTION);
//            System.out.println("baseProtection=" + baseDamageProtection);
            for (CharacterAttributeType attributeType : damageMap.keySet()) {
                double value = damageMap.get(attributeType);
                value = value - (value * (baseDamageProtection - 1) / 100);
                if(attributeType.getOppositeAttribute() != null && value != 0) {
                    double protection = targetPlayer.getSelectedCharacter().getAttributeCompleteValueWithArmor(attributeType.getOppositeAttribute());
//                    System.out.println(String.join(",", attributeType.name() + "=" + value, "protection=" + protection, "penetration=" + penetration));
                    value = value - (value * protection) / (100 * penetration);
//                    System.out.println(attributeType.name() + "=>" + value);
                    damageMap.put(attributeType, value);
                }
            }
            if(damageMap.isEmpty()) {
                damage = (float) (damage - (damage * baseDamageProtection / 100));
            }
        }
        for (Double d : damageMap.values()) {
            damage += d;
        }
        //events
        event.ammount = damage;
        CommandHandler.handleDebugDamageMessage("post", attackerPlayer, targetPlayer, event.source, damage, damageMap);
        if(!(event.source instanceof DamageSourceAttr) || !((DamageSourceAttr) event.source).ignoreEvents()) {
            if (targetPlayer != null) {
                EntityDamagePlayerEvent.Post event1 = new EntityDamagePlayerEvent.Post(targetPlayer, event.entityLiving, event.ammount);
                if (XlvsCoreCommon.EVENT_BUS.post(event1)) {
                    event.setCanceled(true);
                    return;
                }
                event.ammount = event1.getAmount();
            }
            if (attackerPlayer != null) {
                PlayerDamageEntityEvent.Post event1 = new PlayerDamageEntityEvent.Post(attackerPlayer, event.entityLiving, event.ammount);
                if (XlvsCoreCommon.EVENT_BUS.post(event1)) {
                    event.setCanceled(true);
                    return;
                }
                event.ammount = event1.getAmount();
                if (targetPlayer != null) {
                    PlayerDamagePlayerEvent.Post event2 = new PlayerDamagePlayerEvent.Post(attackerPlayer, targetPlayer, event.source, event.ammount);
                    if (XlvsCoreCommon.EVENT_BUS.post(event2)) {
                        event.setCanceled(true);
                        return;
                    }
                    event.ammount = event2.getAmount();
                }
//            System.out.println(damage);
            }
        }
        CommandHandler.handleDebugDamageMessage("finish", attackerPlayer, targetPlayer, event.source, event.ammount, damageMap);
        if (damage <= 0) {
            event.setCanceled(true);
        }
    }
}
