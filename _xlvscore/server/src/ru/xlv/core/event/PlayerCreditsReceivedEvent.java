package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.player.ServerPlayer;

@Getter
@RequiredArgsConstructor
public class PlayerCreditsReceivedEvent extends Event {

    private final ServerPlayer serverPlayer;
    private final int amount;
}
