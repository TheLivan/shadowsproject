package ru.xlv.core.player;

import lombok.extern.java.Log;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.network.PacketSysTimeGet;
import ru.xlv.core.util.ScheduledConsumableTask;

import java.util.HashMap;
import java.util.Map;

@Log
public class PlayerSysTimeTracker implements IPlayerTracker {

    private static final long SEND_PERIOD = 5000L;
    private static final long ALLOWED_DIFF_VALUE = 5000L;

    private static final Map<String, Long> map = new HashMap<>();

    private static final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask = new ScheduledConsumableTask<>(SEND_PERIOD,
            serverPlayer -> XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketSysTimeGet()));

    @Override
    public void onUpdate(ServerPlayer serverPlayer) {
        if(serverPlayer.isOnline()) {
            scheduledConsumableTask.update(serverPlayer);
        }
    }

    public static void setSySTime(String username, long time) {
        synchronized (map) {
            Long aLong = map.get(username);
            if (aLong != null) {
                long l = aLong - time - SEND_PERIOD;
                if (l >= ALLOWED_DIFF_VALUE) {
                    log.warning("The player " + username + " is suspected of using a speed hacks! " + l);
                }
            }
            map.put(username, time);
        }
    }
}
