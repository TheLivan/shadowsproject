package ru.xlv.core.database;

public interface IDatabaseEventListener<T> {
    void onInsert(T object);
    void onDelete(T object);
    void onUpdate(T object);
}
