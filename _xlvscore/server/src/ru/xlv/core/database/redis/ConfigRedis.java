package ru.xlv.core.database.redis;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

public class ConfigRedis implements IConfigGson {

    @Getter
    @Configurable
    private String host, password;
    @Getter
    @Configurable
    private int port;

    private final String filePath;

    public ConfigRedis(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public File getConfigFile() {
        return new File(filePath);
    }
}
