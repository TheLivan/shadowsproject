package ru.xlv.core.database.multiserver;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.database.DatabaseManager;

@RequiredArgsConstructor
public class MultiServer {

    private final DatabaseManager databaseManager;

    public void sendPlayerTravelMetadata(String username, PlayerTravelPacket playerTravelPacket) {

    }

    public PlayerTravelPacket getPlayerTravelMetadata(String username) {
        return null;
    }
}
