package ru.xlv.core.database;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Getter
@Configurable
public class PlayerDefaultNBT implements IConfigGson {

    private final String nbtTagCompound = "{Attributes:[0:{Base:100.0d,Name:\"generic.maxHealth\",},1:{Base:1.0d,Name:\"generic.knockbackResistance\",},2:{Base:0.10000000149011612d,Name:\"generic.movementSpeed\",},3:{Base:1.0d,Name:\"generic.attackDamage\",},],PortalCooldown:0,AbsorptionAmount:0.0f,abilities:{invulnerable:0b,mayfly:0b,instabuild:0b,walkSpeed:0.1f,mayBuild:1b,flying:0b,flySpeed:0.05f,},FallDistance:0.0f,DeathTime:0s,HealF:100.0f,XpTotal:0,Motion:[0:0.0d,1:0.0d,2:0.0d,],Health:100s,foodSaturationLevel:6.0f,Air:300s,OnGround:1b,Dimension:0,Rotation:[0:85.73021f,1:8.045286f,],XpLevel:1,Score:34,Pos:[0:-7.0d,1:46.0d,2:-37.0d,],XpP:0.0f,EnderItems:[],foodLevel:20,foodExhaustionLevel:0.0f,HurtTime:0s,SelectedItemSlot:0,AttackTime:0s,Inventory:[],foodTickTimer:0,}";

    private final File configFile = new File("config/xlvscore/player_default_nbt.json");
}
