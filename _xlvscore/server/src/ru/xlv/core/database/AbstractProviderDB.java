package ru.xlv.core.database;

import ru.xlv.core.common.util.config.IConfigGson;

public abstract class AbstractProviderDB<T extends IConfigGson> {

    protected T config;

    public AbstractProviderDB(T config) {
        this.config = config;
        config.load();
    }

    public abstract void init();

    public abstract void shutdown();
}
