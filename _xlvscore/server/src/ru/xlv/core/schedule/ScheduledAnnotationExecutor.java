package ru.xlv.core.schedule;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.util.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ScheduledAnnotationExecutor {

    @RequiredArgsConstructor
    private static class ScheduleTargetMethod {
        private final Object owner;
        private final Method method;
        private final long period;
        private long lastCallTimeMills;
    }

    private final Logger logger = new Logger(getClass().getSimpleName());
    private final List<ScheduleTargetMethod> syncList = new ArrayList<>();
    private final List<ScheduleTargetMethod> asyncList = new ArrayList<>();

    private Thread thread;

    public void init() {
        Reflections reflections = new Reflections(new MethodAnnotationsScanner());
        reflections.getMethodsAnnotatedWith(Scheduled.class).forEach(this::register);
        if (!syncList.isEmpty()) {
            CommonUtils.registerFMLEvents(this);
        }
        if(!asyncList.isEmpty()) {
            thread = new Thread(() -> {
                while (true) {
                    asyncList.forEach(scheduleTargetMethod -> {
                        if(System.currentTimeMillis() - scheduleTargetMethod.lastCallTimeMills >= scheduleTargetMethod.period) {
                            try {
                                scheduleTargetMethod.method.invoke(null);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                            scheduleTargetMethod.lastCallTimeMills = System.currentTimeMillis();
                        }
                    });
                }
            });
            thread.start();
        }
    }

    public void shutdown() {
        if (thread != null) {
            thread.interrupt();
        }
    }

    private void register(Method method) {
        register(null, method);
    }

    private void register(Object owner, Method method) {
        Scheduled scheduled = method.getAnnotation(Scheduled.class);
        List<ScheduleTargetMethod> list = null;
        switch (scheduled.type()) {
            case SYNC:
                list = syncList;
                break;
            case ASYNC:
                list = asyncList;
        }
        list.removeIf(scheduleTargetMethod -> scheduleTargetMethod.method == method && owner == scheduleTargetMethod.owner);
        list.add(new ScheduleTargetMethod(owner, method, scheduled.period()));
        logger.info("Registered a new @Scheduled method " + method + " with type " + scheduled.type().name());
    }

    @SneakyThrows
    @SubscribeEvent
    public void event(TickEvent.ServerTickEvent event) {
        if(event.phase == TickEvent.Phase.START) {
            for (ScheduleTargetMethod scheduleTargetMethod : syncList) {
                if(System.currentTimeMillis() - scheduleTargetMethod.lastCallTimeMills >= scheduleTargetMethod.period) {
                    scheduleTargetMethod.method.invoke(null);
                    scheduleTargetMethod.lastCallTimeMills = System.currentTimeMillis();
                }
            }
        }
    }

    public void register(Object object) {
        Class<?> clazz = object.getClass();
        while(clazz != null && clazz != Object.class) {
            for (Method method : clazz.getDeclaredMethods()) {
                Scheduled annotation = method.getAnnotation(Scheduled.class);
                if(annotation != null) {
                    register(object, method);
                }
            }
            clazz = clazz.getSuperclass();
        }
    }
}
