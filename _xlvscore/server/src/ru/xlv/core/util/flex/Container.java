package ru.xlv.core.util.flex;

import java.util.function.*;
import java.util.logging.Logger;

public abstract class Container<T> {

    protected static final Logger LOGGER = Logger.getLogger("Container");

    protected final T object;

    protected boolean chain = true;

    protected Container(T object) {
        this.object = object;
    }

    public boolean isObjectNull() {
        return object == null;
    }

    public boolean isCompletedSuccessfully() {
        return chain;
    }

    protected Container<T> isNull() {
        if(chain) {
            chain = isObjectNull();
        }
        return this;
    }

    protected Container<T> notNull() {
        if(chain) {
            chain = !isObjectNull();
        }
        return this;
    }

    protected Container<T> log(String message, Object... params) {
        LOGGER.info(getFormattedMessage(message, params));
        return this;
    }

    protected Container<T> log(Function<T, String> function) {
        if(chain) {
            LOGGER.info(function.apply(object));
        }
        return this;
    }

    protected Container<T> test(Predicate<T> predicate) {
        if(chain) {
            chain = predicate.test(object);
        }
        return this;
    }

    protected Container<T> accept(Consumer<T> consumer) {
        if(chain) {
            consumer.accept(object);
        }
        return this;
    }

    protected Container<T> orAccept(Consumer<T> consumer) {
        if(!chain) {
            consumer.accept(object);
        }
        return this;
    }

    protected <V extends Container<R>, R> V map(Function<T, V> function) {
        return function.apply(object);
    }

    protected String getFormattedMessage(String message, Object... params) {
        for (int i = 0; i < params.length; i++) {
            Object o = params[i];
            message = message.replace("{" + i + "}", String.valueOf(o));
        }
        return message;
    }

    protected abstract <R> BiContainer<?, ?, R> apply(Function<T, R> function);

    protected abstract Container<T> createContainer(T t);

    protected abstract <A> BiContainer<? extends Container<T>, T, A> createBiContainer(A a);

    public static class BiContainer<C extends Container<T>, T, V> extends Container<C> {

        protected final V param;

        public BiContainer(C c, V v, boolean chain) {
            super(c);
            this.chain = chain;
            this.param = v;
        }

        @Override
        protected BiContainer<C, T, V> isNull() {
            if (chain) {
                chain = param == null;
            }
            return this;
        }

        @Override
        protected BiContainer<C, T, V> notNull() {
            if (chain) {
                chain = param != null;
            }
            return this;
        }

        @Override
        protected BiContainer<C, T, V> log(String message, Object... params) {
            return (BiContainer<C, T, V>) super.log(message, params);
        }

        @Override
        protected BiContainer<C, T, V> log(Function<C, String> function) {
            return (BiContainer<C, T, V>) super.log(function);
        }

        protected BiContainer<C, T, V> log(BiFunction<C, V, String> function) {
            if(chain) {
                LOGGER.info(function.apply(object, param));
            }
            return this;
        }

        @Override
        protected BiContainer<C, T, V> test(Predicate<C> predicate) {
            return (BiContainer<C, T, V>) super.test(predicate);
        }

        protected BiContainer<C, T, V> test(BiPredicate<C, V> predicate) {
            if(chain) {
                chain = predicate.test(object, param);
            }
            return this;
        }

        protected BiContainer<C, T, V> accept(BiConsumer<C, V> consumer) {
            if(chain) {
                consumer.accept(object, param);
            }
            return this;
        }

        protected BiContainer<C, T, V> acceptBi(BiConsumer<T, V> consumer) {
            if(chain) {
                consumer.accept(object.object, param);
            }
            return this;
        }

        protected <R> BiContainer<C, T, R> applyBi(Function<T, R> function) {
            return new BiContainer<>(object, function.apply(object.object), chain);
        }

        @Override
        protected <R> BiContainer<C, T, R> apply(Function<C, R> function) {
            return new BiContainer<>(object, function.apply(object), chain);
        }

        @Override
        protected Container<C> createContainer(C c) {
            return new BiContainer<>(c, null, chain);
        }

        @Override
        protected <A> BiContainer<Container<C>, C, A> createBiContainer(A a) {
            return new BiContainer<>(createContainer(object), a, chain);
        }
    }
}
