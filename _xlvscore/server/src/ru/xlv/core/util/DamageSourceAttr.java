package ru.xlv.core.util;

import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import net.minecraft.entity.Entity;
import net.minecraft.util.EntityDamageSource;
import ru.xlv.core.common.player.character.CharacterAttributeType;

@Getter
public class DamageSourceAttr extends EntityDamageSource {

    private final TObjectDoubleMap<CharacterAttributeType> damageMap = new TObjectDoubleHashMap<>();

    @Setter
    @Accessors(fluent = true)
    private boolean ignoreEvents;

    public DamageSourceAttr(Entity p_i1567_2_) {
        super("player_attr", p_i1567_2_);
    }

    public void addAttrDamage(CharacterAttributeType characterAttributeType, double value) {
        damageMap.adjustOrPutValue(characterAttributeType, value, value);
    }
}
