package ru.xlv.core.entity;

import lombok.Getter;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import ru.xlv.core.common.entity.EntityStaticCustomRenderer;
import ru.xlv.core.util.ScheduledConsumableTask;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EntityExecutableOnUpdate extends EntityStaticCustomRenderer {

    private static final Map<UUID, ScheduledConsumableTask<EntityExecutableOnUpdate>> UUID_CONSUMER_MAP = new HashMap<>();

    private final String UUID_KEY = "consumer_uuid";

    @Getter
    private String ownerPlayerName;
    private UUID consumerUUID;
    private ScheduledConsumableTask<EntityExecutableOnUpdate> scheduledTask;

    public EntityExecutableOnUpdate(World p_i1681_1_) {
        super(p_i1681_1_);
    }

    public EntityExecutableOnUpdate(World p_i1681_1_, String ownerPlayerName, ScheduledConsumableTask<EntityExecutableOnUpdate> scheduledTask) {
        super(p_i1681_1_);
        this.ownerPlayerName = ownerPlayerName;
        this.scheduledTask = scheduledTask;
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
        if(scheduledTask != null) {
            scheduledTask.update(this);
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound p_70037_1_) {
        super.readEntityFromNBT(p_70037_1_);
        if(p_70037_1_.hasKey(UUID_KEY)) {
            consumerUUID = UUID.fromString(p_70037_1_.getString(UUID_KEY));
            ScheduledConsumableTask<EntityExecutableOnUpdate> scheduledTask = UUID_CONSUMER_MAP.get(consumerUUID);
            if (scheduledTask == null) {
                setDead();
                return;
            }
            this.scheduledTask = scheduledTask;
        }
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound p_70014_1_) {
        super.writeEntityToNBT(p_70014_1_);
        p_70014_1_.setString(UUID_KEY, consumerUUID.toString());
    }

    @Override
    public void onDeath(DamageSource p_70645_1_) {
        super.onDeath(p_70645_1_);
        UUID_CONSUMER_MAP.remove(consumerUUID);
    }
}
