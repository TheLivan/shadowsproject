package ru.xlv.mochar.inventory;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryUtils;
import ru.xlv.core.common.item.ISlotSpecified;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.event.PlayerEquipItemEvent;
import ru.xlv.core.event.PlayerMoveItemSpecEvent;
import ru.xlv.core.player.ServerPlayer;

import java.util.Iterator;
import java.util.List;

public class MatrixInventoryTransaction {

    protected final IMatrixInventoryProvider providerTo, providerFrom;

    public MatrixInventoryTransaction(IMatrixInventoryProvider inventoryProvider) {
        this(inventoryProvider, inventoryProvider);
    }

    public MatrixInventoryTransaction(IMatrixInventoryProvider providerTo, IMatrixInventoryProvider providerFrom) {
        this.providerTo = providerTo;
        this.providerFrom = providerFrom;
    }

    public boolean moveItem(int fromX, int fromY, int toX, int toY, int rotation) {
        ItemStack itemStack = providerFrom.getMatrixInventory().getMatrixItem(fromX, fromY);
        if (itemStack != null) {
            if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
            int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
            int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
            boolean rotated = Math.abs(rotation - MatrixInventoryUtils.getInvMatrixRotation(itemStack)) % 2 != 0;
            int clickedId = providerFrom.getMatrixInventory().getMatrix()[fromY][fromX];
            if (providerTo.getMatrixInventory().isSpaceFree(toX, toY, rotated ? h : w, rotated ? w : h, clickedId)) {
                if (providerFrom.getMatrixInventory().removeItem(itemStack)) {
                    MatrixInventoryUtils.setInvMatrixRotation(itemStack, rotation);
                    providerTo.getMatrixInventory().setItem(toX, toY, itemStack);
                    if(providerTo != providerFrom) {
                        providerFrom.onMatrixInvItemRemoved(itemStack);
                        providerTo.onMatrixInvItemAdded(itemStack);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public boolean moveItem(int fromX, int fromY) {
        ItemStack itemStack = providerFrom.getMatrixInventory().getMatrixItem(fromX, fromY);
        if (itemStack != null) {
            if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
            int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
            int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
            int[] spaceFor = providerTo.getMatrixInventory().findSpaceFor(0, 0, w, h, true);
            if (spaceFor != null) {
                if (providerFrom.getMatrixInventory().removeItem(itemStack)) {
                    if (providerTo.getMatrixInventory().setItem(spaceFor[0], spaceFor[1], itemStack).isFailure()) {
                        providerFrom.getMatrixInventory().addItem(itemStack);
                        return false;
                    }
                    if(providerTo != providerFrom) {
                        providerFrom.onMatrixInvItemRemoved(itemStack);
                        providerTo.onMatrixInvItemAdded(itemStack);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void moveAll() {
        Iterator<Integer> iterator = providerFrom.getMatrixInventory().getItems().keySet().iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            int[] posOfItem = providerFrom.getMatrixInventory().getPosOfItem(next);
            if (posOfItem != null) {
                ItemStack itemStack = providerFrom.getMatrixInventory().getItems().get(next);
                if (providerTo.getMatrixInventory().addItem(itemStack).isSuccess()) {
                    iterator.remove();
                    if (providerTo != providerFrom) {
                        providerFrom.onMatrixInvItemRemoved(itemStack);
                        providerTo.onMatrixInvItemAdded(itemStack);
                    }
                }
            }
        }
    }

    public boolean dropItem(EntityPlayer entityPlayer, int x, int y) {
        if(providerTo.getMatrixInventory().isPosInsideMatrix(x, y)) {
            ItemStack itemStack = providerTo.getMatrixInventory().getMatrixItem(x, y);
            if (itemStack != null) {
                if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
                if(EnumItemTag.canBeDropped(itemStack)) {
                    boolean flag = providerTo.getMatrixInventory().removeItem(itemStack);
                    if (flag) {
                        providerTo.onMatrixInvItemRemoved(itemStack);
                        EntityItem entityItem = new EntityItem(entityPlayer.worldObj, entityPlayer.posX, entityPlayer.posY + 1, entityPlayer.posZ, itemStack);
                        entityPlayer.worldObj.spawnEntityInWorld(entityItem);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean dropItem(World world, double posX, double posY, double posZ, int x, int y) {
        if(providerTo.getMatrixInventory().isPosInsideMatrix(x, y)) {
            ItemStack itemStack = providerTo.getMatrixInventory().getMatrixItem(x, y);
            if (itemStack != null) {
                if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
                boolean flag = providerTo.getMatrixInventory().removeItem(itemStack);
                if (flag) {
                    providerTo.onMatrixInvItemRemoved(itemStack);
                    EntityItem entityItem = new EntityItem(world, posX, posY + 1, posZ, itemStack);
                    world.spawnEntityInWorld(entityItem);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean moveItemSpecMatrix(ServerPlayer serverPlayer, int x, int y, MatrixInventory.SlotType slotType, boolean fromSpec) {
        if(fromSpec) {
            ItemStack itemStack = serverPlayer.getEntityPlayer().inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
            if(itemStack != null) {
                if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
                if (XlvsCoreCommon.EVENT_BUS.post(new PlayerMoveItemSpecEvent(serverPlayer, itemStack, slotType, true))) {
                    return false;
                }
                int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
                int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
                if (providerTo.getMatrixInventory().isSpaceFree(x, y, w, h, -1)) {
                    serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(slotType.getAssociatedSlotIndex(), null);
                    if (providerTo.getMatrixInventory().setItem(x, y, itemStack).isFailure()) {
                        serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(slotType.getAssociatedSlotIndex(), itemStack);
                        return false;
                    }
                    providerFrom.onInvItemRemovedFromSpecSlot(itemStack);
                    if(providerTo != providerFrom) {
                        providerFrom.onMatrixInvItemRemoved(itemStack);
                        providerTo.onMatrixInvItemAdded(itemStack);
                    }
                    return true;
                }
            }
        } else {
            ItemStack itemStack = providerFrom.getMatrixInventory().getMatrixItem(x, y);
            if(itemStack != null) {
                if(providerFrom.isRemovingItemNotAllowed(itemStack) || providerTo.isAddingItemNotAllowed(itemStack)) return false;
                if(itemStack.getItem() instanceof ISlotSpecified) {
                    switch (slotType) {
                        case FEET:
                        case LEGS:
                        case BRACERS:
                        case BODY:
                        case HEAD:
                            List<MatrixInventory.SlotType> slotTypes = ((ItemArmor) itemStack.getItem()).getSlotTypes();
                            if(slotTypes.isEmpty() || slotTypes.get(0) != slotType) {
                                return false;
                            }
                    }
                }
                if (XlvsCoreCommon.EVENT_BUS.post(new PlayerMoveItemSpecEvent(serverPlayer, itemStack, slotType, false))) {
                    return false;
                }
                providerFrom.getMatrixInventory().removeItem(itemStack);
                serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(slotType.getAssociatedSlotIndex(), itemStack);
                providerTo.onInvItemAddedToSpecSlot(itemStack);
                if(providerTo != providerFrom) {
                    providerFrom.onMatrixInvItemRemoved(itemStack);
                    providerTo.onMatrixInvItemAdded(itemStack);
                }
                XlvsCoreCommon.EVENT_BUS.post(new PlayerEquipItemEvent(serverPlayer, itemStack));
                return true;
            }
        }
        return false;
    }

    public boolean moveItemSpecSpec(ServerPlayer serverPlayer, MatrixInventory.SlotType slotTypeFrom, MatrixInventory.SlotType slotTypeTo) {
        boolean flag = false;
        if(slotTypeFrom.isWeaponSlot() && slotTypeTo.isWeaponSlot() && slotTypeTo != MatrixInventory.SlotType.MELEE_WEAPON) {
            flag = true;
        } else if(slotTypeFrom.isHotSlot() && slotTypeTo.isHotSlot()) {
            flag = true;
        }
        if(flag) {
            ItemStack itemStack = serverPlayer.getEntityPlayer().inventory.getStackInSlot(slotTypeFrom.getAssociatedSlotIndex());
            if (itemStack != null) {
                ItemStack itemStack1 = serverPlayer.getEntityPlayer().inventory.getStackInSlot(slotTypeTo.getAssociatedSlotIndex());
                serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(slotTypeFrom.getAssociatedSlotIndex(), itemStack1);
                serverPlayer.getEntityPlayer().inventory.setInventorySlotContents(slotTypeTo.getAssociatedSlotIndex(), itemStack);
                return true;
            }
        }
        return false;
    }

    public void clearInventory() {
        providerTo.getMatrixInventory().clear();
        providerTo.onMatrixInvCleaned();
    }

    public void resizeMatrix(EntityPlayer entityPlayer, int width, int height) {
        for (int y = 0; y < providerTo.getMatrixInventory().getHeight(); y++) {
            for (int x = 0; x < providerTo.getMatrixInventory().getWidth(); x++) {
                if(x >= width && y >= height) {
                    dropItem(entityPlayer, x, y);
                }
            }
        }
        providerTo.getMatrixInventory().setMatrixSize(width, height);
    }
}
