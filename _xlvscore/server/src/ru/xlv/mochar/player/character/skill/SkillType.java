package ru.xlv.mochar.player.character.skill;

import com.google.gson.internal.Primitives;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import lombok.Getter;
import lombok.ToString;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.mochar.player.character.skill.cost.ISkillCost;
import ru.xlv.mochar.util.Utils;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@ToString
public class SkillType {

    private final CharacterType characterType;
    private final int skillId, familyId;
    private final String name, description, implClassPath;

    private long duration, cooldown;

    private final float iconX, iconY;
    private final String textureName;

    private final TIntList parentIds;
    private final List<String> tags;
    private final List<ISkillCost> skillCosts;
    private final List<SkillLearnRules.SkillLearnRule> learnRules;
    private final Map<String, Object> customParams = new HashMap<>();

    public SkillType(int skillId, int[] parentIds, int familyId, String name, String description, CharacterType characterType, String implClassPath,
                     float iconX, float iconY, String textureName,
                     long duration, long cooldown,
                     ISkillCost[] skillCosts, SkillLearnRules.SkillLearnRule[] skillLearnRules, String[] tags, Map<String, Object> customParams) {
        this(skillId, parentIds, familyId, name, description, characterType, implClassPath, iconX, iconY, textureName, skillCosts, skillLearnRules, tags, customParams);
        this.duration = this.duration == 0 ? duration * 1000L : this.duration;
        this.cooldown = this.cooldown == 0 ? cooldown * 1000L : this.cooldown;
    }

    public SkillType(int skillId, int[] parentIds, int familyId, String name, String description, CharacterType characterType, String implClassPath,
                     float iconX, float iconY, String textureName,
                     ISkillCost[] skillCosts, SkillLearnRules.SkillLearnRule[] skillLearnRules, String[] tags, Map<String, Object> customParams) {
        this.skillId = skillId;
        if(parentIds != null && parentIds.length > 0) {
            this.parentIds = new TIntArrayList(parentIds);
        } else {
            this.parentIds = new TIntArrayList(new int[] {-1});
        }
        this.familyId = familyId;
        this.name = name;
        this.characterType = characterType;
        this.skillCosts = Arrays.asList(skillCosts);
        this.learnRules = Arrays.asList(skillLearnRules);
        this.implClassPath = implClassPath;
        this.tags = Arrays.asList(tags);
        this.customParams.putAll(customParams);
        String d = description;
        for (String s : getCustomParams().keySet()) {
            String replacement = getCustomParams().get(s).toString();
            try {
                int i = Integer.parseInt(replacement);
                replacement = Integer.toString(i);
            } catch (NumberFormatException ignored) {}
            d = d.replace("$" + s + "$", replacement);
        }
        this.description = d;
        this.iconX = iconX;
        this.iconY = iconY;
        this.textureName = textureName == null ? "" : textureName;
        if(customParams.containsKey("cooldown")) {
            this.cooldown = ((Double) customParams.get("cooldown")).intValue() * 1000L;
        }
        if(customParams.containsKey("activationPeriod")) {
            this.duration = ((Double) customParams.get("activationPeriod")).intValue() * 1000L;
        }
    }

    public boolean isActivable() {
        return duration > 0L || cooldown > 0L;
    }

    public <T> T getCustomParam(@Nonnull String key, @Nonnull Class<T> tClass) {
        if(key.equals("")) throw new RuntimeException("Wrong skill param " + key + "!");
        T customParam0 = getCustomParam0(key, tClass);
        if(customParam0 == null) throw new NullPointerException("Skill param " + key + " not found!");
        return customParam0;
    }

    private <T> T getCustomParam0(@Nonnull String key, @Nonnull Class<T> tClass) {
        Object o = customParams.get(key);
        //noinspection unchecked
        T o1 = (T) o;
        if (o == null) {
            if (!Primitives.isPrimitive(tClass) && tClass.isAssignableFrom(Number.class)) return null;
            //noinspection unchecked
            return (T) Utils.castNumber(0, (Class<? extends Number>) tClass);
        }
        if (Primitives.unwrap(o1.getClass()) == tClass) return o1;
        //noinspection unchecked
        return (T) Utils.castNumber((Number) o1, (Class<? extends Number>) tClass);
    }
}
