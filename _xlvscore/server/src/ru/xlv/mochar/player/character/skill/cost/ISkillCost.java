package ru.xlv.mochar.player.character.skill.cost;

import ru.xlv.core.common.network.IPacketComposable;
import ru.xlv.core.player.ServerPlayer;

public interface ISkillCost extends IPacketComposable {

    boolean consume(ServerPlayer serverPlayer);
}
