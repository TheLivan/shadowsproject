package ru.xlv.mochar.player.character.skill.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.IResult;

@Getter
@RequiredArgsConstructor
public enum SkillLearnResult implements IResult {

    SUCCESS(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillLearnResultSuccessMessage()),
    NOT_FOUND(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillLearnResultNotFoundMessage()),
    CONDITIONS_NOT_FULFILLED(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillLearnResultConditionsNotFulfilledMessage()),
    ALREADY_LEARNED(XlvsCore.INSTANCE.getSkillLocalization().getResponseSkillLearnResultAlreadyLearnedMessage());

    private final String responseMessage;

    @Override
    public boolean isSuccess() {
        return this == SUCCESS;
    }
}
