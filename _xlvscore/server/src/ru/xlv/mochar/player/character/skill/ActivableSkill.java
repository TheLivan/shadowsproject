package ru.xlv.mochar.player.character.skill;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.cost.ISkillCost;
import ru.xlv.mochar.player.character.skill.cost.SkillCostItem;
import ru.xlv.mochar.player.character.skill.cost.SkillCostMana;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;
import ru.xlv.mochar.util.Lang;
import ru.xlv.mochar.util.Utils;

import java.util.List;

public abstract class ActivableSkill extends Skill implements ISkillUpdatable {

    private boolean isActive;
    private long activationTimeMills;

    private ServerPlayer serverPlayer;

    public ActivableSkill(SkillType skillType) {
        super(skillType);
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        if(isActive && System.currentTimeMillis() - activationTimeMills >= getSkillType().getDuration()) {
            isActive = false;
            onDeactivated(serverPlayer);
        }
    }

    @Override
    public SkillExecuteResult execute(ServerPlayer serverPlayer) {
        super.execute(serverPlayer);
        if(isActive) {
            Utils.sendMessage(serverPlayer, Lang.SKILL_ALREADY_ACTIVE_MESSAGE);
            return SkillExecuteResult.ALREADY_ACTIVE;
        }
        if(inCooldown()) {
            Utils.sendMessage(serverPlayer, Lang.SKILL_RECHARGING_MESSAGE);
            return SkillExecuteResult.IN_COOLDOWN;
        }
        if (checkConditions(serverPlayer)) {
            getSkillType().getSkillCosts().forEach(iSkillCost -> iSkillCost.consume(serverPlayer));
            activationTimeMills = System.currentTimeMillis();
            isActive = true;
            this.serverPlayer = serverPlayer;
            try {
                onActivate(serverPlayer);
            } catch (Exception e) {
                Utils.sendMessage(serverPlayer, "Произошла ошибка при использовании способности. Пожалуйста, сообщите об этом администрации.");
                return SkillExecuteResult.UNKNOWN_ERROR;
            }
            return SkillExecuteResult.SUCCESS;
        } else {
            return SkillExecuteResult.CONDITIONS_NOT_FULFILLED;
        }
    }

    public abstract void onActivate(ServerPlayer serverPlayer);

    protected void onDeactivated(ServerPlayer serverPlayer) {}

    private boolean checkConditions(ServerPlayer serverPlayer) {
        for (ISkillCost skillCost : getSkillType().getSkillCosts()) {
            if(skillCost instanceof SkillCostItem) {
                if (CommonUtils.countItems(((SkillCostItem) skillCost).getItem(), serverPlayer.getEntityPlayer().inventory) < ((SkillCostItem) skillCost).getAmount()) {
                    return false;
                }
            } else if(skillCost instanceof SkillCostMana) {
                if (serverPlayer.getSelectedCharacter() != null) {
                    CharacterAttribute characterAttribute = serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA);
                    if(characterAttribute.getValue() < ((SkillCostMana) skillCost).getAmount()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    protected void deactivateSkill(ServerPlayer serverPlayer) {
        isActive = false;
        onDeactivated(serverPlayer);
    }

    public void increaseCooldown(long value) {
        if(inCooldown()) {
            activationTimeMills -= value;
        }
    }

    public void reduceCooldown(long value) {
        if(inCooldown()) {
            activationTimeMills += value;
        }
    }

    public void breakCooldown() {
        activationTimeMills = 0L;
    }

    public boolean inCooldown() {
        double mod = 1D;
        if (serverPlayer != null) {
            mod = serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.COOLDOWN_MOD).getValue();
        }
        return System.currentTimeMillis() - activationTimeMills < ((double) getSkillType().getCooldown()) * mod;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    protected int getSkillCategoryId() {
        return 1;
    }

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        super.writeDataToPacket(writableList, byteBufOutputStream);
        double cooldownMod = 1D;
        if (serverPlayer != null) {
            cooldownMod = serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.COOLDOWN_MOD).getValue();
        }
        writableList.add((long) (inCooldown() ? ((getSkillType().getCooldown() - (System.currentTimeMillis() - activationTimeMills)) * cooldownMod) : 0));
        writableList.add((long) (getSkillType().getCooldown() * cooldownMod));
        writableList.add(getSkillType().getDuration());
        writableList.add(isActive());
    }
}
