package ru.xlv.mochar.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BlockEvent;

public class EventListener {

    @SubscribeEvent
    public void event(EntityItemPickupEvent event) {
        event.setCanceled(true);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(BlockEvent.BreakEvent event) {
        if(!event.getPlayer().capabilities.isCreativeMode) {
            event.setCanceled(true);
        }
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void blockSpeedBreak(PlayerEvent.BreakSpeed e) {
        if(!e.entityPlayer.capabilities.isCreativeMode) {
            e.setCanceled(true);
        }
    }

//    @SubscribeEvent
//    public void event(PlayerEvent.SaveToFile event) {
////        System.out.println("save");
////        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.entityPlayer);
////        if(serverPlayer != null) {
////            XlvsMainMod.INSTANCE.getCharacterLoader().save(event.entityPlayer, serverPlayer);
////        }
//    }
//
//    @SubscribeEvent
//    public void event(PlayerEvent.LoadFromFile event) {
////        System.out.println("load");
////        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(event.entityPlayer, true);
////        if(serverPlayer != null) {
////            XlvsMainMod.INSTANCE.getCharacterLoader().load(event.entityPlayer, serverPlayer);
////        }
//    }
}
