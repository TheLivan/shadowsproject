package ru.xlv.mochar.network.matrix;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

public class PacketMatrixInventorySpecItemMove implements IPacketInOnServer {

    public PacketMatrixInventorySpecItemMove() {
    }

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        boolean fromSpec = bbis.readBoolean();
        int x = bbis.readInt();
        int y = bbis.readInt();
        int slotType1 = bbis.readInt();
        MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[slotType1];
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(serverPlayer.getSelectedCharacter());
            if (matrixInventoryTransaction.moveItemSpecMatrix(serverPlayer, x, y, slotType, fromSpec)) {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
            }
        }
    }
}
