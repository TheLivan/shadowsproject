package ru.xlv.mochar.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryIO;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketMatrixInventoryTransactionSync implements IPacketOutServer {

    private MatrixInventory matrixInventory;

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        write(matrixInventory, bbos);
    }

    private void write(MatrixInventory matrixInventory, ByteBufOutputStream bbos) throws IOException {
        MatrixInventoryIO.write(matrixInventory, bbos);
    }
}
