package ru.xlv.mochar.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlayerDeathEvent implements IPacketOutServer {

    private int entityId;

    public PacketPlayerDeathEvent(int entityId) {
        this.entityId = entityId;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityId);
    }
}
