package ru.xlv.mochar.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterDebuffType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.Character;
import ru.xlv.mochar.player.character.skill.experience.Experience;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlayerMainSync implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    public PacketPlayerMainSync(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        Character selectedCharacter = serverPlayer.getSelectedCharacter();
        bbos.writeInt(selectedCharacter.getCharacterType().ordinal());
        bbos.writeInt(selectedCharacter.getWallet().getCredits());
        bbos.writeDouble(serverPlayer.getPlatinum());
        bbos.writeInt(selectedCharacter.getSkillManager().getAvailableActiveSkillSlotAmount());
        bbos.writeInt(selectedCharacter.getCharacterDebuffMap().size());
        for (CharacterDebuffType characterDebuffType : selectedCharacter.getCharacterDebuffMap().keySet()) {
            bbos.writeInt(characterDebuffType.ordinal());
            bbos.writeLong(selectedCharacter.getCharacterDebuffMap().get(characterDebuffType).getCreationTimeMills());
            bbos.writeLong(selectedCharacter.getCharacterDebuffMap().get(characterDebuffType).getPeriod());
        }
        bbos.writeInt(selectedCharacter.getExperienceManager().getExperiences().size());
        for (Experience experience : selectedCharacter.getExperienceManager().getExperiences()) {
            bbos.writeInt(experience.getType().ordinal());
            bbos.writeDouble(experience.getValue());
        }
        bbos.writeInt(selectedCharacter.getCharacterAttributes().size());
        for (CharacterAttribute characterAttribute : selectedCharacter.getCharacterAttributes()) {
            bbos.writeInt(characterAttribute.getType().ordinal());
            bbos.writeDouble(characterAttribute.getValue());
            bbos.writeDouble(characterAttribute.getMaxValue());
        }
        writeCollection(bbos, selectedCharacter.getDiscoveredLocationNames(), (byteBufOutputStream, s) -> bbos.writeUTF(s));
    }
}
