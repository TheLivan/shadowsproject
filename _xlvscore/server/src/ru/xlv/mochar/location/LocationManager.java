package ru.xlv.mochar.location;

import lombok.Getter;

import java.util.*;

public class LocationManager {

    private final Map<String, Location> playerAreaInMap = Collections.synchronizedMap(new HashMap<>());
    @Getter
    private final List<Location> locationList = Collections.synchronizedList(new ArrayList<>());

    public LocationManager() {
        LocationConfig locationConfig = new LocationConfig();
        locationConfig.load();
        locationConfig.getLocations().forEach(locationElement -> {
            Location location = new Location(locationElement.getX0(), locationElement.getZ0(), locationElement.getX1(), locationElement.getZ1(), locationElement.getName());
            if(!locationElement.isAllowPvp()) {
                location.getLocationFlags().add(LocationFlag.NO_PVP);
            }
            addLocation(location);
        });
    }

    public void addLocation(Location location) {
        for (Location a : locationList) {
            if(location.getName().equals(a.getName())) {
                System.out.println("Cannot add an area with the name " + a.getName() + ", bcs the area with same name already registered!");
                return;
            }
        }
        locationList.add(location);
        System.out.println("A new area has added: " + location);
    }

    public void setPlayerLocationIn(String playerName, Location location) {
        if(location == null) {
            playerAreaInMap.remove(playerName);
            return;
        }
        playerAreaInMap.put(playerName, location);
    }

    public Location getPlayerLocationIn(String playerName) {
        return playerAreaInMap.get(playerName);
    }
}
