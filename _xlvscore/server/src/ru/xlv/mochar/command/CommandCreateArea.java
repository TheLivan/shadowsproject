package ru.xlv.mochar.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.mochar.XlvsMainMod;
import ru.xlv.mochar.location.Location;
import ru.xlv.mochar.location.LocationFlag;
import ru.xlv.mochar.util.Utils;

public class CommandCreateArea extends CommandBase {
    @Override
    public String getCommandName() {
        return "createarea";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        int x = Integer.parseInt(p_71515_2_[0]);
        int y = Integer.parseInt(p_71515_2_[1]);
        int x1 = Integer.parseInt(p_71515_2_[2]);
        int y1 = Integer.parseInt(p_71515_2_[3]);
        String[] ss = new String[p_71515_2_.length - 4];
        System.arraycopy(p_71515_2_, 4, ss, 0, ss.length);
        Location location = new Location(x, y, x1, y1, String.join(" ", ss));
        location.addFlag(LocationFlag.NO_PVP);
        XlvsMainMod.INSTANCE.getLocationManager().addLocation(location);
        Utils.sendMessage((EntityPlayer) p_71515_1_, "Success!");
    }
}
