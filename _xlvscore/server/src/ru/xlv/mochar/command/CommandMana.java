package ru.xlv.mochar.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterAttribute;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.util.Utils;

public class CommandMana extends CommandBase {
    @Override
    public String getCommandName() {
        return "addmana";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer((EntityPlayer) p_71515_1_);
        CharacterAttribute characterAttribute = serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA);
        double value = characterAttribute.getValue() + Integer.parseInt(p_71515_2_[0]);
        if (value > characterAttribute.getMaxValue()) {
            characterAttribute.setMaxValue(value);
        }
        characterAttribute.setValue(value);
        Utils.sendMessage(serverPlayer, "Мана добавлена.");
    }
}
