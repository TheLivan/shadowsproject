package ru.xlv.core.common.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;

import java.util.Objects;

@Getter
@Configurable
@RequiredArgsConstructor
public class WorldPosition {
    private final int dimension;
    private final double x, y, z;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorldPosition that = (WorldPosition) o;
        return dimension == that.dimension &&
                Double.compare(that.x, x) == 0 &&
                Double.compare(that.y, y) == 0 &&
                Double.compare(that.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dimension, x, y, z);
    }
}
