package ru.xlv.core.common.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.block.Block;
import ru.xlv.core.util.SoundType;

@Getter
@RequiredArgsConstructor
public class StepSoundRegistryElement {

    private final Block block;
    private final int metadata;
    private final SoundType soundType;
}
