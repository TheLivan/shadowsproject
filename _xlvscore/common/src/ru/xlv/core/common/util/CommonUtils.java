package ru.xlv.core.common.util;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import lombok.Getter;
import lombok.experimental.UtilityClass;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.core.util.CoreUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@UtilityClass
public class CommonUtils {

    @Getter
    private boolean isDebugEnabled;
    static {
        if(CoreUtils.isDebugEnabled || CoreUtils.isStartedFromGradle() || FMLCommonHandler.instance().getSide() == Side.SERVER) {
            isDebugEnabled = true;
        }
    }

    public boolean isServerSide() {
        return FMLCommonHandler.instance().getSide() == Side.SERVER;
    }

    @Nonnull
    public AxisAlignedBB getAxisAlignedBBOf(@Nonnull Entity entity, double radius) {
        return AxisAlignedBB.getBoundingBox(
                entity.posX - radius, entity.posY - radius, entity.posZ - radius,
                entity.posX + radius, entity.posY + radius, entity.posZ + radius
        );
    }

    public <T> void unregisterFMLEvents(Object object) {
        FMLCommonHandler.instance().bus().unregister(object);
    }

    public <T> void unregisterEvents(Object object) {
        MinecraftForge.EVENT_BUS.unregister(object);
    }

    public <T> T registerEvents(Class<T> clazz) {
        try {
            T object = clazz.newInstance();
            MinecraftForge.EVENT_BUS.register(object);
            return object;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> T registerFMLEvents(Class<T> clazz) {
        try {
            T object = clazz.newInstance();
            FMLCommonHandler.instance().bus().register(object);
            return object;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void registerFMLEvents(Object object) {
        FMLCommonHandler.instance().bus().register(object);
    }

    public void registerEvents(Object object) {
        MinecraftForge.EVENT_BUS.register(object);
    }

    public <T> T registerAllEvents(Class<T> clazz) {
        try {
            T object = clazz.newInstance();
            FMLCommonHandler.instance().bus().register(object);
            MinecraftForge.EVENT_BUS.register(object);
            return object;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CompletableFuture<Void> runAsync(ThrowableRunnable throwableRunnable) {
        return CompletableFuture.runAsync(() -> {
            try {
                throwableRunnable.execute();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public CompletableFuture<Void> runAsync(ThrowableRunnable throwableRunnable, ExecutorService executorService) {
        return CompletableFuture.runAsync(() -> {
            try {
                throwableRunnable.execute();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }, executorService);
    }

    public <T> CompletableFuture<T> supplyAsync(Callable<T> callable) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return callable.call();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return null;
        });
    }

    public <T> CompletableFuture<T> supplyAsync(Callable<T> callable, ExecutorService executorService) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return callable.call();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return null;
        }, executorService);
    }

    public int countItems(Item p_146029_1_, InventoryPlayer inventoryPlayer) {
        int c = 0;
        for (int i = 0; i < inventoryPlayer.mainInventory.length; ++i) {
            if (inventoryPlayer.mainInventory[i] != null && inventoryPlayer.mainInventory[i].getItem() == p_146029_1_) {
                c += inventoryPlayer.mainInventory[i].stackSize;
            }
        }
        return c;
    }

    public int[] newIntArray(Integer[] integers) {
        int[] ints = new int[integers.length];
        for (int i = 0; i < integers.length; i++) {
            ints[i] = integers[i];
        }
        return ints;
    }

    public NBTTagList newDoubleNBTList(double... p_70087_1_) {
        NBTTagList nbttaglist = new NBTTagList();
        double[] adouble = p_70087_1_;
        int i = p_70087_1_.length;
        for (int j = 0; j < i; ++j) {
            double d1 = adouble[j];
            nbttaglist.appendTag(new NBTTagDouble(d1));
        }
        return nbttaglist;
    }

    public NBTTagList newFloatNBTList(float... p_70049_1_) {
        NBTTagList nbttaglist = new NBTTagList();
        float[] afloat = p_70049_1_;
        int i = p_70049_1_.length;
        for (int j = 0; j < i; ++j) {
            float f1 = afloat[j];
            nbttaglist.appendTag(new NBTTagFloat(f1));
        }
        return nbttaglist;
    }

    public List<Class<?>> getClassesHierarchy(Class<?> clazz) {
        List<Class<?>> list = new ArrayList<>();
        Class<?> c = clazz;
        while(c != null) {
            list.add(c);
            c = c.getSuperclass();
        }
        return list;
    }
}
