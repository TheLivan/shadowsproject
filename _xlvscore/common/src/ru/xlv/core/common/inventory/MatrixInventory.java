package ru.xlv.core.common.inventory;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import lombok.Getter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Predicate;

@Getter
public class MatrixInventory implements ISavableNBT {

    @DatabaseValue
    private final Map<Integer, ItemStack> items = new HashMap<>();

    @DatabaseValue
    private int width, height;
    @DatabaseValue
    private int[][] matrix;

    private boolean needSync;

    protected MatrixInventory(int width, int height) {
        this();
        setMatrixSize(width, height);
    }

    protected MatrixInventory() {
        this.matrix = new int[0][0];
    }

    public AddItemResult addItem(ItemStack itemStack) {
        return addItem(0, 0, itemStack, true);
    }

    public AddItemResult setItem(int x, int y, ItemStack itemStack) {
        return addItem(x, y, itemStack, false);
    }

    public AddItemResult addItem(int x, int y, ItemStack itemStack, boolean lookupAllInventory) {
//        printMatrix();
        if(isPosInsideMatrix(x, y)) {
            int width = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
            int height = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
            int[] xy = findSpaceFor(x, y, width, height, lookupAllInventory);
            if (xy != null) {
                int id = -1;
                if(items.containsValue(itemStack)) {
                    for (Integer integer : items.keySet()) {
                        if (items.get(integer).equals(itemStack)) {
                            id = integer;
                            break;
                        }
                    }
                } else {
                    id = getNextItemId();
                }
                items.put(id, itemStack);
                for (int j = xy[1]; j < xy[1] + height; j++) {
                    for (int i = xy[0]; i < xy[0] + width; i++) {
                        matrix[j][i] = id;
                    }
                }
                return AddItemResult.SUCCESS;
            } else {
                return AddItemResult.NO_FREE_SPACE;
            }
        }
        return AddItemResult.UNKNOWN;
    }

    public void verify() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int id = this.matrix[y][x];
                if(id != -1 && !items.containsKey(id)) {
                    this.matrix[y][x] = -1;
                }
            }
        }
        items.keySet().removeIf(next -> getPosOfItem(next) == null);
    }

    public void clear() {
        this.items.clear();
        this.matrix = new int[height][width];
        fillMatrixByNulls();
    }

    public boolean removeItemFromMatrix(int x, int y) {
        ItemStack itemStack = getMatrixItem(x, y);
        if (itemStack != null) {
            return removeItem(itemStack);
        }
        return false;
    }

    public boolean removeItem(ItemStack itemStack) {
        int width = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
        int height = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
        for (int j = 0; j < this.height; j++) {
            for (int i = 0; i < this.width; i++) {
                int id = matrix[j][i];
                if (id != -1) {
                    ItemStack found = items.get(id);
                    if (found == itemStack) {
                        removeItemMatrix(i, j, width, height, id);
                        items.remove(id);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean removeItem(Item item) {
        ItemStack itemStack = null;
        for (ItemStack value : items.values()) {
            if(value.getItem() == item) {
                itemStack = value;
                break;
            }
        }
        if (itemStack != null) {
            removeItem(itemStack);
        }
        return false;
    }

    public boolean removeItems(Item... items) {
        List<ItemStack> list = new ArrayList<>();
        for (ItemStack value : this.items.values()) {
            for (Item item : items) {
                if(value.getItem() == item) {
                    list.add(value);
                }
            }
        }
        list.forEach(this::removeItem);
        boolean flag = !list.isEmpty();
        list.clear();
        return flag;
    }

    public int removeItems(Predicate<ItemStack> predicate) {
        TIntList list = new TIntArrayList();
        for (Integer id : items.keySet()) {
            ItemStack itemStack = items.get(id);
            if(predicate.test(itemStack)) {
                list.add(id);
            }
        }
        list.forEach(value -> {
            int[] xy = getPosOfItem(value);
            removeItemFromMatrix(xy[0], xy[1]);
            return true;
        });
        return list.size();
    }

    public int getItemAmount(Item item) {
        int amount = 0;
        for (ItemStack value : items.values()) {
            if(value.getItem() == item) {
                amount++;
            }
        }
        return amount;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public boolean containsItemStack(ItemStack itemStack) {
        for (int j = 0; j < this.height; j++) {
            for (int i = 0; i < this.width; i++) {
                int id = matrix[j][i];
                if (id != -1) {
                    ItemStack found = items.get(id);
                    if (ItemStack.areItemStacksEqual(found, itemStack)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isSpaceFree(int x, int y, int w, int h, int movedId) {
        if(!isPosInsideMatrix(x, y) || !isPosInsideMatrix(x + w - 1, y + h - 1)) return false;
        for (int j = y; j < y + h; j++) {
            for (int i = x; i < x + w; i++) {
                int id = matrix[j][i];
                if (id != -1 && id != movedId) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean hasSpaceFor(ItemStack itemStack) {
        int w = MatrixInventoryUtils.getInvMatrixWidth(itemStack);
        int h = MatrixInventoryUtils.getInvMatrixHeight(itemStack);
        return findSpaceFor(0, 0, w, h, true) != null;
    }

    public ItemStack getMatrixItem(int x, int y) {
        if(isPosInsideMatrix(x, y)) {
            int id = matrix[y][x];
            if (id != -1) {
                return items.get(id);
            }
        }
        return null;
    }

    public int[] getPosOfItem(int id) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int id1 = matrix[y][x];
                if(id == id1) {
                    return new int[] {x, y};
                }
            }
        }
        return null;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        nbtTagCompound.setInteger("width", width);
        nbtTagCompound.setInteger("height", height);
        nbtTagCompound.setInteger("itemAmount", items.size());
        int counter = 0;
        for (Integer integer : items.keySet()) {
            int[] posOfItem = getPosOfItem(integer);
            if(posOfItem == null) {
//                printMatrix();
                throw new RuntimeException("An error has occurred during writing matrix to NBT.");
            }
            NBTTagCompound nbtTagCompound1 = new NBTTagCompound();
            items.get(integer).writeToNBT(nbtTagCompound1);
            nbtTagCompound1.setInteger("invMatrixX", posOfItem[0]);
            nbtTagCompound1.setInteger("invMatrixY", posOfItem[1]);
            nbtTagCompound.setTag(Integer.toString(counter), nbtTagCompound1);
            counter++;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
        items.clear();
        setMatrixSize(nbtLoader.getInteger(nbtTagCompound, "width", width), nbtLoader.getInteger(nbtTagCompound, "height", height));
        int itemAmount = nbtTagCompound.getInteger("itemAmount");
        for (int i = 0; i < itemAmount; i++) {
            NBTTagCompound nbtTagCompound1 = (NBTTagCompound) nbtTagCompound.getTag(Integer.toString(i));
            ItemStack itemStack = ItemStack.loadItemStackFromNBT(nbtTagCompound1);
            int startX = nbtTagCompound1.getInteger("invMatrixX");
            int startY = nbtTagCompound1.getInteger("invMatrixY");
            setItem(startX, startY, itemStack);
        }
    }

    private void removeItemMatrix(int startX, int startY, int width, int height, int id) {
        for (int x = startX; x < startX + width; x++) {
            for (int y = startY; y < startY + height; y++) {
                if(matrix[y][x] != id) {
                    throw new RuntimeException("An error has occurred during removing item " + id + " from inventory");
                }
                matrix[y][x] = -1;
            }
        }
    }

    public void printMatrix() {
        for (int y = 0; y < height; y++) {
            System.out.print("[");
            for (int x = 0; x < width; x++) {
                int id = matrix[y][x];
                System.out.print((id == -1 ? id : (id < 10 ? "0" + id : id)) + ",");
            }
            System.out.print("]\n");
        }
    }

    /**
     * @param lookupAllInventory specifies the type of search. Set it to true to lookup all inventory.
     * @return an int array contains start x and y of suitable space or null if inv has no available space.
     * */
    @Nullable
    public int[] findSpaceFor(int startX, int startY, int width, int height, boolean lookupAllInventory) {
        for (int y = startY; y < this.height; y++) {
            label:
            for (int x = startX; x < this.width; x++) {
                if (matrix[y][x] == -1) {
                    for (int j = 0; j < height; j++) {
                        for (int i = 0; i < width; i++) {
                            if (y + j >= this.height || x + i >= this.width || matrix[y + j][x + i] != -1) {
                                if (lookupAllInventory) {
                                    continue label;
                                }
                                return null;
                            }
                        }
                    }
                    return new int[] {x, y};
                }
            }
        }
        return null;
    }

    public void setMatrixSize(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrix = new int[height][width];
        fillMatrixByNulls();
    }

    public void resizeMatrix(int width, int height) {
        int[][] matrix = new int[height][width];
        if(this.height <= height && this.width <= width) {
            System.arraycopy(this.matrix, 0, matrix, 0, this.matrix.length);
        } else {
            throw new UnsupportedOperationException();
        }
        this.width = width;
        this.height = height;
        this.matrix = matrix;
    }

    public void fillMatrixByNulls() {
        for (int y = 0; y < height; y++) {
            Arrays.fill(matrix[y], -1);
        }
    }

    public boolean isPosInsideMatrix(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    private int getNextItemId() {
        int i = 0;
        while(items.containsKey(i)) i++;
        return i;
    }

    public enum SlotType {
        HEAD(36),
        BODY(37),
        BRACERS(35),
        LEGS(38),
        FEET(39),
        BACKPACK(10),
        MAIN_WEAPON(0),
        SECOND_WEAPON(1),
        ADD_WEAPON(2),
        MELEE_WEAPON(3),
        HOT_SLOT0(11),
        HOT_SLOT1(5),
        HOT_SLOT2(6),
        HOT_SLOT3(7),
        HOT_SLOT4(8);

        private final int associatedSlotIndex;

        SlotType(int associatedSlotIndex) {
            this.associatedSlotIndex = associatedSlotIndex;
        }

        public int getAssociatedSlotIndex() {
            return associatedSlotIndex;
        }

        public boolean isArmorSlot() {
            return this == HEAD || this == BODY || this == BRACERS || this == LEGS || this == FEET;
        }

        public boolean isWeaponSlot() {
            return this == MAIN_WEAPON || this == SECOND_WEAPON || this == ADD_WEAPON || this == MELEE_WEAPON;
        }

        public boolean isHotSlot() {
            return this == HOT_SLOT0 || this == HOT_SLOT1 || this == HOT_SLOT2 || this == HOT_SLOT3 || this == HOT_SLOT4;
        }
    }

    public synchronized void setNeedSync(boolean needSync) {
        this.needSync = needSync;
    }

    public synchronized boolean isNeedSync() {
        return this.needSync;
    }
}
