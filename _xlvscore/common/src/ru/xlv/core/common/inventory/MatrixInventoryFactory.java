package ru.xlv.core.common.inventory;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import java.util.function.Function;

public class MatrixInventoryFactory {

    public static MatrixInventory create() {
        return new MatrixInventory();
    }

    public static MatrixInventory create(int width, int height) {
        return new MatrixInventory(width, height);
    }

    public static MatrixInventory create(ItemStack... itemStacks) {
        MatrixInventory matrixInventory = new MatrixInventory(10, 3);
        for (ItemStack itemStack : itemStacks) {
            addItem(matrixInventory, itemStack);
        }
        return matrixInventory;
    }

    public static MatrixInventory create(IInventory inventory) {
        MatrixInventory matrixInventory = new MatrixInventory(10, 3);
        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            addItem(matrixInventory, inventory.getStackInSlot(i));
        }
        return matrixInventory;
    }

    public static MatrixInventory create(int inventorySize, Function<Integer, ItemStack> function) {
        MatrixInventory matrixInventory = new MatrixInventory(10, 3);
        for (int i = 0; i < inventorySize; i++) {
            addItem(matrixInventory, function.apply(i));
        }
        return matrixInventory;
    }

    private static void addItem(MatrixInventory matrixInventory, ItemStack itemStack) {
        if(itemStack != null) {
            itemStack = itemStack.copy();
            int stackSize = itemStack.stackSize;
            itemStack.stackSize = 1;
            for (int i = 0; i < stackSize; i++) {
                if (matrixInventory.addItem(itemStack).isFailure()) {
                    matrixInventory.setMatrixSize(matrixInventory.getWidth(), matrixInventory.getHeight() + 1);
                    i--;
                }
            }
        }
    }
}
