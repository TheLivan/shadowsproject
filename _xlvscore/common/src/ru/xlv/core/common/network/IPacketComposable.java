package ru.xlv.core.common.network;

import gnu.trove.TIntCollection;
import io.netty.buffer.ByteBufOutputStream;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface IPacketComposable {

    void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream);

    default void writeDataToPacketPost(ByteBufOutputStream byteBufOutputStream) {}

    default void writeComposableCollection(@Nullable Collection<? extends IPacketComposable> collection, List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        if(collection == null) {
            writableList.add(0);
            return;
        }
        writableList.add(collection.size());
        collection.forEach(o -> o.writeDataToPacket(writableList, byteBufOutputStream));
    }

    default void writeSerializableCollection(@Nullable Collection<? extends Serializable> collection, List<Object> writableList) {
        if(collection == null) {
            writableList.add(0);
            return;
        }
        writableList.add(collection.size());
        writableList.addAll(collection);
    }

    default void writeArray(TIntCollection tIntCollection, List<Object> writableList) {
        writableList.add(tIntCollection.size());
        tIntCollection.forEach(writableList::add);
    }
}
