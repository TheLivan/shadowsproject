package ru.xlv.core.common.network;

import java.util.logging.Logger;

public class PacketHandler extends PacketHandlerBasic {

    protected final Logger logger = Logger.getLogger("PH");

    public PacketHandler(PacketRegistry packetRegistry) {
        super(packetRegistry, "xlvscore");
    }
}
