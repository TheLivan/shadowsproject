package ru.xlv.core.common.player.stat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@AllArgsConstructor
public class StatDoubleProvider implements IStatProvider {

    private double value;

    public void increment(double d) {
        if(value < Double.MAX_VALUE - d) {
            value += d;
        }
    }

    @Override
    public void increment() {
        increment(1);
    }

    @Override
    public Double getStatValue() {
        return value;
    }
}
