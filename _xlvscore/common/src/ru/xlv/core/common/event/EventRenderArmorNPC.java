package ru.xlv.core.common.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.EntityLivingBase;

@AllArgsConstructor
@Getter
public class EventRenderArmorNPC extends Event {
    private final EntityLivingBase entityLivingBase;
    private final RenderLiving render;
}
