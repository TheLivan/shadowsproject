package ru.xlv.core.common.item;

import lombok.AccessLevel;
import lombok.Getter;
import ru.xlv.core.common.item.quality.EnumItemQuality;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Getter
public class ConfigItemBase implements IConfigGson {

    private static final String FILE_PATH_FORMAT = "config/xlvscore/items/%s.json";

    @Configurable(comment = "Название предмета.")
    private String displayName = "";
    @Configurable(comment = "Второе название предмета.")
    private String secondName = "";

    @Configurable(comment = "Ширина предмета в клетках. Минимальное значение: 1")
    private final int matrixWidth = 1;
    @Configurable(comment = "Высота предмета в клетках. Минимальное значение: 1")
    private final int matrixHeight = 1;

    @Configurable(comment = "Качество предмета. Возможные варианты: GARBAGE - Мусорный,WORN_OUT - Изношенное,LOW_QUALITY - Низкокачественное,FMCG - Ширпотреб,BASIC - Базовое качество, HIGH - Повышенное качество, DISTINCTIVE - Отличительное качество, HIGHEST - Наивысшее качество, PERFECT - Идеальное качество, CUSTOMIZED - Заказное качество")
    private EnumItemQuality itemQuality = EnumItemQuality.BASIC;
    @Configurable(comment = "Редкость предмета. Возможные варианты: SLAG - Шлаковый, COMMON - Распространенный, UNCOMMON - Необычный, RARE - Редкий, EPIC - Эпический, SPECIAL - Особый, LEGENDARY - Легендарный")
    private EnumItemRarity itemRarity = EnumItemRarity.COMMON;

    @Configurable(comment = "Описание предмета")
    private String description = "";

    @Configurable(comment = "Лозунг предмета.")
    private String slogan = "";

    @Configurable(comment = "Теги предмета через запятую. Возможные варианты: PISTOL, SMG, AUTOMATIC, RIFLE, CARBINE, SNIPER, SHOTGUN, MACHINE_GUN, LASER_GUN, ENERGY_GUN, UNREGISTERED_ITEM, REGISTERED_ITEM, UNINSURED_ITEM, INSURED_ITEM, PERSONAL_CANDIDATE, PERSONAL")
    private String tags = "";

    @Getter(value = AccessLevel.NONE)
    protected final String itemName;

    public ConfigItemBase(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(FILE_PATH_FORMAT, itemName));
    }
}
