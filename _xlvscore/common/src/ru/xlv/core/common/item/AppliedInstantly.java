package ru.xlv.core.common.item;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AppliedInstantly {

    private final Type type;
    private final int value;

    public enum Type {
        CREDITS,
        EXP
    }
}
