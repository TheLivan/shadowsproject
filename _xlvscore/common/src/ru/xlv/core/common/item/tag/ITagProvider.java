package ru.xlv.core.common.item.tag;

import java.util.List;

public interface ITagProvider {

    List<EnumItemTag> getTagsProvided();
}
