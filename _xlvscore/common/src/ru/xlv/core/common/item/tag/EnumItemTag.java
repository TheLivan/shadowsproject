package ru.xlv.core.common.item.tag;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public enum EnumItemTag {

    PISTOL("Пистолет", CharacterAttributeType.BALLISTIC_DAMAGE),
    REVOLVER("Револьвер", CharacterAttributeType.BALLISTIC_DAMAGE),
    SMG("Пистолет-пулемет", CharacterAttributeType.BALLISTIC_DAMAGE),
    AUTOMATIC("Автоматическое", CharacterAttributeType.BALLISTIC_DAMAGE),
    RIFLE("Винтовка", CharacterAttributeType.BALLISTIC_DAMAGE),
    CARBINE("Карабин", CharacterAttributeType.BALLISTIC_DAMAGE),
    SNIPER("Снайперское", CharacterAttributeType.BALLISTIC_DAMAGE),
    SHOTGUN("Дробовик", CharacterAttributeType.FRACTAL_DAMAGE),
    MACHINE_GUN("Пулемет", CharacterAttributeType.BALLISTIC_DAMAGE),
    LASER_GUN("Лазерное", CharacterAttributeType.BALLISTIC_DAMAGE),
    ENERGY_GUN("Энергетическое", CharacterAttributeType.ENERGY_DAMAGE),
    MELEE("Оружие ближнего боя", CharacterAttributeType.CUT_DAMAGE),

    UNREGISTERED_ITEM("Незарегистрированный"),
    REGISTERED_ITEM("Зарегистрированный"),
    UNINSURED_ITEM("Незастрахованный"),
    INSURED_ITEM("Застрахованный"),
    PERSONAL_CANDIDATE("Личный при получении"),
    PERSONAL("Личный");

    private final String displayName;
    private CharacterAttributeType[] modifAttributeTypes;

    EnumItemTag(String displayName) {
        this.displayName = displayName;
    }

    EnumItemTag(String displayName, CharacterAttributeType... modifAttributeTypes) {
        this.displayName = displayName;
        this.modifAttributeTypes = modifAttributeTypes;
    }

    public boolean hasModifAttributeTypes() {
        return modifAttributeTypes != null && modifAttributeTypes.length > 0;
    }

    public CharacterAttributeType[] getModifAttributeTypes() {
        return modifAttributeTypes;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static void setPersonal(ItemStack itemStack, EntityPlayer entityPlayer) {
        if(itemStack == null) return;
        if(entityPlayer == null) return;
        if(itemStack.getTagCompound() == null) {
            itemStack.setTagCompound(new NBTTagCompound());
        }
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound().getCompoundTag("enumItemTags");
        if(nbtTagCompound == null) {
            itemStack.getTagCompound().setTag("enumItemTags", nbtTagCompound = new NBTTagCompound());
        }
        nbtTagCompound.setString("personalFor", entityPlayer.getCommandSenderName());
        itemStack.getTagCompound().setTag("enumItemTags", nbtTagCompound);
    }

    public static void setItemTags(ItemStack itemStack, EnumItemTag... enumItemTags) {
        if(itemStack == null) return;
        if(itemStack.getTagCompound() == null) {
            itemStack.setTagCompound(new NBTTagCompound());
        }
        NBTTagCompound nbtTagCompound = new NBTTagCompound();
        int[] ii = new int[enumItemTags.length];
        for (int i = 0; i < enumItemTags.length; i++) {
            ii[i] = enumItemTags[i].ordinal();
        }
        ArrayList<Integer> list = new ArrayList<>();
        for (int i : ii) {
            list.add(i);
        }
        writeListToNBT(nbtTagCompound, "tagOrdinals", list, i -> Integer.toString(i));
//        nbtTagCompound.setIntArray("tagOrdinals", ii);
        itemStack.getTagCompound().setTag("enumItemTags", nbtTagCompound);
    }

    public static List<EnumItemTag> getItemTags(ItemStack itemStack) {
        if(itemStack != null && itemStack.getTagCompound() != null) {
            NBTTagCompound nbtTagCompound = itemStack.getTagCompound().getCompoundTag("enumItemTags");
            if(nbtTagCompound != null) {
                List<EnumItemTag> list = new ArrayList<>();
//                int[] tagOrdinals = nbtTagCompound.getIntArray("tagOrdinals");
                if(nbtTagCompound.hasKey("tagOrdinals")) {
                    readListFromNBT(nbtTagCompound, "tagOrdinals", list, s -> EnumItemTag.values()[Integer.parseInt(s)]);
                }
//                if (tagOrdinals != null) {
//                    for (int tagOrdinal : tagOrdinals) {
//                        list.add(EnumItemTag.values()[tagOrdinal]);
//                    }
//                }
                return list;
            }
        }
        return null;
    }

    private static <T> void readListFromNBT(NBTTagCompound nbtTagCompound, String tagName, List<T> list, Function<String, T> function) {
        String string = nbtTagCompound.getString(tagName);
        String[] split = string.split(",");
        for (String s : split) {
            T apply = function.apply(s);
            if (apply != null) {
                list.add(apply);
            }
        }
    }

    private static <T> void writeListToNBT(NBTTagCompound nbtTagCompound, String tagName, List<T> list, Function<T, String> function) {
        if(!list.isEmpty()) {
            StringBuilder string = new StringBuilder();
            for (T t : list) {
                string.append(function.apply(t)).append(",");
            }
            if (string.length() > 0) {
                string = new StringBuilder(string.substring(0, string.length() - 1));
            }
            nbtTagCompound.setString(tagName, string.toString());
        }
    }

    public static boolean hasTag(ItemStack itemStack, EnumItemTag enumItemTag) {
        List<EnumItemTag> itemTags = getItemTags(itemStack);
        if (itemTags != null) {
            return itemTags.contains(enumItemTag);
        }
        return false;
    }

    public static boolean hasTagsAnd(ItemStack itemStack, EnumItemTag... enumItemTags) {
        List<EnumItemTag> itemTags = getItemTags(itemStack);
        if (itemTags != null) {
            for (EnumItemTag enumItemTag : enumItemTags) {
                if(!itemTags.contains(enumItemTag)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean hasTagsOr(ItemStack itemStack, EnumItemTag... enumItemTags) {
        List<EnumItemTag> itemTags = getItemTags(itemStack);
        if (itemTags != null) {
            for (EnumItemTag enumItemTag : enumItemTags) {
                boolean contains = itemTags.contains(enumItemTag);
                if(contains) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean canBeDropped(ItemStack itemStack) {
        List<EnumItemTag> itemTags = getItemTags(itemStack);
        if (itemTags != null) {
            return !areTagsCanSaveItem(itemTags);
        }
        return true;
    }

    public static boolean isTagCanBeDropped(EnumItemTag enumItemTag) {
        return enumItemTag != PERSONAL && enumItemTag != INSURED_ITEM;
    }

    public static boolean isTagCanBeContained(EnumItemTag enumItemTag) {
        return enumItemTag != PERSONAL && enumItemTag != INSURED_ITEM;
    }

    public static boolean areTagsCanSaveItem(List<EnumItemTag> enumItemTags) {
        return (enumItemTags.contains(EnumItemTag.INSURED_ITEM) || enumItemTags.contains(EnumItemTag.PERSONAL));
    }
}
