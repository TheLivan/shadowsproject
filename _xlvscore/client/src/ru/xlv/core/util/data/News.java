package ru.xlv.core.util.data;

import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

@Getter
@ToString
public class News {

    private final String article, text, newsURL;
    private InputStream imageInputStream;

    public News(String article, String text, String imageUrl, String newsURL) {
        this.article = article;
        this.text = text;
        this.newsURL = newsURL;

        if(imageUrl != null && imageUrl.length() > 0) {
            try {

                URL url = new URL(imageUrl);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "NING/1.0");
                imageInputStream = urlConnection.getInputStream();

//                boolean isJPG = imageUrl.endsWith(".jpg");
//                bufferedImage = ImageIO.read(urlConnection.getInputStream());
//                if(isJPG) {
//                    ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
//                    if(ImageIO.write(bufferedImage, "png", byteArrayOut)) {
//                        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOut.toByteArray());
//                        bufferedImage = ImageIO.read(byteArrayInputStream);
//                        byteArrayInputStream.close();
//                    } else {
//                        bufferedImage = null;
//                    }
//                    byteArrayOut.close();
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Empty image!");
        }
    }
}
