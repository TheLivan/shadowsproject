package ru.xlv.core.util;

public class KeyBindingsExecutive extends AbstractKeyExecutive {

    private final Runnable runnable;

    public KeyBindingsExecutive(String description, int keyId, String category, Runnable runnable) {
        super(description, keyId, category);
        this.runnable = runnable;
    }

    public void execute() {
        runnable.run();
    }
}
