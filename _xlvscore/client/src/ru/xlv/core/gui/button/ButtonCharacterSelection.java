package ru.xlv.core.gui.button;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.gui.GuiCharacterMainMenu;
import ru.xlv.core.resource.preload.PreLoadableResource;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

public class ButtonCharacterSelection extends GuiButtonAdvanced {

    private final CharacterType characterType;
    private final int level;
    private boolean isActive;
    
    @PreLoadableResource public static final ResourceLocation resLocIconPlus = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/plus_icon.png");
    @PreLoadableResource public static final ResourceLocation textureBtnCharacter = new ResourceLocation(XlvsMainMod.MODID, "textures/gui/btn_character.png");

    private ResourceLocation classTexture;

    public ButtonCharacterSelection(int buttonId, float x, float y, float widthIn, float heightIn, CharacterType characterType, int level) {
        super(buttonId, x, y, widthIn, heightIn, "");
        this.characterType = characterType;
        this.level = level;
        if(characterType == CharacterType.TANK) {
            classTexture = GuiCharacterMainMenu.textureCharacterTank;
        } else if(characterType == CharacterType.ASSASSIN) {
            classTexture = GuiCharacterMainMenu.textureCharacterStormTrooper;
        } else if(characterType == CharacterType.MEDIC) {
            classTexture = GuiCharacterMainMenu.textureCharacterMedic;
        }
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            Utils.bindTexture(textureBtnCharacter);
            if(isHovered(mouseX, mouseY) && textureHover != null) {
                Utils.bindTexture(textureHover);
            }
            GuiDrawUtils.drawRect(xPosition, yPosition, width, height);

            float rectWidth = ScaleGui.get(530f);
            float rectHeight = ScaleGui.get(81f);
            float x = xPosition + width / 2f - rectWidth / 2f;
            float y = yPosition - rectHeight - ScaleGui.get(16f);
            GuiDrawUtils.drawRect(GuiCharacterMainMenu.resLocCharacterItemBg, x, y, rectWidth, rectHeight);

            if(characterType != null) {
                rectWidth = ScaleGui.get(81f);
                rectHeight = ScaleGui.get(81f);
                GuiDrawUtils.drawRect(classTexture, x, y, rectWidth, rectHeight);
            }

           	rectWidth = ScaleGui.get(10f);
        	rectHeight = ScaleGui.get(11f);
        	x = xPosition + width / 14f;
        	y = yPosition + height / 3f;
            GuiDrawUtils.drawRect(resLocIconPlus, x, y, rectWidth, rectHeight);

            rectWidth = ScaleGui.get(40f);
            rectHeight = ScaleGui.get(40f);
        	x = xPosition + height * 10.4f;
        	y = yPosition - height * 2.6f;
            GuiDrawUtils.drawRect(GuiCharacterMainMenu.resLocEllipse, x, y, rectWidth, rectHeight);

            int j = 14737632;

            if (packedFGColour != 0) {
                j = packedFGColour;
            } else if (!this.enabled) {
                j = 10526880;
            } else if (this.field_146123_n) {
                j = 16777120;
            }
            if(isActive) j = 16777120;
            float textScale = 1.16f;
            GuiDrawUtils.drawStringNoXYScale(FontType.ROBOTO_REGULAR, "Выбрать другого героя", this.xPosition + width / 5.5f, this.yPosition + height / 2.8f, textScale, j);
            drawText();
        }
    }

    @Override
    protected void drawText() {
        float textScale = 2.9f;
        GuiDrawUtils.drawStringNoXYScale(FontType.FUTURA_PT_DEMI, "Класс: " + characterType.getDisplayName(), this.xPosition - width / 2.8f, this.yPosition - ScaleGui.get(68f), textScale, 0xffffff);
//        float textScale = height / 15f;
//        DrawUtils.drawString(FontType.FUTURA_PT_DEMI, characterType.getDisplayName(), this.xPosition - width / 2.4f, this.yPosition - height / (29 / 74f), textScale, textScale, 0xffffff);
//        String charString = "Герой: " + characterType.getDisplayName();
//        textScale = height / 20f;
//        DrawUtils.drawString(FontType.OpenSans_Italic, charString, this.xPosition - width / 2.4f, this.yPosition - height / (29 / 50f), textScale, textScale, 0x6d6d6d);
        String levelString = "" + level;
        textScale = 1.75f;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.FUTURA_PT_BOLD, levelString, this.xPosition + this.width * 1.7f,
        		this.yPosition - height * 2.15f, textScale, 0xe79839);
    }
}
