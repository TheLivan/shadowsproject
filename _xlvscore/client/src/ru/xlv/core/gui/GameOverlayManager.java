package ru.xlv.core.gui;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;
import java.util.List;

public class GameOverlayManager {

    private final List<GameOverlayElement> overlayElements = new ArrayList<>();

    public GameOverlayManager() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void event(RenderGameOverlayEvent.Pre event) {
        overlayElements.forEach(gameOverlayElement -> gameOverlayElement.renderPre(event));
    }

    @SubscribeEvent
    public void event(RenderGameOverlayEvent.Post event) {
        overlayElements.forEach(gameOverlayElement -> gameOverlayElement.renderPost(event));
    }

    public void registerElement(GameOverlayElement gameOverlayElement) {
        gameOverlayElement.init(new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight));
        overlayElements.add(gameOverlayElement);
    }

    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        for(GameOverlayElement gameOverlayElement : overlayElements) {
            gameOverlayElement.mouseClicked(Mouse.getEventX(), Minecraft.getMinecraft().displayHeight - Mouse.getEventY() - 1, mouseButton);
        }
    }

    public synchronized <T extends GameOverlayElement> T getOverlayElement(Class<T> tClass) {
        synchronized (overlayElements) {
            for (GameOverlayElement overlayElement : overlayElements) {
                if(overlayElement.getClass() == tClass) {
                    return tClass.cast(overlayElement);
                }
            }
        }
        return null;
    }

    public List<GameOverlayElement> getOverlayElements() {
        return overlayElements;
    }
}
