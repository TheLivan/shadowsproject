package ru.xlv.core.gui.overlay;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DropdownElement {

    private final String name;
    private final Runnable runnable;
}
