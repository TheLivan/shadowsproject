package ru.xlv.core.achievement;

import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AchievementUtils {

    public static Achievement readAchievement(ByteBufInputStream byteBufInputStream) throws IOException {
        Achievement achievement = new Achievement();
        achievement.setName(byteBufInputStream.readUTF());
        achievement.setDescription(byteBufInputStream.readUTF());
        achievement.setImageURL(byteBufInputStream.readUTF());
        achievement.setMaxProgress(byteBufInputStream.readInt());
        achievement.setProgress(byteBufInputStream.readInt());
        return achievement;
    }

    public static List<Achievement> readAchievementList(ByteBufInputStream byteBufInputStream) throws IOException {
        List<Achievement> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(readAchievement(byteBufInputStream));
        }
        return list;
    }
}
