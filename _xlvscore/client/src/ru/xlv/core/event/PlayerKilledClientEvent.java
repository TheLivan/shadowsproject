package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;

@Getter
@RequiredArgsConstructor
public class PlayerKilledClientEvent extends Event {

    private final EntityPlayer entityPlayer;
}
