package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import ru.xlv.core.common.util.CompletableFutureBuilder;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.resource.ResourceManager;
import ru.xlv.mochar.XlvsMainMod;

public class TickListener {

    @SubscribeEvent
    public void event(TickEvent.ClientTickEvent event) {
        if(event.phase == TickEvent.Phase.START) {
            CompletableFutureBuilder.SyncQueueHandler.update();
        }
    }

    @SubscribeEvent
    public void event(TickEvent.RenderTickEvent event) {
        if(event.phase == TickEvent.Phase.END) {
            ResourceManager.updateSync();
            ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
            if(clientMainPlayer != null) clientMainPlayer.updateUsableItem();
        }
    }
}
