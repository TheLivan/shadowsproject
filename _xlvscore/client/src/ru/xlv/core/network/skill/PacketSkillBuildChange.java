package ru.xlv.core.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketSkillBuildChange implements IPacketCallbackEffective<Boolean> {

    private int skillBuildIndex;

    private boolean success;

    public PacketSkillBuildChange(int skillBuildIndex) {
        this.skillBuildIndex = skillBuildIndex;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(skillBuildIndex);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        new PacketSkillBuildSync().read(bbis);
        success = bbis.readBoolean();
    }

    @Nullable
    @Override
    public Boolean getResult() {
        return success;
    }
}
