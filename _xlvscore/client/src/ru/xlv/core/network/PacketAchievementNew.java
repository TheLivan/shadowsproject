package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementUtils;
import ru.xlv.core.achievement.Achievement;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketAchievementNew implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        Achievement achievement = AchievementUtils.readAchievement(bbis);
        XlvsCore.INSTANCE.getAchievementHandler().onAchieved(achievement);
    }
}
