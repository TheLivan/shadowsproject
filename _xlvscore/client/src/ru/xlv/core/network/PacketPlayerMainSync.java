package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.player.character.*;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlayerMainSync implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        XlvsMainMod.INSTANCE.getClientMainPlayer().setCharacterType(CharacterType.values()[bbis.readInt()]);
        XlvsMainMod.INSTANCE.getClientMainPlayer().setCredits(bbis.readInt());
        XlvsMainMod.INSTANCE.getClientMainPlayer().setPlatinum(bbis.readDouble());
        XlvsMainMod.INSTANCE.getClientMainPlayer().setAvailableHotSlotAmount(bbis.readInt());
        int c = bbis.readInt();
        XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterDebuffMap().clear();
        for (int i = 0; i < c; i++) {
            CharacterDebuffType characterDebuffType = CharacterDebuffType.values()[bbis.readInt()];
            long creationTimeMills = bbis.readLong();
            long period = bbis.readLong();
            CharacterDebuff characterDebuff = new CharacterDebuff(characterDebuffType, period);
            characterDebuff.setCreationTimeMills(creationTimeMills);
            XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterDebuffMap().put(characterDebuffType, characterDebuff);
        }
        c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            ExperienceType experienceType = ExperienceType.values()[bbis.readInt()];
            double exp = bbis.readDouble();
            XlvsMainMod.INSTANCE.getClientMainPlayer().updateExperience(experienceType, exp);
        }
        c = bbis.readInt();
        for (int i = 0; i < c; i++) {
            XlvsMainMod.INSTANCE.getClientMainPlayer().updateCharacterAttribute(CharacterAttributeType.values()[bbis.readInt()], bbis.readDouble(), bbis.readDouble());
        }
        XlvsMainMod.INSTANCE.getClientMainPlayer().updateDiscoveredLocationNames(readList(bbis, bbis::readUTF));
    }
}
