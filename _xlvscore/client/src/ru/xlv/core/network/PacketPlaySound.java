package ru.xlv.core.network;

import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlaySound implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        boolean isGuiSound = bbis.readBoolean();
        boolean bySoundKey = bbis.readBoolean();
        String soundKey;
        if(bySoundKey) {
            soundKey = bbis.readUTF();
        } else {
            soundKey = SoundType.values()[bbis.readInt()].getSoundKey();
        }
        if(!isGuiSound) {
            float volume = bbis.readFloat();
            float pitch = bbis.readFloat();
            double x = bbis.readDouble();
            double y = bbis.readDouble();
            double z = bbis.readDouble();
            SoundUtils.playSound(Minecraft.getMinecraft().theWorld, x, y, z, soundKey, volume, pitch, false);
        } else {
            SoundUtils.playGuiSound(soundKey);
        }
    }
}
