package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.inventory.MatrixInventoryIO;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixPlayerInventorySync implements IPacketIn, IPacketOut {

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        MatrixInventoryIO.read(XlvsMainMod.INSTANCE.getClientMainPlayer().getMatrixInventory(), data);
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {

    }
}
