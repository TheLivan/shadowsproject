package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

public class PacketMatrixInventorySpecItemMove implements IPacketOut {

    private boolean fromSpec;
    private MatrixInventory.SlotType slotType;
    private int x, y;

    public PacketMatrixInventorySpecItemMove(boolean fromSpec, MatrixInventory.SlotType slotType, int x, int y) {
        this.fromSpec = fromSpec;
        this.slotType = slotType;
        this.x = x;
        this.y = y;
    }

    public PacketMatrixInventorySpecItemMove() {
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(fromSpec);
        bbos.writeInt(x);
        bbos.writeInt(y);
        bbos.writeInt(slotType.ordinal());
    }
}
