package ru.xlv.core.network.matrix;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.inventory.MatrixInventoryIO;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.mochar.XlvsMainMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionSync implements IPacketIn {

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        MatrixInventoryIO.read(XlvsMainMod.INSTANCE.getClientMainPlayer().getTransactionMatrixInventory(), bbis);
    }
}
