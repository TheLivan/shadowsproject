package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionTakeAll implements IPacketOut {
    @Override
    public void write(ByteBufOutputStream data) throws IOException {}
}
