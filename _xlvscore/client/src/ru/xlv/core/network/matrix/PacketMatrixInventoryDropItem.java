package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketMatrixInventoryDropItem implements IPacketOut {

    private int x, y;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(x);
        bbos.writeInt(y);
    }
}
