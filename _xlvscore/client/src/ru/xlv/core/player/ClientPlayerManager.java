package ru.xlv.core.player;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.inventory.MatrixInventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClientPlayerManager {

    public static final ClientPlayerManager INSTANCE = new ClientPlayerManager();

    private final List<ClientPlayer> clientPlayerList = Collections.synchronizedList(new ArrayList<>());

    private ClientPlayerManager() {}

    public void syncPlayer(ClientPlayer clientPlayer) {
        ClientPlayer clientPlayer1 = getPlayerByName(clientPlayer.getPlayerName());
        if(clientPlayer1 == null) {
            clientPlayerList.add(clientPlayer);
            return;
        }
        clientPlayer1.setCharacterType(clientPlayer.getCharacterType());
        clientPlayer1.setLevel(clientPlayer.getLevel());
        clientPlayer1.setLastOnlinePeriodTimeMills(clientPlayer.getLastOnlinePeriodTimeMills());
        if(clientPlayer.getCurrentBracers() != clientPlayer1.getCurrentBracers()) {
            EntityPlayer playerEntityByName = Minecraft.getMinecraft().theWorld.getPlayerEntityByName(clientPlayer1.getPlayerName());
            if(playerEntityByName != null && playerEntityByName.getDistanceToEntity(Minecraft.getMinecraft().thePlayer) < 64F) {
                playerEntityByName.inventory.setInventorySlotContents(MatrixInventory.SlotType.BRACERS.getAssociatedSlotIndex(), clientPlayer.getCurrentBracers());
            }
        }
        clientPlayer1.setCurrentBracers(clientPlayer.getCurrentBracers());
    }

    public ClientPlayer getPlayerByName(String name) {
        for (ClientPlayer clientPlayer : clientPlayerList) {
            if(clientPlayer.getPlayerName().equals(name)) {
                return clientPlayer;
            }
        }
        return null;
    }

    public ClientPlayer getPlayer(EntityPlayer entityPlayer) {
        return getPlayerByName(entityPlayer.getCommandSenderName());
    }
}
