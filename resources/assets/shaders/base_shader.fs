//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330
out vec4 outputColor;

#define DIRECTIONAL_LIGHTS 1

in vec2 texCoords;
in vec2 lightMapCoords;
in vec4 color;
in vec3 position;
in vec3 normal;
in vec3 tangent;
in vec3 delta;

uniform bool useTexture;
uniform sampler2D diffuse;
uniform bool useLightMap;
uniform sampler2D lightMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D emissionMap;
uniform sampler2D glossMap;

uniform bool useNormalMapping;
uniform bool useSpecularMapping;
uniform bool useEmissionMapping;
uniform bool useGlossMapping;

uniform float emissionPower;

uniform bool useDirectLight;
uniform bool usePointLights;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

vec3 calculateDirectLight(in DirLight light, in vec3 E, in vec3 normal, in vec3 pos, in float specularUnit, in float shininess) {
	vec3 L = normalize(delta);
	float nDotL = max(dot(normal,L), 0.0);

	if(nDotL > 0)
	{
		vec3 R = normalize(-reflect(L,normal));
		float rDotE = max(dot(R,E),0.0);
		float power = pow(rDotE, 1.0 + 127.0 * shininess);
		vec3 amb = light.color / 6.0;		
		vec3 dif = light.color * nDotL; 
		vec3 spec = light.color * power * specularUnit * light.specular;

		vec3 final = amb + dif + spec;
			
		return final;
	} else return vec3(0,0,0);
}

void main() {
	vec4 diffuseColor = vec4(1.0,1.0,1.0,1.0);
	if(useTexture) {
		diffuseColor = texture(diffuse, texCoords) * color;
	} else {
		diffuseColor = color;
	}
	
	vec3 lightMapColor = vec3(1.0, 1.0, 1.0);
	
	if(useLightMap) {
		vec2 newLightMapCoords = lightMapCoords;
	
		outputColor.rgb = texture(lightMap, newLightMapCoords).xyz;//apply default mc lightmap
		
		if(usePointLights) newLightMapCoords.x = 0.05;
		lightMapColor = texture(lightMap, newLightMapCoords).xyz;//lightmap for dir light
	} else {
		outputColor.rgb = vec3(1.0, 1.0, 1.0);
	}
	
	outputColor.w = diffuseColor.w;
	
	if(useEmissionMapping) {
		vec3 em = texture(emissionMap, texCoords).rgb * emissionPower;
		outputColor.rgb += em;
	}

	if(useDirectLight) {
		vec3 unitNormal = normal;
		float specularUnit = 1.0;
		float shininess = 0.25; 
	
		if(useNormalMapping) {
			vec4 normalTex = texture(normalMap, texCoords);
			vec4 normalMapValue = 2.0 * normalTex - 1.0;
			vec3 bitang = cross(normal, tangent);
			unitNormal = normalize(normalMapValue.rgb);
		
			mat3 toTangentSpace = mat3(
				tangent, bitang, normal
			);
			
			unitNormal = normalize(toTangentSpace * unitNormal);
		}
		
		if(useSpecularMapping) {
			specularUnit = texture(specularMap, texCoords).r;
		}
		
		if(useGlossMapping) {
			shininess = texture(glossMap, texCoords).r;
		}
		
		vec3 E = normalize(-position.xyz);
		vec3 lightValue = lightMapColor * calculateDirectLight(directLight[0], E, unitNormal, position, specularUnit, shininess);

		outputColor.rgb += lightValue;
		outputColor.rgb *= diffuseColor.rgb;
	} else {
		outputColor.rgb *= diffuseColor.rgb;
	}
}