#version 120

varying vec2 textureCoords;

uniform sampler2D textureScene;
uniform sampler2D textureBloom;
uniform float exposure;
uniform float gamma;
uniform bool useGammaAndExposure;

void main() {	
	if(useGammaAndExposure) {
		vec3 col = texture2D(textureScene,textureCoords).rgb;//*(1.5-dot(textureCoords-0.5,textureCoords-0.5)*2.);

		vec3 bloom = texture2D(textureBloom,textureCoords/2.).rgb;
	
		col += bloom*100./6.;
	
		vec3 result = vec3(1.0) - exp(-col * exposure);
		result = pow(result, vec3(1.0 / gamma));
		gl_FragColor = vec4(result, 1.0);
	} else {
		vec3 bloom = texture2D(textureScene,textureCoords/2.).rgb;
		
		gl_FragColor = vec4(bloom*100./6., 1.0);
	}
}