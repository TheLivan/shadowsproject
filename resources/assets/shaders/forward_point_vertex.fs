//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330
out vec4 outputColor;

#define POINT_LIGHTS 1

in vec2 texCoords;
in vec2 lightMapCoords;
in vec4 color;
in vec3 lightColor;

uniform bool useTexture;
uniform bool useLightMap;
uniform sampler2D diffuse;
uniform sampler2D lightMap;

void main() {

	vec4 diffuseColor = vec4(1.0,1.0,1.0,1.0);
	
	if(useTexture) {
		diffuseColor = texture(diffuse, texCoords) * color;
	} else {
		diffuseColor = color;
	}
	
	//vec3 lightMapColor = vec3(1.0, 1.0, 1.0);
	
	//if(useLightMap) {
	//	vec2 newLightMapCoords = lightMapCoords;
	//	newLightMapCoords.x = 0.05;
	//	lightMapColor = texture(lightMap, newLightMapCoords).xyz;
	//}
	
	outputColor.w = diffuseColor.w;
	outputColor.rgb = diffuseColor.rgb * lightColor;
}