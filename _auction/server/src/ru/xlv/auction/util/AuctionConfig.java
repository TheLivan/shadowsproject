package ru.xlv.auction.util;

import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;

@Configurable
public class AuctionConfig implements IConfigGson {

    public double taxPerLot = 0.05D;
    public double depositPerLot = 0.025D;

    public double termTypeFewDaysModifier = 1.0D;
    public double tempTypeOneWeekModifier = 1.5D;
    public double tempTypeTwoWeeksModifier = 2.0D;

    @Override
    public File getConfigFile() {
        return new File("config/auction/config.json");
    }
}
