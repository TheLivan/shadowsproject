package ru.xlv.auction.handle.lot.result;

import ru.xlv.auction.XlvsAuctionMod;

public enum AuctionBetResult {

    SUCCESS(XlvsAuctionMod.INSTANCE.getLocalization().responseBetSuccess),
    TOO_LOW_BID(XlvsAuctionMod.INSTANCE.getLocalization().responseBetTooLowBid),
    PLAYER_NOT_FOUND(XlvsAuctionMod.INSTANCE.getLocalization().responseBetPlayerNotFound),
    NOT_ENOUGH_MONEY(XlvsAuctionMod.INSTANCE.getLocalization().responseBetNotEnoughMoney),
    DATABASE_ERROR(XlvsAuctionMod.INSTANCE.getLocalization().responseBetDatabaseError),
    NOT_ACTIVE_LOT(XlvsAuctionMod.INSTANCE.getLocalization().responseBetNotActiveLot),
    LOT_NOT_FOUND(XlvsAuctionMod.INSTANCE.getLocalization().responseBetLotNOtFound),
    UNKNOWN_ERROR(XlvsAuctionMod.INSTANCE.getLocalization().responseBetUnknown);

    private String responseMessage;

    AuctionBetResult(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
