package ru.xlv.auction.handle;

import lombok.Getter;
import net.minecraft.item.ItemStack;
import ru.xlv.auction.XlvsAuctionMod;
import ru.xlv.auction.common.lot.AuctionBet;
import ru.xlv.auction.common.lot.LotState;
import ru.xlv.auction.handle.lot.AuctionLot;
import ru.xlv.auction.handle.lot.TermType;
import ru.xlv.auction.handle.lot.result.AuctionBetResult;
import ru.xlv.auction.handle.lot.result.AuctionPlaceResult;
import ru.xlv.auction.handle.lot.result.AuctionReturnResult;
import ru.xlv.auction.util.AuctionLocalization;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementType;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.common.util.Logger;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachmentCredits;
import ru.xlv.post.common.PostAttachmentItemStack;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.result.PostSendResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class AuctionHandler {

    @Getter
    private final List<AuctionLot> auctionLotList = Collections.synchronizedList(new ArrayList<>());

    private final Logger LOGGER = new Logger("Auction");

    public void init() {
        XlvsAuctionMod.INSTANCE.getDatabaseManager().getAllLots().thenAccept(auctionLotList::addAll);
    }

    public void shutdown() {}

//    public CompletableFuture<AuctionBuyResult> buyLot(String username, AuctionLot lot) {
//        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayerByName(username);
//        if (serverPlayer == null) {
//            return CompletableFuture.completedFuture(AuctionBuyResult.BUYER_NOT_FOUND);
//        }
//        int balance = serverPlayer.getSelectedCharacter().getWallet().getCurrency();
//        if (balance < lot.getStartCost()) {
//            return CompletableFuture.completedFuture(AuctionBuyResult.NOT_ENOUGH_MONEY);
//        }
//        if (lot.getLotState() != LotState.ACTIVE) {
//            return CompletableFuture.completedFuture(AuctionBuyResult.NOT_ACTIVE_LOT);
//        }
//        ServerPlayer seller = XlvsCore.INSTANCE.getPlayerManager().getPlayerByName(lot.getSeller());
//        if (seller == null) {
//            return CompletableFuture.completedFuture(AuctionBuyResult.SELLER_NOT_FOUND);
//        }
//        if(!serverPlayer.getSelectedCharacter().getWallet().consume(lot.getStartCost())) {
//            return CompletableFuture.completedFuture(AuctionBuyResult.WITHDRAW_ERROR);
//        }
//        seller.getSelectedCharacter().getWallet().add((int) (lot.getStartCost() - lot.getStartCost() * XlvsAuctionMod.INSTANCE.getConfig().taxPerLot + lot.getStartCost() * XlvsAuctionMod.INSTANCE.getConfig().depositPerLot));
//
//        lot.setLotState(LotState.COMPLETED);
//
//        return XlvsAuctionMod.INSTANCE.getDatabaseManager().updateLot(lot).handle((aBoolean, throwable2) -> {
//            if (aBoolean && throwable2 == null) {
//                return AuctionBuyResult.SUCCESS;
//            }
//            return AuctionBuyResult.DATABASE_ERROR;
//        });
//    }

    public CompletableFuture<AuctionBetResult> makeBet(String username, int amount, String uuid) {
        AuctionLot auctionLot = getAuctionLotByUuid(uuid);
        if(auctionLot == null) {
            return CompletableFuture.completedFuture(AuctionBetResult.LOT_NOT_FOUND);
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer == null) {
            return CompletableFuture.completedFuture(AuctionBetResult.PLAYER_NOT_FOUND);
        }
        AuctionBet lastBet = getLastBet(auctionLot);
        if(lastBet != null && amount <= lastBet.getAmount()) {
            return CompletableFuture.completedFuture(AuctionBetResult.TOO_LOW_BID);
        }
        if(auctionLot.getLotState() != LotState.ACTIVE) {
            return CompletableFuture.completedFuture(AuctionBetResult.NOT_ACTIVE_LOT);
        }
        int balance = serverPlayer.getSelectedCharacter().getWallet().getCredits();
        AuctionBet auctionBet = getLastBet(auctionLot);
        if(balance < auctionBet.getAmount() || amount < auctionBet.getAmount() || !serverPlayer.getSelectedCharacter().getWallet().consumeCredits(amount)) {
            return CompletableFuture.completedFuture(AuctionBetResult.NOT_ENOUGH_MONEY);
        }
        AuctionBet auctionBet1 = new AuctionBet(username, amount);
        return XlvsAuctionMod.INSTANCE.getDatabaseManager().addBet(auctionLot, auctionBet1).handle((aBoolean, throwable) -> {
            if(!aBoolean || throwable != null) {
                return AuctionBetResult.DATABASE_ERROR;
            }
            if(returnLastBet(auctionLot) == PostSendResult.SUCCESS) {
                auctionLot.getBets().add(auctionBet1);
                return AuctionBetResult.SUCCESS;
            }
            return AuctionBetResult.UNKNOWN_ERROR;
        });
    }

    public CompletableFuture<AuctionPlaceResult> placeLot(String username, int matrixX, int matrixY, int cost, TermType termType) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if(serverPlayer == null) {
            return CompletableFuture.completedFuture(AuctionPlaceResult.UNKNOWN_ERROR);
        }
        final double finalCost = (cost + cost * XlvsAuctionMod.INSTANCE.getConfig().depositPerLot) + termType.getModifier();
        int balance = serverPlayer.getSelectedCharacter().getWallet().getCredits();
        if(balance < finalCost || !serverPlayer.getSelectedCharacter().getWallet().consumeCredits((int) (finalCost))) {
            return CompletableFuture.completedFuture(AuctionPlaceResult.NOT_ENOUGH_MONEY);
        }
        ItemStack itemStack = serverPlayer.getSelectedCharacter().getMatrixInventory().getMatrixItem(matrixX, matrixY);
        if (itemStack == null || !serverPlayer.getSelectedCharacter().getMatrixInventory().removeItem(itemStack)) {
            return CompletableFuture.completedFuture(AuctionPlaceResult.NOT_FOUND_ITEM);
        }
        AuctionLot auctionLot = createAuctionLot(username, itemStack, cost);
        return XlvsAuctionMod.INSTANCE.getDatabaseManager().placeLot(auctionLot).handle((auctionLot1, throwable) -> {
            if(throwable != null) {
                throwable.printStackTrace();
                return AuctionPlaceResult.DATABASE_ERROR;
            }
            auctionLotList.add(auctionLot1);
            return AuctionPlaceResult.SUCCESS;
        });
    }

    public CompletableFuture<AuctionReturnResult> returnLot(AuctionLot auctionLot) {
        AuctionLocalization auctionLocalization = XlvsAuctionMod.INSTANCE.getLocalization();
        boolean sold = !auctionLot.getBets().isEmpty();
        String message = sold ? auctionLocalization.getFormatted(auctionLocalization.postLotReturnMessage, auctionLot.getSeller(), auctionLot.getItemStack())
                : auctionLocalization.getFormatted(auctionLocalization.postLotSoldMessage, auctionLot.getSeller(), auctionLot.getItemStack(), getLastBet(auctionLot).getAmount());
        PostObject postObject = new PostObject(
                auctionLocalization.postDefaultArticle,
                message,
                auctionLocalization.postAccountName,
                auctionLot.getSeller()
        );
        if(!sold) {
            postObject.getAttachments().add(new PostAttachmentItemStack(auctionLot.getItemStack()));
        } else {
            postObject.getAttachments().add(new PostAttachmentCredits(getLastBet(auctionLot).getAmount()));

        }
        return XlvsPostMod.INSTANCE.getPostHandler().sendPost(postObject).thenApply(postSendResult -> {
            if(postSendResult == PostSendResult.SUCCESS) {
                if(sold) {
                    if (sendLotToBuyer(auctionLot) == PostSendResult.SUCCESS) {
                        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(auctionLot.getSeller());
                        if (serverPlayer != null) {
                            StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
                            ((StatIntProvider) statManager.getStatProvider(StatType.TOTAL_CREDIT_FROM_AUCTION_RECEIVED)).increment(getLastBet(auctionLot).getAmount());
                            statManager.getStatProvider(StatType.TOTAL_ITEMS_SALES_ON_AUCTION).increment();
                        }
                        return AuctionReturnResult.SUCCESS;
                    }
                } else {
                    if (returnLastBet(auctionLot) == PostSendResult.SUCCESS) {
                        return AuctionReturnResult.SUCCESS;
                    }
                }
            } else {
                LOGGER.error("An error has occurred during sending a post of returning the lot: %s", auctionLot);
            }
            return AuctionReturnResult.UNKNOWN_ERROR;
        });
    }

    private PostSendResult sendLotToBuyer(AuctionLot auctionLot) {
        AuctionBet auctionBet = getLastBet(auctionLot);
        if(auctionBet != null) {
            AuctionLocalization auctionLocalization = XlvsAuctionMod.INSTANCE.getLocalization();
            PostObject postObject1 = new PostObject(
                    auctionLocalization.postDefaultArticle,
                    auctionLocalization.getFormatted(auctionLocalization.postLotBoughtMessage, auctionBet.getBidder(), auctionLot.getItemStack()),
                    auctionLocalization.postAccountName,
                    auctionBet.getBidder()
            );
            postObject1.getAttachments().add(new PostAttachmentItemStack(auctionLot.getItemStack()));
            ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(auctionBet.getBidder());
            if (serverPlayer != null) {
                StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
                statManager.getStatProvider(StatType.TOTAL_ITEMS_PURCHASED_FROM_AUCTION).increment();
                ((StatIntProvider) statManager.getStatProvider(StatType.TOTAL_CREDIT_ON_AUCTION_SPENT)).increment(auctionBet.getAmount());
                XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(serverPlayer, AchievementType.BUY_AUCTION_ITEM);
            }
            return XlvsPostMod.INSTANCE.getPostHandler().sendPostSync(postObject1);
        }
        return PostSendResult.SUCCESS;
    }

    /**
     * @return success when a message has been successfully sent or the last bet == null
     * */
    private PostSendResult returnLastBet(AuctionLot auctionLot) {
        AuctionBet auctionBet = getLastBet(auctionLot);
        if(auctionBet != null) {
            AuctionLocalization auctionLocalization = XlvsAuctionMod.INSTANCE.getLocalization();
            PostObject postObject1 = new PostObject(
                    auctionLocalization.postDefaultArticle,
                    auctionLocalization.getFormatted(auctionLocalization.postLotBetReturnMessage, auctionBet.getBidder(), auctionLot.getItemStack()),
                    auctionLocalization.postAccountName,
                    auctionBet.getBidder()
            );
            postObject1.getAttachments().add(new PostAttachmentCredits(auctionBet.getAmount()));
            return XlvsPostMod.INSTANCE.getPostHandler().sendPostSync(postObject1);
        }
        return PostSendResult.SUCCESS;
    }

    private AuctionLot getAuctionLotByUuid(String uuid) {
        for (AuctionLot auctionLot : auctionLotList) {
            if(auctionLot.getUuid().toString().equals(uuid)) {
                return auctionLot;
            }
        }
        return null;
    }

    //perepesat pod sync ? RoflanZadumalsya
    public CompletableFuture<List<AuctionLot>> searchLot(String searchRequest) {
        return CompletableFuture.supplyAsync(() -> auctionLotList.stream()
                .filter(auctionLot -> searchRequest.contains(auctionLot.getSeller()) || searchRequest.contains(auctionLot.getItemStack().getDisplayName())
                        || searchRequest.contains(auctionLot.getItemStack().getItem().getItemStackDisplayName(auctionLot.getItemStack())))
                .collect(Collectors.toList()));
    }

    public CompletableFuture<List<AuctionLot>> getAllLots(String username) {
        return XlvsAuctionMod.INSTANCE.getDatabaseManager().getAllLots(username, LotState.ACTIVE).thenApply(auctionLots -> auctionLots);
    }

    public AuctionLot createAuctionLot(String username, ItemStack itemStack, int cost) {
        AuctionLot auctionLot = new AuctionLot(username, itemStack, cost, System.currentTimeMillis());
        auctionLot.setLotState(LotState.ACTIVE);
        return auctionLot;
    }

    public AuctionBet getLastBet(AuctionLot auctionLot) {
        return auctionLot.getBets().isEmpty() ? null : auctionLot.getBets().get(auctionLot.getBets().size() - 1);
    }
}
