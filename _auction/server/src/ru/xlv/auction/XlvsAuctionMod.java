package ru.xlv.auction;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import lombok.Getter;
import ru.xlv.auction.database.DatabaseEventListener;
import ru.xlv.auction.database.DatabaseManager;
import ru.xlv.auction.handle.AuctionHandler;
import ru.xlv.auction.network.*;
import ru.xlv.auction.util.AuctionConfig;
import ru.xlv.auction.util.AuctionLocalization;
import ru.xlv.core.XlvsCore;

import static ru.xlv.auction.XlvsAuctionMod.MODID;

@Mod(
        name = "XlvsAuctionMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsAuctionMod {

    public static final String MODID = "xlvsauc";

    @Mod.Instance(MODID)
    public static XlvsAuctionMod INSTANCE;

    @Getter
    private AuctionHandler auctionHandler;
    @Getter
    private DatabaseManager databaseManager;
    @Getter
    private final AuctionLocalization localization = new AuctionLocalization();
    @Getter
    private final AuctionConfig config = new AuctionConfig();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        databaseManager = new DatabaseManager();
        databaseManager.init(new DatabaseEventListener());
        auctionHandler = new AuctionHandler();
        auctionHandler.init();


        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketAuctionBet(),
                new PacketAuctionMyList(),
                new PacketAuctionLotsSync(),
                new PacketAuctionPlace(),
                new PacketAuctionSearch()
        );
        config.load();
        localization.load();
    }

    @Mod.EventHandler
    public void event(FMLServerStoppingEvent event) {
        auctionHandler.shutdown();
        databaseManager.shutdown();
    }
}
