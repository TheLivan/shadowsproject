package ru.xlv.auction.common.util;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.item.ItemStack;
import ru.xlv.auction.common.lot.AuctionBet;
import ru.xlv.auction.common.lot.LotState;
import ru.xlv.auction.common.lot.SimpleAuctionLot;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AuctionUtils {

    public static <T extends SimpleAuctionLot> void writeAuctionLot(T auctionLot, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(auctionLot.getSeller());
        byteBufOutputStream.writeInt(auctionLot.getStartCost());
        byteBufOutputStream.writeInt(auctionLot.getLotState().ordinal());
        byteBufOutputStream.writeLong(auctionLot.getTimestamp());
        ByteBufUtils.writeItemStack(byteBufOutputStream.buffer(), auctionLot.getItemStack());
        byteBufOutputStream.writeInt(auctionLot.getBets().size());
        for (AuctionBet bet : auctionLot.getBets()) {
            byteBufOutputStream.writeUTF(bet.getBidder());
            byteBufOutputStream.writeInt(bet.getAmount());
        }
    }

    public static SimpleAuctionLot readAuctionLot(ByteBufInputStream byteBufInputStream) throws IOException {
        String seller = byteBufInputStream.readUTF();
        int startCost = byteBufInputStream.readInt();
        int ordinal = byteBufInputStream.readInt();
        if(ordinal < 0 || ordinal >= LotState.values().length) throw new IOException();
        long timestamp = byteBufInputStream.readLong();
        ItemStack itemStack = ByteBufUtils.readItemStack(byteBufInputStream.getBuffer());
        List<AuctionBet> bets = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            bets.add(new AuctionBet(byteBufInputStream.readUTF(), byteBufInputStream.readInt()));
        }
        SimpleAuctionLot auctionLot = new SimpleAuctionLot(seller, itemStack, startCost, timestamp);
        auctionLot.setLotState(LotState.values()[ordinal]);
        auctionLot.getBets().addAll(bets);
        return auctionLot;
    }

    public static List<SimpleAuctionLot> readAuctionLotList(ByteBufInputStream byteBufInputStream) throws IOException {
        List<SimpleAuctionLot> list = new ArrayList<>();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            list.add(readAuctionLot(byteBufInputStream));
        }
        return list;
    }

    public static void writeAuctionLotList(List<? extends SimpleAuctionLot> list, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(list.size());
        for (SimpleAuctionLot simpleAuctionLot : list) {
            writeAuctionLot(simpleAuctionLot, byteBufOutputStream);
        }
    }
}
