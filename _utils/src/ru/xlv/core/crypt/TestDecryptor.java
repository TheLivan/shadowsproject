package ru.xlv.core.crypt;

import ru.xlv.core.util.Main;

import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Используется только в рамках запуска из среды разработки
 * */
public class TestDecryptor implements IDecryptor {

    private Main main = new Main("zGJtHGNHkFD6zST5+HbQnA==".getBytes());

    private InputStream getDecryptedInputStream0(InputStream inputStream) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        return main.d(inputStream);
    }

    public InputStream getDecryptedInputStream(InputStream inputStream) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            byte[] b = new byte[1024];
            int numberOfBytedRead;
            InputStream is = getDecryptedInputStream0(inputStream);
            while ((numberOfBytedRead = is.read(b)) >= 0) {
                baos.write(b, 0, numberOfBytedRead);
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(baos.toByteArray());
            baos.close();
            return byteArrayInputStream;
        } catch (Throwable throwable) {
            return inputStream;
        }
    }
}
