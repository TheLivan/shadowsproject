package ru.xlv.core.util;

import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class FlexArrays {

    public int[] getIntArrayOf(List<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
}
