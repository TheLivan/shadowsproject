package ru.xlv.core.util;

import ru.xlv.core.api.IResourceLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FolderResourceLoader implements IResourceLoader {

    public static final String TARGET_DIR = "..\\resources";
    public File dir;

    public FolderResourceLoader() {
        dir = new File(TARGET_DIR, "assets");
    }

    @Override
    public InputStream getResourceInputStream(String path) throws IOException {
        return new FileInputStream(new File(dir, path));
    }

    public File getFile() {
        return dir;
    }
}
