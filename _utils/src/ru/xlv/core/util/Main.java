package ru.xlv.core.util;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class Main {
    private byte[] localKey;

    public static byte[] a(byte[] a, byte[] b) {
        try {
            return c(a, b);
        } catch (NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException var3) {
            var3.printStackTrace();
            return a;
        }
    }

    public static byte[] b(String a, byte[] b) {
        try {
            return e(a, b).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | IOException | NoSuchPaddingException var3) {
            var3.printStackTrace();
            return null;
        }
    }

    private static byte[] c(byte[] a, byte[] b) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES");
        b = Base64.getDecoder().decode(b);
        SecretKey secretKey = new SecretKeySpec(b, 0, b.length, "AES");
        cipher.init(2, secretKey);
        return cipher.doFinal(a);
    }

    public Main() {
    }

    public Main(byte[] key) {
        this.localKey = key;
    }

    public CipherInputStream d(InputStream a) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, new SecretKeySpec(this.localKey, 0, this.localKey.length, "AES"));
        return new CipherInputStream(a, cipher);
    }

    private static SecretKey e(String a, byte[] b) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, new SecretKeySpec(b, 0, b.length, "AES"));
        byte[] img = cipher.doFinal(Files.readAllBytes(Paths.get(a)));
        BufferedImage bi = ImageIO.read(new ByteArrayInputStream(img));
        return new SecretKeySpec(f(bi), "AES");
    }

    private static byte[] f(BufferedImage in) {
        List<Byte> bytes = new ArrayList<>();

        int i;
        label31:
        for(int y = 0; y < in.getHeight(); ++y) {
            for(i = 0; i < in.getWidth(); ++i) {
                int color = in.getRGB(i, y);
                byte b = (byte)(color & 3 | (color >> 8 & 7) << 2 | (color >> 16 & 7) << 5);
                if (b == 0) {
                    break label31;
                }

                bytes.add(b);
            }
        }

        byte[] out = new byte[bytes.size()];

        for(i = 0; i < bytes.size(); ++i) {
            out[i] = bytes.get(i);
        }

        return out;
    }
}
