package ru.xlv.core.util;

import java.io.IOException;

public interface ThrCallable<T> {
    T call() throws IOException;
}
