package ru.xlv.container.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.container.entity.EntityContainerServer;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerSyncInteract implements IPacketOutServer {

    private EntityContainerServer entityContainerServer;

    public PacketContainerSyncInteract(EntityContainerServer entityContainerServer) {
        this.entityContainerServer = entityContainerServer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(entityContainerServer.getEntityId());
        bbos.writeUTF(entityContainerServer.getPlayerName());
        bbos.writeInt(entityContainerServer.getCharacterType().ordinal());
        bbos.writeInt(entityContainerServer.getPlayerLevel());
//        MatrixInventoryIO.write(entityContainerServer.getMatrixInventory(), bbos);
    }
}
