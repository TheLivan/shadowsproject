package ru.xlv.container;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.container.entity.EntityContainerServer;
import ru.xlv.container.event.EventListener;
import ru.xlv.container.network.PacketContainerInteract;
import ru.xlv.container.network.PacketContainerSync;
import ru.xlv.container.network.PacketContainerSyncInteract;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;

import static ru.xlv.container.XlvsContainerMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsContainerMod {

    static final String MODID = "xlvscntrs";

    @Mod.Instance(MODID)
    public static XlvsContainerMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        EventListener eventListener = new EventListener();
        XlvsCoreCommon.EVENT_BUS.register(eventListener);
        MinecraftForge.EVENT_BUS.register(eventListener);
        EntityRegistry.registerGlobalEntityID(EntityContainerServer.class, "EntityContainer", 71);
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketContainerInteract(),
                new PacketContainerSync(),
                new PacketContainerSyncInteract()
        );
    }
}
