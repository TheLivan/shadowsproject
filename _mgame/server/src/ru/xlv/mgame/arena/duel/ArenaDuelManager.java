package ru.xlv.mgame.arena.duel;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.schedule.Scheduled;
import ru.xlv.core.util.Flex;
import ru.xlv.mgame.arena.ArenaManager;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

@RequiredArgsConstructor
public class ArenaDuelManager extends ArenaManager<ArenaDuel> {

    @RequiredArgsConstructor
    private static class DuelInvite {
        private static final long REMOVAL_TIME_MILLS = 10000L;
        private final long creationTimeMills = System.currentTimeMillis();
        private final ServerPlayer initiatorPlayer, targetPlayer;
    }

    private final List<DuelInvite> duelInvites = new ArrayList<>();
    private final Queue<ArenaDuel> arenaQueue = new LinkedList<>();

    private final DuelConfig duelConfig;
    private final DuelLocalization localization;

    {
        XlvsCore.INSTANCE.getScheduledAnnotationExecutor().register(this);
    }

    public DuelInviteResult initiateDuel(String playerName, String targetName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(playerName);
        synchronized (duelInvites) {
            if (getDuelInviteByInitiator(serverPlayer) != null) {
                return DuelInviteResult.INVITE_SPAM;
            }
        }
        ServerPlayer serverPlayer1 = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if (serverPlayer1 == null || !serverPlayer1.isOnline()) {
            return DuelInviteResult.PLAYER_NOT_FOUND;
        }
        synchronized (duelInvites) {
            duelInvites.add(new DuelInvite(serverPlayer, serverPlayer1));
        }
        if (XlvsCore.INSTANCE.getNotificationService() != null) {
            XlvsCore.INSTANCE.getNotificationService().sendNotification(serverPlayer1, localization.getFormatted(localization.getNewInviteChatMessage(), playerName));
        }
        return DuelInviteResult.SUCCESS;
    }

    public DuelAcceptResult acceptDuel(String playerName, String initiatorName) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(playerName);
        ServerPlayer initiatorPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(initiatorName);
        DuelInvite duelInviteByInitiator = getDuelInviteByInitiator(initiatorPlayer);
        synchronized (duelInvites) {
            if (!initiatorPlayer.isOnline() || duelInviteByInitiator == null || duelInviteByInitiator.targetPlayer != serverPlayer || !duelInvites.remove(duelInviteByInitiator)) {
                return DuelAcceptResult.NOT_VALID_INVITE;
            }
        }
        ArenaDuel arenaDuel = new ArenaDuel();
        arenaDuel.addPlayer(initiatorPlayer);
        arenaDuel.addPlayer(serverPlayer);
        arenaQueue.add(arenaDuel);
        return DuelAcceptResult.SUCCESS;
    }

    @Nullable
    public DuelInvite getDuelInviteByInitiator(ServerPlayer serverPlayer) {
        synchronized (duelInvites) {
            return Flex.getCollectionElement(duelInvites, duelInvite -> duelInvite.initiatorPlayer == serverPlayer);
        }
    }

    @SuppressWarnings("unused")
    @Scheduled(period = 100L)
    public void update() {
        synchronized (duelInvites) {
            duelInvites.removeIf(duelInvite -> System.currentTimeMillis() - duelInvite.creationTimeMills >= DuelInvite.REMOVAL_TIME_MILLS);
        }
    }

    @Override
    protected void onArenaStopped(ArenaDuel arenaDuel) {
        super.onArenaStopped(arenaDuel);
        synchronized (arenaQueue) {
            while (getArenaList().size() < duelConfig.getWorldPositions().size() && !arenaQueue.isEmpty()) {
                ArenaDuel candidate = arenaQueue.poll();
                arenaDuel.getSpawnPositions().setReserved(false);
                for (DuelConfig.SpawnPositions spawnPositions : duelConfig.getWorldPositions()) {
                    if(!spawnPositions.isReserved()) {
                        candidate.setSpawnPositions(spawnPositions);
                        candidate.start();
                        synchronized (getArenaList()) {
                            getArenaList().add(candidate);
                        }
                        spawnPositions.setReserved(true);
                        break;
                    }
                }
            }
        }
    }
}
