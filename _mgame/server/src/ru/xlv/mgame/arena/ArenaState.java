package ru.xlv.mgame.arena;

public enum ArenaState {

    REGISTRATION,
    PREPARATION,
    MATCH_PROCESS,
    POST_MATCH,
    DONE

}
