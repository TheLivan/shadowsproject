package ru.xlv.armor.item;

import ru.krogenit.armor.item.ItemArmorByPart;
import ru.xlv.armor.util.ConfigArmor;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeType;

public class ItemArmorByPartServer extends ItemArmorByPart {

    public ItemArmorByPartServer(String name, MatrixInventory.SlotType slotType, SetFamily setFamily) {
        super(name, slotType, setFamily);
        ConfigArmor configArmor = new ConfigArmor(name);
        configArmor.load();
        addAttributeProtection(CharacterAttributeType.BALLISTIC_PROTECTION, configArmor.getBallisticProtection());
        addAttributeProtection(CharacterAttributeType.CUT_PROTECTION, configArmor.getCutProtection());
        addAttributeProtection(CharacterAttributeType.ENERGY_PROTECTION, configArmor.getEnergyProtection());
        addAttributeProtection(CharacterAttributeType.THERMAL_PROTECTION, configArmor.getThermalProtection());
        addAttributeProtection(CharacterAttributeType.FIRE_PROTECTION, configArmor.getFireProtection());
        addAttributeProtection(CharacterAttributeType.ELECT_PROTECTION, configArmor.getElectProtection());
        addAttributeProtection(CharacterAttributeType.TOXIC_PROTECTION, configArmor.getToxicProtection());
        addAttributeProtection(CharacterAttributeType.RADIATION_PROTECTION, configArmor.getRadiationProtection());
        addAttributeProtection(CharacterAttributeType.EXPLOSION_PROTECTION, configArmor.getExplosionProtection());
        addAttributeProtection(CharacterAttributeType.WEIGHT, configArmor.getWeight());
        setItemQuality(configArmor.getItemQuality());
        setItemRarity(configArmor.getItemRarity());
        setMaxDamage(configArmor.getHardness());
        setInvMatrixSize(configArmor.getMatrixWidth(), configArmor.getMatrixHeight());
        setDescription(configArmor.getDescription());
        setSlogan(configArmor.getSlogan());
        setDisplayName(configArmor.getDisplayName());
        setSecondName(configArmor.getSecondName());
        String[] split = configArmor.getTags().split(",");
        for (String s : split) {
            if(s.length() > 0) getItemTags().add(EnumItemTag.valueOf(s.trim()));
        }
    }

    public ItemArmorByPartServer(String name, MatrixInventory.SlotType slotType) {
        this(name, slotType, null);
    }

    private void addAttributeProtection(CharacterAttributeType characterAttributeType, double value) {
        if(value > 0D) {
            setCharacterAttributeBoost(characterAttributeType, value);
        }
    }
}
