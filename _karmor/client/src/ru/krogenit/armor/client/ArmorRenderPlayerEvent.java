package ru.krogenit.armor.client;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderPlayerEvent;
import ru.krogenit.armor.CoreArmorClient;
import ru.krogenit.armor.item.ItemArmorByPart;
import ru.krogenit.client.event.EventRenderArmorInventory;
import ru.krogenit.client.event.EventRenderArmorMainMenu;
import ru.xlv.core.common.inventory.MatrixInventory;

public class ArmorRenderPlayerEvent {

    @SubscribeEvent
    public void event(EventRenderArmorInventory e) {
        EntityPlayer p = e.getEntityPlayer();
        RenderPlayer renderer = e.getRenderPlayer();
        for(int i = MatrixInventory.SlotType.HEAD.ordinal(); i <= MatrixInventory.SlotType.FEET.ordinal(); i++) {
            MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
            if(e.getDraggedSlotType() != -1 && MatrixInventory.SlotType.values()[e.getDraggedSlotType()] == slotType)
                continue;
            ItemStack armor = p.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
            if(armor != null && armor.getItem() instanceof ItemArmorByPart) {
                ItemArmorByPart itemArmorByPart = (ItemArmorByPart) armor.getItem();
                IArmorRenderer render = CoreArmorClient.getModelForArmor(armor.getItem().getUnlocalizedName());
                if(render != null) {
                    render.render(renderer.modelBipedMain, itemArmorByPart.getSlotTypes().get(0), p, armor);
                }
            }
        }
    }

    @SubscribeEvent
    public void event(EventRenderArmorMainMenu e) {
        for(int i = MatrixInventory.SlotType.HEAD.ordinal(); i <= MatrixInventory.SlotType.FEET.ordinal(); i++) {
            MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
            String itemName = "";
            switch (slotType) {
                case BODY: itemName = e.getBody(); break;
                case HEAD: itemName = e.getHead(); break;
                case BRACERS: itemName = e.getBracers(); break;
                case LEGS: itemName = e.getPants(); break;
                case FEET: itemName = e.getBoots();
            }

            IArmorRenderer render = CoreArmorClient.getModelForArmor(itemName);
            if(render != null) {
                render.render(e.getRenderPlayer().modelBipedMain, slotType, e.getEntityPlayer(), null);
            }
        }
    }

    @SubscribeEvent
    public void event(RenderPlayerEvent.Specials.Post e) {
        if(e.entityPlayer != null && !e.entityPlayer.isInvisible()) {
            for (int i = MatrixInventory.SlotType.HEAD.ordinal(); i <= MatrixInventory.SlotType.FEET.ordinal(); i++) {
                MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[i];
                ItemStack armor = e.entityPlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
                if (armor != null && armor.getItem() instanceof ItemArmorByPart) {
                    ItemArmorByPart itemArmorByPart = (ItemArmorByPart) armor.getItem();
                    IArmorRenderer render = CoreArmorClient.getModelForArmor(armor.getItem().getUnlocalizedName());
                    if (render != null) {
                        render.render(e.renderer.modelBipedMain, itemArmorByPart.getSlotTypes().get(0), e.entityPlayer, armor);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void event(RenderPlayerEvent.SetArmorModel e) {
        if(e.stack != null && e.stack.getItem() instanceof ItemArmorByPart) {
            e.result = -2;
        }
    }
}
