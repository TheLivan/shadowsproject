package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.PlayerDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillShockBullet extends Skill {

    private final CharacterAttributeMod characterAttributeMod;

    private ServerPlayer serverPlayer;

    public SkillShockBullet(SkillType skillType) {
        super(skillType);
        final double speedMod = 1D + skillType.getCustomParam("speed_add_enemy_value", double.class) / 100D;
        final long period = ((long) (skillType.getCustomParam("delta_time", double.class) * 1000));
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(period)
                .build();
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerDamagePlayerEvent event) {
        if(serverPlayer == event.getAttacker()) {
            event.getTarget().getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        }
    }

    @Nonnull
    @Override
    public SkillShockBullet clone() {
        return new SkillShockBullet(getSkillType());
    }
}
