package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillRestructuringArmor extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillRestructuringArmor(SkillType skillType) {
        super(skillType);
        double protectionMod = 1D + skillType.getCustomParam("protect_add_self_all_value", double.class) / 100D;
        double regen = skillType.getCustomParam("hp_regen_self_value", double.class);
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer.getEntityPlayer().heal((float) regen);
        });
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillRestructuringArmor clone() {
        return new SkillRestructuringArmor(getSkillType());
    }
}
