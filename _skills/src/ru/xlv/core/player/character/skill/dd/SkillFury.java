package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillFury extends ActivableSkill {

    private final double damageMod;
    private final double protectionMod;

    public SkillFury(SkillType skillType) {
        super(skillType);
        damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
        protectionMod = 1D + skillType.getCustomParam("income_damage_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(protectionMod)
                .period(getSkillType().getDuration())
                .build());
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_MOD)
                .valueMod(damageMod)
                .period(getSkillType().getDuration())
                .build());
    }

    @Nonnull
    @Override
    public SkillFury clone() {
        return new SkillFury(getSkillType());
    }
}
