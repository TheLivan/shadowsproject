package ru.xlv.core.player.character.skill.medic;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillCombatBanner extends ActivableSkill {

    private static final UUID CHARACTER_ATTRIBUTE_DAMAGE_BOOST_UUID = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    private double x, y, z;

    public SkillCombatBanner(SkillType skillType) {
        super(skillType);
        final double healthRegen = skillType.getCustomParam("hp_regen_value", double.class);
        final double manaRegen = skillType.getCustomParam("mana_regen_value", double.class);
        final double damageMod = 1D + skillType.getCustomParam("outcome_damage_value", double.class) / 100D;
        final double radius = skillType.getCustomParam("range", double.class);
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_MOD)
                .valueMod(damageMod)
                .period(1000L)
                .uuid(CHARACTER_ATTRIBUTE_DAMAGE_BOOST_UUID)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
            if (playerGroup != null) {
                playerGroup.getPlayers()
                        .stream()
                        .filter(serverPlayer1 -> serverPlayer.getEntityPlayer().getDistance(x, y, z) < radius)
                        .forEach(serverPlayer1 -> {
                            serverPlayer1.getEntityPlayer().heal((float) healthRegen);
                            serverPlayer1.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaRegen);
                            serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
                        });
            } else {
                serverPlayer.getEntityPlayer().heal((float) healthRegen);
                serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaRegen);
                serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            }
        });
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        x = serverPlayer.getEntityPlayer().posX;
        y = serverPlayer.getEntityPlayer().posY;
        z = serverPlayer.getEntityPlayer().posZ;
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            scheduledConsumableTask.update(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillCombatBanner clone() {
        return new SkillCombatBanner(getSkillType());
    }
}
