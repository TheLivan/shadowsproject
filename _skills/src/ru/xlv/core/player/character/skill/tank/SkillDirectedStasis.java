package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillDirectedStasis extends ActivableSkill {

    private final double radius;
    private final long cooldownAdd;
    private final double speedAndDamageModPerSkill;

    public SkillDirectedStasis(SkillType skillType) {
        super(skillType);
        radius = skillType.getCustomParam("range", double.class);
        cooldownAdd = ((long) (skillType.getCustomParam("enemy_skill_add_recharge_time", double.class) * 1000));
        speedAndDamageModPerSkill = skillType.getCustomParam("enemy_speed_and_damage_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        SkillUtils.getEnemyServerPlayersAround(serverPlayer, radius).forEach(serverPlayer1 -> {
            serverPlayer1.getSelectedCharacter().getSkillManager().getSelectedActiveSkills().forEach(activableSkill -> activableSkill.increaseCooldown(cooldownAdd));
            long count = serverPlayer1.getSelectedCharacter().getSkillManager().getSelectedActiveSkills().stream().filter(ActivableSkill::inCooldown).count();
            CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                    .attributeType(CharacterAttributeType.MOVE_SPEED)
                    .valueMod(1D + speedAndDamageModPerSkill * count)
                    .period(getSkillType().getDuration())
                    .build();
            CharacterAttributeMod characterAttributeMod1 = CharacterAttributeMod.builder()
                    .attributeType(CharacterAttributeType.DAMAGE_MOD)
                    .valueMod(1D + speedAndDamageModPerSkill * count)
                    .period(getSkillType().getDuration())
                    .build();
            serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            serverPlayer1.getSelectedCharacter().addAttributeMod(characterAttributeMod1, true);
        });
    }

    @Nonnull
    @Override
    public SkillDirectedStasis clone() {
        return new SkillDirectedStasis(getSkillType());
    }
}
