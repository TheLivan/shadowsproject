package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillCharge extends ActivableSkill {

    private final long reduceCooldown;
    private final double manaAdd;

    public SkillCharge(SkillType skillType) {
        super(skillType);
        reduceCooldown = ((long) (skillType.getCustomParam("reduce_reload_skill_time", double.class) * 1000));
        manaAdd = skillType.getCustomParam("mana_add_self_value", double.class);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().getSkillManager().getSelectedActiveSkills().forEach(activableSkill -> {
            if (activableSkill != null) {
                activableSkill.reduceCooldown(-reduceCooldown);
            }
        });
        serverPlayer.getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaAdd);
    }

    @Nonnull
    @Override
    public SkillCharge clone() {
        return new SkillCharge(getSkillType());
    }
}
