package ru.xlv.core.player.character.skill;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillEmptyActive extends ActivableSkill {

    public SkillEmptyActive(SkillType skillType) {
        super(skillType);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        System.out.println(getClass().getName() + " activated");
    }

    @Nonnull
    @Override
    public SkillEmptyActive clone() {
        return new SkillEmptyActive(getSkillType());
    }
}
