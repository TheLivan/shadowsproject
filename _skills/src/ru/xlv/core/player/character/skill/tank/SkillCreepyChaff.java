package ru.xlv.core.player.character.skill.tank;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillCreepyChaff extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    public SkillCreepyChaff(SkillType skillType) {
        super(skillType);
        double damageMod = 1D + skillType.getCustomParam("outcome_cut_damage_value", double.class) / 100;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.CUT_DAMAGE)
                .valueMod(damageMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true));
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Nonnull
    @Override
    public SkillCreepyChaff clone() {
        return new SkillCreepyChaff(getSkillType());
    }
}
