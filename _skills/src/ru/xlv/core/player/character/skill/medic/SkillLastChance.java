package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillLastChance extends Skill {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final double healthScope;
    private final CharacterAttributeMod characterAttributeMod;

    private ServerPlayer serverPlayer;

    private long startTime;

    public SkillLastChance(SkillType skillType) {
        super(skillType);
        healthScope = skillType.getCustomParam("if_self_hp_is_lower_than", double.class);
        final double damageMod = 1D + skillType.getCustomParam("shield_add_self_value", double.class) / 100D;
        final long period = (long) (skillType.getCustomParam("delta_time", double.class) * 1000);
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.DAMAGE_BASE_PROTECTION)
                .valueMod(damageMod)
                .period(period)
                .uuid(MOD_UUID)
                .build();
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        serverPlayer.getSelectedCharacter().removeAttributeMod(characterAttributeMod);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Post event) {
        if(serverPlayer == event.getServerPlayer() && event.getServerPlayer().getEntityPlayer().getHealth() <= healthScope && System.currentTimeMillis() >= startTime && !event.getServerPlayer().getSelectedCharacter().containsAttributeMod(characterAttributeMod)) {
            event.getServerPlayer().getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
            startTime = System.currentTimeMillis() + getSkillType().getCooldown();
        }
    }

    @Nonnull
    @Override
    public SkillLastChance clone() {
        return new SkillLastChance(getSkillType());
    }
}
