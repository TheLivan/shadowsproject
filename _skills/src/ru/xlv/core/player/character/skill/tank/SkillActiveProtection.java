package ru.xlv.core.player.character.skill.tank;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.event.EntityDamagePlayerEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillActiveProtection extends ActivableSkill {

    private final double damageMod;
    private final double damageLimit;
    private final double manaAdd;

    private ServerPlayer serverPlayer;
    private double damageReceived;

    public SkillActiveProtection(SkillType skillType) {
        super(skillType);
        damageMod = skillType.getCustomParam("income_damage_value", double.class);
        damageLimit = skillType.getCustomParam("each_income_damage_value", double.class);
        manaAdd = skillType.getCustomParam("add_mana", double.class);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    protected void onDeactivated(ServerPlayer serverPlayer) {
        super.onDeactivated(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(EntityDamagePlayerEvent.Post event) {
        if(event.getServerPlayer() == serverPlayer) {
            event.setAmount(event.getAmount() + (float) (event.getAmount() * damageMod));
            damageReceived += event.getAmount();
            double manaAdd = 0;
            while(damageReceived >= damageLimit) {
                damageReceived -= damageLimit;
                manaAdd =+ this.manaAdd;
            }
            if(manaAdd != 0) {
                event.getServerPlayer().getSelectedCharacter().getAttribute(CharacterAttributeType.MANA).addValue(manaAdd);
            }
        }
    }

    @Nonnull
    @Override
    public SkillActiveProtection clone() {
        return new SkillActiveProtection(getSkillType());
    }
}
