package foxz.command;

import foxz.commandhelper.ChMcLogger;
import foxz.commandhelper.annotations.Command;
import foxz.commandhelper.annotations.SubCommand;
import foxz.commandhelper.permissions.OpOnly;
import foxz.commandhelper.permissions.PlayerOnly;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import noppes.npcs.NoppesUtilServer;
import noppes.npcs.constants.EnumGuiType;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.roles.RoleCompanion;
import noppes.npcs.roles.RoleFollower;

import java.util.List;

@Command(
    name = "npc",
    desc = "NPC manipulation",
    usage = "<name> <command>"
)
public class CmdNpc extends ChMcLogger
{
    public EntityNPCInterface selectedNpc;

    CmdNpc(Object ctorParm)
    {
        super(ctorParm);
    }

    @SubCommand(
        desc = "Set Home (respawn place)",
        usage = ""
    )
    public void home(String[] args)
    {
        double posX = (double)this.pcParam.getPlayerCoordinates().posX;
        double posY = (double)this.pcParam.getPlayerCoordinates().posY;
        double posZ = (double)this.pcParam.getPlayerCoordinates().posZ;

        if (args.length == 3)
        {
            posX = CommandBase.func_110666_a(this.pcParam, this.selectedNpc.posX, args[0]);
            posY = CommandBase.func_110665_a(this.pcParam, this.selectedNpc.posY, args[1].trim(), 0, 0);
            posZ = CommandBase.func_110666_a(this.pcParam, this.selectedNpc.posZ, args[2]);
        }

        this.selectedNpc.ai.startPos = new int[] {MathHelper.floor_double(posX), MathHelper.floor_double(posY), MathHelper.floor_double(posZ)};
    }

    @SubCommand(
        desc = "Set npc visibility",
        usage = ""
    )
    public void visible(String[] args)
    {
        if (args.length >= 1)
        {
            boolean bo = args[0].equalsIgnoreCase("true");
            boolean semi = args[0].equalsIgnoreCase("semi");
            int current = this.selectedNpc.display.visible;

            if (semi)
            {
                this.selectedNpc.display.visible = 2;
            }
            else if (bo)
            {
                this.selectedNpc.display.visible = 0;
            }
            else
            {
                this.selectedNpc.display.visible = 1;
            }

            if (current != this.selectedNpc.display.visible)
            {
                this.selectedNpc.updateClient = true;
            }
        }
    }

    @SubCommand(
        desc = "Delete an NPC",
        usage = ""
    )
    public void delete(String[] args)
    {
        this.selectedNpc.delete();
    }

    @SubCommand(
        desc = "Owner",
        usage = ""
    )
    public void owner(String[] args)
    {
        if (args.length < 1)
        {
            EntityPlayer player = null;

            if (this.selectedNpc.roleInterface instanceof RoleFollower)
            {
                player = ((RoleFollower)this.selectedNpc.roleInterface).owner;
            }

            if (this.selectedNpc.roleInterface instanceof RoleCompanion)
            {
                player = ((RoleCompanion)this.selectedNpc.roleInterface).owner;
            }

            if (player == null)
            {
                this.sendmessage("No owner");
            }
            else
            {
                this.sendmessage("Owner is: " + player.getCommandSenderName());
            }
        }
        else
        {
            EntityPlayerMP player1 = CommandBase.getPlayer(this.pcParam, args[0]);

            if (this.selectedNpc.roleInterface instanceof RoleFollower)
            {
                ((RoleFollower)this.selectedNpc.roleInterface).setOwner(player1);
            }

            if (this.selectedNpc.roleInterface instanceof RoleCompanion)
            {
                ((RoleCompanion)this.selectedNpc.roleInterface).setOwner(player1);
            }
        }
    }

    @SubCommand(
        desc = "Set npc name",
        usage = ""
    )
    public void name(String[] args)
    {
        if (args.length >= 1)
        {
            String name = args[0];

            for (int i = 1; i < args.length; ++i)
            {
                name = name + " " + args[i];
            }

            if (!this.selectedNpc.display.name.equals(name))
            {
                this.selectedNpc.display.name = name;
                this.selectedNpc.updateClient = true;
            }
        }
    }

    @SubCommand(
        desc = "Creates an NPC",
        usage = "[name]",
        permissions = {PlayerOnly.class, OpOnly.class}
    )
    public void create(String[] args)
    {
        EntityPlayerMP player = (EntityPlayerMP)this.pcParam;
        World pw = player.getEntityWorld();
        EntityCustomNpc npc = new EntityCustomNpc(pw);

        if (args.length > 0)
        {
            npc.display.name = args[0];
        }

        npc.setPositionAndRotation(player.posX, player.posY, player.posZ, player.cameraYaw, player.cameraPitch);
        npc.ai.startPos = new int[] {MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ)};
        pw.spawnEntityInWorld(npc);
        npc.setHealth(npc.getMaxHealth());
        NoppesUtilServer.sendOpenGui(player, EnumGuiType.MainMenuDisplay, npc);
    }

    public List addTabCompletion(ICommandSender par1, String[] args)
    {
        return args.length == 2 ? CommandBase.getListOfStringsMatchingLastWord(args, new String[] {"create", "home", "visible", "delete", "owner", "name"}): (args.length == 3 && args[1].equalsIgnoreCase("owner") ? CommandBase.getListOfStringsMatchingLastWord(args, MinecraftServer.getServer().getAllUsernames()) : super.addTabCompletion(par1, args));
    }
}
