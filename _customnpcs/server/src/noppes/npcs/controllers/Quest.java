package noppes.npcs.controllers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.ICompatibilty;
import noppes.npcs.NpcMiscInventory;
import noppes.npcs.Server;
import noppes.npcs.VersionCompatibility;
import noppes.npcs.constants.*;
import noppes.npcs.quests.*;

public class Quest implements ICompatibilty
{
    public int version;
    public int id;
    public EnumQuestType type;
    public EnumQuestRepeat repeat;
    public EnumQuestCompletion completion;
    public EnumQuestExperienceType experienceType;
    public int combatReward, researchReward, surviveReward;
    public int credits, platina;
    public String title;
    public QuestCategory category;
    public String logText;
    public String completeText;
    public String completerNpc;
    public int nextQuestid;
    public String nextQuestTitle;
    public PlayerMail mail;
    public String command;
    public QuestInterface questInterface;
    public int rewardExp;
    public NpcMiscInventory rewardItems;
    public boolean randomReward;
    public FactionOptions factionOptions;
    public int dialogAfterQuestId;

    public Quest()
    {
        this.version = VersionCompatibility.ModRev;
        this.id = -1;
        this.type = EnumQuestType.Item;
        this.repeat = EnumQuestRepeat.NONE;
        this.completion = EnumQuestCompletion.Npc;
        this.title = "default";
        this.logText = "";
        this.completeText = "";
        this.completerNpc = "";
        this.nextQuestid = -1;
        this.nextQuestTitle = "";
        this.mail = new PlayerMail();
        this.command = "";
        this.questInterface = new QuestItem();
        this.rewardExp = 0;
        this.rewardItems = new NpcMiscInventory(9);
        this.randomReward = false;
        this.factionOptions = new FactionOptions();
        this.experienceType = EnumQuestExperienceType.COMBAT;
    }

    public void readNBT(NBTTagCompound compound)
    {
        this.id = compound.getInteger("Id");
        this.readNBTPartial(compound);
    }

    public void readNBTPartial(NBTTagCompound compound)
    {
        this.version = compound.getInteger("ModRev");
        VersionCompatibility.CheckAvailabilityCompatibility(this, compound);
        this.setType(EnumQuestType.values()[compound.getInteger("Type")]);
        this.title = compound.getString("Title");
        this.logText = compound.getString("Text");
        this.completeText = compound.getString("CompleteText");
        this.completerNpc = compound.getString("CompleterNpc");
        this.command = compound.getString("QuestCommand");
        this.nextQuestid = compound.getInteger("NextQuestId");
        this.nextQuestTitle = compound.getString("NextQuestTitle");

        if (this.hasNewQuest())
        {
            this.nextQuestTitle = this.getNextQuest().title;
        }
        else
        {
            this.nextQuestTitle = "";
        }

        this.randomReward = compound.getBoolean("RandomReward");
        this.rewardExp = compound.getInteger("RewardExp");
        this.rewardItems.setFromNBT(compound.getCompoundTag("Rewards"));
        this.completion = EnumQuestCompletion.values()[compound.getInteger("QuestCompletion")];
        this.repeat = EnumQuestRepeat.values()[compound.getInteger("QuestRepeat")];
        this.questInterface.readEntityFromNBT(compound);
        this.factionOptions.readFromNBT(compound.getCompoundTag("QuestFactionPoints"));
        this.mail.readNBT(compound.getCompoundTag("QuestMail"));
        this.dialogAfterQuestId = compound.getInteger("DialogAfterQuest");
        this.experienceType = EnumQuestExperienceType.values()[compound.getInteger("experienceType")];
        this.combatReward = compound.getInteger("combatReward");
        this.researchReward = compound.getInteger("researchReward");
        this.surviveReward = compound.getInteger("surviveReward");
        this.credits = compound.getInteger("credits");
        this.platina = compound.getInteger("platina");
    }

    public void setType(EnumQuestType questType)
    {
        this.type = questType;

        if (this.type == EnumQuestType.Item)
        {
            this.questInterface = new QuestItem();
        }
        else if (this.type == EnumQuestType.Dialog)
        {
            this.questInterface = new QuestDialog();
        }
        else if (this.type != EnumQuestType.Kill && this.type != EnumQuestType.AreaKill)
        {
            if (this.type == EnumQuestType.Location)
            {
                this.questInterface = new QuestLocation();
            }
        }
        else
        {
            this.questInterface = new QuestKill();
        }

        if (this.questInterface != null)
        {
            this.questInterface.questId = this.id;
        }
    }

    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        compound.setInteger("Id", this.id);
        return this.writeToNBTPartial(compound);
    }

    public NBTTagCompound writeToNBTPartial(NBTTagCompound compound)
    {
        compound.setInteger("ModRev", this.version);
        compound.setInteger("Type", this.type.ordinal());
        compound.setString("Title", this.title);
        compound.setString("Text", this.logText);
        compound.setString("CompleteText", this.completeText);
        compound.setString("CompleterNpc", this.completerNpc);
        compound.setInteger("NextQuestId", this.nextQuestid);
        compound.setString("NextQuestTitle", this.nextQuestTitle);
        compound.setInteger("RewardExp", this.rewardExp);
        compound.setTag("Rewards", this.rewardItems.getToNBT());
        compound.setString("QuestCommand", this.command);
        compound.setBoolean("RandomReward", this.randomReward);
        compound.setInteger("QuestCompletion", this.completion.ordinal());
        compound.setInteger("QuestRepeat", this.repeat.ordinal());
        this.questInterface.writeEntityToNBT(compound);
        compound.setTag("QuestFactionPoints", this.factionOptions.writeToNBT(new NBTTagCompound()));
        compound.setTag("QuestMail", this.mail.writeNBT());
        compound.setInteger("DialogAfterQuest", this.dialogAfterQuestId);
        compound.setInteger("experienceType", this.experienceType.ordinal());
        compound.setInteger("combatReward", this.combatReward);
        compound.setInteger("researchReward", this.researchReward);
        compound.setInteger("surviveReward", this.surviveReward);
        compound.setInteger("credits", credits);
        compound.setInteger("platina", platina);
        return compound;
    }

    public boolean hasNewQuest()
    {
        return this.getNextQuest() != null;
    }

    public Quest getNextQuest()
    {
        return QuestController.instance == null ? null : QuestController.instance.quests.get(Integer.valueOf(this.nextQuestid));
    }

    public boolean complete(EntityPlayer player, QuestData data)
    {
        if (this.completion == EnumQuestCompletion.Instant)
        {
            Server.sendData((EntityPlayerMP)player, EnumPacketClient.QUEST_COMPLETION, data.quest.writeToNBT(new NBTTagCompound()));
            return true;
        }
        else
        {
            return false;
        }
    }

    public Quest copy()
    {
        Quest quest = new Quest();
        quest.readNBT(this.writeToNBT(new NBTTagCompound()));
        return quest;
    }

    public int getVersion()
    {
        return this.version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }
}
