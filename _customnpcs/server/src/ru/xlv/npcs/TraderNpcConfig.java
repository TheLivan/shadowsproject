package ru.xlv.npcs;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
public class TraderNpcConfig {

    @Getter
    @Configurable
    @RequiredArgsConstructor
    public class TraderNpcInventoryModel implements IConfigGson {
        private final List<ItemStackModel> itemList = new ArrayList<>();
        private String npcName;
        private transient final File configFile;
    }

    @Getter
    @Configurable
    @RequiredArgsConstructor
    public class ItemStackModel {
        private final int itemId;
        private final int metadata;
    }

    private final Map<String, Collection<ItemStack>> TRADER_INVENTORY_MAP = new HashMap<>();

    private final File DIR = new File("config/npc/trade/");

    public void load() {
        File[] files = DIR.listFiles();
        if (files != null) {
            for (File file : files) {
                TraderNpcInventoryModel traderNpcInventoryModel = new TraderNpcInventoryModel(file);
                traderNpcInventoryModel.load();
                List<ItemStack> collect = traderNpcInventoryModel.getItemList().stream()
                        .map(itemStackModel -> new ItemStack(Item.getItemById(itemStackModel.itemId), 1, itemStackModel.metadata))
                        .collect(Collectors.toList());
                TRADER_INVENTORY_MAP.put(traderNpcInventoryModel.getNpcName(), collect);
            }
        }
    }

    public void save(String npcName, Collection<ItemStack> inventory) {
        File file = new File(DIR, npcName + ".json");
        TraderNpcInventoryModel traderNpcInventoryModel = new TraderNpcInventoryModel(file);
        traderNpcInventoryModel.npcName = npcName;
        traderNpcInventoryModel.getItemList().addAll(inventory.stream()
                .map(itemStack -> new ItemStackModel(Item.getIdFromItem(itemStack.getItem()), itemStack.getItemDamage()))
                .collect(Collectors.toList())
        );
        traderNpcInventoryModel.save();
    }

    public Collection<ItemStack> getTraderInventory(String npcName) {
        return TRADER_INVENTORY_MAP.get(npcName);
    }
}
