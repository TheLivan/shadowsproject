package noppes.npcs.constants;

import java.util.ArrayList;

public enum EnumNavType
{
    Default("aitactics.rush"),
    Dodge("aitactics.stagger"),
    Surround("aitactics.orbit"),
    HitNRun("aitactics.hitandrun"),
    Ambush("aitactics.ambush"),
    Stalk("aitactics.stalk"),
    None("gui.none");
    String name;

    private EnumNavType(String name)
    {
        this.name = name;
    }

    public static String[] names()
    {
        ArrayList list = new ArrayList();
        EnumNavType[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3)
        {
            EnumNavType e = var1[var3];
            list.add(e.name);
        }

        return (String[])list.toArray(new String[list.size()]);
    }
}
