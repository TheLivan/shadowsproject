package noppes.npcs.constants;

public enum EnumAnimation
{
    NONE,
    SITTING,
    LYING,
    SNEAKING,
    DANCING,
    AIMING,
    CRAWLING,
    HUG,
    CRY,
    WAVING,
    BOW;

    public int getWalkingAnimation()
    {
        return this == SNEAKING ? 1 : (this == AIMING ? 2 : (this == DANCING ? 3 : (this == CRAWLING ? 4 : (this == HUG ? 5 : 0))));
    }
}
