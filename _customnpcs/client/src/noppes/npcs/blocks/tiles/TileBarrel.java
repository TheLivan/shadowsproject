package noppes.npcs.blocks.tiles;

public class TileBarrel extends TileNpcContainer
{
    public String getName()
    {
        return "tile.npcBarrel.name";
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }
}
