package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCHumanMale extends EntityNPCInterface
{
    public EntityNPCHumanMale(World world)
    {
        super(world);
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
