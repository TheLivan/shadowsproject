package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.constants.EnumAnimation;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNpcMonsterMale extends EntityNPCInterface
{
    public EntityNpcMonsterMale(World world)
    {
        super(world);
        this.display.texture = "customnpcs:textures/entity/monstermale/ZombieSteve.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            npc.ai.animationType = EnumAnimation.HUG;
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
