package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCOrcFemale extends EntityNPCInterface
{
    public EntityNPCOrcFemale(World world)
    {
        super(world);
        this.scaleX = this.scaleY = this.scaleZ = 0.9375F;
        this.display.texture = "customnpcs:textures/entity/orcfemale/StrandedFemaleOrc.png";
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.breasts = 2;
            data.legs.setScale(0.9F, 0.65F);
            data.arms.setScale(0.9F, 0.65F);
            data.body.setScale(1.0F, 0.65F, 1.1F);
            data.head.setScale(0.85F, 0.85F);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
