package noppes.npcs.client.gui.model;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import noppes.npcs.ModelPartData;
import noppes.npcs.client.gui.util.GuiModelInterface;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.client.gui.util.GuiNpcLabel;
import noppes.npcs.entity.EntityCustomNpc;

public class GuiModelArms extends GuiModelInterface
{
    private final String[] arrParticles = new String[] {"gui.no", "Both", "Left", "Right"};
    private GuiScreen parent;

    public GuiModelArms(GuiScreen parent, EntityCustomNpc npc)
    {
        super(npc);
        this.parent = parent;
        this.xOffset = 60;
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        int y = this.guiTop + 20;
        ModelPartData claws = this.playerdata.getPartData("claws");
        GuiNpcButton var10001;
        int var10004 = this.guiLeft + 50;
        y += 22;
        var10001 = new GuiNpcButton(0, var10004, y, 70, 20, this.arrParticles, claws == null ? 0 : claws.type + 1);
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(0, "Claws", this.guiLeft, y + 5, 16777215));

        if (claws != null)
        {
            this.addButton(new GuiNpcButton(10, this.guiLeft + 122, y, 40, 20, claws.getColor()));
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        super.actionPerformed(guiButton);
        GuiNpcButton button = (GuiNpcButton) guiButton;

        if (button.id == 0)
        {
            if (button.getValue() == 0)
            {
                this.playerdata.removePart("claws");
            }
            else
            {
                ModelPartData data = this.playerdata.getOrCreatePart("claws");
                data.type = (byte)(button.getValue() - 1);
            }

            this.initGui();
        }

        if (button.id == 10)
        {
            this.mc.displayGuiScreen(new GuiModelColor(this, this.playerdata.getPartData("claws"), this.npc));
        }
    }

    public void close()
    {
        this.mc.displayGuiScreen(this.parent);
    }
}
