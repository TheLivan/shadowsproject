package noppes.npcs.client.gui.mainmenu;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.DataStats;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.*;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.entity.EntityNPCInterface;

public class GuiNpcStats extends GuiNPCInterface2 implements ITextfieldListener, IGuiData
{
    private DataStats stats;

    public GuiNpcStats(EntityNPCInterface npc)
    {
        super(npc, 2);
        this.stats = npc.stats;
        Client.sendData(EnumPacketServer.MainmenuStatsGet, new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        int y = this.guiTop + 10;
        this.addLabel(new GuiNpcLabel(0, "stats.health", this.guiLeft + 5, y + 5));
        this.addTextField(new GuiNpcTextField(0, this, this.guiLeft + 85, y, 50, 18, this.stats.maxHealth + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(1, Integer.MAX_VALUE, 20);
        this.addLabel(new GuiNpcLabel(1, "stats.aggro", this.guiLeft + 140, y + 5));
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.guiLeft + 220, y, 50, 18, this.stats.aggroRange + ""));
        this.getTextField(1).numbersOnly = true;
        this.getTextField(1).setMinMaxDefault(1, 64, 2);
        this.addLabel(new GuiNpcLabel(34, "stats.creaturetype", this.guiLeft + 275, y + 5));
        this.addButton(new GuiNpcButton(8, this.guiLeft + 355, y, 56, 20, new String[] {"stats.normal", "stats.undead", "stats.arthropod"}, this.stats.creatureType.ordinal()));
        GuiNpcButton var10001;
        int var10004 = this.guiLeft + 82;
        y += 22;
        var10001 = new GuiNpcButton(0, var10004, y, 56, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(2, "stats.respawn", this.guiLeft + 5, y + 5));
        var10004 = this.guiLeft + 82;
        y += 22;
        var10001 = new GuiNpcButton(2, var10004, y, 56, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(5, "stats.meleeproperties", this.guiLeft + 5, y + 5));
        var10004 = this.guiLeft + 82;
        y += 22;
        var10001 = new GuiNpcButton(3, var10004, y, 56, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(6, "stats.rangedproperties", this.guiLeft + 5, y + 5));
        this.addButton(new GuiNpcButton(9, this.guiLeft + 217, y, 56, 20, "selectServer.edit"));
        this.addLabel(new GuiNpcLabel(7, "stats.projectileproperties", this.guiLeft + 140, y + 5));
        var10004 = this.guiLeft + 82;
        y += 34;
        var10001 = new GuiNpcButton(15, var10004, y, 56, 20, "selectServer.edit");
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(15, "potion.resistance", this.guiLeft + 5, y + 5));
        var10004 = this.guiLeft + 82;
        y += 34;
        var10001 = new GuiNpcButton(4, var10004, y, 56, 20, new String[] {"gui.no", "gui.yes"}, this.npc.isImmuneToFire() ? 1 : 0);
        this.addButton(var10001);
        this.addLabel(new GuiNpcLabel(10, "stats.fireimmune", this.guiLeft + 5, y + 5));
        this.addButton(new GuiNpcButton(5, this.guiLeft + 217, y, 56, 20, new String[] {"gui.no", "gui.yes"}, this.stats.canDrown ? 1 : 0));
        this.addLabel(new GuiNpcLabel(11, "stats.candrown", this.guiLeft + 140, y + 5));
        this.addTextField((new GuiNpcTextField(14, this, this.guiLeft + 355, y, 56, 20, this.stats.healthRegen + "")).setNumbersOnly());
        this.addLabel(new GuiNpcLabel(14, "stats.regenhealth", this.guiLeft + 275, y + 5));
        GuiNpcTextField var2;
        int var10005 = this.guiLeft + 355;
        y += 22;
        var2 = new GuiNpcTextField(16, this, var10005, y, 56, 20, this.stats.combatRegen + "");
        this.addTextField(var2.setNumbersOnly());
        this.addLabel(new GuiNpcLabel(16, "stats.combatregen", this.guiLeft + 275, y + 5));
        this.addButton(new GuiNpcButton(6, this.guiLeft + 82, y, 56, 20, new String[] {"gui.no", "gui.yes"}, this.stats.burnInSun ? 1 : 0));
        this.addLabel(new GuiNpcLabel(12, "stats.burninsun", this.guiLeft + 5, y + 5));
        this.addButton(new GuiNpcButton(7, this.guiLeft + 217, y, 56, 20, new String[] {"gui.no", "gui.yes"}, this.stats.noFallDamage ? 1 : 0));
        this.addLabel(new GuiNpcLabel(13, "stats.nofalldamage", this.guiLeft + 140, y + 5));
        GuiNpcButtonYesNo var3;
        var10004 = this.guiLeft + 82;
        y += 22;
        var3 = new GuiNpcButtonYesNo(17, var10004, y, 56, 20, this.stats.potionImmune);
        this.addButton(var3);
        this.addLabel(new GuiNpcLabel(17, "stats.potionImmune", this.guiLeft + 5, y + 5));
        this.addButton(new GuiNpcButtonYesNo(18, this.guiLeft + 217, y, 56, 20, this.stats.attackInvisible));
        this.addLabel(new GuiNpcLabel(18, "stats.attackInvisible", this.guiLeft + 140, y + 5));
    }

    public void unFocused(GuiNpcTextField textfield)
    {
        if (textfield.id == 0)
        {
            this.stats.maxHealth = textfield.getInteger();
            this.npc.heal((float)this.stats.maxHealth);
        }
        else if (textfield.id == 1)
        {
            this.stats.aggroRange = textfield.getInteger();
        }
        else if (textfield.id == 14)
        {
            this.stats.healthRegen = textfield.getInteger();
        }
        else if (textfield.id == 16)
        {
            this.stats.combatRegen = textfield.getInteger();
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        GuiNpcButton button = (GuiNpcButton) guiButton;

        if (button.id == 0)
        {
            this.setSubGui(new SubGuiNpcRespawn(this.stats));
        }
        else if (button.id == 2)
        {
            this.setSubGui(new SubGuiNpcMeleeProperties(this.stats));
        }
        else if (button.id == 3)
        {
            this.setSubGui(new SubGuiNpcRangeProperties(this.stats));
        }
        else if (button.id == 4)
        {
            this.npc.setImmuneToFire(button.getValue() == 1);
        }
        else if (button.id == 5)
        {
            this.stats.canDrown = button.getValue() == 1;
        }
        else if (button.id == 6)
        {
            this.stats.burnInSun = button.getValue() == 1;
        }
        else if (button.id == 7)
        {
            this.stats.noFallDamage = button.getValue() == 1;
        }
        else if (button.id == 8)
        {
            this.stats.creatureType = EnumCreatureAttribute.values()[button.getValue()];
        }
        else if (button.id == 9)
        {
            this.setSubGui(new SubGuiNpcProjectiles(this.stats));
        }
        else if (button.id == 15)
        {
            this.setSubGui(new SubGuiNpcResistanceProperties(this.stats.resistances));
        }
        else if (button.id == 17)
        {
            this.stats.potionImmune = ((GuiNpcButtonYesNo) guiButton).getBoolean();
        }
        else if (button.id == 18)
        {
            this.stats.potionImmune = ((GuiNpcButtonYesNo) guiButton).getBoolean();
        }
    }

    public void save()
    {
        Client.sendData(EnumPacketServer.MainmenuStatsSave, new Object[] {this.stats.writeToNBT(new NBTTagCompound())});
    }

    public void setGuiData(NBTTagCompound compound)
    {
        this.stats.readToNBT(compound);
        this.initGui();
    }
}
