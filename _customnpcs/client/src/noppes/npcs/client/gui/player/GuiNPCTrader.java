package noppes.npcs.client.gui.player;

import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.client.CustomNpcResourceListener;
import noppes.npcs.client.gui.util.GuiContainerNPCInterface;
import noppes.npcs.containers.ContainerNPCTrader;
import noppes.npcs.entity.EntityNPCInterface;
import noppes.npcs.roles.RoleTrader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class GuiNPCTrader extends GuiContainerNPCInterface
{
    private final ResourceLocation resource = new ResourceLocation("customnpcs", "textures/gui/trader.png");
    private final ResourceLocation slot = new ResourceLocation("customnpcs", "textures/gui/slot.png");
    private final RoleTrader role;
    private final ContainerNPCTrader container;

    public GuiNPCTrader(EntityNPCInterface npc, ContainerNPCTrader container)
    {
        super(npc, container);
        this.container = container;
        this.role = (RoleTrader)npc.roleInterface;
        this.closeOnEsc = true;
        this.ySize = 224;
        this.xSize = 223;
        this.title = "role.trader";
    }

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        this.drawWorldBackground(0);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.resource);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
        this.mc.renderEngine.bindTexture(this.slot);

        for (int slot = 0; slot < 18; ++slot)
        {
            int x = this.guiLeft + slot % 3 * 72 + 10;
            int y = this.guiTop + slot / 3 * 21 + 6;
            ItemStack item = this.role.inventoryCurrency.items.get(Integer.valueOf(slot));
            ItemStack item2 = this.role.inventoryCurrency.items.get(Integer.valueOf(slot + 18));

            if (item == null)
            {
                item = item2;
                item2 = null;
            }

            if (NoppesUtilPlayer.compareItems(item, item2, false, false))
            {
                item = item.copy();
                item.stackSize += item2.stackSize;
                item2 = null;
            }

            ItemStack sold = (ItemStack)this.role.inventorySold.items.get(Integer.valueOf(slot));
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mc.renderEngine.bindTexture(this.slot);
            this.drawTexturedModalRect(x + 42, y, 0, 0, 18, 18);

            if (item != null && sold != null)
            {
                RenderHelper.enableGUIStandardItemLighting();

                if (item2 != null)
                {
                    itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, this.mc.renderEngine, item2, x, y + 1);
                    itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, this.mc.renderEngine, item2, x, y + 1);
                }

                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, this.mc.renderEngine, item, x + 18, y + 1);
                itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, this.mc.renderEngine, item, x + 18, y + 1);
                RenderHelper.disableStandardItemLighting();
                this.fontRendererObj.drawString("=", x + 36, y + 5, CustomNpcResourceListener.DefaultTextColor);
            }
        }

        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        RenderHelper.enableStandardItemLighting();
        super.drawGuiContainerBackgroundLayer(f, i, j);
    }

    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        for (int slot = 0; slot < 18; ++slot)
        {
            int x = slot % 3 * 72 + 10;
            int y = slot / 3 * 21 + 6;
            ItemStack item = this.role.inventoryCurrency.items.get(Integer.valueOf(slot));
            ItemStack item2 = this.role.inventoryCurrency.items.get(Integer.valueOf(slot + 18));

            if (item == null)
            {
                item = item2;
                item2 = null;
            }

            if (NoppesUtilPlayer.compareItems(item, item2, this.role.ignoreDamage, this.role.ignoreNBT))
            {
                item = item.copy();
                item.stackSize += item2.stackSize;
                item2 = null;
            }

            ItemStack sold = (ItemStack)this.role.inventorySold.items.get(Integer.valueOf(slot));

            if (item != null && sold != null)
            {
                if (this.func_146978_c(x + 43, y + 1, 16, 16, par1, par2))
                {
                    String title;

                    if (!this.container.canBuy(slot, this.player))
                    {
                        GL11.glTranslatef(0.0F, 0.0F, 300.0F);

                        if (item != null && !NoppesUtilPlayer.compareItems((EntityPlayer)this.player, item, this.role.ignoreDamage, this.role.ignoreNBT))
                        {
                            this.drawGradientRect(x + 17, y, x + 35, y + 18, 1886851088, 1886851088);
                        }

                        if (item2 != null && !NoppesUtilPlayer.compareItems((EntityPlayer)this.player, item2, this.role.ignoreDamage, this.role.ignoreNBT))
                        {
                            this.drawGradientRect(x - 1, y, x + 17, y + 18, 1886851088, 1886851088);
                        }

                        title = StatCollector.translateToLocal("trader.insufficient");
                        this.fontRendererObj.drawString(title, (this.xSize - this.fontRendererObj.getStringWidth(title)) / 2, 131, 14483456);
                        GL11.glTranslatef(0.0F, 0.0F, -300.0F);
                    }
                    else
                    {
                        title = StatCollector.translateToLocal("trader.sufficient");
                        this.fontRendererObj.drawString(title, (this.xSize - this.fontRendererObj.getStringWidth(title)) / 2, 131, 56576);
                    }
                }

                if (this.func_146978_c(x, y, 16, 16, par1, par2) && item2 != null)
                {
                    this.renderToolTip(item2, par1 - this.guiLeft, par2 - this.guiTop);
                }

                if (this.func_146978_c(x + 18, y, 16, 16, par1, par2))
                {
                    this.renderToolTip(item, par1 - this.guiLeft, par2 - this.guiTop);
                }
            }
        }
    }

    public void save() {}

    public RoleTrader getRole() {
        return role;
    }
}
