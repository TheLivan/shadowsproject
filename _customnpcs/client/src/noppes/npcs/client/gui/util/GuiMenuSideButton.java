package noppes.npcs.client.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiMenuSideButton extends GuiNpcButton
{
    public static final ResourceLocation resource = new ResourceLocation("customnpcs", "textures/gui/menusidebutton.png");
    public boolean active;

    public GuiMenuSideButton(int i, int j, int k, String s)
    {
        this(i, j, k, 200, 20, s);
    }

    public GuiMenuSideButton(int i, int j, int k, int l, int i1, String s)
    {
        super(i, j, k, l, i1, s);
        this.active = false;
    }

    public int getHoverState(boolean field_146123_n)
    {
        return this.active ? 0 : 1;
    }

    /**
     * Draws this button to the screen.
     */
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = mc.fontRenderer;
            mc.renderEngine.bindTexture(resource);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            int width = ((int) this.width) + (this.active ? 2 : 0);
            this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + width && mouseY < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, k * 22, width, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            String text = "";
            float maxWidth = width * 0.75F;

            if ((float)fontrenderer.getStringWidth(this.displayString) > maxWidth)
            {
                for (int h = 0; h < this.displayString.length(); ++h)
                {
                    char c = this.displayString.charAt(h);

                    if ((float)fontrenderer.getStringWidth(text + c) > maxWidth)
                    {
                        break;
                    }

                    text = text + c;
                }

                text = text + "...";
            }
            else
            {
                text = this.displayString;
            }

            if (this.active)
            {
                this.drawCenteredString(fontrenderer, text, this.xPosition + width / 2, this.yPosition + (this.height - 8) / 2, 16777120);
            }
            else if (this.field_146123_n)
            {
                this.drawCenteredString(fontrenderer, text, this.xPosition + width / 2, this.yPosition + (this.height - 8) / 2, 16777120);
            }
            else
            {
                this.drawCenteredString(fontrenderer, text, this.xPosition + width / 2, this.yPosition + (this.height - 8) / 2, 14737632);
            }
        }
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {}

    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int i, int j) {}

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        return !this.active && this.visible && this.field_146123_n;
    }
}
