package noppes.npcs.client.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;

public class GuiMenuTopButton extends GuiNpcButton
{
    public static final ResourceLocation resource = new ResourceLocation("customnpcs", "textures/gui/menutopbutton.png");
    protected int height;
    public boolean active;
    public boolean hover;
    public boolean rotated;
    public IButtonListener listener;

    public GuiMenuTopButton(int i, float j, int k, String s)
    {
        super(i, (int) j, k, StatCollector.translateToLocal(s));
        this.hover = false;
        this.rotated = false;
        this.active = false;
        this.width = Minecraft.getMinecraft().fontRenderer.getStringWidth(this.displayString) + 12;
        this.height = 20;
    }

    public GuiMenuTopButton(int i, GuiButton parent, String s)
    {
        this(i, ((int) (parent.xPosition + parent.width)), ((int) parent.yPosition), s);
    }

    public GuiMenuTopButton(int i, GuiButton parent, String s, IButtonListener listener)
    {
        this(i, parent, s);
        this.listener = listener;
    }

    public int getHoverState(boolean field_146123_n)
    {
        byte byte0 = 1;

        if (this.active)
        {
            byte0 = 0;
        }
        else if (field_146123_n)
        {
            byte0 = 2;
        }

        return byte0;
    }

    /**
     * Draws this button to the screen.
     */
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.getVisible())
        {
            GL11.glPushMatrix();
            mc.renderEngine.bindTexture(resource);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            int height = this.height - (this.active ? 0 : 2);
            this.hover = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.getWidth() && mouseY < this.yPosition + height;
            int k = this.getHoverState(this.hover);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, k * 20, this.getWidth() / 2, height);
            this.drawTexturedModalRect(this.xPosition + this.getWidth() / 2, this.yPosition, 200 - this.getWidth() / 2, k * 20, this.getWidth() / 2, height);
            this.mouseDragged(mc, mouseX, mouseY);
            FontRenderer fontrenderer = mc.fontRenderer;

            if (this.rotated)
            {
                GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
            }

            if (this.active)
            {
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.getWidth() / 2, this.yPosition + (height - 8) / 2, 16777120);
            }
            else if (this.hover)
            {
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.getWidth() / 2, this.yPosition + (height - 8) / 2, 16777120);
            }
            else
            {
                this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.getWidth() / 2, this.yPosition + (height - 8) / 2, 14737632);
            }

            GL11.glPopMatrix();
        }
    }

    /**
     * Fired when the mouse button is dragged. Equivalent of MouseListener.mouseDragged(MouseEvent e).
     */
    protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {}

    /**
     * Fired when the mouse button is released. Equivalent of MouseListener.mouseReleased(MouseEvent e).
     */
    public void mouseReleased(int i, int j) {}

    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        boolean bo = !this.active && this.getVisible() && this.hover;

        if (bo && this.listener != null)
        {
            this.listener.actionPerformed(this);
            return false;
        }
        else
        {
            return bo;
        }
    }
}
