package foxz.command;

import foxz.commandhelper.ChMcLogger;
import foxz.commandhelper.annotations.Command;
import foxz.commandhelper.annotations.SubCommand;
import foxz.commandhelper.permissions.OpOnly;
import foxz.commandhelper.permissions.ParamCheck;
import foxz.commandhelper.permissions.PlayerOnly;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentTranslation;
import noppes.npcs.controllers.FactionController;
import noppes.npcs.controllers.PlayerData;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.*;

@Command(
    name = "noppes",
    desc = "noppes root command",
    sub = {CmdClone.class, CmdScript.class, CmdQuest.class, CmdDialog.class, CmdConfig.class}
)
public class CmdNoppes extends ChMcLogger
{
    public CmdFaction cmdfaction;
    public CmdNpc cmdnpc;
    public static Map < String, Class<? >> SlayMap = new LinkedHashMap();

    public CmdNoppes(Object sender)
    {
        super(sender);
        this.cmdfaction = new CmdFaction(this.ctorParm);
        this.cmdnpc = new CmdNpc(this.ctorParm);
        SlayMap.clear();
        SlayMap.put("all", EntityLivingBase.class);
        SlayMap.put("mobs", EntityMob.class);
        SlayMap.put("animals", EntityAnimal.class);
        SlayMap.put("items", EntityItem.class);
        SlayMap.put("xporbs", EntityXPOrb.class);
        SlayMap.put("npcs", EntityNPCInterface.class);
        HashMap list = new HashMap(EntityList.stringToClassMapping);
        Iterator var3 = list.keySet().iterator();

        while (var3.hasNext())
        {
            String name = (String)var3.next();
            Class cls = (Class)list.get(name);

            if (!EntityNPCInterface.class.isAssignableFrom(cls) && EntityLivingBase.class.isAssignableFrom(cls))
            {
                SlayMap.put(name.toLowerCase(), cls);
            }
        }

        SlayMap.remove("monster");
        SlayMap.remove("mob");
    }

    @SubCommand(
        name = "faction",
        desc = "Faction operations",
        usage = "<player> <faction> <command>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public Boolean faction(String[] args)
    {
        String playername = args[0];
        String factionname = args[1];
        this.cmdfaction.data = this.getPlayersData(playername);

        if (this.cmdfaction.data.isEmpty())
        {
            this.sendmessage(String.format("Unknow player \'%s\'", new Object[] {playername}));
            return Boolean.valueOf(false);
        }
        else
        {
            try
            {
                this.cmdfaction.selectedFaction = FactionController.getInstance().getFaction(Integer.parseInt(factionname));
            }
            catch (NumberFormatException var6)
            {
                this.cmdfaction.selectedFaction = FactionController.getInstance().getFactionFromName(factionname);
            }

            if (this.cmdfaction.selectedFaction == null)
            {
                this.sendmessage(String.format("Unknow facion \'%s", new Object[] {factionname}));
                return Boolean.valueOf(false);
            }
            else
            {
                args = (String[])Arrays.copyOfRange(args, 2, args.length);
                this.cmdfaction.processCommand(this.pcParam, args);
                Iterator e = this.cmdfaction.data.iterator();

                while (e.hasNext())
                {
                    PlayerData playerdata = (PlayerData)e.next();
                    playerdata.saveNBTData((NBTTagCompound)null);
                }

                return Boolean.valueOf(true);
            }
        }
    }

    @SubCommand(
        desc = "NPC manipulations",
        usage = "<npc> <command>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public boolean npc(String[] args)
    {
        String npcname = args[0].replace("%", " ");
        args = (String[])Arrays.copyOfRange(args, 1, args.length);

        if (args[0].equalsIgnoreCase("create"))
        {
            this.cmdnpc.processCommand(this.pcParam, new String[] {args[0], npcname});
            return true;
        }
        else
        {
            int x = this.pcParam.getPlayerCoordinates().posX;
            int y = this.pcParam.getPlayerCoordinates().posY;
            int z = this.pcParam.getPlayerCoordinates().posZ;
            List list = this.getNearbeEntityFromPlayer(EntityNPCInterface.class, this.pcParam.getEntityWorld(), x, y, z, 80);
            EntityNPCInterface closest = null;
            Iterator var8 = list.iterator();

            while (var8.hasNext())
            {
                EntityNPCInterface npc = (EntityNPCInterface)var8.next();
                String name = npc.display.name.replace(" ", "_");

                if (name.equalsIgnoreCase(npcname) && (closest == null || closest.getDistanceSq((double)x, (double)y, (double)z) > npc.getDistanceSq((double)x, (double)y, (double)z)))
                {
                    closest = npc;
                }
            }

            if (closest != null)
            {
                this.cmdnpc.selectedNpc = closest;
                this.cmdnpc.processCommand(this.pcParam, args);
                this.cmdnpc.selectedNpc = null;
            }
            else
            {
                this.sendmessage(String.format("NPC \'%s\' was not found", new Object[] {npcname}));
            }

            return true;
        }
    }

    @SubCommand(
        name = "slay",
        desc = "Kills given entity within range. Also has all, mobs, animal options. Can have multiple types",
        usage = "<type>.. [range]",
        permissions = {PlayerOnly.class, OpOnly.class, ParamCheck.class}
    )
    public Boolean slay(String[] args)
    {
        EntityPlayerMP player = (EntityPlayerMP)this.pcParam;
        ArrayList toDelete = new ArrayList();
        boolean deleteNPCs = false;
        String[] count = args;
        int range = args.length;

        for (int box = 0; box < range; ++box)
        {
            String list = count[box];
            list = list.toLowerCase();
            Class cls = (Class)SlayMap.get(list);

            if (cls != null)
            {
                toDelete.add(cls);
            }

            if (list.equals("mobs"))
            {
                toDelete.add(EntityGhast.class);
                toDelete.add(EntityDragon.class);
            }

            if (list.equals("npcs"))
            {
                deleteNPCs = true;
            }
        }

        int var12 = 0;
        range = 120;

        try
        {
            range = Integer.parseInt(args[args.length - 1]);
        }
        catch (NumberFormatException var11)
        {
            ;
        }

        AxisAlignedBB var13 = player.boundingBox.expand((double)range, (double)range, (double)range);
        List var14 = player.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, var13);
        Iterator var15 = var14.iterator();
        Entity entity;

        while (var15.hasNext())
        {
            entity = (Entity)var15.next();

            if (!(entity instanceof EntityPlayer) && (!(entity instanceof EntityTameable) || !((EntityTameable)entity).isTamed()) && (!(entity instanceof EntityNPCInterface) || deleteNPCs) && this.delete(entity, toDelete))
            {
                ++var12;
            }
        }

        if (toDelete.contains(EntityXPOrb.class))
        {
            var14 = player.worldObj.getEntitiesWithinAABB(EntityXPOrb.class, var13);

            for (var15 = var14.iterator(); var15.hasNext(); ++var12)
            {
                entity = (Entity)var15.next();
                entity.isDead = true;
            }
        }

        if (toDelete.contains(EntityItem.class))
        {
            var14 = player.worldObj.getEntitiesWithinAABB(EntityItem.class, var13);

            for (var15 = var14.iterator(); var15.hasNext(); ++var12)
            {
                entity = (Entity)var15.next();
                entity.isDead = true;
            }
        }

        player.addChatMessage(new ChatComponentTranslation(var12 + " entities deleted", new Object[0]));
        return Boolean.valueOf(true);
    }

    private boolean delete(Entity entity, ArrayList < Class<? >> toDelete)
    {
        Iterator var3 = toDelete.iterator();
        Class delete;

        do
        {
            if (!var3.hasNext())
            {
                return false;
            }

            delete = (Class)var3.next();
        }
        while (delete == EntityAnimal.class && entity instanceof EntityHorse || !delete.isAssignableFrom(entity.getClass()));

        entity.isDead = true;
        return true;
    }

    public List addTabCompletion(ICommandSender par1, String[] args)
    {
        return args[0].equalsIgnoreCase("slay") ? CommandBase.getListOfStringsMatchingLastWord(args, (String[])SlayMap.keySet().toArray(new String[SlayMap.size()])) : (args[0].equalsIgnoreCase("npc") && args.length == 3 ? this.cmdnpc.addTabCompletion(par1, (String[])Arrays.copyOfRange(args, 1, args.length)) : (args[0].equalsIgnoreCase("faction") && args.length == 4 ? CommandBase.getListOfStringsMatchingLastWord(args, new String[] {"add", "subtract", "set", "reset", "drop", "create"}): super.addTabCompletion(par1, args)));
    }
}
