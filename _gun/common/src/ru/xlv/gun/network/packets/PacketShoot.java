package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.relauncher.Side;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class PacketShoot implements IMessage {

	public int toSend;
	public boolean headshot;
	public double posX;
	public double posY;
	public double posZ;
	public double posXFloat;
	public double posYFloat;
	public double posZFloat;
	public ItemStack heldItem;
	public NBTTagCompound nbt;
	private float yaw;
	private float pitch;

	public PacketShoot() {
	}

	public PacketShoot(int toSend, boolean headshot, double posX, double posY, double posZ, float yaw, float pitch, ItemStack heldItem, double posXfloat, double posYfloat, double posZfloat) {
		this.toSend = toSend;
		this.headshot = headshot;
		this.posX = posX;
		this.posY = posY;
		this.posZ = posZ;
		this.posXFloat = posXfloat;
		this.posYFloat = posYfloat;
		this.posZFloat = posZfloat;
		this.yaw = yaw;
		this.pitch = pitch;
		this.heldItem = heldItem;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(toSend);
		buf.writeBoolean(headshot);
		buf.writeDouble(posX);
		buf.writeDouble(posY);
		buf.writeDouble(posZ);
		buf.writeDouble(posXFloat);
		buf.writeDouble(posYFloat);
		buf.writeDouble(posZFloat);
		buf.writeFloat(yaw);
		buf.writeFloat(pitch);
		ByteBufUtils.writeItemStack(buf, heldItem);
		ByteBufUtils.writeTag(buf, nbt);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
			toSend = buf.readInt();
			headshot = buf.readBoolean();
			posX = buf.readDouble();
			posY = buf.readDouble();
			posZ = buf.readDouble();
			posXFloat = buf.readDouble();
			posYFloat = buf.readDouble();
			posZFloat = buf.readDouble();
			yaw = buf.readFloat();
			pitch = buf.readFloat();
			heldItem = ByteBufUtils.readItemStack(buf);
		}
	}
}