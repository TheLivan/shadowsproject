package ru.xlv.gun.item;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import java.util.List;

public class ItemTestArmor extends ItemArmor implements IItemModelProvider {

    public ItemTestArmor(ArmorMaterial armorMaterial, int par3, int par4, String name) {
        super(armorMaterial, par3, par4);
        this.setCreativeTab(Tabs.tabArmor);
        this.setUnlocalizedName(name);
        this.setTextureName("sp:armor/22");
        this.setNoRepair();
    }


    public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean advancedTooltips) {
        if (Keyboard.isKeyDown(42)) {
            if (stack.hasTagCompound()) {
                if (stack.stackTagCompound.hasKey("owner")) {
                    lines.add(EnumChatFormatting.RED + "Первый владелец брони: " + EnumChatFormatting.YELLOW + stack.stackTagCompound.getString("owner"));
                }
            } else {
                lines.add(EnumChatFormatting.RED + "Первый владелец брони: " + EnumChatFormatting.YELLOW + "Не установлено");
            }
            lines.add(EnumChatFormatting.WHITE + "-------------------------------------------------");
            lines.add(EnumChatFormatting.GOLD + "✯✯✯✯");
            lines.add(EnumChatFormatting.BLUE + "Защита: " + EnumChatFormatting.YELLOW + "19");
            lines.add(EnumChatFormatting.GREEN + "Пассивные умения:");
            lines.add(EnumChatFormatting.DARK_GREEN + "- Увеличивает максимальное здоровье игрока на:" + EnumChatFormatting.YELLOW + " 15%");
            if (stack.getTagCompound() != null) {
                lines.add(EnumChatFormatting.RED + "Количество использований скилла: " + EnumChatFormatting.YELLOW + stack.getTagCompound().getString("useCount"));
            } else {
                lines.add(EnumChatFormatting.RED + "Количество использований скилла: " + EnumChatFormatting.YELLOW + "0");
            }
            lines.add(EnumChatFormatting.WHITE + "-------------------------------------------------");
        } else {
            lines.add(EnumChatFormatting.RED + "Зажмите SHIFT,чтобы узнать характеристики");
        }
    }

    public void onArmorTick(World world, EntityPlayer player, ItemStack stack) {
//			player.addPotionEffect(new PotionEffect(16, 53, 0, true));
        if (!player.isPotionActive(Potion.moveSpeed.getId())) {
            player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), 60, 0, true));
        }
        if (player.getMaxHealth() == 100) {
            player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(115);
        }
        if (stack.getTagCompound() == null) {
            NBTTagCompound tag = new NBTTagCompound();
            tag.setInteger("useCount", 0);
            stack.setTagCompound(tag);
            if (!tag.hasKey("owner")) {
                tag.setString("owner", player.getCommandSenderName());
            }
        }
        if (stack.getTagCompound() != null) {
            NBTTagCompound tag = stack.getTagCompound();
            if (!tag.hasKey("owner")) {
                tag.setString("owner", player.getCommandSenderName());
            }
        }
    }

    @SideOnly(Side.CLIENT)
    public String getArmorTexture(ItemStack armor, Entity entity, int slot, String type) {
        return "sp:textures/items/null.png";
    }

    @Override
    public String getRenderClassPath() {
        return "ru.xlv.gun.render.armor.RenderTestArmor";
    }
}