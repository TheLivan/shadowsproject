package ru.xlv.gun.item;


import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemAmmo extends Item {

    private String icon;

    public ItemAmmo(String icon, int maxRounds) {
        this.icon = icon;
        this.setMaxStackSize(1);
        this.setCreativeTab(Tabs.tabMagazines);
        this.setMaxDamage(maxRounds);
        this.setHasSubtypes(true);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister icon) {
        itemIcon = icon.registerIcon("batmod:" + this.icon);
    }

    public boolean isRepairable() {
        return true;
    }

    public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean b) {
        lines.add("Патроны: " + (stack.getMaxDamage() - stack.getItemDamage()) + "/" + stack.getMaxDamage());
    }
}
