package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.network.PacketHandler;
import ru.xlv.gun.network.packets.PacketShoot;
import ru.xlv.gun.network.packets.PacketSpawnBlood;

public class PacketShootHandler implements IMessageHandler<PacketShoot, IMessage> {

    @Override
    public IMessage onMessage(PacketShoot message, MessageContext ctx) {
        EntityPlayerMP sp = ctx.getServerHandler().playerEntity;
        int amount = message.toSend;
        double hitEntPosX = message.posX;
        double hitEntPosZ = message.posZ;
        boolean headShot = message.headshot;
        ItemStack helditem = message.heldItem;
        /** антипинг система. проверка на координаты текущей позиции на сервере и клиенте*/
        boolean isEntityReallyDamaged;
        World w = sp.worldObj;
        if (!w.isRemote && sp.getCurrentEquippedItem() != null && sp.getCurrentEquippedItem().getItem() instanceof ItemWeapon) {
            ItemStack gs = sp.getCurrentEquippedItem();
            if (helditem.getItem() instanceof ItemWeapon) {
                if (gs.getTagCompound().getInteger("ammo") > 0 && gs.getTagCompound().getFloat("reloadTime") == 0) {
                    if (w.getEntityByID(amount) != null) {
                        isEntityReallyDamaged = ((hitEntPosX - (w.getEntityByID(amount).lastTickPosX - 2.5F)) * (hitEntPosZ - (w.getEntityByID(amount).lastTickPosZ - 2.5F))) >= 0;
                        if (isEntityReallyDamaged) {
                            if (headShot) {
                                PacketHandler.INSTANCE.sendToAll(new PacketSpawnBlood(message.posXFloat, message.posYFloat, message.posZFloat, true));
                                w.getEntityByID(amount).attackEntityFrom(DamageSource.causePlayerDamage(sp), gs.getTagCompound().getFloat("damage") * 1.5F);
                            } else {
                                PacketHandler.INSTANCE.sendToAll(new PacketSpawnBlood(message.posXFloat, message.posYFloat, message.posZFloat, false));
                                w.getEntityByID(amount).attackEntityFrom(DamageSource.causePlayerDamage(sp), gs.getTagCompound().getFloat("damage"));
                            }
                            w.getEntityByID(amount).hurtResistantTime = 0;
                        }
                    }
                }
            }
        }
        return null;
    }
}
