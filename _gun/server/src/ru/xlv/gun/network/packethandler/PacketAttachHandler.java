package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.gun.item.ItemAttachment;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.network.packets.PacketAttachButton;

public class PacketAttachHandler implements IMessageHandler<PacketAttachButton, IMessage> {

	@Override
	public IMessage onMessage(PacketAttachButton message, MessageContext ctx) {
		EntityPlayerMP serverPlayer = ctx.getServerHandler().playerEntity;
		int length = serverPlayer.inventory.getSizeInventory();
		String attachname = message.attachname;
		if (!serverPlayer.worldObj.isRemote) {
			NBTTagCompound nbtStack = serverPlayer.getHeldItem().getTagCompound();
			for (int i = 0; i < length; i++) {
				if (serverPlayer.inventory.getStackInSlot(i) != null && serverPlayer.inventory.getStackInSlot(i).getItem() instanceof ItemAttachment) {
					if (attachname.equals("Прицел ПК-АС")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.pkas);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("SUSAT")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.susat);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("Barska Sight")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.barska);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("Прицел Кобра")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.kobra);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("Прицел ПСО-1")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.pso);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("Leupold HAMR")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.leupold);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("ACOG")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.acog);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("Прицел ПОСП")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.posp);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}
					if (attachname.equals("TR20")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.trAc);
						nbtStack.setString("scope", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					} else if (attachname.equals("OTAN Silencer")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.natoSilencer);
						nbtStack.setString("stvol", attachname);

						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					} else if (attachname.equals("Osprey")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.osprey);
						nbtStack.setString("stvol", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					} else if (attachname.equals("ПБС-4")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.pbs4);
						nbtStack.setString("stvol", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					} else if (attachname.equals("7x62 AAC SDN")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.aac762sdn);
						nbtStack.setString("stvol", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					} else if (attachname.equals("Лазер")) {
						serverPlayer.inventoryContainer.detectAndSendChanges();
						serverPlayer.inventory.consumeInventoryItem(Items.laser);
						nbtStack.setString("laser", attachname);
						serverPlayer.inventoryContainer.detectAndSendChanges();
						break;
					}

				}
			}
		}
		return null;
	}
}