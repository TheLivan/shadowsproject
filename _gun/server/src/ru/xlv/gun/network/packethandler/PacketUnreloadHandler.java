package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayerMP;
import ru.xlv.gun.network.packets.PacketUnreload;
import ru.xlv.gun.util.ExtendedPlayer;

public class PacketUnreloadHandler implements IMessageHandler<PacketUnreload, IMessage> {

	@Override
	public IMessage onMessage(PacketUnreload message, MessageContext ctx) {
		EntityPlayerMP serverPlayer = ctx.getServerHandler().playerEntity;
		ExtendedPlayer ep = ExtendedPlayer.get(serverPlayer);
		ep.setReloadKeyUnpressed();
		return null;
	}
}
