package ru.xlv.gun;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import ru.xlv.gun.gui.GuiCustomPlayerInventory;
import ru.xlv.gun.gui.GuiGunModTable;
import ru.xlv.gun.util.ExtendedPlayer;

public class GuiHandlerClient extends CommonGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == 1) {
            return new GuiCustomPlayerInventory(player, player.inventory, ExtendedPlayer.get(player).inventory);
        }

        if (ID == 2) {
            return new GuiGunModTable(player.inventory, world);
        } else {
            return null;
        }
    }
}
