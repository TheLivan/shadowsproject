package ru.xlv.gun.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class CustomCrossHair {

    private int state;
    public final Minecraft mc;
    private int currentLineWidth;
    private int stateTimer;

    public CustomCrossHair() {
        this.mc = Minecraft.getMinecraft();
    }

    public void draw(final Tessellator tes) {
        final float n = 0.5f;
        GL11.glScalef(n, n, n);
        final float n2 = (Display.getHeight() < Display.getWidth()) ? Display.getHeight() : ((float) Display.getWidth());
        int currentLineWidth;
        if (n2 > 1080.0f) {
            currentLineWidth = 4;
        } else {
            currentLineWidth = 2;
        }
        this.currentLineWidth = currentLineWidth;
        GL11.glColor4f(1f, 1f, 1f, 1.0f);
        GL11.glLineWidth((float) this.currentLineWidth);
        try {
            tes.draw();
        } catch (IllegalStateException ignored) {}
        if (this.state != 0) {
            final float n4 = 0.0f;
            this.drawQuad(tes, n4, n4);
            this.drawQuad(tes, -this.currentLineWidth, -this.currentLineWidth);
            this.drawQuad(tes, -this.currentLineWidth, this.currentLineWidth);
            this.drawQuad(tes, this.currentLineWidth, this.currentLineWidth);
            this.drawQuad(tes, this.currentLineWidth, -this.currentLineWidth);

            if (this.state == 2) {
                this.drawQuad(tes, -this.currentLineWidth * 2, -this.currentLineWidth * 2);
                this.drawQuad(tes, -this.currentLineWidth * 2, this.currentLineWidth * 2);
                this.drawQuad(tes, this.currentLineWidth * 2, this.currentLineWidth * 2);
                this.drawQuad(tes, this.currentLineWidth * 2, -this.currentLineWidth * 2);
            }
        }
    }


    private void drawQuad(final Tessellator tes, final float n2, final float f) {
        tes.startDrawingQuads();
        Tessellator tessellator;
        if (this.currentLineWidth == 3) {
            tessellator = tes;
            tes.addVertex((double) (n2 - 1.0f), (double) (f - 1.0f), 0.0);
            tes.addVertex((double) (n2 - 1.0f), (double) (f + 2.0f), 0.0);
            tes.addVertex((double) (n2 + 2.0f), (double) (f + 2.0f), 0.0);
            tes.addVertex((double) (n2 + 2.0f), (double) (f - 1.0f), 0.0);
        } else {
            tessellator = tes;
            tes.addVertex((double) (n2 - this.currentLineWidth / 2), (double) (f - this.currentLineWidth / 2), 0.0);
            tes.addVertex((double) (n2 - this.currentLineWidth / 2), (double) (f + this.currentLineWidth / 2), 0.0);
            tes.addVertex((double) (n2 + this.currentLineWidth / 2), (double) (f + this.currentLineWidth / 2), 0.0);
            tes.addVertex((double) (n2 + this.currentLineWidth / 2), (double) (f - this.currentLineWidth / 2), 0.0);
        }
        tessellator.draw();
    }

    public void setState(final int tes) {
        final int stateTimer = 10;
        this.state = tes;
        this.stateTimer = stateTimer;
    }

    public void update() {
        if (this.stateTimer > 0) {
            --this.stateTimer;
            return;
        }
        if (this.stateTimer == 0) {
            final boolean state = false;
            this.stateTimer = -1;
            this.state = (state ? 1 : 0);
        }
    }
}
