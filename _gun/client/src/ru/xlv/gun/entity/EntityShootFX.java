package ru.xlv.gun.entity;

import net.minecraft.client.particle.EntityFX;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class EntityShootFX extends EntityFX {

   private int parage = 0;

   public EntityShootFX(World world) {
      super(world, 0, 0, 0);
   }

   public EntityShootFX(EntityPlayer shooter, World world, double xoff, double yoff, double zoff) {
      super(world, xoff, yoff, zoff, 0.0D, 0.0D, 0.0D);
      super.renderDistanceWeight = 64.0D;
      this.setLocationAndAngles(shooter.posX, posY, shooter.posZ, shooter.rotationYaw, shooter.rotationPitch);
      this.posX -= Math.sin(shooter.rotationYaw / 180.0F * 3.141593F) * Math.cos(shooter.rotationPitch / 180.0F * 3.141593F) * 0.75F;
      this.posZ += Math.cos(shooter.rotationYaw / 180.0F * 3.141593F) * Math.cos(shooter.rotationPitch / 180.0F * 3.141593F) * 0.75F;
      this.posY -= Math.sin(shooter.rotationPitch / 180.0F * 3.141593F) * 0.75F;
      setPosition(this.posX, this.posY, this.posZ);
      this.yOffset = 0.0F;
   }

   public void onUpdate() {
      if (this.parage++ >= 1) {
         setDead();
      }
   }

   public int getFXLayer() {
      return 3;
   }
}

