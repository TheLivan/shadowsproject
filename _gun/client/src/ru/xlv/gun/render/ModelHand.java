package ru.xlv.gun.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.entity.Entity;

public class ModelHand extends ModelBase {
    //fields
    ModelRenderer rightarm;
    ModelRenderer leftarm;

    public ModelHand() {
        textureWidth = 64;
        textureHeight = 32;

        rightarm = new ModelRenderer(this, 40, 16);
        rightarm.addBox(-3F, -2F, -2F, 4, 12, 4);
        rightarm.setRotationPoint(-5F, 2F, 0F);
        rightarm.setTextureSize(64, 32);
        rightarm.mirror = true;
        setRotation(rightarm, 0F, 0F, 0F);
        leftarm = new ModelRenderer(this, 40, 16);
        leftarm.addBox(-1F, -2F, -2F, 4, 12, 4);
        leftarm.setRotationPoint(5F, 2F, 0F);
        leftarm.setTextureSize(64, 32);
        leftarm.mirror = true;
        setRotation(leftarm, 0F, 0F, 0F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5, AbstractClientPlayer p) {

        super.render(entity, f, f1, f2, f3, f4, f5);

        setRotationAngles(f, f1, f2, f3, f4, f5, entity);

        rightarm.render(f5);
        leftarm.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }

    public void render_left(AbstractClientPlayer p) {
        float f5 = 0.0625F;
        TextureManager renderengine = Minecraft.getMinecraft().renderEngine;
        renderengine.bindTexture(p.getLocationSkin());

        leftarm.render(f5);
    }

    public void render_left_left(AbstractClientPlayer p) {
        float f5 = 0.0625F;
        TextureManager renderengine = Minecraft.getMinecraft().renderEngine;

        ru.xlv.core.util.Utils.bindTexture(p.getLocationSkin());
        leftarm.render(f5);
    }

    public void render_right(AbstractClientPlayer p) {
        float f5 = 0.0625F;
        TextureManager renderengine = Minecraft.getMinecraft().renderEngine;
        renderengine.bindTexture(p.getLocationSkin());
        rightarm.render(f5);
    }

    public void render_right_right(AbstractClientPlayer p) {
        float f5 = 0.0625F;
        TextureManager renderengine = Minecraft.getMinecraft().renderEngine;
        ru.xlv.core.util.Utils.bindTexture(p.getLocationSkin());
        rightarm.render(f5);
    }
}
