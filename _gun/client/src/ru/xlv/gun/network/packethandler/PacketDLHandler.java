package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import ru.xlv.gun.ClientEventHandler;
import ru.xlv.gun.network.packets.PacketDamageLocation;

public class PacketDLHandler implements IMessageHandler<PacketDamageLocation, IMessage> {

    @SideOnly(Side.CLIENT)
    public IMessage onMessage(PacketDamageLocation message, MessageContext ctx) {
       EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
       ClientEventHandler.getDamageLocation(player.rotationYaw, player.posX, player.posZ, message.attackerPosX, message.attackerPosZ);
       return null;
    }
 }