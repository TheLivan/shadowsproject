package ru.xlv.gun.network.packethandler;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import ru.xlv.gun.XlvsMainMod;


public class MessageGunModTable implements IMessage {

    public int playerID;

    public MessageGunModTable() {}

    public MessageGunModTable(int playerID) {
        this.playerID = playerID;
    }

    public void toBytes(ByteBuf out) {
        out.writeInt(this.playerID);
    }

    public void fromBytes(ByteBuf in) {
        this.playerID = in.readInt();
    }

    public static class Handler implements IMessageHandler<MessageGunModTable, IMessage> {

        public IMessage onMessage(MessageGunModTable message, MessageContext ctx) {
            MinecraftServer server = MinecraftServer.getServer();
            if (server.getEntityWorld().getEntityByID(message.playerID) != null && server.getEntityWorld().getEntityByID(message.playerID) instanceof EntityPlayer) {
                EntityPlayer thePlayer = (EntityPlayer) server.getEntityWorld().getEntityByID(message.playerID);
                thePlayer.openGui(XlvsMainMod.instance, 2, thePlayer.getEntityWorld(), (int) thePlayer.posX, (int) thePlayer.posY, (int) thePlayer.posZ);
            }
            return null;
        }
    }
}
