package ru.xlv.gun;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.client.event.*;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.xlv.gun.entity.BulletHoleFX;
import ru.xlv.gun.entity.EntityBulletCasing;
import ru.xlv.gun.gui.CustomCrossHair;
import ru.xlv.gun.gui.GameOverlay;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemAttachment;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.network.PacketHandler;
import ru.xlv.gun.network.packethandler.MessageGunModTable;
import ru.xlv.gun.network.packets.*;
import ru.xlv.gun.render.PlayerRenderHandler;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.render.armor.IArmorRenderer;
import ru.xlv.gun.util.ExtendedPlayer;
import ru.xlv.gun.util.Utils;

import java.util.ArrayList;
import java.util.List;


@SideOnly(Side.CLIENT)
public class ClientEventHandler {

	private Minecraft mc = Minecraft.getMinecraft();
	public static boolean aiming;
	private boolean shooting;
	private boolean keyDown;
	private boolean keyUp;
	private float timer;
	private float ifelse = 1F;
	private int useTimer = 0;
	/**
	 * The player's mouse sensitivity setting, as it was before being hacked by my mod
	 * The player's original FOV
	 */
	private float originalFOV = mc.gameSettings.mouseSensitivity;

	private static int changelock = 0;
	private static int numberLastSlot = 0;
	public static String name;

	public static CustomCrossHair crossHair = new CustomCrossHair();

	public ClientEventHandler() {
		this.keyDown = false;
		this.keyUp = true;
	}

	@SubscribeEvent
	public void onPlayerInteractBlock(PlayerInteractEvent e) {
		EntityClientPlayerMP player = FMLClientHandler.instance().getClient().thePlayer;
		if(!player.capabilities.isCreativeMode) {
			if(e.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK && !Keyboard.isKeyDown(33) ) {
				Block b = this.mc.theWorld.getBlock(e.x, e.y, e.z);
				if(b.equals(Blocks.wooden_door)) {
					e.setCanceled(true);
				}
			}
		}
		if(e.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) {
			Block b = this.mc.theWorld.getBlock(e.x, e.y, e.z);
			if(b.equals(Blocks.hopper)) {
				e.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onBreak(BlockEvent.BreakEvent e) {
		if(!e.getPlayer().capabilities.isCreativeMode) {
			e.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void blockSpeedBreak(PlayerEvent.BreakSpeed e) {
		if(!e.entityPlayer.capabilities.isCreativeMode) {
			e.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void removeHighlight(DrawBlockHighlightEvent event) {
		if(!event.player.capabilities.isCreativeMode) {
			event.setCanceled(true);
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST, receiveCanceled = true)
	public void onKeyInputDoor(KeyInputEvent e) {
		EntityClientPlayerMP player = FMLClientHandler.instance().getClient().thePlayer;
		if(Keyboard.isKeyDown(33)) {
			if(this.mc.objectMouseOver == null) {
				return;
			}
			int i = this.mc.objectMouseOver.blockX;
			int j = this.mc.objectMouseOver.blockY;
			int k = this.mc.objectMouseOver.blockZ;
			Block b = this.mc.theWorld.getBlock(i, j, k);
			if(b.equals(Blocks.wooden_door)) {
				this.mc.playerController.onPlayerRightClick(this.mc.thePlayer, this.mc.theWorld, this.mc.thePlayer.inventory.getCurrentItem(), i, j, k, this.mc.objectMouseOver.sideHit, this.mc.objectMouseOver.hitVec);
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void eventHandler(RenderGameOverlayEvent event) {
		aiming = (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem() != null)
				&& (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem().getItem() instanceof ItemWeapon)
				&& (Mouse.isButtonDown(1)) && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() && (FMLClientHandler.instance().getClient().currentScreen == null));

		if (event.type == ElementType.EXPERIENCE || event.type == ElementType.ARMOR) {
			event.setCanceled(true);
		}
//		if (Minecraft.getMinecraft().isSingleplayer()) throw new RuntimeException();
	}

	@SubscribeEvent
	public void renderDynamicCrossHair(final RenderGameOverlayEvent.Pre e) {
		final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		GL11.glPushMatrix();
		GL11.glTranslatef((float) (resolution.getScaledWidth() / 2), (float) (resolution.getScaledHeight() / 2), 0.0f);
		GL11.glEnable(3042);
		GL11.glDisable(3553);
		crossHair.draw(Tessellator.instance);
		GL11.glEnable(3553);
		GL11.glPopMatrix();
	}

	@SubscribeEvent
	public void scopeZoom(FOVUpdateEvent event) {
		if (mc.thePlayer.getHeldItem() != null && mc.thePlayer.getHeldItem().getItem() instanceof ItemWeapon) {
			ItemStack gunStack = mc.thePlayer.getHeldItem();
			ItemWeapon gun = (ItemWeapon) gunStack.getItem();

			aiming = (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem() != null)
					&& (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem().getItem() instanceof ItemWeapon)
					&& (Mouse.isButtonDown(1)) && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() && (FMLClientHandler.instance().getClient().currentScreen == null));

			if (gunStack.getItem() instanceof ItemWeapon) {
				if (gunStack.getTagCompound() != null) {
					if (gunStack.getTagCompound().getString("scope").length() > 0) {
						ItemAttachment scopeItem = new ItemAttachment(null, 1, 1, 1f, 0, AttachmentType.scope);
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.leupold)) {
							scopeItem = (ItemAttachment) Items.leupold;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.pkas)) {
							scopeItem = (ItemAttachment) Items.pkas;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.pso)) {
							scopeItem = (ItemAttachment) Items.pso;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.susat)) {
							scopeItem = (ItemAttachment) Items.susat;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.trAc)) {
							scopeItem = (ItemAttachment) Items.trAc;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.posp)) {
							scopeItem = (ItemAttachment) Items.posp;
						}
						if (AttachmentType.checkAttachment(gunStack, "scope", Items.acog)) {
							scopeItem = (ItemAttachment) Items.acog;
						}
						if (Utils.isPlayerAiming() && gunStack.getTagCompound().getString("scope").length() > 2 && (gun != Items.sv98 && gun != Items.l96)) {
							if (event.newfov > scopeItem.zoom / 10) {
								event.newfov -= scopeItem.zoom / 5;
							}
							if (ifelse < scopeItem.zoom) {
								mc.gameSettings.mouseSensitivity -= scopeItem.zoom * originalFOV * 0.01F;
								ifelse += 0.25F;
							}
						} else if (aiming && gun == Items.ramjet) {
							if (event.newfov > 5 / 10) {
								event.newfov -= (float) 4 / 5;
							}
							if (ifelse < 4) {
								mc.gameSettings.mouseSensitivity -= 4 * originalFOV * 0.01F;
								ifelse += 0.25F;
							}
						} else if (aiming && gunStack.getTagCompound().getString("scope").length() > 2 && (gun == Items.sv98 || gun == Items.l96) && gun.otdachaTimer < 1) {
							if (event.newfov > scopeItem.zoom / 10) {
								event.newfov -= scopeItem.zoom / 5;
							}

							if (ifelse < scopeItem.zoom) {
								mc.gameSettings.mouseSensitivity -= scopeItem.zoom * originalFOV * 0.0125F;
								ifelse += 0.25F;
							}
						} else {
							event.newfov = event.fov;
							mc.gameSettings.mouseSensitivity = originalFOV;
							ifelse = 1F;
						}
					} else {
						if (aiming && gun == Items.ramjet) {
							if (event.newfov > 5 / 10) {
								event.newfov -= (float) 2 / 5;
							}
							if (ifelse < 4) {
								mc.gameSettings.mouseSensitivity -= 4 * originalFOV * 0.01F;
								ifelse += 0.25F;
							}
						} else {
							event.newfov = event.fov;
							mc.gameSettings.mouseSensitivity = originalFOV;
							ifelse = 1F;
						}
					}
				}
			}
		}
	}

	public static void getDamageLocation(float playerYaw, double x1, double z1, double x2, double z2) {
		double dx = x1 - x2;
		double dz = z1 - z2;
		double angle = MathHelper.wrapAngleTo180_double(playerYaw - Math.toDegrees(Math.atan2(dz, dx)));

		if (angle > -45 && angle <= 45) {
			GameOverlay.damageLocation = 5f;
			GameOverlay.damageLocat = "EAST";
		} else if (angle > 45 && angle <= 135) {
			GameOverlay.damageLocation = 5f;
			GameOverlay.damageLocat = "NORTH";
		} else if (angle < -45 && angle >= -135) {
			GameOverlay.damageLocation = 5f;
			GameOverlay.damageLocat = "SOUTH";
		} else {
			GameOverlay.damageLocation = 5f;
			GameOverlay.damageLocat = "WEST";
		}
	}

	@SideOnly(value = Side.CLIENT)
	@SubscribeEvent
	public void lanternHolding(RenderLivingEvent.Pre event) {
		if (!event.isCanceled() && event.entity instanceof EntityPlayer) {
			ItemStack item = event.entity.getHeldItem();
			if (item == null) {
				return;
			}
			if (item.getItem() instanceof ItemWeapon) {
				RenderPlayer rp = (RenderPlayer) event.renderer;
				rp.modelBipedMain.aimedBow = true;
				rp.modelArmor.aimedBow = true;
				rp.modelArmorChestplate.aimedBow = true;
			}
		}
	}

	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event) {
		ExtendedPlayer ep = ExtendedPlayer.get(this.mc.thePlayer);
		if (XlvsMainMod.unloadKey.isPressed() && this.mc.thePlayer.getCurrentEquippedItem() != null && this.mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemWeapon) {
			PacketHandler.INSTANCE.sendToServer(new PacketUnreloadMag());
		}
		if (XlvsMainMod.modGuiKey.isPressed() && FMLClientHandler.instance().getClient().inGameHasFocus) {
			PacketHandler.INSTANCE.sendToServer(new MessageGunModTable(mc.thePlayer.getEntityId()));
		}
		if (XlvsMainMod.reloadKey.isPressed() && !this.keyDown && Minecraft.getMinecraft().currentScreen == null && this.mc.thePlayer.getCurrentEquippedItem() != null && this.mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemWeapon) {
			PacketHandler.INSTANCE.sendToServer(new PacketReload());
			ep.setReloadKeyPressed();
			this.timer = 1;
			this.keyDown = true;
			this.keyUp = false;
		}
		if (!this.keyUp && timer == 0) {
			PacketHandler.INSTANCE.sendToServer(new PacketUnreload());
			ep.setReloadKeyUnpressed();
			this.keyDown = false;
			this.keyUp = true;
		}
	}

	private ArrayList<MovingObjectPosition> rayTrace(EntityLivingBase entity, double distance, float partialTickTime, double xCord, double yCord, double zCord) {
		Vec3 vec3 = getPosition(entity, partialTickTime);
		Vec3 vec32 = vec3.addVector(xCord * distance, yCord * distance, zCord * distance);
		return func_147447_a(entity.worldObj, vec3, vec32, false, false, true);
	}

	private static Vec3 getPosition(EntityLivingBase entity, float partialTickTime) {
		if (partialTickTime == 1.0F) {
			return Vec3.createVectorHelper(entity.posX, entity.posY + (entity instanceof EntityPlayer ? entity.getEyeHeight() : 0), entity.posZ);
		} else {
			double d0 = entity.prevPosX + (entity.posX - entity.prevPosX) * (double) partialTickTime;
			double d1 = entity.prevPosY + (entity.posY - entity.prevPosY) * (double) partialTickTime;
			double d2 = entity.prevPosZ + (entity.posZ - entity.prevPosZ) * (double) partialTickTime;
			return Vec3.createVectorHelper(d0, d1 + (entity instanceof EntityPlayer ? entity.getEyeHeight() : 0), d2);
		}
	}

	private static ArrayList<MovingObjectPosition> func_147447_a(World world, Vec3 vec1, Vec3 vec2, boolean p_147447_3_, boolean p_147447_4_, boolean p_147447_5_) {
		if (Double.isNaN(vec1.xCoord) && Double.isNaN(vec1.yCoord) && Double.isNaN(vec1.zCoord)) return null;
		if (Double.isNaN(vec2.xCoord) && Double.isNaN(vec2.yCoord) && Double.isNaN(vec2.zCoord)) return null;
		ArrayList<MovingObjectPosition> list = new ArrayList<>();
		int i = MathHelper.floor_double(vec2.xCoord);
		int j = MathHelper.floor_double(vec2.yCoord);
		int k = MathHelper.floor_double(vec2.zCoord);
		int l = MathHelper.floor_double(vec1.xCoord);
		int i1 = MathHelper.floor_double(vec1.yCoord);
		int j1 = MathHelper.floor_double(vec1.zCoord);
		Block block = world.getBlock(l, i1, j1);
		int k1 = world.getBlockMetadata(l, i1, j1);
		if ((!p_147447_4_ || block.getCollisionBoundingBoxFromPool(world, l, i1, j1) != null) && block.canCollideCheck(k1, p_147447_3_)) {
			MovingObjectPosition movingobjectposition = block.collisionRayTrace(world, l, i1, j1, vec1, vec2);
			if (movingobjectposition != null)
				list.add(movingobjectposition);
		}
		MovingObjectPosition movingobjectposition2 = null;
		k1 = 200;
		while (k1-- >= 0) {
			if (Double.isNaN(vec1.xCoord) || Double.isNaN(vec1.yCoord) || Double.isNaN(vec1.zCoord))
				return null;
			if (l == i && i1 == j && j1 == k) {
				if (p_147447_5_) list.add(movingobjectposition2);
			}
			boolean flag6 = true;
			boolean flag3 = true;
			boolean flag4 = true;
			double d0 = 999.0D;
			double d1 = 999.0D;
			double d2 = 999.0D;
			if (i > l) {
				d0 = (double) l + 1.0D;
			} else if (i < l) {
				d0 = (double) l + 0.0D;
			} else {
				flag6 = false;
			}
			if (j > i1) {
				d1 = (double) i1 + 1.0D;
			} else if (j < i1) {
				d1 = (double) i1 + 0.0D;
			} else {
				flag3 = false;
			}

			if (k > j1) {
				d2 = (double) j1 + 1.0D;
			} else if (k < j1) {
				d2 = (double) j1 + 0.0D;
			} else {
				flag4 = false;
			}
			double d3 = 999.0D;
			double d4 = 999.0D;
			double d5 = 999.0D;
			double d6 = vec2.xCoord - vec1.xCoord;
			double d7 = vec2.yCoord - vec1.yCoord;
			double d8 = vec2.zCoord - vec1.zCoord;
			if (flag6) {
				d3 = (d0 - vec1.xCoord) / d6;
			}
			if (flag3) {
				d4 = (d1 - vec1.yCoord) / d7;
			}
			if (flag4) {
				d5 = (d2 - vec1.zCoord) / d8;
			}
			byte b0;
			if (d3 < d4 && d3 < d5) {
				if (i > l) {
					b0 = 4;
				} else {
					b0 = 5;
				}
				vec1.xCoord = d0;
				vec1.yCoord += d7 * d3;
				vec1.zCoord += d8 * d3;
			} else if (d4 < d5) {
				if (j > i1) {
					b0 = 0;
				} else {
					b0 = 1;
				}

				vec1.xCoord += d6 * d4;
				vec1.yCoord = d1;
				vec1.zCoord += d8 * d4;
			} else {
				if (k > j1) {
					b0 = 2;
				} else {
					b0 = 3;
				}
				vec1.xCoord += d6 * d5;
				vec1.yCoord += d7 * d5;
				vec1.zCoord = d2;
			}
			Vec3 vec32 = Vec3.createVectorHelper(vec1.xCoord, vec1.yCoord, vec1.zCoord);
			l = (int) (vec32.xCoord = (double) MathHelper.floor_double(vec1.xCoord));
			if (b0 == 5) {
				--l;
				++vec32.xCoord;
			}
			i1 = (int) (vec32.yCoord = (double) MathHelper.floor_double(vec1.yCoord));
			if (b0 == 1) {
				--i1;
				++vec32.yCoord;
			}
			j1 = (int) (vec32.zCoord = (double) MathHelper.floor_double(vec1.zCoord));
			if (b0 == 3) {
				--j1;
				++vec32.zCoord;
			}
			Block block1 = world.getBlock(l, i1, j1);
			int l1 = world.getBlockMetadata(l, i1, j1);
			if (!p_147447_4_ || block1.getCollisionBoundingBoxFromPool(world, l, i1, j1) != null) {
				if (block1.canCollideCheck(l1, p_147447_3_)) {
					MovingObjectPosition movingobjectposition1 = block1.collisionRayTrace(world, l, i1, j1, vec1, vec2);
					if (movingobjectposition1 != null)
						list.add(movingobjectposition1);
				} else {
					movingobjectposition2 = new MovingObjectPosition(l, i1, j1, b0, vec1, false);
				}
			}
		}
		if (p_147447_5_)
			list.add(movingobjectposition2);
		return list;
	}

	private void createDamageVec(float f) {
		NBTTagCompound weaponNBT = mc.thePlayer.getHeldItem().getTagCompound();
		ItemWeapon gun = (ItemWeapon) mc.thePlayer.getCurrentEquippedItem().getItem();
		if (this.mc.renderViewEntity != null) {
			if (this.mc.theWorld != null) {
				for (int k = 0; k < gun.getShots(); k++) {
					this.mc.pointedEntity = null;
					double d0 = 196D;
					this.mc.objectMouseOver = this.mc.renderViewEntity.rayTrace(d0, f);
					double d1 = d0;
					Vec3 vec3 = this.mc.renderViewEntity.getPosition(f);
					if (this.mc.objectMouseOver != null) {
						d1 = this.mc.objectMouseOver.hitVec.distanceTo(vec3);

					}

					Vec3 vec31 = this.mc.renderViewEntity.getLook(f);
					vec31.xCoord += mc.theWorld.rand.nextGaussian() * (double) (mc.theWorld.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) weaponNBT.getFloat("spread");
					vec31.yCoord += mc.theWorld.rand.nextGaussian() * (double) (mc.theWorld.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) weaponNBT.getFloat("spread");
					vec31.zCoord += mc.theWorld.rand.nextGaussian() * (double) (mc.theWorld.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) weaponNBT.getFloat("spread");
					double xCord = vec31.xCoord;
					double yCord = vec31.yCoord;
					double zCord = vec31.zCoord;

					ArrayList<MovingObjectPosition> blocklist = rayTrace(mc.thePlayer, d0, 1F, xCord, yCord, zCord);
					for (MovingObjectPosition mop : blocklist) {
						Block block = mc.theWorld.getBlock(mop.blockX, mop.blockY, mop.blockZ);
						if (block.getMaterial() == Material.glass || block.getMaterial() == Material.vine || block.getMaterial() == Material.plants
								|| block.getMaterial() == Material.air || block.getMaterial() == Material.leaves || block.getMaterial() == Material.web) {
						} else {
							d0 = mop.hitVec.distanceTo(vec3);
							if (mc.gameSettings.fancyGraphics)
								Minecraft.getMinecraft().effectRenderer.addEffect(new BulletHoleFX(mc.theWorld, mop.hitVec.xCoord, mop.hitVec.yCoord, mop.hitVec.zCoord, mop.sideHit, block));
							if (block.getMaterial() != Material.air)
								break;
						}
					}
					Vec3 vec32 = vec3.addVector(vec31.xCoord * d0, vec31.yCoord * d0, vec31.zCoord * d0);
					Entity pointedEntity = null;
					Vec3 vec33 = null;
					float f1 = 1.0F;
					double d4 = d0;
					List list = this.mc.theWorld.getEntitiesWithinAABBExcludingEntity(this.mc.renderViewEntity, this.mc.renderViewEntity.boundingBox.addCoord(
							vec31.xCoord * d0, vec31.yCoord * d0, vec31.zCoord * d0).expand((double) f1, (double) f1, (double) f1));
					double d2 = d4;
					for (int i = list.size(); i > 0; --i) {
						Entity entity = (Entity) list.get(i - 1);
						if (entity.canBeCollidedWith() && !entity.isDead) {
							float f2 = entity.getCollisionBorderSize();
							AxisAlignedBB axisalignedbb = entity.boundingBox.expand((double) f2, (double) f2, (double) f2);
							MovingObjectPosition movingobjectposition = axisalignedbb.calculateIntercept(vec3, vec32);
							if (axisalignedbb.isVecInside(vec3)) {
								if (0.0D < d2 || d2 == 0.0D) {
									pointedEntity = entity;
									vec33 = movingobjectposition == null ? vec3 : movingobjectposition.hitVec;
									GameOverlay.hitMakerTime = 1F;
									d2 = 0D;
									break;
								}
							} else if (movingobjectposition != null) {
								double d3 = vec3.distanceTo(movingobjectposition.hitVec);
								if (d3 < d2 || d2 == 0.0D) {
									if (entity == this.mc.renderViewEntity.ridingEntity && !entity.canRiderInteract()) {
										if (d2 == 0.0D) {
											pointedEntity = entity;
											vec33 = movingobjectposition.hitVec;
										}
									} else if (!(entity instanceof EntityEnderCrystal)) {
										if (entity instanceof EntityLivingBase && ((EntityLivingBase) entity).getHealth() > 0) {
											pointedEntity = entity;
											float headPosition = pointedEntity.getEyeHeight();
											vec33 = movingobjectposition.hitVec;

											d2 = d3;
											double hitPos = vec33.yCoord - pointedEntity.posY;

											boolean headShot = ((hitPos - (headPosition - 0.3F)) * (headPosition + 0.5F - hitPos) >= 0);
											double hitEntPosX = pointedEntity.posX;
											double hitEntPosY = pointedEntity.posY;
											double hitEntPosZ = pointedEntity.posZ;
											ItemStack ItemHeld = mc.thePlayer.getCurrentEquippedItem();
											PacketHandler.INSTANCE.sendToServer(new PacketShoot(pointedEntity.getEntityId(), headShot, hitEntPosX, hitEntPosY, hitEntPosZ,
													this.mc.thePlayer.rotationYaw, this.mc.thePlayer.rotationPitch, ItemHeld, vec33.xCoord, vec33.yCoord, vec33.zCoord));
											GameOverlay.isHeadShot = headShot;
											GameOverlay.hitMakerTime = 1F;
											break;
										}
									}
								}
							}
						}
					}
					if (pointedEntity != null && (d2 < d1 || this.mc.objectMouseOver == null)) {
						this.mc.objectMouseOver = new MovingObjectPosition(pointedEntity, vec33);

						if (pointedEntity instanceof EntityLivingBase || pointedEntity instanceof EntityItemFrame) {
							this.mc.pointedEntity = pointedEntity;
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void doAnimations(TickEvent.ClientTickEvent event) {
		crossHair.update();

		if (GameOverlay.hitMakerTime > 0) {
			GameOverlay.hitMakerTime -= 0.045F * RenderWeaponThings.renderTicks;
		}
		if (mc.thePlayer != null && event.phase == TickEvent.Phase.START) {
			ItemStack stack = mc.thePlayer.getCurrentEquippedItem();
			if (stack != null && stack.getItem() instanceof ItemWeapon) {
				ItemWeapon weapon = (ItemWeapon) stack.getItem();
				int shootTimer = (int) weapon.otdachaTimer;
				RenderWeaponThings.lastRun1Progress = RenderWeaponThings.run1Progress;
				if ((!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() || Minecraft.getMinecraft().thePlayer.isPotionActive(Potion.moveSpeed)) || (weapon == Items.sv98 || weapon == Items.l96) && weapon.otdachaTimer > 2) {
					RenderWeaponThings.run1Progress *= (0.52F);
				} else if (stack.getTagCompound().getFloat("reloadTime") < 5F) {
					RenderWeaponThings.run1Progress = 1F - (1F - RenderWeaponThings.run1Progress) * (0.65F);
				}

				RenderWeaponThings.lastRunProgress = RenderWeaponThings.runProgress;
				if (!Utils.isPlayerAiming() || (weapon == Items.sv98 || weapon == Items.l96) && weapon.otdachaTimer > 2) {
					RenderWeaponThings.runProgress *= 0.52F * RenderWeaponThings.renderTicks;
				} else if (stack.getTagCompound().getFloat("reloadTime") < 5F) {
					RenderWeaponThings.runProgress = 1F - (1F - RenderWeaponThings.runProgress) * (0.52F);
				}

				RenderWeaponThings.lastRun2Progress = RenderWeaponThings.run2Progress;
				if (shootTimer == weapon.shootSpeed - 1) {
					RenderWeaponThings.run2Progress = 1F - (1F - RenderWeaponThings.run2Progress) * (0.3F);
				} else {
					RenderWeaponThings.run2Progress *= (0.3F);
				}

				RenderWeaponThings.lastRun3Progress = RenderWeaponThings.run3Progress;
				if (stack.getItem() instanceof ItemWeapon && !(stack.getTagCompound().getFloat("reloadTime") > 6F)) {
					RenderWeaponThings.run3Progress *= (0.55F);
				} else {
					RenderWeaponThings.run3Progress = 1F - (1F - RenderWeaponThings.run3Progress) * (0.6F);
				}
				RenderWeaponThings.lastRun4Progress = RenderWeaponThings.run4Progress;
				if (RenderWeaponThings.livingdown) {
					RenderWeaponThings.run4Progress -= (0.018F);
					if (RenderWeaponThings.run4Progress < 0.045F) {
						RenderWeaponThings.livingdown = false;
					}
				} else {
					RenderWeaponThings.run4Progress += (0.018F);
					if (RenderWeaponThings.run4Progress > 0.9F) {
						RenderWeaponThings.livingdown = true;
					}
				}
				RenderWeaponThings.lastRun5Progress = RenderWeaponThings.run5Progress;
				if (shootTimer < 35 && shootTimer > 10) {

					RenderWeaponThings.run5Progress = 1F - (1F - RenderWeaponThings.run5Progress) * (0.6F);
				} else {
					RenderWeaponThings.run5Progress *= (0.6F);
				}
			}
		}
	}

	@SubscribeEvent
	public void onRenderTick(TickEvent.ClientTickEvent event) {
		if (mc.theWorld != null && mc.renderViewEntity != null) {
			if (!aiming && originalFOV != mc.gameSettings.mouseSensitivity && ifelse == 1F) {
				originalFOV = mc.gameSettings.mouseSensitivity;
			}
			ItemStack gunStack = this.mc.thePlayer.getCurrentEquippedItem();

			if (useTimer > 0)
				useTimer--;

			if (this.mc.thePlayer.getCurrentEquippedItem() != null && this.mc.currentScreen == null && this.mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemWeapon && changelock == 0) {
				ItemWeapon gun = (ItemWeapon) gunStack.getItem();
				if (Mouse.isButtonDown(0) && this.timer == 0 && gun.isAutoMode() && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() || Minecraft.getMinecraft().thePlayer.isPotionActive(Potion.moveSpeed))) {
					if (gunStack.getTagCompound().getInteger("ammo") > 0 && gunStack.getTagCompound().getFloat("reloadTime") == 0) {
						createDamageVec(1F);
						PacketHandler.INSTANCE.sendToServer(new PacketShSound(1));
						PlayerRenderHandler.scaleCrosshair += 0.3;
						mc.theWorld.spawnEntityInWorld(new EntityBulletCasing(mc.theWorld, mc.renderViewEntity));
						timer = gun.getShootSpeed();
						this.mc.thePlayer.setAngles(gunStack.getTagCompound().getFloat("recoil"), gunStack.getTagCompound().getFloat("recoil") / 0.15F);
						gun.setOtdachaTimer();
//						IEntityPlayerSP sm = SmartMoving.getPlayerBase(mc.thePlayer);
//						net.smart.moving.SmartMoving m = sm.getMoving();
//						if (m.isCrawling) {
//							PacketHandler.INSTANCE.sendToServer(new PacketShootAll(mc.thePlayer.getCommandSenderName(), true));
//						} else {
//							PacketHandler.INSTANCE.sendToServer(new PacketShootAll(mc.thePlayer.getCommandSenderName(), false));
//						}
						gunStack.getTagCompound().setInteger("ammo", gunStack.getTagCompound().getInteger("ammo") - 1);
						if (!(gunStack.getTagCompound().getString("stvol").length() > 2))
							mc.thePlayer.playSound(gun.getShootSound(), 2, 1);
						else
							mc.thePlayer.playSound(gun.getSilSound(), 1, 1);
					}
				}
				if (Mouse.isButtonDown(0) && this.timer == 0 && !gun.isAutoMode() && !this.shooting && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() || Minecraft.getMinecraft().thePlayer.isPotionActive(Potion.moveSpeed)) && changelock == 0) {
					if (gunStack.getTagCompound().getInteger("ammo") > 0 && gunStack.getTagCompound().getFloat("reloadTime") == 0) {
						createDamageVec(1F);
						PacketHandler.INSTANCE.sendToServer(new PacketShSound(1));
						PlayerRenderHandler.scaleCrosshair += 0.3;
						mc.theWorld.spawnEntityInWorld(new EntityBulletCasing(mc.theWorld, mc.renderViewEntity));
						timer = gun.getShootSpeed();
						this.mc.thePlayer.setAngles(gunStack.getTagCompound().getFloat("recoil"), gunStack.getTagCompound().getFloat("recoil") / 0.15F);
						gun.setOtdachaTimer();
						gunStack.getTagCompound().setInteger("ammo", gunStack.getTagCompound().getInteger("ammo") - 1);
						if (!(gunStack.getTagCompound().getString("stvol").length() > 2))
							mc.thePlayer.playSound(gun.getShootSound(), 2, 1);
						else
							mc.thePlayer.playSound(gun.getSilSound(), 1, 1);

//						IEntityPlayerSP sm = SmartMoving.getPlayerBase(mc.thePlayer);
//						net.smart.moving.SmartMoving m = sm.getMoving();
//						if (m.isCrawling) {
//							PacketHandler.INSTANCE.sendToServer(new PacketShootAll(mc.thePlayer.getCommandSenderName(), true));
//						} else {
//							PacketHandler.INSTANCE.sendToServer(new PacketShootAll(mc.thePlayer.getCommandSenderName(), false));
//						}
					}
					this.shooting = true;
				} else if (!Mouse.isButtonDown(0) && !gun.isAutoMode() && this.shooting) {
					this.shooting = false;
				}
			}
		}
	}

	@SubscribeEvent
	public void lockSwap(net.minecraftforge.client.event.MouseEvent e) {
		if (e.dwheel > 0) {
			changelock = 20;
		}
	}

	@SubscribeEvent
	public void tickers(TickEvent.ClientTickEvent event) {
		if (mc.theWorld != null && mc.renderViewEntity != null) {
			if (this.mc.thePlayer.getCurrentEquippedItem() != null && this.mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemWeapon) {
				ItemWeapon gun = (ItemWeapon) mc.thePlayer.getHeldItem().getItem();
				if (this.timer > 0) {
					this.timer -= 0.5F;
				}
				if (gun.otdachaTimer > 0)
					gun.otdachaTimer -= 0.5F;
			}
			if (this.timer < 0) {
				this.timer = 0;
			}
			if (changelock > 0) {
				changelock--;
			}
			if (numberLastSlot != mc.thePlayer.inventory.currentItem) {
				changelock = 20;
				numberLastSlot = mc.thePlayer.inventory.currentItem;
			} else {
				numberLastSlot = mc.thePlayer.inventory.currentItem;
			}
		}
	}

	@SubscribeEvent
	public void tickers(TickEvent.RenderTickEvent event) {
		RenderWeaponThings.renderTicks = event.renderTickTime;
	}

}