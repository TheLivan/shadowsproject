package ru.xlv.guide.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.guide.XlvsGuideMod;
import ru.xlv.guide.common.GuideUtils;

import java.io.IOException;

@NoArgsConstructor
public class PacketGuideSync implements IPacketOutServer {
    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        GuideUtils.writeGuideList(XlvsGuideMod.INSTANCE.getGuideManager().getGuideList(), bbos);
    }
}
