package ru.krogenit.guns.render;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemGunClient;
import ru.krogenit.guns.network.packet.PacketPlaySoundClient;
import ru.krogenit.guns.network.packet.PacketShootClient;
import ru.krogenit.lighting.LightManagerClient;
import ru.krogenit.shaders.FrameBuffer;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.ShaderProgram;
import ru.krogenit.shaders.pbr.IPBR;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;
import ru.xlv.mochar.XlvsMainMod;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public abstract class GunAnimationHelper extends ItemAnimationHelper implements IItemRenderer, IConfigGson {
    private static final String CONFIG_PATH_FORMAT = "config/renderer/gun/%s_cfg.json";
    public static float curZoom, curSens, prevZoom, prevSens;

    protected float zatvor;
    protected Vector3f knockback = new Vector3f();
    protected Vector3f aim = new Vector3f();
    protected Vector3f predAmin = new Vector3f();
    protected Vector3f sprintingAnim = new Vector3f();
    protected Vector3f emptyAmin = new Vector3f();
    protected Vector3f modifiAnim = new Vector3f();
    protected boolean[] isSound = new boolean[15];

    @Configurable @IgnoreObf
    @Getter protected List<AnimationNode> fullReloadAnimationNodes = new ArrayList<>();
    @Configurable @IgnoreObf
    @Getter protected List<AnimationNode> reloadAnimationNodes = new ArrayList<>();
    @Configurable @IgnoreObf
    @Getter protected List<AnimationNode> unloadAnimationNodes = new ArrayList<>();

    protected boolean isRenderAmmo;
    @Setter @Getter protected boolean isAiming;
    @Setter @Getter protected boolean isPlayPostShootAnim;
    public boolean isPlayUnloadAnim;
    public boolean isPlayLoadAnim;
    public boolean isPlayReloadAnim;
    @Setter @Getter protected boolean isPlayPredAnimation;
    public boolean isPlayCustomAnim;

    @Setter @Getter protected boolean forcePlayAim;
    @Setter @Getter protected boolean forcePlaySprint;

    protected float flashAnimation, flashRotation, weaponUpByNearBlock, prevWeaponUpByNearBlock;
    public int lastAmmo, ammo, bulletsToLoad, previusAmmo, lastAmmoType;

    protected final ItemGun itemGun;

    public GunAnimationHelper(ItemGun itemGun) {
        this.itemGun = itemGun;
        for (int i = 0; i < 15; i++) {
            anim[i] = new Vector3f();
        }
    }

    public void init() {
        load();
        for(AnimationNode node : reloadAnimationNodes) {
            node.init();
        }
        for(AnimationNode node : unloadAnimationNodes) {
            node.init();
        }
        if(fullReloadAnimationNodes == null) {
            fullReloadAnimationNodes = new ArrayList<>();
        }
        for(AnimationNode node : fullReloadAnimationNodes) {
            node.init();
        }
    }

    public boolean isNeedPreAnimation() {
        return false;
    }

    public void updatePredAnimation(ItemStack item) { }

    public abstract void createParticles(ItemStack item, ItemRenderType type, EntityPlayer shooter);

    protected void playSound(int i, String s) {
        if (!isSound[i] && isAnim[i]) {
            isSound[i] = true;
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketPlaySoundClient(s));
        }
    }

    public void clearIsAnims() {
        for (int i = 0; i < 15; i++) {
            isSound[i] = false;
            isAnim[i] = false;
        }
    }

    protected boolean isAllAnimsClear() {
        for (int i = 0; i < 10; i++)
            if (anim[i].length() != 0) return false;
        for(AnimationNode node : reloadAnimationNodes) {
            if(node.getAnimation().length() != 0) return false;
        }
        for(AnimationNode node : unloadAnimationNodes) {
            if(node.getAnimation().length() != 0) return false;
        }
        for(AnimationNode node : fullReloadAnimationNodes) {
            if(node.getAnimation().length() != 0) return false;
        }
        return true;
    }

    public void onEmpty(ItemStack item) {
        if (isNeedPreAnimation()) return;
        else XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketShootClient(0));

        emptyAmin.x = 1f;
        emptyAmin.y = 1f;
        emptyAmin.z = 1f;
    }

    public boolean onShoot(ItemStack item, ItemRenderType type) {
        if (!mc.thePlayer.isSprinting() && !isPlayLoadAnim && !isPlayUnloadAnim && !isPlayPostShootAnim && !isPlayPredAnimation && !isPlayCustomAnim && !isPlayReloadAnim && weaponUpByNearBlock <= 0.1f && isAllAnimsClear()) {
            if (isNeedPreAnimation()) return false;
            lastAmmoType = item.getTagCompound().getBoolean("ammo" + item.getTagCompound().getInteger("ammo")) ? 1 : 0;
            knockback.x = 1f;
            zatvor = 1f;
            flashAnimation = 1f;
            flashRotation = mc.theWorld.rand.nextFloat() * 360;
            isPlayPostShootAnim = true;
            createParticles(item, type, mc.thePlayer);
            return true;
        } else return false;
    }

    public boolean onReload(ItemStack item, EnumReloadType type, int prevAmmo, int newammo) {
        if (!mc.thePlayer.isSprinting() && !isAiming && !isPlayLoadAnim && !isPlayPostShootAnim && !isPlayCustomAnim && !isPlayUnloadAnim && !isPlayPredAnimation && !isPlayReloadAnim && isAllAnimsClear()) {
            switch (type) {
                case RELOAD:
                    isPlayReloadAnim = true;
                    ammo = newammo;
                    bulletsToLoad = newammo;
                    break;
                case UNLOAD:
                    isPlayUnloadAnim = true;
                    previusAmmo = prevAmmo;
                    break;
                case LOAD:
                    isPlayLoadAnim = true;
                    ammo = newammo;
                    bulletsToLoad = newammo;
                    break;
            }

            return true;
        }

        return false;
    }

    public boolean onCustomAnim(ItemStack item, ItemRenderType type) {
        if (!mc.thePlayer.isSprinting() && !isPlayLoadAnim && !isPlayUnloadAnim && !isPlayPostShootAnim && !isPlayPredAnimation && !isPlayCustomAnim && !isPlayReloadAnim && isAllAnimsClear()) {
            isPlayCustomAnim = true;
            return true;
        } else return false;
    }

    public void onFlashLight(ItemStack item, EntityPlayer p, boolean hasGlushak) {
        OtherPlayerRenderInfo info = new OtherPlayerRenderInfo(mc.theWorld.rand.nextFloat() * 360, 1f);
        otherFlashLights.put(p.getDisplayName(), info);
        if(!hasGlushak) LightManagerClient.createWeaponFlashLight(p);
        if(mc.thePlayer != p) {
            createParticles(item, ItemRenderType.EQUIPPED, p);
        }
    }

    protected HashMap<String, OtherPlayerRenderInfo> otherFlashLights = new HashMap<>();

    protected void updateOtherFlashLights(EntityPlayer player) {
        String key = player.getDisplayName();
        OtherPlayerRenderInfo info = otherFlashLights.get(key);
        if (info != null) {
            if (info.knockback > 0) info.knockback -= 0.08f * animSpeed;
            if (info.knockback < 0) otherFlashLights.remove(key);
            if (info.knockback == 0) otherFlashLights.put(key, info);
        }
    }

    static OtherPlayerRenderInfo defaultEquippedRenderInfo = new OtherPlayerRenderInfo(0, 0);

    protected OtherPlayerRenderInfo getOtherPlayerInfo(Object data) {
        if (data instanceof EntityPlayer) {
            OtherPlayerRenderInfo info = otherFlashLights.get(((EntityPlayer) data).getDisplayName());
            if (info != null) return info;
        }

        return defaultEquippedRenderInfo;
    }

    private void finishReload(ItemStack item) {
        ItemGunClient weapon = (ItemGunClient) item.getItem();
        weapon.onReload(item, mc.theWorld, mc.thePlayer, true);
    }

    protected void checkFinishAnimation(int i, ItemStack item) {
        if (isAnim[i]) {
            isPlayLoadAnim = false;
            isPlayPostShootAnim = false;
            clearIsAnims();
            if (isPlayUnloadAnim || isPlayReloadAnim) {
                isPlayReloadAnim = false;
                isPlayUnloadAnim = false;
                finishReload(item);
            }
        }
    }

    public void createBasePaticles(ItemStack item, ItemRenderType type, Vector3f sleeveSpawn, Vector3f smokeSpawn, boolean spawnSleeve, boolean needSmoke, int smokeSize, EntityPlayer shooter) {
        float x = (float) shooter.posX;
        float y = (float) shooter.posY;
        float z = (float) shooter.posZ;

        if(mc.thePlayer != shooter) {
            smokeSpawn.y += shooter.getEyeHeight() - 0.1f;
            smokeSpawn.z += 0.5f;
        }

//        if(spawnSleeve) {
//            EntitySleeve sleeve = new EntitySleeve(mc.theWorld, shooter, true, new Vector3f(sleeveSpawn.x, sleeveSpawn.y, sleeveSpawn.z), lastAmmoType, 0f);
//            mc.theWorld.spawnEntityInWorld(sleeve);
//        }

//        if(needSmoke) {
//            MathRotateHelper.rotateAboutX(smokeSpawn, Math.toRadians(shooter.rotationPitch));
//            MathRotateHelper.rotateAboutY(smokeSpawn, Math.toRadians(0 - shooter.rotationYaw));
//            EntitySmokeFXGun fx = new EntitySmokeFXGun(mc.theWorld, x + smokeSpawn.x, y + smokeSpawn.y, z + smokeSpawn.z);
//            mc.theWorld.spawnEntityInWorld(fx);
//
//            for(int i=0;i<smokeSize;i++) {
//                fx = new EntitySmokeFXGun(mc.theWorld, x + smokeSpawn.x, y + smokeSpawn.y, z + smokeSpawn.z, false, true);
//                float m = 80f;
//                float s = (mc.theWorld.rand.nextFloat() - 0.5f)/m;
//                float s1 = (mc.theWorld.rand.nextFloat() - 0.5f)/m;
//                Vector3f speed = new Vector3f(s,s1,0);
//                MathRotateHelper.rotateAboutX(speed, Math.toRadians(shooter.rotationPitch));
//                MathRotateHelper.rotateAboutY(speed, Math.toRadians(0 - shooter.rotationYaw));
//                fx.motionX = speed.x;
//                fx.motionY = speed.y;
//                fx.motionZ = speed.z;
//                mc.theWorld.spawnEntityInWorld(fx);
//            }
//        }
    }

    protected void playZoom() {
        boolean hasKolimator = false;
        boolean hasInfrared = false;
        boolean hasOptica = false;

        if(curZoom == 0.0f) {
            prevZoom = mc.gameSettings.fovSetting;
            prevSens = mc.gameSettings.mouseSensitivity;
        }

        if(hasKolimator) {
            if (curZoom < 20) {
                float speed = 2f*animSpeed;
                mc.gameSettings.fovSetting -= speed;
                mc.gameSettings.mouseSensitivity -= speed/200.0f;
                curZoom += speed;
                curSens += speed/200.0f;
                if(mc.gameSettings.fovSetting <= 30f) {
                    curZoom += (mc.gameSettings.fovSetting-30);
                    mc.gameSettings.fovSetting = 30f;
                }
                if(mc.gameSettings.mouseSensitivity <= 0.25f) {
                    curSens += (mc.gameSettings.mouseSensitivity - 0.25f);
                    mc.gameSettings.mouseSensitivity = 0.25f;
                }
            }
        } else if(hasInfrared) {
            if (curZoom < 40) {
                float speed = 4f*animSpeed;
                mc.gameSettings.fovSetting -= speed;
                mc.gameSettings.mouseSensitivity -= speed/200.0f;
                curZoom += speed;
                curSens += speed/200.0f;
                if(mc.gameSettings.fovSetting <= 30f) {
                    curZoom += (mc.gameSettings.fovSetting-30);
                    mc.gameSettings.fovSetting = 30f;
                }
                if(mc.gameSettings.mouseSensitivity <= 0.15f) {
                    curSens += (mc.gameSettings.mouseSensitivity - 0.15f);
                    mc.gameSettings.mouseSensitivity = 0.15f;
                }
            }
        } else if(hasOptica) {
            if (curZoom < 65) {
                float speed = 6f*animSpeed;
                mc.gameSettings.fovSetting -= speed;
                mc.gameSettings.mouseSensitivity -= speed/200.0f;
                curZoom += speed;
                curSens += speed/200.0f;
                if(mc.gameSettings.fovSetting <= 30f) {
                    curZoom += (mc.gameSettings.fovSetting-30);
                    mc.gameSettings.fovSetting = 30f;
                }

                if(mc.gameSettings.mouseSensitivity <= 0.1f) {
                    curSens += (mc.gameSettings.mouseSensitivity - 0.1f);
                    mc.gameSettings.mouseSensitivity = 0.1f;
                }
            }
        }
    }

    protected void playUnzoom() {
        float speed = 40f*animSpeed;
        if (curZoom > 0) {
            mc.gameSettings.fovSetting += speed;
            curZoom -=speed;
            if(curZoom < 0) {
                mc.gameSettings.fovSetting += (curZoom);
                curZoom = 0;

                if(mc.gameSettings.fovSetting != prevZoom)
                    mc.gameSettings.fovSetting = prevZoom;
            }
        }
        if(curSens > 0) {
            mc.gameSettings.mouseSensitivity += speed/200.0f;
            curSens -= speed/200.0f;
            if(curSens<0) {
                mc.gameSettings.mouseSensitivity += (curSens);
                curSens = 0;

                if(mc.gameSettings.mouseSensitivity != prevSens)
                    mc.gameSettings.mouseSensitivity = prevSens;
            }
        }
    }

    public void updateAim() {
        if ((Mouse.isButtonDown(1) && mc.currentScreen == null && !isPlayCustomAnim && !isPlayLoadAnim && !isPlayUnloadAnim && !isPlayReloadAnim && !mc.thePlayer.isSprinting()) || forcePlayAim) {
            if (!isAiming) {
                playZoomInSound();
            }
            playAnimation(aim, 0.1f, 1, 1, EnumAnimationType.SlowEnd);
            isAiming = true;
            playZoom();
        } else {
            if (isAiming) playZoomOutSound();
            isAiming = false;
            playAnimation(aim, -0.1f, -1, -1, EnumAnimationType.SlowEnd);
            playUnzoom();
        }
    }

    protected void playZoomInSound() {
    }

    protected void playZoomOutSound() {
    }

    public void updateKnockback() {
        if (knockback.x > 0) {
            playAnimation(knockback, -0.1f, -1, -1, EnumAnimationType.SlowEnd);
        } else if (knockback.x < 0) knockback.x = 0;
    }

    private void updateSprintAnim(boolean isForward) {
        if (isForward) playAnimation(sprintingAnim, 0.06f, 0.06f, 0.06f, EnumAnimationType.SlowEnd);
        else playAnimation(sprintingAnim, -0.06f, -0.06f, -0.06f, EnumAnimationType.SlowEnd);
    }

    public void updateBase(ItemStack item, ItemRenderType type) {
        if (!isPlayLoadAnim && !isPlayUnloadAnim && !isPlayCustomAnim) {
            if (mc.thePlayer.isSprinting() || forcePlaySprint) updateSprintAnim(true);
            else {
                updateIDLEanim(!isAiming);
                updateSprintAnim(false);
            }
            if (item.getTagCompound() != null) {
                if (item.getTagCompound().getBoolean("hasammo")) isRenderAmmo = true;
                else isRenderAmmo = false;
                ammo = item.getTagCompound().getInteger("ammo");
                if (lastAmmo == 0) lastAmmo = ammo;
            }
        } else updateIDLEanim(false);

        if(isPlayCustomAnim) {
            updateCustomAnim(item, type);
        }

        if(zatvor > 0) {
            zatvor -= animSpeed * 0.15f;
            if(zatvor < 0) zatvor = 0;
        }
    }

    protected void updateCustomAnim(ItemStack item, ItemRenderType type) {

    }

    protected void updateIDLEanim(boolean type) {
        EntityPlayer p = mc.thePlayer;
        float dist = 1;
        Vec3 vec3 = Vec3.createVectorHelper(TileEntityRendererDispatcher.staticPlayerX, TileEntityRendererDispatcher.staticPlayerY + (double) p.getEyeHeight(), TileEntityRendererDispatcher.staticPlayerZ);
        Vec3 vec31 = Vec3.createVectorHelper(TileEntityRendererDispatcher.staticPlayerX + p.getLookVec().xCoord * dist, TileEntityRendererDispatcher.staticPlayerY + p.getLookVec().yCoord * dist + (double) p.getEyeHeight(), TileEntityRendererDispatcher.staticPlayerZ + p.getLookVec().zCoord * dist);
        MovingObjectPosition mop = mc.theWorld.rayTraceBlocks_do_do(vec3, vec31, true, false, false);

        if (mop != null && mop.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK) {
            prevWeaponUpByNearBlock = (float) mop.hitVec.distanceTo(vec31);
            if (prevWeaponUpByNearBlock < 0) prevWeaponUpByNearBlock = 0;
        } else prevWeaponUpByNearBlock = 0;

        if (weaponUpByNearBlock > prevWeaponUpByNearBlock) {
            weaponUpByNearBlock -= 0.05f * animSpeed;
            if(weaponUpByNearBlock < prevWeaponUpByNearBlock) weaponUpByNearBlock = prevWeaponUpByNearBlock;
        } else if(weaponUpByNearBlock < prevWeaponUpByNearBlock) {
            weaponUpByNearBlock += 0.05f * animSpeed;
            if(weaponUpByNearBlock > prevWeaponUpByNearBlock) weaponUpByNearBlock = prevWeaponUpByNearBlock;
        }
    }

    protected void setUnloadReloadSpeed() {
        double value = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.WEAPON_RELOAD_MOD).getValue();
        if (isPlayLoadAnim) {
            animSpeed *= itemGun.getReloadSpeed() * value;
        } else if (isPlayUnloadAnim) {
            animSpeed *= itemGun.getUnloadSpeed() * value;
        } else if(isPlayReloadAnim) {
            animSpeed *= itemGun.getReloadSpeed() * value;
        }
    }

    protected void updateReloadAnimation(ItemStack item) {
        if (isPlayLoadAnim) {
            int id = 0;
            for(AnimationNode node : reloadAnimationNodes) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(id++, reloadAnimationNodes.get(node.getId()).getAnimation(), speed.x, speed.y, speed.z, node.getType());
            }

            checkFinishAnimation(id > 0 ? id - 1 : 0, item);
        } else if(isPlayUnloadAnim) {
            int id = 0;
            for(AnimationNode node : unloadAnimationNodes) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(id++, unloadAnimationNodes.get(node.getId()).getAnimation(), speed.x, speed.y, speed.z, node.getType());
            }

            checkFinishAnimation(id > 0 ? id - 1 : 0, item);
        } else if(isPlayReloadAnim) {
            int id = 0;
            for(AnimationNode node : fullReloadAnimationNodes) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(id++, fullReloadAnimationNodes.get(node.getId()).getAnimation(), speed.x, speed.y, speed.z, node.getType());
            }

            checkFinishAnimation(id > 0 ? id - 1 : 0, item);
        } else {
            for(AnimationNode node : reloadAnimationNodes) {
                playAnimation(node.getAnimation(), -0.1f, -0.1f, -0.1f, node.getType());
            }
            for(AnimationNode node : unloadAnimationNodes) {
                playAnimation(node.getAnimation(), -0.1f, -0.1f, -0.1f, node.getType());
            }
            for(AnimationNode node : fullReloadAnimationNodes) {
                playAnimation(node.getAnimation(), -0.1f, -0.1f, -0.1f, node.getType());
            }

            if (isAllAnimsClear()) {
                clearIsAnims();
                if(startAnimationTime != 0) {
                    totalAnimationTime = System.currentTimeMillis() - startAnimationTime;
                    startAnimationTime = 0;
                }
            }
        }
    }

    public abstract void updatePostShootAnimation();

    @Override
    public boolean handleRenderType(ItemStack item, IItemRenderer.ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(IItemRenderer.ItemRenderType type, ItemStack item, IItemRenderer.ItemRendererHelper helper) {
        return false;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {

    }

    float uMin = 0;
    float vMin = 0;
    float uMax = 0f;
    float vMax = 0f;

    private static final TextureDDS flashTexture = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/part_iskry.dds"));

    protected void renderFlash(float knockback, float rotation, ItemStack item) {
        if (knockback <= 0.0f || KrogenitShaders.lightPass) return;
        IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
        glDisable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        glAlphaFunc(GL_GREATER, 0.000001F);
        glDepthMask(false);
        glColor4f(1f, 1f, 1f, 1f);
        glRotatef(rotation, 0, 0, 1);
        TextureLoaderDDS.bindTexture(flashTexture);
        TextureLoaderDDS.bindEmissionMap(flashTexture, shader, 2f);
        //if (flashTexture.texture != 0) {
        int offset = (int) ((1f - knockback) * 25);

        if(offset > 15) {
            uMin = 0.75f;
            vMin = 0.75f;
            vMax = 1f;
            uMax = 1f;
        } else {
            uMin = 0;
            vMin = 0;
            vMax = 0.25f;
            uMax = 0.25f;

            for(int i=0;i<offset;i++) {
                uMin += 0.25f;
                uMax += 0.25f;
                if(uMax == 1.25f) {
                    vMin += 0.25f;
                    vMax += 0.25f;
                    uMin = 0.0f;
                    uMax = 0.25f;
                }
            }
        }

        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        t.addVertexWithUV(-5, 5, 1, uMin, vMax);
        t.addVertexWithUV(5, 5, 1, uMax, vMax);
        t.addVertexWithUV(5, -5, 1, uMax, vMin);
        t.addVertexWithUV(-5, -5, 1, uMin, vMin);
        t.draw();
        // }
        glAlphaFunc(GL_GREATER, 0.1F);
        glDisable(GL_BLEND);
        glEnable(GL_CULL_FACE);
        glColor4f(1f, 1f, 1f, 1f);
        glDepthMask(true);
        flashAnimation -= animSpeed * 0.06f;
        shader.setEmissionMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }

    protected static int lensTexture;
    private static FrameBuffer lensFrameBuffer;

    protected int prepareLensTexture(float zoom, float uMin, float uMax, float vMin, float vMax) {
        if (lensTexture == 0) {
            lensTexture = glGenTextures();
            lensFrameBuffer = new FrameBuffer(512, 512);
//            mc.checkGLError("CREATE FRAME BUFFER");
        }
        int off = 256;
        glBindTexture(GL_TEXTURE_2D, lensFrameBuffer.getTexture());
        //забираем информацию из дефолтного фрейм буффера
        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, mc.displayWidth / 2 - off, mc.displayHeight / 2 - off, off * 2, off * 2, 0);
//        mc.checkGLError("COPY TEXTURE");
//        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0 ,0, mc.displayWidth, mc.displayHeight, 0);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        {
            glLoadIdentity();
            glOrtho(0.0D, 1, 0.0D, 1, 0.0D, 1.0D);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            {
                glLoadIdentity();
                ShaderProgram currentShader = KrogenitShaders.getCurrentShader();
                currentShader.disable();
                glDisable(GL_DEPTH_TEST);
                glDepthMask(false);
                glDisable(GL_ALPHA_TEST);
                glDisable(GL_LIGHTING);
                mc.entityRenderer.disableLightmap(0);

                //получаем информацию из текущего буфера в новый
                lensFrameBuffer.bind(true);
//                mc.checkGLError("BIND BUFFER");
                lensFrameBuffer.clear();
//                mc.checkGLError("CLEAR BUFFER");

                glBindTexture(GL_TEXTURE_2D, mc.getFramebuffer().framebufferTexture);
                Tessellator tessellator = Tessellator.instance;
                //скейлинг для сохранения аспекта под размер экрана
                float f = mc.displayWidth > mc.displayHeight ? ((mc.displayWidth / (float) mc.displayHeight) - 1f) * 16f : 0;
                float f1 = mc.displayHeight > mc.displayWidth ? ((mc.displayHeight / (float) mc.displayWidth) - 1f) * 9f : 0;
                tessellator.startDrawingQuads();
                tessellator.addVertexWithUV((0f + zoom) - f, 0f + zoom - f1, 0f, uMin, vMax);
                tessellator.addVertexWithUV((1 - zoom) + f, 0f + zoom - f1, 0f, uMax, vMax);
                tessellator.addVertexWithUV((1 - zoom) + f, 1 - zoom + f1, 0f, uMax, vMin);
                tessellator.addVertexWithUV((0f + zoom) - f, 1 - zoom + f1, 0f, uMin, vMin);
                tessellator.draw();
//                mc.checkGLError("RENDER TO BUFFER");

                glEnable(GL_DEPTH_TEST);
                glEnable(GL_ALPHA_TEST);
                glDepthMask(true);
                glEnable(GL_LIGHTING);
                mc.entityRenderer.enableLightmap(0);
                currentShader.enable();
                mc.getFramebuffer().bindFramebuffer(true);
//                mc.checkGLError("BIND MINECRAFT BUFFER");
            }
            glPopMatrix();
            glMatrixMode(GL_PROJECTION);
        }
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);

        return lensFrameBuffer.getTexture();
    }

    protected float getXWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : reloadAnimationNodes) {
            value += (node.getWeights()[weightId].x * node.getAnimation().x);
        }
        for(AnimationNode node : unloadAnimationNodes) {
            value += (node.getWeights()[weightId].x * node.getAnimation().x);
        }
        for(AnimationNode node : fullReloadAnimationNodes) {
            value += (node.getWeights()[weightId].x * node.getAnimation().x);
        }

        return value;
    }

    protected float getYWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : reloadAnimationNodes) {
            value += (node.getWeights()[weightId].y * node.getAnimation().y);
        }
        for(AnimationNode node : unloadAnimationNodes) {
            value += (node.getWeights()[weightId].y * node.getAnimation().y);
        }
        for(AnimationNode node : fullReloadAnimationNodes) {
            value += (node.getWeights()[weightId].y * node.getAnimation().y);
        }

        return value;
    }

    protected float getZWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : reloadAnimationNodes) {
            value += (node.getWeights()[weightId].z * node.getAnimation().z);
        }
        for(AnimationNode node : unloadAnimationNodes) {
            value += (node.getWeights()[weightId].z * node.getAnimation().z);
        }
        for(AnimationNode node : fullReloadAnimationNodes) {
            value += (node.getWeights()[weightId].z * node.getAnimation().z);
        }

        return value;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemGun.getUnlocalizedName()));
    }
}
