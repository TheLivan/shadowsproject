package ru.krogenit.guns.render;

public class OtherPlayerRenderInfo {
    public float flashRotation, knockback;

    public OtherPlayerRenderInfo(float flashRotation, float knockback) {
        this.flashRotation = flashRotation;
        this.knockback = knockback;
    }
}