package ru.krogenit.guns.render;

public enum EnumAnimationType {
    Defualt, SlowEnd, SlowStart, SlowStartAndEnd,
    FastXOnYZ,
    FastYOnX, FastYOnZ, FastXOnY, FastZOnY, FastXZOnY,
    FastYOnXZ, FastYZOnX,
    YOnXZ, XZOnY;
}
