package ru.krogenit.guns.render.entity;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.guns.entity.EntitySmokeFXGun;
import ru.krogenit.utils.AnimationHelper;

import static org.lwjgl.opengl.GL11.*;

public class RenderEntitySmokeFX extends Render {

    private static final Minecraft mc = Minecraft.getMinecraft();

    @Override
    public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        postRender = true;
    }

    @Override
    public void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        EntitySmokeFXGun smoke = (EntitySmokeFXGun) entity;
        Vector4f color = smoke.color;

        float speed = AnimationHelper.getAnimationSpeed();
        smoke.rotaion.z += smoke.rotaion.x * speed;
        smoke.rotaion.w += 0.0005f * speed;

        if (smoke.rotaion.x < 0) {
            smoke.rotaion.x += 0.001f * speed;
            if (smoke.rotaion.x > 0) smoke.rotaion.x = 0;
        } else if (smoke.rotaion.x > 0) {
            smoke.rotaion.x -= 0.001f * speed;
            if (smoke.rotaion.x < 0) smoke.rotaion.x = 0;
        }

        if(smoke.isMoving) {
            smoke.color.w -= 0.00025f * speed;
            if (smoke.color.w < -0.0f) {
                smoke.setDead();
            }
        } else {
            if (!smoke.fadeOut) {
                smoke.color.w += 0.004f * speed;
                if (smoke.color.w >= 0.15f) smoke.fadeOut = true;
            } else {
                smoke.color.w -= 0.002f * speed;
                if (smoke.color.w < -0.0f) {
                    smoke.setDead();
                }
            }
        }

        glPushMatrix();
        glTranslatef((float) x, (float) y, (float) z);
        float scale = smoke.rotaion.w;
        glScalef(scale, scale, scale);
        glColor4f(color.x, color.y, color.z, 1f);

        if(mc.gameSettings.thirdPersonView == 2) {
            glRotatef(180F-this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
            glRotatef(this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
        } else {
            glRotatef(180F-this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
            glRotatef(-this.renderManager.playerViewX/2f, 1.0F, 0.0F, 0.0F);
        }
        glRotatef(smoke.rotaion.z, 0, 0, 1);
        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        t.setNormal(0.0F, 1.0F, 0.0F);
        t.addVertexWithUV(-1, (double) -1, 0, smoke.uMin, smoke.vMax);
        t.addVertexWithUV(1, (double) -1, 0, smoke.uMax, smoke.vMax);
        t.addVertexWithUV(1, (double) 1, 0, smoke.uMax, smoke.vMin);
        t.addVertexWithUV(-1, (double) 1, 0, smoke.uMin, smoke.vMin);
        t.draw();
        glPopMatrix();
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        return null;
    }
}
