package ru.krogenit.guns.render;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.client.IItemRenderer;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;
import ru.xlv.core.util.obf.IgnoreObf;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public abstract class AbstractMeleeWeaponRenderer extends ItemAnimationHelper implements IItemRenderer, IConfigGson {
    private static final String CONFIG_PATH_FORMAT = "config/renderer/melee/%s_cfg.json";

    protected Model weaponModel;
    protected ItemMeleeWeaponClient itemMelee;
    @Getter @Setter protected boolean isPlayHitAnimation, isPlayUseAnimation;
    @Configurable(comment = "rightClickAnimations") @IgnoreObf
    @Getter protected List<AnimationNode> rightClickAnimations = new ArrayList<>();
    @Configurable(comment = "hitAnimations") @IgnoreObf
    @Getter protected List<AnimationNode> hitAnimations = new ArrayList<>();

    public AbstractMeleeWeaponRenderer(Model weaponModel, ItemMeleeWeaponClient itemMelee) {
        this.weaponModel = weaponModel;
        this.itemMelee = itemMelee;
    }

    public void init() {
        load();
        for(AnimationNode node : rightClickAnimations) {
            node.init();
        }
        for(AnimationNode node : hitAnimations) {
            node.init();
        }
        if(inHudPosition == null) inHudPosition = new Vector3fConfigurable(23, 7, -700);
        if(inHudRotation == null) inHudRotation = new Vector3fConfigurable(0f, 80, -176);
        if(inHudScale == null) inHudScale = new Vector3fConfigurable(5, 5, 5);
    }

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return false;
    }

    public abstract void renderInModifyGui(ItemStack itemStack);
    protected abstract void renderWeapon();

    @Configurable(comment = "allPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable allPosition = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "allRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable allRotation = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "allScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable allScale = new Vector3fConfigurable(1f, 1f, 1f);

    @Configurable(comment = "gunPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunPosition = new Vector3fConfigurable(0.0f, 0f, 1.75f);
    @Configurable(comment = "gunRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunRotation = new Vector3fConfigurable(0.0f, 0f, 0f);
    @Configurable(comment = "gunScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable gunScale = new Vector3fConfigurable(1f, 1f, 1f);

    protected void renderFirstPerson(ItemStack itemStack) {
        glRotatef(28F, 0, 0, 1);
        glRotatef(86F, 0, 1, 0);

        glTranslatef(allPosition.x + getXWeights(0), allPosition.y + getYWeights(0), allPosition.z + getZWeights(0));
        glScalef(allScale.x, allScale.y, allScale.z);
        glRotatef(allRotation.x + getXWeights(1), 1, 0, 0);
        glRotatef(allRotation.z + getZWeights(1), 0, 0, 1);
        glRotatef(allRotation.y + getYWeights(1), 0, 1, 0);

        glPushMatrix();
        {
            glTranslatef(gunPosition.x + getXWeights(2), gunPosition.y + getYWeights(2), gunPosition.z + getZWeights(2));
            glRotatef(gunRotation.x + getXWeights(3), 1, 0, 0);
            glRotatef(gunRotation.z + getZWeights(3), 0, 0, 1);
            glRotatef(gunRotation.y + getYWeights(3), 0, 1, 0);
            glScalef(gunScale.x, gunScale.y, gunScale.z);
            renderWeapon();
        }
        glPopMatrix();

        renderLeftHand();
        renderRightHand();
    }

    @Configurable(comment = "equippedPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedPosition = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "equippedRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedRotation = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "equippedScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable equippedScale = new Vector3fConfigurable(1f, 1f, 1f);

    protected void renderEquipped(ItemStack itemStack) {
        glRotatef(-2, 1, 0, 0);
        glRotatef(35, 0, 0, 1);
        glRotatef(85, 0, 1, 0);
        glTranslatef(equippedPosition.x, equippedPosition.y, equippedPosition.z);
        glRotatef(equippedRotation.x, 1, 0, 0);
        glRotatef(equippedRotation.z, 0, 0, 1);
        glRotatef(equippedRotation.y, 0, 1, 0);
        glScalef(equippedScale.x, equippedScale.y, equippedScale.z);
        renderWeapon();
    }

    @Configurable(comment = "inventoryPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryPosition = new Vector3fConfigurable(6.3f, 7f, 0f);
    @Configurable(comment = "inventoryRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryRotation = new Vector3fConfigurable(0f, -90f, -220f);
    @Configurable(comment = "inventoryScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable inventoryScale = new Vector3fConfigurable(1f, 1f, 1f);

    protected void renderInventory(ItemStack itemStack) {
        glTranslatef(inventoryPosition.x, inventoryPosition.y, inventoryPosition.z);
        glRotatef(inventoryRotation.x, 1, 0, 0);
        glRotatef(inventoryRotation.z, 0, 0, 1);
        glRotatef(inventoryRotation.y, 0, 1, 0);
        glScalef(inventoryScale.x, inventoryScale.y, inventoryScale.z);
        renderWeapon();
    }

    @Configurable(comment = "entityPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityPosition = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "entityRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityRotation = new Vector3fConfigurable(0f, 0f, 0f);
    @Configurable(comment = "entityScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable entityScale = new Vector3fConfigurable(1f, 1f, 1f);

    protected void renderAsEntity(ItemStack itemStack) {
        glTranslatef(entityPosition.x, entityPosition.y, entityPosition.z);
        glRotatef(entityRotation.x, 1, 0, 0);
        glRotatef(entityRotation.z, 0, 0, 1);
        glRotatef(entityRotation.y, 0, 1, 0);
        glScalef(entityScale.x, entityScale.y, entityScale.z);
        renderWeapon();
    }

    @Configurable(comment = "leftHandPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandPosition = new Vector3fConfigurable(0.6f, 0.6f, 0.2f);
    @Configurable(comment = "leftHandRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandRotation = new Vector3fConfigurable(90F, 5, 25);
    @Configurable(comment = "leftHandScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable leftHandScale = new Vector3fConfigurable(1.5f, 2.5f, 1.5f);

    protected void renderLeftHand() {
        KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        glTranslatef(leftHandPosition.x + getXWeights(4), leftHandPosition.y + getYWeights(4), leftHandPosition.z + getZWeights(4));
        glScaled(leftHandScale.x, leftHandScale.y, leftHandScale.z);
        glRotatef(leftHandRotation.x + getXWeights(5), 1, 0, 0);
        glRotatef(leftHandRotation.z + getZWeights(5), 0, 0, 1);
        glRotatef(leftHandRotation.y + getYWeights(5), 0, 1, 0);
        hand.renderLeft();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    @Configurable(comment = "rightHandPosition") @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandPosition = new Vector3fConfigurable(-0.2f, 0.6f, 0.3f);
    @Configurable(comment = "rightHandRotation") @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandRotation = new Vector3fConfigurable(80f, 0f, -5f);
    @Configurable(comment = "rightHandScale") @IgnoreObf
    @Getter protected final Vector3fConfigurable rightHandScale = new Vector3fConfigurable(1.5f, 2.5f, 1.5f);

    protected void renderRightHand() {
        KrogenitShaders.getCurrentPBRShader(true);
        glPushMatrix();
        glTranslatef(rightHandPosition.x + getXWeights(6), rightHandPosition.y + getYWeights(6), rightHandPosition.z + getZWeights(6));
        glScaled(rightHandScale.x, rightHandScale.y, rightHandScale.z);
        glRotatef(rightHandRotation.x + getXWeights(7), 1, 0, 0);
        glRotatef(rightHandRotation.z + getZWeights(7), 0, 0, 1);
        glRotatef(rightHandRotation.y + getYWeights(7), 0, 1, 0);
        hand.renderRight();
        glPopMatrix();
        KrogenitShaders.finishCurrentShader();
    }

    @Configurable(comment = "inHudPosition") @IgnoreObf
    @Getter protected Vector3fConfigurable inHudPosition = new Vector3fConfigurable(23f, 7f, -700f);
    @Configurable(comment = "inHudRotation") @IgnoreObf
    @Getter protected Vector3fConfigurable inHudRotation = new Vector3fConfigurable(0f, 80f, -176f);
    @Configurable(comment = "inHudScale") @IgnoreObf
    @Getter protected Vector3fConfigurable inHudScale = new Vector3fConfigurable(5f, 5f, 5f);

    public void renderGunInHud(IPBR shader) {
        glPushMatrix();
        glTranslatef(inHudPosition.x, inHudPosition.y, inHudPosition.z);
        glScaled(inHudScale.x, inHudScale.y, inHudScale.z);
        glRotatef(inHudRotation.x, 1, 0, 0);
        glRotatef(inHudRotation.z, 0, 0, 1);
        glRotatef(inHudRotation.y, 0, 1, 0);
        weaponModel.render(shader);
        glPopMatrix();
    }

    private void updateAnimations() {
        if(mc.thePlayer.getItemInUseCount() > 0 || isPlayUseAnimation) {
            int id = 0;
            for(AnimationNode node : rightClickAnimations) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(id++, rightClickAnimations.get(node.getId()).getAnimation(), speed.x, speed.y, speed.z, node.getType());
            }
        } else {
            for(AnimationNode node : rightClickAnimations) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(node.getAnimation(), -speed.x, -speed.y, -speed.z, node.getType());
            }

            if(isAllAnimsClear()) clearIsAnims();
        }

        if(isPlayHitAnimation) {
            int id = 0;
            for(AnimationNode node : hitAnimations) {
                Vector3fConfigurable speed = node.getSpeed();
                playAnimation(id++, hitAnimations.get(node.getId()).getAnimation(), speed.x, speed.y, speed.z, node.getType());
            }

            checkFinishAnimation(id > 0 ? id - 1 : 0);

            if(!isPlayHitAnimation) {
                MovingObjectPosition objectMouseOver = mc.objectMouseOver;
                if (objectMouseOver.entityHit != null &&!(objectMouseOver.entityHit instanceof EntityItem))
                    mc.playerController.attackEntity(mc.thePlayer, objectMouseOver.entityHit);
            }
        } else {
            for(AnimationNode node : hitAnimations) {
                playAnimation(node.getAnimation(), -0.1f, -0.1f, -0.1f, EnumAnimationType.SlowEnd);
            }

            if(isAllAnimsClear()) {
                clearIsAnims();
                if(startAnimationTime != 0) {
                    totalAnimationTime = System.currentTimeMillis() - startAnimationTime;
                    startAnimationTime = 0;
                }
            }
        }
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data) {
        animSpeed = AnimationHelper.getAnimationSpeed();
        switch (type) {
            case EQUIPPED_FIRST_PERSON:
                if(!KrogenitShaders.lightPass) {
                    updateAnimations();
                }
                renderFirstPerson(itemStack);
                return;
            case EQUIPPED:
                renderEquipped(itemStack);
                return;
            case INVENTORY:
                renderInventory(itemStack);
                return;
            case ENTITY:
                renderAsEntity(itemStack);
        }
    }

    @Override
    public void renderItemPost(ItemRenderType type, ItemStack item, Object... data) {

    }

    protected float getXWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : rightClickAnimations) {
            value += (node.getWeights()[weightId].x * node.getAnimation().x);
        }
        for(AnimationNode node : hitAnimations) {
            value += (node.getWeights()[weightId].x * node.getAnimation().x);
        }
        return value;
    }

    protected float getYWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : rightClickAnimations) {
            value += (node.getWeights()[weightId].y * node.getAnimation().y);
        }
        for(AnimationNode node : hitAnimations) {
            value += (node.getWeights()[weightId].y * node.getAnimation().y);
        }

        return value;
    }

    protected float getZWeights(int weightId) {
        float value = 0f;
        for(AnimationNode node : rightClickAnimations) {
            value += (node.getWeights()[weightId].z * node.getAnimation().z);
        }
        for(AnimationNode node : hitAnimations) {
            value += (node.getWeights()[weightId].z * node.getAnimation().z);
        }

        return value;
    }

    protected boolean isAllAnimsClear() {
        for(AnimationNode node : rightClickAnimations) {
            if(node.getAnimation().length() != 0) return false;
        }
        for(AnimationNode node : hitAnimations) {
            if(node.getAnimation().length() != 0) return false;
        }
        return true;
    }

    public void clearIsAnims() {
        for (int i = 0; i < 15; i++) {
            isAnim[i] = false;
        }
    }

    protected void checkFinishAnimation(int i) {
        if (isAnim[i]) {
            isPlayHitAnimation = false;
            clearIsAnims();
        }
    }

    public boolean canHit() {
        return !isPlayHitAnimation && isAllAnimsClear();
    }

    public void hit() {
        isPlayHitAnimation = true;
    }

    @Override
    public File getConfigFile() {
        return new File(String.format(CONFIG_PATH_FORMAT, itemMelee.getUnlocalizedName()));
    }
}
