package ru.krogenit.guns.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.item.ItemStack;
import ru.krogenit.armor.CoreArmorClient;
import ru.krogenit.armor.client.IArmorRenderer;
import ru.xlv.core.common.inventory.MatrixInventory;

public class Hand extends ModelBase {

    private final ModelRenderer arm;
    private final Minecraft mc = Minecraft.getMinecraft();
    private static final MatrixInventory.SlotType[] handArmorTypes = new MatrixInventory.SlotType[] {MatrixInventory.SlotType.BRACERS, MatrixInventory.SlotType.BODY};

    public Hand() {
        textureWidth = 64;
        textureHeight = 32;

        arm = new ModelRenderer(this, 40, 16);
        arm.addBox(0F, -6F, 0F, 4, 12, 4);
        arm.setRotationPoint(0F, 0F, 0F);
        arm.setTextureSize(64, 32);
        arm.mirror = true;
    }

    private void renderLeftArmor() {
        for(MatrixInventory.SlotType slotType : handArmorTypes) {
            ItemStack armor = mc.thePlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
            if(armor != null) {
                IArmorRenderer render = CoreArmorClient.getModelForArmor(armor.getItem().getUnlocalizedName());
                if(render != null) {
                    if(slotType == MatrixInventory.SlotType.BRACERS) render.renderLeftBracerFirstPerson();
                    else render.renderLeftBodyArmFirstPerson();
                }
            }
        }
    }

    private void renderRightArmor() {
        for(MatrixInventory.SlotType slotType : handArmorTypes) {
            ItemStack armor = mc.thePlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
            if(armor != null) {
                IArmorRenderer render = CoreArmorClient.getModelForArmor(armor.getItem().getUnlocalizedName());
                if(render != null) {
                    if(slotType == MatrixInventory.SlotType.BRACERS) render.renderRightBracerFirstPerson();
                    else render.renderRightBodyArmFirstPerson();
                }
            }
        }
    }

    public void renderLeft() {
        mc.renderEngine.bindTexture(mc.thePlayer.getLocationSkin());
        arm.render(0.0625f);
        renderLeftArmor();
    }

    public void renderRight() {
        mc.renderEngine.bindTexture(mc.thePlayer.getLocationSkin());
        arm.render(0.0625f);
        renderRightArmor();
    }
}
