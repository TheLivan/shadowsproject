package ru.krogenit.guns.keys;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.input.Mouse;
import ru.krogenit.guns.item.EnumGunType;
import ru.krogenit.guns.item.ItemGunClient;

public class ThreadShootKeyGuns extends Thread {
    private Minecraft mc = Minecraft.getMinecraft();
    public static long lastShootTime;
    boolean isMouseDown;
    private boolean canClickMouseButtonAfterScreen;
    private long lastGuiTime;

    @Override
    public void run() {
        while (true) {
            if (mc.thePlayer != null) {
                if (mc.currentScreen == null) {
                    if (Mouse.isButtonDown(0)) {
                        if (canClickMouseButtonAfterScreen && !isMouseDown) onLeftClick(mc.thePlayer);
                    }
                    else {
                        isMouseDown = false;
                    }

                    if(!canClickMouseButtonAfterScreen && System.currentTimeMillis() - lastGuiTime > 1000) {
                        canClickMouseButtonAfterScreen = true;
                    }
                } else {
                    canClickMouseButtonAfterScreen = false;
                    lastGuiTime = System.currentTimeMillis();
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void onLeftClick(EntityPlayer p) {
        ItemStack item = p.getCurrentEquippedItem();
        if (item != null) {
            if (item.getItem() instanceof ItemGunClient) {
                ItemGunClient gun = (ItemGunClient) item.getItem();
                long time = System.currentTimeMillis();
                if (time - lastShootTime > gun.getRate()) {
                    boolean isSprinting = mc.thePlayer.sprintingTicksLeft != 0;
                    boolean isJump = !mc.thePlayer.onGround;
                    boolean isWalk = !isSprinting && (mc.thePlayer.movementInput.moveForward != 0 || mc.thePlayer.movementInput.moveStrafe != 0);
                    boolean isLying = mc.thePlayer.moving.isCrawling;
                    boolean isSneak = !isLying && mc.thePlayer.isSneaking();
                    boolean isPkm = Mouse.isButtonDown(1);

                    float totalAccuracy = (isSprinting ? gun.getAccSprint() : 0) + (isWalk ? gun.getAccWalk() : 0) + (isJump ? gun.getAccJump() : 0) + (isSneak ? gun.getAccShift() : 0) + (isPkm ? gun.getAccPkm() : 0) + (isLying ? gun.getAccLying() : 0);

                    gun.onItemLeftClick(item, p.worldObj, p, totalAccuracy);
                    isMouseDown = gun.getGunType() != EnumGunType.AUTO && gun.getGunType() != EnumGunType.AUTO_SHOTGUN;
                }
            }
        }
    }
}
