package ru.krogenit.guns;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.guns.creativetab.CreativeTabGuns;
import ru.krogenit.guns.entity.EntityBullet;
import ru.krogenit.guns.entity.EntityHole;
import ru.krogenit.guns.entity.EntitySleeve;
import ru.krogenit.guns.entity.EntitySmokeFXGun;
import ru.krogenit.guns.item.*;
import ru.krogenit.guns.item.ammo.ItemAmmoClient;
import ru.krogenit.guns.item.medicine.ItemMedicineClient;
import ru.krogenit.guns.keys.KeyBindingsGuns;
import ru.krogenit.guns.keys.ThreadShootKeyGuns;
import ru.krogenit.guns.network.PacketGunSync;
import ru.krogenit.guns.network.PacketMeleeSync;
import ru.krogenit.guns.network.packet.*;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.guns.render.GunAnimationHelper;
import ru.krogenit.guns.render.ammo.ItemGunAmmoRenderer;
import ru.krogenit.guns.render.entity.RenderEntityBullet;
import ru.krogenit.guns.render.entity.RenderEntityHole;
import ru.krogenit.guns.render.entity.RenderEntitySleeve;
import ru.krogenit.guns.render.entity.RenderEntitySmokeFX;
import ru.krogenit.guns.render.guns.*;
import ru.krogenit.guns.render.medicine.ItemMedicineRenderer;
import ru.krogenit.guns.render.melee.AzalletRenderer;
import ru.krogenit.guns.render.melee.CrasherRenderer;
import ru.krogenit.guns.render.melee.RadzegRenderer;
import ru.krogenit.model_loader.Model;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;

@Mod(modid = CoreGunsCommon.MODID, name = "Guns Client", version = "1.0.0")
public class CoreGunsClient extends CoreGunsCommon {

    @Mod.Instance(CoreGunsCommon.MODID)
    public static CoreGunsClient instance;

    public static final CreativeTabs tabGuns = new CreativeTabGuns(CreativeTabs.getNextID(), "guns");

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLPreInitializationEvent e) {
        ThreadShootKeyGuns t = new ThreadShootKeyGuns();
        t.start();
        MinecraftForge.EVENT_BUS.register(new ClientEventsGun());
        FMLCommonHandler.instance().bus().register(new ClientTickHandlerGuns());
        FMLCommonHandler.instance().bus().register(new KeyBindingsGuns().init());
        RenderingRegistry.registerEntityRenderingHandler(EntityBullet.class, new RenderEntityBullet());
        RenderingRegistry.registerEntityRenderingHandler(EntityHole.class, new RenderEntityHole());
        RenderingRegistry.registerEntityRenderingHandler(EntitySmokeFXGun.class, new RenderEntitySmokeFX());
        RenderingRegistry.registerEntityRenderingHandler(EntitySleeve.class, new RenderEntitySleeve());
    }

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLInitializationEvent e) {
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(CoreGunsCommon.MODID,
                new PacketPlaySoundClient(),
                new PacketFlashlightClient(),
                new PacketBulletClient(),
                new PacketReloadClient(),
                new PacketModifiClient(),
                new PacketShootClient(),
                new PacketDamageClient(),
                new PacketMeleeSync()
        );

       registeredGunAmmo.add(ammo_incarn09 = new ItemAmmoClient("ammo_incarn09", 12).setNeedTexture().setItemDisplayName("Патроны для Incarn-09"));
       registeredGunAmmo.add(ammo_arkonista = new ItemAmmoClient("ammo_arkonista", 30).setItemDisplayName("Патроны для Arkonista"));
       registeredGunAmmo.add(ammo_contributor = new ItemAmmoClient("ammo_contributor", 45).setItemDisplayName("Патроны для Contributor"));
       registeredGunAmmo.add(ammo_isaac = new ItemAmmoClient("ammo_isaac", 6).setItemDisplayName("Патроны для Isaac"));
       registeredGunAmmo.add(ammo_keeper = new ItemAmmoClient("ammo_keeper", 30).setItemDisplayName("Патроны для Keeper"));
       registeredGunAmmo.add(ammo_prowler = new ItemAmmoClient("ammo_prowler", 40).setItemDisplayName("Патроны для Prowler"));
       registeredGunAmmo.add(ammo_seeker = new ItemAmmoClient("ammo_seeker", 6).setItemDisplayName("Патроны для Seeker"));
       registeredGunAmmo.add(ammo_slash = new ItemAmmoClient("ammo_slash", 45).setItemDisplayName("Патроны для Slash"));
       registeredGunAmmo.add(ammo_spitter = new ItemAmmoClient("ammo_spitter", 25).setItemDisplayName("Патроны для Spitter"));
       registeredGunAmmo.add(ammo_wasp = new ItemAmmoClient("ammo_wasp", 25).setItemDisplayName("Патроны для Wasp"));
       registeredGunAmmo.add(ammo_asr37 = new ItemAmmoClient("ammo_asr37", 35).setItemDisplayName("Патроны для ASR-37"));
       registeredGunAmmo.add(ammo_barracuda = new ItemAmmoClient("ammo_barracuda", 6).setItemDisplayName("Патроны для Barracuda"));
       registeredGunAmmo.add(ammo_bonecrusher = new ItemAmmoClient("ammo_bonecrusher", 4).setNeedTexture().setItemDisplayName("Патроны для BoneCRUSHer"));
       registeredGunAmmo.add(ammo_devourer = new ItemAmmoClient("ammo_devourer", 8).setNeedTexture().setItemDisplayName("Патроны для Devourer"));
       registeredGunAmmo.add(ammo_hyperion = new ItemAmmoClient("ammo_hyperion", 1).setNeedTexture().setItemDisplayName("Патрон для Hyperion"));
       registeredGunAmmo.add(ammo_osiris = new ItemAmmoClient("ammo_osiris", 10).setNeedTexture().setItemDisplayName("Патроны для Osiris"));
       registeredGunAmmo.add(ammo_radZinger = new ItemAmmoClient("ammo_radZinger", 30).setItemDisplayName("Патроны для Rad-Zinger"));
       registeredGunAmmo.add(ammo_ragnarok = new ItemAmmoClient("ammo_ragnarok", 120).setItemDisplayName("Патроны для Ragnaröк"));
       registeredGunAmmo.add(ammo_vertmunt = new ItemAmmoClient("ammo_vertmunt", 8).setNeedTexture().setItemDisplayName("Патроны для VertMunt"));

        registeredGunList.add(incarn09 = new ItemGunClient(
                "incarn09",
                0,
                ammo_incarn09,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.PISTOL,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(isaac = new ItemGunClient(
                "isaac",
                0,
                ammo_isaac,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.PISTOL,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(spitter = new ItemGunClient(
                "spitter",
                0,
                ammo_spitter,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(keeper = new ItemGunClient(
                "keeper",
                0,
                ammo_keeper,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(seeker = new ItemGunClient(
                "seeker",
                0,
                ammo_seeker,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.PISTOL,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(contributor = new ItemGunClient(
                "contributor",
                0,
                ammo_contributor,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(prowler = new ItemGunClient(
                "prowler",
                0,
                ammo_prowler,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(arkonista = new ItemGunClient(
                "arkonista",
                0,
                ammo_arkonista,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(wasp = new ItemGunClient(
                "wasp",
                0,
                ammo_wasp,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(slash = new ItemGunClient(
                "slash",
                0,
                ammo_slash,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(barracuda = new ItemGunClient(
                "barracuda",
                0,
                ammo_barracuda,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.SHOTGUN,
                8,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.ADD_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON));
        registeredGunList.add(asr37 = new ItemGunClient(
                "asr37",
                0,
                ammo_asr37,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(vertmunt = new ItemGunClient(
                "vertmunt",
                0,
                ammo_vertmunt,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO_SHOTGUN,
                8,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(ragnarok = new ItemGunClient(
                "ragnarok",
                0,
                ammo_ragnarok,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(osiris = new ItemGunClient(
                "osiris",
                0,
                ammo_osiris,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.SNIPER_RIFLE,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(devourer = new ItemGunClient(
                "devourer",
                0,
                ammo_devourer,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.SNIPER_RIFLE,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(radZinger = new ItemGunClient(
                "radZinger",
                0,
                ammo_radZinger,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.AUTO,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(bonecrusher = new ItemGunClient(
                "bonecrusher",
                0,
                ammo_bonecrusher,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.SHOTGUN,
                8,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));
        registeredGunList.add(hyperion = new ItemGunClient(
                "hyperion",
                0,
                ammo_hyperion,
                0,
                new float[] {0,0,0,0,0,0,0,0,0},
                0,
                0,
                new Vector4f(1f,1f,1f,1f),
                EnumGunType.SNIPER_RIFLE,
                1,
                0,
                0
        ).addSlotType(MatrixInventory.SlotType.MAIN_WEAPON).addSlotType(MatrixInventory.SlotType.SECOND_WEAPON));

        registeredMeleeWeapons.add(radzeg = new ItemMeleeWeaponClient("radzeg", 1, 1000).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));
        registeredMeleeWeapons.add(azallet = new ItemMeleeWeaponClient("azallet", 1, 1000).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));
        registeredMeleeWeapons.add(crasher = new ItemMeleeWeaponClient("crasher", 1, 1000).addSlotType(MatrixInventory.SlotType.MELEE_WEAPON));

        initMedicine();
        registerItems();
        registerItemRenders();

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketGunSync());
    }

    public void initMedicine() {
        firstAidKitBig = new ItemMedicineClient("firstaidkit_big", 30);
        firstAidKitSmall = new ItemMedicineClient("firstaidkit_small", 30);
        syringe = new ItemMedicineClient("syringe", 30);
    }

    public void registerItemRenders() {
        for (ItemGun gun : registeredGunList) {
            MinecraftForgeClient.registerItemRenderer(gun, getGunRenderer((ItemGunClient) gun));
        }
        for (ItemMeleeWeapon weapon : registeredMeleeWeapons) {
            MinecraftForgeClient.registerItemRenderer(weapon, getWeaponRender((ItemMeleeWeaponClient) weapon));
        }
        for(ItemAmmo ammo : registeredGunAmmo) {
            IItemRenderer ammoRenderer = getAmmoRenderer((ItemAmmoClient) ammo);
            if(ammoRenderer != null) MinecraftForgeClient.registerItemRenderer(ammo, ammoRenderer);
        }

        ItemMedicineRenderer itemMedicineRenderer = new ItemMedicineRenderer(new Model(new ResourceLocation("items", "models/" + firstAidKitBig.getName() + ".obj")), firstAidKitBig);
        MinecraftForgeClient.registerItemRenderer(firstAidKitBig, itemMedicineRenderer);
        itemMedicineRenderer.init();
        ItemMedicineRenderer itemMedicineRenderer1 = new ItemMedicineRenderer(new Model(new ResourceLocation("items", "models/" + firstAidKitSmall.getName() + ".obj")), firstAidKitSmall);
        itemMedicineRenderer1.init();
        MinecraftForgeClient.registerItemRenderer(firstAidKitSmall, itemMedicineRenderer1);
        ItemMedicineRenderer itemMedicineRenderer2 = new ItemMedicineRenderer(new Model(new ResourceLocation("items", "models/" + syringe.getName() + ".obj")), syringe);
        itemMedicineRenderer2.init();
        MinecraftForgeClient.registerItemRenderer(syringe, itemMedicineRenderer2);
    }

    private IItemRenderer getAmmoRenderer(ItemAmmoClient itemAmmoClient) {
        ItemGunAmmoRenderer renderer = null;
        switch (itemAmmoClient.getName()) {
//            case "ammo_incarn09":
            case "ammo_arkonista":
                ArkonistaRenderer arkonistaRenderer = (ArkonistaRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(arkonista), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(arkonistaRenderer.getGunModel(), itemAmmoClient, "Mag",
                        arkonistaRenderer.getResourceLocationStatefulGaussDiffuse(), null, null, null, null, 0f);
                break;
            case "ammo_contributor":
                ContributorRenderer contributorRenderer = (ContributorRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(contributor), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(contributorRenderer.getGunModel(), itemAmmoClient, "Mag",
                        contributorRenderer.getResourceLocationStatefulDiffuse(),
                        contributorRenderer.getResourceLocationStatefulNormal(),
                        contributorRenderer.getResourceLocationStatefulSpecular(),
                        contributorRenderer.getResourceLocationStatefulGlassMap(), null, 0f);
                break;
            case "ammo_isaac":
                IsaacRenderer isaacRenderer = (IsaacRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(isaac), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(isaacRenderer.getGunModel(), itemAmmoClient, "Baraban",
                        isaacRenderer.getResourceLocationStatefulDiffuse(),
                        isaacRenderer.getResourceLocationStatefulNormal(),
                        isaacRenderer.getResourceLocationStatefulSpecular(),
                        isaacRenderer.getResourceLocationStatefulGlassMap(),
                        isaacRenderer.getResourceLocationStatefulEmission(), 5f);
                break;
            case "ammo_keeper":
                KeeperRenderer keeperRenderer = (KeeperRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(keeper), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(keeperRenderer.getGunModel(), itemAmmoClient, "MAG",
                       keeperRenderer.getResourceLocationStatefulDiffuse(),
                       keeperRenderer.getResourceLocationStatefulNormal(),
                       keeperRenderer.getResourceLocationStatefulSpecular(),
                       null,
                       null, 0);
                break;
            case "ammo_prowler":
                ProwlerRenderer prowlerRenderer = (ProwlerRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(prowler), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(prowlerRenderer.getGunModel(), itemAmmoClient, "KoreLibMesh19",
                        prowlerRenderer.getResourceLocationStatefulDiffuse(),
                        prowlerRenderer.getResourceLocationStatefulNormal(),
                        null,
                        null,
                        null, 0);
                break;
            case "ammo_seeker":
                SeekerRenderer seekerRenderer = (SeekerRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(seeker), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(seekerRenderer.getGunModel(), itemAmmoClient, "Baraban",
                        seekerRenderer.getResourceLocationStatefulDiffuse(),
                        seekerRenderer.getResourceLocationStatefulNormal(),
                        seekerRenderer.getResourceLocationStatefulSpecular(),
                        seekerRenderer.getResourceLocationStatefulGlassMap(),
                        null, 0);
                break;
            case "ammo_slash":
                SlashRenderer slashRenderer = (SlashRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(slash), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(slashRenderer.getGunModel(), itemAmmoClient, "Mag",
                        slashRenderer.getResourceLocationStatefulDiffuse(),
                        slashRenderer.getResourceLocationStatefulNormal(),
                        slashRenderer.getResourceLocationStatefulSpecular(),
                        slashRenderer.getResourceLocationStatefulGlassMap(),
                        null, 0);
                break;
            case "ammo_spitter":
                SpitterRenderer spitterRenderer = (SpitterRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(spitter), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(spitterRenderer.getGunModel(), itemAmmoClient, "MAG",
                        spitterRenderer.getResourceLocationStatefulMagDiffuse(),
                        spitterRenderer.getResourceLocationStatefulMagNormal(),
                        spitterRenderer.getResourceLocationStatefulMagSpecular(),
                        spitterRenderer.getResourceLocationStatefulMagGlassMap(),
                        null, 0);
                break;
            case "ammo_wasp":
                WaspRenderer waspRenderer = (WaspRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(wasp), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(waspRenderer.getGunModel(), itemAmmoClient, "MAG",
                        waspRenderer.getResourceLocationStatefulDiffuse(),
                        waspRenderer.getResourceLocationStatefulNormal(),
                        waspRenderer.getResourceLocationStatefulSpecular(),
                        null,
                        waspRenderer.getResourceLocationStatefulEmission(), 5);
                break;
            case "ammo_asr37":
                Asr37Renderer asr37Renderer = (Asr37Renderer) MinecraftForgeClient.getItemRenderer(new ItemStack(asr37), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(asr37Renderer.getGunModel(), itemAmmoClient, "Magazine_Mag_LP_meshId4_name",
                        asr37Renderer.getResourceLocationStatefulMagazineDiffuse(),
                        asr37Renderer.getResourceLocationStatefulMagazineNormal(),
                        asr37Renderer.getResourceLocationStatefulMagazineSpecular(),
                        asr37Renderer.getResourceLocationStatefulMagazineGlassMap(),
                        null, 5);
                break;
            case "ammo_barracuda":
                BarracudaRenderer barracudaRenderer = (BarracudaRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(barracuda), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(barracudaRenderer.getGunModel(), itemAmmoClient, "Barracuda_Ammo",
                        barracudaRenderer.getResourceLocationStatefulDiffuse(),
                        barracudaRenderer.getResourceLocationStatefulNormal(),
                        barracudaRenderer.getResourceLocationStatefulSpecular(),
                        null,
                        null, 5);
                break;
//            case "ammo_bonecrusher":
//                BoneCrusherRenderer boneCrusherRenderer = (BoneCrusherRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(bonecrusher), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
//                renderer = new ItemGunAmmoRenderer(boneCrusherRenderer.getGunModel(), itemAmmoClient, "Barracuda_Ammo",
//                        boneCrusherRenderer.getResourceLocationStatefulDiffuse(),
//                        boneCrusherRenderer.getResourceLocationStatefulNormal(),
//                        boneCrusherRenderer.getResourceLocationStatefulSpecular(),
//                        null,
//                        null, 5);
//                break;
//            case "ammo_devourer":
//            case "ammo_hyperion":
//            case "ammo_osiris":
            case "ammo_radZinger":
                RadZingerRenderer radZingerRenderer = (RadZingerRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(radZinger), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(radZingerRenderer.getGunModel(), itemAmmoClient, "Mag",
                        radZingerRenderer.getResourceLocationStatefulDiffuse(),
                        radZingerRenderer.getResourceLocationStatefulNormal(),
                        radZingerRenderer.getResourceLocationStatefulSpecular(),
                        radZingerRenderer.getResourceLocationStatefulGlassmap(),
                        null, 5);
                break;
            case "ammo_ragnarok":
                RagnarokRenderer ragnarokRenderer = (RagnarokRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(ragnarok), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
                renderer = new ItemGunAmmoRenderer(ragnarokRenderer.getGunModel(), itemAmmoClient, "Ammo",
                        ragnarokRenderer.getResourceLocationStatefulDiffuse(),
                        ragnarokRenderer.getResourceLocationStatefulNormal(),
                        ragnarokRenderer.getResourceLocationStatefulSpecular(),
                        ragnarokRenderer.getResourceLocationStatefulGlassMap(),
                        null, 5);
                break;
//            case "ammo_vertmunt":
//                VertmuntRenderer vertmuntRenderer = (VertmuntRenderer) MinecraftForgeClient.getItemRenderer(new ItemStack(vertmunt), IItemRenderer.ItemRenderType.FIRST_PERSON_MAP);
//                renderer = new ItemGunAmmoRenderer(vertmuntRenderer.getGunModel(), itemAmmoClient, "Ammo",
//                        vertmuntRenderer.getResourceLocationStatefulDiffuse(),
//                        vertmuntRenderer.getResourceLocationStatefulNormal(),
//                        vertmuntRenderer.getResourceLocationStatefulSpecular(),
//                        vertmuntRenderer.getResourceLocationStatefulGlassMap(),
//                        null, 5);
//                break;
        }

        if(renderer != null) renderer.init();

        return renderer;
    }

    private IItemRenderer getWeaponRender(ItemMeleeWeaponClient weapon) {
        AbstractMeleeWeaponRenderer renderer = null;
        switch (weapon.getName()) {
            case "radzeg": renderer = new RadzegRenderer(weapon); break;
            case "azallet": renderer = new AzalletRenderer(weapon); break;
            case "crasher": renderer = new CrasherRenderer(weapon); break;
        }

        if(renderer != null) renderer.init();

        return renderer;
    }

    private IItemRenderer getGunRenderer(ItemGunClient gun) {
        GunAnimationHelper renderer = null;
        switch (gun.getName()) {
            case "incarn09": renderer = new Incarn09Renderer(gun); break;
            case "slash": renderer = new SlashRenderer(gun); break;
            case "wasp": renderer = new WaspRenderer(gun); break;
            case "arkonista": renderer = new ArkonistaRenderer(gun); break;
            case "isaac": renderer = new IsaacRenderer(gun); break;
            case "spitter": renderer = new SpitterRenderer(gun); break;
            case "keeper": renderer = new KeeperRenderer(gun); break;
            case "seeker": renderer = new SeekerRenderer(gun); break;
            case "contributor": renderer = new ContributorRenderer(gun); break;
            case "prowler": renderer = new ProwlerRenderer(gun); break;
            case "asr37": renderer = new Asr37Renderer(gun); break;
            case "barracuda": renderer = new BarracudaRenderer(gun); break;
            case "bonecrusher": renderer = new BoneCrusherRenderer(gun); break;
            case "devourer": renderer = new DevourerRenderer(gun); break;
            case "hyperion": renderer = new HyperionRenderer(gun); break;
            case "osiris": renderer = new OsirisRenderer(gun); break;
            case "radZinger": renderer = new RadZingerRenderer(gun); break;
            case "ragnarok": renderer = new RagnarokRenderer(gun); break;
            case "vertmunt": renderer = new VertmuntRenderer(gun); break;
        }

        if(renderer != null) renderer.init();

        return renderer;
    }
}
