package ru.krogenit.guns.entity;

import net.minecraft.client.particle.EntityFX;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector4f;

public class EntitySmokeFXGun extends EntityFX {
    public float uMin = 0;
    public float vMin = 0;
    public float uMax = 1f;
    public float vMax = 1f;
    public Vector4f color = new Vector4f();
    public Vector4f rotaion = new Vector4f();

    public boolean fadeOut = false;
    public boolean isAnimating, isMoving;

    public EntitySmokeFXGun(World w, double x, double y, double z, boolean isAnimating, boolean isMoving) {
        super(w, x, y, z);
        uMin = w.rand.nextInt(4) / 4.0f;
        uMax = uMin + 0.25f;
        vMin = w.rand.nextInt(4) / 4.0f;
        vMax = vMin + 0.25f;
        motionY += 0.002f;
        this.isMoving = isMoving;
        this.particleMaxAge = 25;
        float color = 0.4f;
        this.color = new Vector4f(color, color, color, 0.02f);
        this.rotaion.x = (w.rand.nextFloat() - 0.5f) / 2.5f;
        this.rotaion.w = 0.25f;
        this.rotaion.z = w.rand.nextFloat() * 360;
    }

    public EntitySmokeFXGun(World w, double x, double y, double z, boolean isAnimating) {
        super(w, x, y, z);
        uMin = w.rand.nextInt(4) / 4.0f;
        uMax = uMin + 0.25f;
        vMin = w.rand.nextInt(4) / 4.0f;
        vMax = vMin + 0.25f;
        motionY += 0.002f;
        this.particleMaxAge = 25;
        float color = 0.5f;
        this.color = new Vector4f(color, color, color, 0.001f);
        this.rotaion.x = (w.rand.nextFloat() - 0.5f) / 2.5f;
        this.rotaion.w = 0.2f;
        this.rotaion.z = w.rand.nextFloat() * 360;
    }

    public EntitySmokeFXGun(World w, double x, double y, double z) {
        super(w, x, y, z);
        uMin = w.rand.nextInt(4) / 4.0f;
        uMax = uMin + 0.25f;
        vMin = w.rand.nextInt(4) / 4.0f;
        vMax = vMin + 0.25f;
        motionY += 0.002f;
        this.particleMaxAge = 25;
        float color = 0.4f;
        this.color = new Vector4f(color, color, color, 0.025f);
        this.rotaion.x = (w.rand.nextFloat() - 0.5f) / 2.5f;
        this.rotaion.w = 0.2f;
        this.rotaion.z = w.rand.nextFloat() * 360;
    }

    @Override
    public void onUpdate() {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;

        this.motionY -= 0.04D * (double) this.particleGravity;
        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        if(isMoving) {
            double slow = 0.99;
            this.motionX *= slow;
            this.motionY *= slow;
            this.motionZ *= slow;
        } else {
            this.motionX *= 0.9800000190734863D;
            this.motionY *= 0.9800000190734863D;
            this.motionZ *= 0.9800000190734863D;
        }

        if (this.onGround) {
            this.motionX *= 0.699999988079071D;
            this.motionZ *= 0.699999988079071D;
        }
    }

}
