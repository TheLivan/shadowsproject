package ru.krogenit.guns;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.RenderPlayerEvent;
import org.lwjgl.input.Mouse;
import ru.krogenit.client.event.*;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemGunClient;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;

public class ClientEventsGun {

    private static final Minecraft mc = Minecraft.getMinecraft();

    @SubscribeEvent
    public void event(EventAttackObjectMouseOver e) {
        ItemStack itemStack = mc.thePlayer.getCurrentEquippedItem();
        if(itemStack != null) {
            Item item = itemStack.getItem();
            if (item instanceof ItemMeleeWeaponClient) {
                e.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void event(EventUpdateItemInSlot e) {
        ItemStack newItemStack = e.getNewItemStack();
        if(mc.thePlayer != null && newItemStack != null) {
            Item item1 = newItemStack.getItem();
            if(item1 instanceof ItemGunClient) {
                int slotIndex = e.getSlotIndex();
                ItemStack itemStack = mc.thePlayer.inventoryContainer.getSlot(slotIndex).getStack();
                if (itemStack != null && itemStack == mc.thePlayer.getCurrentEquippedItem()) {
                    Item item = itemStack.getItem();
                    if (item instanceof ItemGunClient) {
                        if(item == item1)
                            e.setCanceled(true);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void event(RenderPlayerEvent.Pre event) {
        EntityPlayer p = event.entityPlayer;
        ItemStack item = p.getCurrentEquippedItem();

        if (item != null && item.getItem() instanceof ItemGun) {
            event.renderer.modelBipedMain.aimedBow = true;
        }
    }

    @SubscribeEvent
    public void event(EventViewBobbingFirstPerson event) {
        EntityPlayer p = mc.thePlayer;
        ItemStack item = p.getCurrentEquippedItem();

        if (item != null && item.getItem() instanceof ItemGun && Mouse.isButtonDown(1) && !mc.thePlayer.isSprinting()) {
            event.setCanceled(true);
        }
    }

    private ItemStack prevItem;
    private boolean startChangeItem;

    @SubscribeEvent
    public void event(EventSwingAnimationFirstPerson event) {
        ItemStack itemStack = event.getItemStack();

        if (itemStack != null && itemStack.getItem() instanceof ItemMeleeWeaponClient) {
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void event(EventItemActionRotate event) {
        ItemStack itemStack = event.getItemStack();
        if (itemStack != null) {
            if(itemStack.getItem() instanceof ItemMeleeWeaponClient && event.getEntityPlayer() == mc.thePlayer && mc.gameSettings.thirdPersonView == 0) {
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void event(EventChangeItemAnim event) {
        EntityPlayer p = mc.thePlayer;
        ItemStack itemStack = p.getCurrentEquippedItem();

        if(prevItem != itemStack) {
            prevItem = itemStack;
        }

        if (itemStack != null && itemStack.getItem() instanceof ItemGunClient && prevItem == itemStack) {
            event.setCanceled(true);
        }
    }
}
