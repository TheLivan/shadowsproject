package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.SoundHelperGuns;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.util.SoundType;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
public class PacketFlashlightClient implements IPacketOut, IPacketIn {

    private String playerName;
    private boolean lowAmmo;

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        playerName = data.readUTF();
        lowAmmo = data.readBoolean();
        handleMessageOnClientSide();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeUTF(playerName);
        data.writeBoolean(lowAmmo);
    }

    public void handleMessageOnClientSide() {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayer p = mc.thePlayer;
        if (mc.theWorld != null) {
            EntityPlayer p1 = mc.theWorld.getPlayerEntityByName(playerName);

            if (p1 != null && p1 != p) {
                ItemStack item = p1.getCurrentEquippedItem();

                if (item != null && item.getItem() instanceof ItemGun) {
                    boolean hasGlushak = false;//inventory.hasGlushak();TODO: silencer
                    ItemGun gun = (ItemGun) item.getItem();

                    String s;
                    if (hasGlushak) {
                        s = CoreGunsCommon.MODID + ":guns." + gun.getName() + "." + gun.getName() + "_shoot_silence";
                    } else {
                        s = SoundType.DEFAULT_DOMAIN + gun.getShootSound();
                    }

                    Minecraft.getMinecraft().theWorld.playSound((float) p1.posX, (float) p1.posY, (float) p1.posZ, s, 1.0f, SoundHelperGuns.getRandomPitch(), false);
                    AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, IItemRenderer.ItemRenderType.EQUIPPED);
                    render.onFlashLight(item, p1, hasGlushak);
                }
            }
        }
    }
}
