package ru.krogenit.guns.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.item.ItemUseResult;

public class ItemMedicineServer extends ItemMedicine {

    private final int healAmount;

    public ItemMedicineServer(String name, int healAmount) {
        super(name);
        this.healAmount = healAmount;
    }

    @Override
    public ItemUseResult useItem(EntityPlayer entityPlayer, ItemStack itemStack, int slotIndex) {
        entityPlayer.heal(healAmount);

        if(!entityPlayer.capabilities.isCreativeMode) {
            --itemStack.stackSize;

            if (itemStack.stackSize <= 0) {
                entityPlayer.inventory.setInventorySlotContents(slotIndex, null);
            }

            if (!entityPlayer.isUsingItem()) {
                ((EntityPlayerMP) entityPlayer).sendContainerToPlayer(entityPlayer.inventoryContainer);
            }
        }

        return new ItemUseResult(ItemUseResult.Type.SUCCESS);
    }

    @Override
    public void update(ItemStack itemStack) {

    }

    @Override
    public float getAnimation(ItemStack itemStack) {
        return 0;
    }
}
