package ru.krogenit.guns.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.util.GunUtils;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketGunSync implements IPacketOutServer {

    private ItemGun itemGun;

    public PacketGunSync(ItemGun itemGun) {
        this.itemGun = itemGun;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        GunUtils.writeGun(itemGun, bbos);
    }
}
