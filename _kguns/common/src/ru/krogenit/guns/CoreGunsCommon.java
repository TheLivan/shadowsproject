package ru.krogenit.guns;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import ru.krogenit.guns.item.ItemAmmo;
import ru.krogenit.guns.item.ItemGun;
import ru.krogenit.guns.item.ItemMedicine;
import ru.krogenit.guns.item.ItemMeleeWeapon;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CoreGunsCommon {

    public static final String MODID = "guns";
    public static final boolean IS_UNLIMITED_AMMO = false;
    public static final boolean IS_FULL_RELOAD = true;

    //нужен для синхронизации
    public static final List<ItemGun> registeredGunList = new ArrayList<>();
    public static final List<ItemMeleeWeapon> registeredMeleeWeapons = new ArrayList<>();
    public static final List<ItemAmmo> registeredGunAmmo = new ArrayList<>();

    public static ItemGun incarn09;
    public static ItemGun arkonista;
    public static ItemGun contributor;
    public static ItemGun isaac;
    public static ItemGun keeper;
    public static ItemGun prowler;
    public static ItemGun seeker;
    public static ItemGun slash;
    public static ItemGun spitter;
    public static ItemGun wasp;

    public static ItemGun asr37;
    public static ItemGun barracuda;
    public static ItemGun bonecrusher;
    public static ItemGun devourer;
    public static ItemGun hyperion;
    public static ItemGun osiris;
    public static ItemGun radZinger;
    public static ItemGun ragnarok;
    public static ItemGun vertmunt;

    public static ItemAmmo ammo_incarn09;
    public static ItemAmmo ammo_arkonista;
    public static ItemAmmo ammo_contributor;
    public static ItemAmmo ammo_isaac;
    public static ItemAmmo ammo_keeper;
    public static ItemAmmo ammo_prowler;
    public static ItemAmmo ammo_seeker;
    public static ItemAmmo ammo_slash;
    public static ItemAmmo ammo_spitter;
    public static ItemAmmo ammo_wasp;

    public static ItemAmmo ammo_asr37;
    public static ItemAmmo ammo_barracuda;
    public static ItemAmmo ammo_bonecrusher;
    public static ItemAmmo ammo_devourer;
    public static ItemAmmo ammo_hyperion;
    public static ItemAmmo ammo_osiris;
    public static ItemAmmo ammo_radZinger;
    public static ItemAmmo ammo_ragnarok;
    public static ItemAmmo ammo_vertmunt;

    public static ItemMeleeWeapon azallet;
    public static ItemMeleeWeapon crasher;
    public static ItemMeleeWeapon radzeg;

    public static ItemMedicine firstAidKitBig;
    public static ItemMedicine firstAidKitSmall;
    public static ItemMedicine syringe;

    public void init(FMLInitializationEvent e) {
        initItems();
        registerItems();
    }

    protected void initItems() {

    }

    protected void registerItems() {
        for (Field field : CoreGunsCommon.class.getFields()) {
            Class<?> type = field.getType();
            if(type.isAssignableFrom(ItemGun.class)) {
                try {
                    ItemGun item = (ItemGun) field.get(null);
                    GameRegistry.registerItem(item, item.getUnlocalizedName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else if(type.isAssignableFrom(ItemMeleeWeapon.class)) {
                try {
                    ItemMeleeWeapon item = (ItemMeleeWeapon) field.get(null);
                    GameRegistry.registerItem(item, item.getUnlocalizedName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else if(type.isAssignableFrom(ItemMedicine.class)) {
                try {
                    ItemMedicine item = (ItemMedicine) field.get(null);
                    GameRegistry.registerItem(item, item.getUnlocalizedName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}