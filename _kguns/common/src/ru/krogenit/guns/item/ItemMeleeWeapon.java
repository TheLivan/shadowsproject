package ru.krogenit.guns.item;

import com.google.common.collect.Multimap;
import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.IAttributeProvider;
import ru.xlv.core.common.item.ISlotSpecified;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemMeleeWeapon extends ItemBase implements IAttributeProvider, ISlotSpecified {

    @Getter
    protected Map<CharacterAttributeType, CharacterAttributeMod> characterAttributeBoostMap = new HashMap<>();
    protected final List<MatrixInventory.SlotType> slotTypes = new ArrayList<>();
    private final float damage;

    public ItemMeleeWeapon(String name, float damage, int durability) {
        super(name);
        this.damage = damage;
        this.maxStackSize = 1;
        this.setMaxDamage(durability);
    }

    public float getStrVsBlock(ItemStack itemStack, Block block) {
        if (block == Blocks.web) {
            return 15.0F;
        } else {
            Material material = block.getMaterial();
            return material != Material.plants && material != Material.vine && material != Material.coral && material != Material.leaves && material != Material.gourd ? 1.0F : 1.5F;
        }
    }

    public boolean hitEntity(ItemStack itemStack, EntityLivingBase livingBase, EntityLivingBase livingBase1) {
        itemStack.damageItem(1, livingBase1);
        return true;
    }

    public boolean onBlockDestroyed(ItemStack itemStack, World world, Block block, int x, int y, int z, EntityLivingBase livingBase) {
        if ((double) block.getBlockHardness(world, x, y, z) != 0.0D) {
            itemStack.damageItem(2, livingBase);
        }

        return true;
    }

    public EnumAction getItemUseAction(ItemStack itemStack) {
        return EnumAction.block;
    }

    public int getMaxItemUseDuration(ItemStack itemStack) {
        return 72000;
    }

    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
        entityPlayer.setItemInUse(itemStack, this.getMaxItemUseDuration(itemStack));
        return itemStack;
    }

    public boolean canHarvestBlock(Block block) {
        return block == Blocks.web;
    }

    public Multimap getItemAttributeModifiers() {
        Multimap multimap = super.getItemAttributeModifiers();
        multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(), new AttributeModifier(field_111210_e, "Weapon modifier", this.damage, 0));
        return multimap;
    }

    public ItemMeleeWeapon addSlotType(MatrixInventory.SlotType slotType) {
        this.slotTypes.add(slotType);
        return this;
    }

    @Override
    public List<MatrixInventory.SlotType> getSlotTypes() {
        return slotTypes;
    }
}
