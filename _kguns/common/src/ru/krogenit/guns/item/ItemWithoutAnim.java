package ru.krogenit.guns.item;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.common.item.ItemBase;

public class ItemWithoutAnim extends ItemBase {

    protected boolean needTexture;
    private boolean canAttack;

    public ItemWithoutAnim(int size, String name) {
        super(name);
        this.iconString = name;
        setMaxStackSize(size);
        setUnlocalizedName(name);
    }

    public ItemWithoutAnim setNeedTexture() {
        this.needTexture = true;
        return this;
    }

    public ItemWithoutAnim setCanAttack() {
        this.canAttack = true;
        return this;
    }

    @Override
    public void registerIcons(IIconRegister par1IconRegister) {
        if(needTexture) this.itemIcon = par1IconRegister.registerIcon("guns:" + iconString);
    }

    public boolean onEntitySwing(EntityLivingBase e, ItemStack stack) {
        return !canAttack;
    }

    public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player) {
        return !canAttack;
    }

    @Override
    public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLivingBase, EntityLivingBase par3EntityLivingBase) {
        return !canAttack;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        return !canAttack;
    }

    @Override
    public boolean canHarvestBlock(Block par1Block, ItemStack itemStack) {
        return canAttack;
    }

    public float getStrVsBlock(ItemStack var1, Block var2) {
        return func_150893_a(var1, var2);
    }

    public float func_150893_a(ItemStack var1, Block var2) {
        return canAttack? 1.0F : 0.0F;
    }

    protected void chekTagCompound(ItemStack i) {
        if (i.getTagCompound() == null) {
            i.setTagCompound(new NBTTagCompound());
        }
    }

    public float getFloat(ItemStack item, String name) {
        chekTagCompound(item);
        return item.getTagCompound().getFloat(name);
    }

    public void setFloat(ItemStack item, String name, float data) {
        chekTagCompound(item);
        item.getTagCompound().setFloat(name, data);
    }

    public int getInteger(ItemStack item, String name) {
        chekTagCompound(item);
        return item.getTagCompound().getInteger(name);
    }

    public void setInteger(ItemStack item, String name, int data) {
        chekTagCompound(item);
        item.getTagCompound().setInteger(name, data);
    }

    public boolean getBoolean(ItemStack item, String name) {
        chekTagCompound(item);
        return item.getTagCompound().getBoolean(name);
    }

    public void setBoolean(ItemStack item, String name, boolean data) {
        chekTagCompound(item);
        item.getTagCompound().setBoolean(name, data);
    }
}
