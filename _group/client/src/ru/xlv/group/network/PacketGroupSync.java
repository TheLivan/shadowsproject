package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.common.Group;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketGroupSync implements IPacketCallbackEffective<Boolean>, IPacketIn {

    private boolean success;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException { }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        success = bbis.readBoolean();
        if(success) {
            Group group = readGroup(bbis);
            XlvsGroupMod.INSTANCE.setGroup(group);
        }
    }

    @Nullable
    @Override
    public Boolean getResult() {
        return success;
    }

    private static Group readGroup(ByteBufInputStream byteBufInputStream) throws IOException {
        String leaderName = byteBufInputStream.readUTF();
        Group group = new Group(leaderName);
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            String username = byteBufInputStream.readUTF();
            if(username.equals(group.getLeader())) continue;
            group.getPlayers().add(username);
        }
        return group;
    }
}
