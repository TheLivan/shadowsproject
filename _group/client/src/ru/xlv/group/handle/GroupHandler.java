package ru.xlv.group.handle;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.group.event.EventGroupInvite;
import ru.xlv.group.network.PacketGroupInviteAccept;

public class GroupHandler {

    @Getter @Setter private String pendingInviteFromPlayer;

    public void handleIncomingInvite(String username) {
        pendingInviteFromPlayer = username;
        XlvsCoreCommon.EVENT_BUS.post(new EventGroupInvite(username));
    }

    public void acceptInvite() {
        if(pendingInviteFromPlayer != null) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketGroupInviteAccept(pendingInviteFromPlayer)).thenAcceptSync(s -> {

            });
        }

        pendingInviteFromPlayer = null;
    }

    public void declineInvite() {
        pendingInviteFromPlayer = null;
    }
}
