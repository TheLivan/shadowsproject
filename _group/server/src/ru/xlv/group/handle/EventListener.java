package ru.xlv.group.handle;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.event.ServerPlayerLoginEvent;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledTask;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.network.PacketGroupSync;

public class EventListener {

    private static final long GROUP_INVITE_REMOVAL_TIME_MILLS = 15000L;
    private static final long GROUP_INVITE_UPDATE_TIME_MILLS = 500L;
    private static final long GROUP_SYNC_TIME_MILLS = 300L;
//    private static final long GROUP_STAT_UPDATE_TIME_MILLS = 1000L;

    private final ScheduledTask inviteUpdateScheduledTask = new ScheduledTask(GROUP_INVITE_UPDATE_TIME_MILLS, () -> {
        synchronized (XlvsGroupMod.INSTANCE.getGroupHandler().getGroupList()) {
            for (GroupServer group : XlvsGroupMod.INSTANCE.getGroupHandler().getGroupList()) {
                synchronized (group.getInvites()) {
                    group.getInvites().removeIf(groupInvite -> System.currentTimeMillis() - groupInvite.getCreationTimeMills() >= GROUP_INVITE_REMOVAL_TIME_MILLS);
                }
            }
        }
    });
    private final ScheduledTask groupSyncScheduledTask = new ScheduledTask(GROUP_SYNC_TIME_MILLS, () -> {
        synchronized (XlvsGroupMod.INSTANCE.getGroupHandler().getGroupList()) {
            XlvsGroupMod.INSTANCE.getGroupHandler().getGroupList().forEach(group -> sendPacketToGroup(group, new PacketGroupSync()));
        }
    });
//    private final ScheduledTask groupStatUpdateScheduledTask = new ScheduledTask(GROUP_STAT_UPDATE_TIME_MILLS, () -> {
//        synchronized (groupList) {
//            groupList.forEach(group -> {
//                group.getPlayers()
//            });
//        }
//    });

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(TickEvent.ServerTickEvent event) {
        inviteUpdateScheduledTask.update();
        groupSyncScheduledTask.update();
    }

    @SubscribeEvent
    public void event(ServerPlayerLoginEvent event) {
        XlvsGroupMod.INSTANCE.getGroupHandler().handleLeave(event.getServerPlayer());
    }

    private void sendPacketToGroup(GroupServer group, IPacketOutServer packet) {
        for (ServerPlayer serverPlayer : group.getPlayers()) {
            if (serverPlayer != null) {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), packet);
            }
        }
    }
}
