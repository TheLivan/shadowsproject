package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.result.GroupInviteAcceptResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketGroupInviteAccept implements IPacketCallbackOnServer {

    private GroupInviteAcceptResult groupInviteAcceptResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String initiatorName = bbis.readUTF();
        groupInviteAcceptResult = XlvsGroupMod.INSTANCE.getGroupHandler().handleInviteAccept(initiatorName, entityPlayer.getCommandSenderName());
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(groupInviteAcceptResult.getResponseMessage());
    }
}
