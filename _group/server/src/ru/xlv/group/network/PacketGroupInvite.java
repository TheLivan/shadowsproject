package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.result.GroupInviteResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketGroupInvite implements IPacketCallbackOnServer {

    private GroupInviteResult groupInviteResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String target = bbis.readUTF();
        groupInviteResult = XlvsGroupMod.INSTANCE.getGroupHandler().handleInvite(entityPlayer.getCommandSenderName(), target);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(groupInviteResult.getResponseMessage());
    }
}
