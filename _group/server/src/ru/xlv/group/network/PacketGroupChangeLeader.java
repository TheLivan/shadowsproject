package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.result.GroupChangeLeaderResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketGroupChangeLeader implements IPacketCallbackOnServer {

    private GroupChangeLeaderResult groupChangeLeaderResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String username = bbis.readUTF();
        groupChangeLeaderResult = XlvsGroupMod.INSTANCE.getGroupHandler().handleChangeLeader(entityPlayer.getCommandSenderName(), username);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(groupChangeLeaderResult.getResponseMessage());
    }
}
