package ru.krogenit.advancedblockmodels;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel.VectorAABB;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;

public class ServerTickHandler {
	
	@SubscribeEvent
	@SuppressWarnings("unused")
	public void event(TickEvent.ServerTickEvent event) {
		if(event.phase == Phase.END) {
			checkTEModels();
		}
	}
	
	private void checkTEModels() {
		World w = MinecraftServer.getServer().worldServers[0];
		synchronized (TEBlockModel.posToAdd) {
			for (int i = 0; i < TEBlockModel.posToAdd.size(); i++) {
				VectorAABB vect = TEBlockModel.posToAdd.get(i);
				if (TEBlockModel.placeTEHelp(w, vect.aabb, (int) vect.vect.x, (int) vect.vect.y, (int) vect.vect.z, i))
					vect.complete = true;
			}
			for (int i = 0; i < TEBlockModel.posToAdd.size(); i++) {
				VectorAABB vect = TEBlockModel.posToAdd.get(i);
				if (vect.complete) {
					TEBlockModel.posToAdd.remove(i);
					i--;
				}
			}
		}

		synchronized(TEBlockModel.partsToRemove) {
			while (TEBlockModel.partsToRemove.size() > 0) {
				TEModel p = TEBlockModel.partsToRemove.remove(0);
				TEBlockModel te = p.getTe();

				if (te.getParts().size() == 0 && !BlockModel.isTEHaveObject(te))
					w.setBlockToAir(te.xCoord, te.yCoord, te.zCoord);

				AxisAlignedBB axisAlignedBB = p.getAabb();
				if (axisAlignedBB != null) {
					Vector3f position = p.getPosition();
					float newX = position.x + te.xCoord;
					float newY = position.y + te.yCoord;
					float newZ = position.z + te.zCoord;

					int minx = MathHelper.floor_double(axisAlignedBB.minX + newX);
					int miny = MathHelper.floor_double(axisAlignedBB.minY + newY);
					int minz = MathHelper.floor_double(axisAlignedBB.minZ + newZ);
					int maxx = MathHelper.floor_double(axisAlignedBB.maxX + newX) + 1;
					int maxy = MathHelper.floor_double(axisAlignedBB.maxY + newY) + 1;
					int maxz = MathHelper.floor_double(axisAlignedBB.maxZ + newZ) + 1;

					for (int x1 = minx; x1 < maxx; x1++)
						for (int y1 = miny; y1 < maxy; y1++)
							for (int z1 = minz; z1 < maxz; z1++) {
								if (x1 != te.xCoord || y1 != te.yCoord || z1 != te.zCoord) {
									TileEntity tile1 = w.getTileEntity(x1, y1, z1);
									if (tile1 instanceof TEBlockModel) {
										TEBlockModel te1 = (TEBlockModel) tile1;
										te1.getChildParts().remove(p);
										if (te1.getParts().size() == 0 && !BlockModel.isTEHaveObject(te1)) {
											w.setBlockToAir(te1.xCoord, te1.yCoord, te1.zCoord);
										}
									}
								}
							}
				}
			}
		}
	}
}
