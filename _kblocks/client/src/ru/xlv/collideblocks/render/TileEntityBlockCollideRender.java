package ru.xlv.collideblocks.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Vec3;
import org.lwjgl.opengl.GL11;
import ru.xlv.collideblocks.common.tile.TileEntityBlockCollide;

public class TileEntityBlockCollideRender extends TileEntitySpecialRenderer {

	public static boolean iDebugEnabled;

	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick) {
		render((TileEntityBlockCollide) tileEntity, x, y, z, timeTick);
	}

	@Override
	public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

	}

	private void render(TileEntityBlockCollide tile, double x, double y, double z, float t) {
		if (!iDebugEnabled)
			return;
		GL11.glPushMatrix();
		GL11.glTranslated(x, y, z);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glColor4f(0, 1, 0, 0.4f);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDepthMask(false);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		Minecraft.getMinecraft().entityRenderer.disableLightmap(0D);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		Vec3 min = Vec3.createVectorHelper(tile.minX, tile.minY, tile.minZ);
		Vec3 max = Vec3.createVectorHelper(tile.maxX - 1, tile.maxY - 1, tile.maxZ - 1);
		drawWorldBounds(Tessellator.instance, min, max, false);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		Minecraft.getMinecraft().entityRenderer.enableLightmap(0D);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glPopMatrix();
	}

	public static void drawWorldBounds(Tessellator t, Vec3 firstEdge, Vec3 secondEdge, boolean check) {
		t.startDrawingQuads();

		double yMinPos = Double.min(firstEdge.yCoord, secondEdge.yCoord);
		double yMaxPos = Double.max(firstEdge.yCoord, secondEdge.yCoord);

		double xMinPos = Double.min(firstEdge.xCoord, secondEdge.xCoord);
		double xMaxPos = Double.max(firstEdge.xCoord, secondEdge.xCoord);

		double zMinPos = Double.min(firstEdge.zCoord, secondEdge.zCoord);
		double zMaxPos = Double.max(firstEdge.zCoord, secondEdge.zCoord);
		if (!check) {
			yMinPos = firstEdge.yCoord;
			yMaxPos = secondEdge.yCoord;
			xMinPos = firstEdge.xCoord;
			xMaxPos = secondEdge.xCoord;
			zMinPos = firstEdge.zCoord;
			zMaxPos = secondEdge.zCoord;
		}

		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 0.001f + 1);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 0.001f + 1);

		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 0.001f + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 0.001f + 1);

		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 0.001f + 1);
		t.addVertex(xMaxPos + 1, yMinPos, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMinPos, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMinPos, zMaxPos + 0.001f + 1);

		t.addVertex(xMaxPos + 1, yMinPos, zMinPos + 0.001f);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos + 0.001f);
		t.addVertex(xMinPos, yMinPos, zMinPos + 0.001f);

		t.addVertex(xMaxPos + 1 - 0.001f, yMinPos, zMaxPos + 1);
		t.addVertex(xMaxPos + 1 - 0.001f, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMaxPos + 1 - 0.001f, yMaxPos + 1, zMinPos);
		t.addVertex(xMaxPos + 1 - 0.001f, yMinPos, zMinPos);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 1 - 0.001f);
		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1 - 0.001f);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1 - 0.001f);
		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1 - 0.001f);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos);
		t.addVertex(xMinPos, yMinPos, zMinPos);

		t.addVertex(xMinPos + 0.001f, yMinPos, zMinPos);
		t.addVertex(xMinPos + 0.001f, yMaxPos + 1, zMinPos);
		t.addVertex(xMinPos + 0.001f, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMinPos + 0.001f, yMinPos, zMaxPos + 1);

		t.addVertex(xMaxPos + 1, yMinPos, zMinPos);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1);

		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMinPos, yMinPos, zMaxPos + 1);

		t.addVertex(xMinPos, yMinPos, zMinPos);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos);
		t.addVertex(xMaxPos + 1, yMinPos, zMinPos);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 0.001f + 1);
		t.addVertex(xMinPos, yMinPos, zMinPos + 0.001f);
		t.addVertex(xMaxPos + 1, yMinPos, zMinPos + 0.001f);
		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 0.001f + 1);

		t.draw();

		t.startDrawing(GL11.GL_LINES);
		t.setColorRGBA(0, 0, 0, 255);
		GL11.glLineWidth(1);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 1);
		t.addVertex(xMinPos, yMinPos, zMinPos);

		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos);

		t.addVertex(xMinPos, yMinPos, zMinPos);
		t.addVertex(xMinPos, yMaxPos + 1, zMinPos);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 1);
		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1);

		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMinPos, zMinPos);

		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos);

		t.addVertex(xMaxPos + 1, yMinPos, zMinPos);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos);

		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1);

		t.addVertex(xMinPos, yMaxPos + 1, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMaxPos + 1);

		t.addVertex(xMinPos, yMinPos, zMaxPos + 1);
		t.addVertex(xMaxPos + 1, yMinPos, zMaxPos + 1);

		t.addVertex(xMinPos, yMaxPos + 1, zMinPos);
		t.addVertex(xMaxPos + 1, yMaxPos + 1, zMinPos);

		t.addVertex(xMinPos, yMinPos, zMinPos);
		t.addVertex(xMaxPos + 1, yMinPos, zMinPos);

		t.draw();
	}
}
