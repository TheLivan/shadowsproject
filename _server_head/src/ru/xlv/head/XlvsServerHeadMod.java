package ru.xlv.head;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.head.event.FirstJoinEventListener;
import ru.xlv.head.event.InsureEventListener;

import static ru.xlv.head.XlvsServerHeadMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0",
        acceptableRemoteVersions = "*"
)
@Getter
public class XlvsServerHeadMod {

    static final String MODID = "xlvsserver";

    @Mod.Instance(MODID)
    public static XlvsServerHeadMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        XlvsCoreCommon.EVENT_BUS.register(new InsureEventListener());
        XlvsCoreCommon.EVENT_BUS.register(new FirstJoinEventListener());
    }
}
