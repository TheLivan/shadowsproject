package ru.krogenit.lighting;

import net.minecraft.client.Minecraft;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import ru.krogenit.lighting.gui.GuiBlockLight;
import ru.krogenit.lighting.network.PacketDispatcherLighting;
import ru.krogenit.lighting.network.PacketUpdateBlockLight;

public class LightPlacer {

	private static Minecraft mc = Minecraft.getMinecraft();

	public static void placeLight() {
		if(mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectType.BLOCK) {
			int x = mc.objectMouseOver.blockX;
			int y = mc.objectMouseOver.blockY;
			int z = mc.objectMouseOver.blockZ;
			PointLight p = LightManagerClient.getLight(x, y, z);
			if(p != null) {
				mc.displayGuiScreen(new GuiBlockLight(x, y, z, p));
			} else {
				p = new PointLight(1.0f, 1.0f, 1.0f, 10f);
				mc.displayGuiScreen(new GuiBlockLight(x,y,z, p));
				PacketDispatcherLighting.sendToServer(new PacketUpdateBlockLight(p.power, p.color.x, p.color.y, p.color.z, x, y, z));
			}
		}
	}
}
