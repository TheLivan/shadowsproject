package ru.krogenit.lighting;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;

public class GuiLightingEvents {

	@SubscribeEvent
	@SuppressWarnings("unused")
	public void event(GuiOpenEvent event) {
		if(event.gui instanceof GuiMainMenu || event.gui instanceof GuiDisconnected) {
			LightManagerClient.clear();
		}
	}
	
}
