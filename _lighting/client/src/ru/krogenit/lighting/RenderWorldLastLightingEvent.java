package ru.krogenit.lighting;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.opengl.GL11;

import java.util.List;

public class RenderWorldLastLightingEvent {

    static Tessellator tessellator = Tessellator.instance;
    static Minecraft mc = Minecraft.getMinecraft();

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(RenderWorldLastEvent event) {
        if(mc.thePlayer.capabilities.isCreativeMode && mc.thePlayer.inventory.getCurrentItem() != null && mc.thePlayer.inventory.getCurrentItem().getItem() == CoreLightingCommon.itemLightPlacer) {
            List<PointLight> lights = LightManagerClient.getNearestLightsInFrustum();

            GL11.glPushMatrix();
            GL11.glTranslatef((float)-TileEntityRendererDispatcher.staticPlayerX,
                    (float)-TileEntityRendererDispatcher.staticPlayerY, (float)-TileEntityRendererDispatcher.staticPlayerZ);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glDepthMask(true);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
            GL11.glColor4f(1f,1f,1f,0.5f);
            tessellator.startDrawingQuads();
            for(PointLight p : lights) {
                renderRegion(p);
            }
            tessellator.draw();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glPopMatrix();
        }
    }

    private static void renderRegion(PointLight p) {
        float size = 0.1f;
        float xpos = p.pos.x - size;
        float ypos = p.pos.y - size;
        float zpos = p.pos.z - size;
        float xdist = 0.4f;
        float ydist = 0.4f;
        float zdist = 0.4f;
        float xsize = p.pos.x + size;
        float ysize = p.pos.y + size;
        float zsize = p.pos.z + size;

        float maxColor = Math.max(p.color.x, p.color.y);
        maxColor = Math.max(p.color.z, maxColor);

        tessellator.setColorOpaque_F(p.color.x / maxColor, p.color.y / maxColor, p.color.z / maxColor);
        tessellator.addVertexWithUV(xpos, ysize, zpos, 0, ydist);
        tessellator.addVertexWithUV(xsize, ysize, zpos, xdist, ydist);
        tessellator.addVertexWithUV(xsize, ypos, zpos, xdist, 0);
        tessellator.addVertexWithUV(xpos, ypos, zpos, 0, 0);

        tessellator.addVertexWithUV(xpos, ysize, zsize, 0, ydist);
        tessellator.addVertexWithUV(xpos, ysize, zpos, zdist, ydist);
        tessellator.addVertexWithUV(xpos, ypos, zpos, zdist, 0.0F);
        tessellator.addVertexWithUV(xpos, ypos, zsize, 0, 0.0F);

        tessellator.addVertexWithUV(xsize, ypos, zsize, xdist, 0.0F);
        tessellator.addVertexWithUV(xsize, ysize, zsize, xdist, ydist);
        tessellator.addVertexWithUV(xpos, ysize, zsize, 0, ydist);
        tessellator.addVertexWithUV(xpos, ypos, zsize, 0, 0.0F);

        tessellator.addVertexWithUV(xsize, ysize, zpos, 0, ydist);
        tessellator.addVertexWithUV(xsize, ysize, zsize, zdist, ydist);
        tessellator.addVertexWithUV(xsize, ypos, zsize, zdist, 0.0F);
        tessellator.addVertexWithUV(xsize, ypos, zpos, 0, 0.0F);

        tessellator.addVertexWithUV(xpos, ypos, zpos, 0, xdist);
        tessellator.addVertexWithUV(xsize, ypos, zpos, 0, 0.0F);
        tessellator.addVertexWithUV(xsize, ypos, zsize, zdist, 0.0F);
        tessellator.addVertexWithUV(xpos, ypos, zsize, zdist, xdist);

        tessellator.addVertexWithUV(xpos, ysize, zpos, 0, xdist);
        tessellator.addVertexWithUV(xpos, ysize, zsize, zdist, xdist);
        tessellator.addVertexWithUV(xsize, ysize, zsize, zdist, 0.0F);
        tessellator.addVertexWithUV(xsize, ysize, zpos, 0, 0.0F);
    }
}
