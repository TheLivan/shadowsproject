package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import ru.krogenit.lighting.EnumLightEvent;
import ru.krogenit.lighting.LightManagerServer;

public class PacketUpdateLightHandlerServer extends AbstractServerMessageHandler<PacketUpdateLight> {

    @Override
    public IMessage handleMessageOnServerSide(PacketUpdateLight msg, MessageContext ctx) {
        LightManagerServer.removeLight(msg.getX(), msg.getY(), msg.getZ());
        PacketDispatcherLighting.sendToAllAround(new PacketUpdateLight(msg.getX(), msg.getY(), msg.getZ(), 0, EnumLightEvent.Remove), 0, msg.getX(), msg.getY(), msg.getZ(), 128);

        return null;
    }
}
