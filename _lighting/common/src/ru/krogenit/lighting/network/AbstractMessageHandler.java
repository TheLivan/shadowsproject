package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class AbstractMessageHandler<T extends IMessage> implements IMessageHandler<T, IMessage> {
	@SideOnly(Side.CLIENT)
	public abstract IMessage handleMessageOnClientSide(T msg, MessageContext ctx);

	public abstract IMessage handleMessageOnServerSide(T msg, MessageContext ctx);

	@Override
	public IMessage onMessage(T msg, MessageContext ctx) {
		if (ctx.side.isClient()) {
			return handleMessageOnClientSide(msg, ctx);
		} else {
			return handleMessageOnServerSide(msg, ctx);
		}
	}
}
