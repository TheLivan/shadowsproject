package ru.krogenit.lighting;

public enum EnumLightEvent {
    Add((byte) 0), Remove((byte) 1), RemoveStandardModel((byte) 2);

    private byte type;

    EnumLightEvent(byte b) {
        this.type = b;
    }

    public byte toByte() {
        return type;
    }
}