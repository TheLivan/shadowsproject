package ru.xlv.friend.common;

import lombok.Data;

@Data
public class FriendRelation {

    public enum State {
        INVITATION,
        FRIENDS
    }

    private final String initiator, target;
    private State state = State.INVITATION;
}
