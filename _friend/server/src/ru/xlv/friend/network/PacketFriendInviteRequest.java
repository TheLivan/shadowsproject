package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.handle.result.FriendInviteResult;
import ru.xlv.mochar.network.PacketClientPlayerSync;

import java.io.IOException;

@NoArgsConstructor
public class PacketFriendInviteRequest implements IPacketCallbackOnServer {

    private static final RequestController<String> REQUEST_CONTROLLER = new RequestController.Periodic<>(200L);

    private FriendInviteResult friendInviteResult;
    private String targetName;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        if (REQUEST_CONTROLLER.tryRequest(entityPlayer.getCommandSenderName())) {
            targetName = bbis.readUTF();
            XlvsFriendMod.INSTANCE.getFriendHandler().handleRelation(entityPlayer.getCommandSenderName(), targetName).thenAccept(friendInviteResult -> {
                this.friendInviteResult = friendInviteResult;
                packetCallbackSender.send();
            });
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(friendInviteResult != null);
        if(friendInviteResult != null) {
            FlexPlayer.of(targetName)
                    .accept(serverPlayer -> {
                        try {
                            bbos.writeBoolean(serverPlayer != null);
                            if(serverPlayer != null) {
                                PacketClientPlayerSync.writeClientPlayer(serverPlayer, bbos);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
            bbos.writeUTF(friendInviteResult.getResponseMessage());
        } else {
            bbos.writeUTF(XlvsCore.INSTANCE.getLocalization().responseTooManyRequests);
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
