package ru.xlv.friend.handle;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.friend.common.FriendRelation;

import java.util.*;
import java.util.stream.Collectors;

public class FriendHandler {

    @Getter
    private final List<FriendRelation> relations = Collections.synchronizedList(new ArrayList<>());

    public void sync(List<FriendRelation> list) {
        synchronized (relations) {
            relations.clear();
            relations.addAll(list);
        }
    }

    public void removeFriend(String playerName) {
        synchronized (relations) {
            for(int i = 0; i < relations.size();i++) {
                FriendRelation relation = relations.get(i);
                if((relation.getTarget().equals(playerName) || relation.getInitiator().equals(playerName)) &&
                        relation.getState() == FriendRelation.State.FRIENDS) {
                    relations.remove(relation);
                    return;
                }
            }
        }
    }

    public void addFriend(String playerName) {
        synchronized (relations) {
            for (FriendRelation relation : relations) {
                if (relation.getInitiator().equals(playerName) &&
                        relation.getState() == FriendRelation.State.INVITATION) {
                    relation.setState(FriendRelation.State.FRIENDS);
                    return;
                }
            }
        }
    }

    public void removeRequest(String playerName) {
        synchronized (relations) {
            for(int i = 0; i < relations.size();i++) {
                FriendRelation relation = relations.get(i);
                if (relation.getInitiator().equals(playerName) &&
                        relation.getState() == FriendRelation.State.INVITATION) {
                    relations.remove(relation);
                    return;
                }
            }
        }
    }

    public boolean isFriend(String playerName) {
        synchronized (relations) {
            for(FriendRelation relation : relations) {
                if((relation.getTarget().equals(playerName) || relation.getInitiator().equals(playerName)) &&
                relation.getState() == FriendRelation.State.FRIENDS) return true;
            }
        }

        return false;
    }

    public List<EntityPlayer> getAllOnlineFriends() {
        Minecraft mc = Minecraft.getMinecraft();
        List<FriendRelation> collect;
        synchronized (relations) {
            collect = relations.stream()
                    .filter(friendRelation -> friendRelation.getState() == FriendRelation.State.FRIENDS)
                    .collect(Collectors.toList());
        }

        List<EntityPlayer> onlineFriends = new ArrayList<>();
        for(FriendRelation relation : collect) {
            String friendName = (relation.getTarget().equals(mc.thePlayer.getDisplayName()) ? relation.getInitiator() : relation.getTarget());
            synchronized (mc.theWorld.playerEntities) {
                EntityPlayer entityPlayer = mc.theWorld.getPlayerEntityByName(friendName);
                if(entityPlayer != null) {
                    onlineFriends.add(entityPlayer);
                }
            }
        }

        return onlineFriends;
    }

    public List<FriendRelation> getAllFriends() {
        synchronized (relations) {
            return relations.stream()
                    .filter(friendRelation -> friendRelation.getState() == FriendRelation.State.FRIENDS)
                    .collect(Collectors.toList());
        }
    }

    public Set<String> getAllFriendNames() {
        Set<String> set = new HashSet<>();
        getAllFriends().forEach(friendRelation -> {
            set.add(friendRelation.getInitiator());
            set.add(friendRelation.getTarget());
        });
        set.remove(Minecraft.getMinecraft().thePlayer.getCommandSenderName());
        return set;
    }

    public List<FriendRelation> getAllIncomingInvites() {
        synchronized (relations) {
            return relations.stream()
                    .filter(friendRelation -> !friendRelation.getInitiator().equals(Minecraft.getMinecraft().thePlayer.getCommandSenderName()) &&
                            friendRelation.getState() == FriendRelation.State.INVITATION)
                    .collect(Collectors.toList());
        }
    }

    public List<FriendRelation> getAllOutgoingInvites() {
        synchronized (relations) {
            return relations.stream()
                    .filter(friendRelation -> friendRelation.getInitiator().equals(Minecraft.getMinecraft().thePlayer.getCommandSenderName()) &&
                            friendRelation.getState() == FriendRelation.State.INVITATION)
                    .collect(Collectors.toList());
        }
    }
}
