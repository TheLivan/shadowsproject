package ru.xlv.friend.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.friend.common.FriendIO;
import ru.xlv.friend.common.FriendRelation;

import java.io.IOException;

@NoArgsConstructor
public class PacketFriendInvite implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        FriendRelation friendRelation = FriendIO.readFriendRelation(bbis);
        //todo обработка входящего запроса дружбы
    }
}
