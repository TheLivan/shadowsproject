package ru.xlv.cases.handle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.ItemStack;

@Getter
@RequiredArgsConstructor
public class CaseItem {

    private final ItemStack itemStack;
    private final int chance;
}
