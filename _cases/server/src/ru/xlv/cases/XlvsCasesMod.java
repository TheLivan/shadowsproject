package ru.xlv.cases;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.cases.handle.CaseHandler;
import ru.xlv.cases.handle.CaseManager;
import ru.xlv.cases.network.PacketCaseListSync;
import ru.xlv.cases.network.PacketCaseRoll;
import ru.xlv.cases.util.CaseLocalization;
import ru.xlv.cases.util.ConfigCases;
import ru.xlv.core.XlvsCore;

import static ru.xlv.cases.XlvsCasesMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
@Getter
public class XlvsCasesMod {

    static final String MODID = "xlvscases";

    @Mod.Instance(MODID)
    public static XlvsCasesMod INSTANCE;

    private CaseHandler caseHandler;

    private final CaseLocalization localization = new CaseLocalization();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        localization.load();
        ConfigCases config = new ConfigCases();
        config.load();
        CaseManager caseManager = new CaseManager(config);
        caseHandler = new CaseHandler(caseManager);
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketCaseRoll(),
                new PacketCaseListSync()
        );
    }
}
