package ru.xlv.cases.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class ConfigCases implements IConfigGson {

    @Getter
    @RequiredArgsConstructor
    @Configurable
    public static class CaseModel {
        private final String name;
        private final int price;
        private final String imageUrl;
        private final List<CaseItemModel> items = new ArrayList<>();
    }

    @Getter
    @RequiredArgsConstructor
    @Configurable
    public static class CaseItemModel {
        private final String unlocalizedName;
        private final int metadata;
        private final int amount;
        private final int chance;
        private final String jsonNbtTagCompound;
    }

    private final List<CaseModel> cases = new ArrayList<>();

    private transient final File configFile = new File("config/cases/config.json");
}
