package ru.xlv.cases.handle;

import lombok.experimental.UtilityClass;
import ru.xlv.cases.network.PacketCaseListSync;
import ru.xlv.cases.pojo.Case;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.SyncResultHandler;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class CaseHandler {

    private final List<Case> CASES = new ArrayList<>();

    public SyncResultHandler<List<Case>> syncCases() {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketCaseListSync());
    }

    public List<Case> getCases() {
        return CASES;
    }
}
