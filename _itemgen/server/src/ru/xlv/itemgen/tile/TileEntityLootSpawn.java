package ru.xlv.itemgen.tile;

import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityChest;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;
import ru.xlv.core.common.storage.NBTLoader;
import ru.xlv.core.item.ItemStackFactory;
import ru.xlv.itemgen.XlvsItemGenMod;
import ru.xlv.itemgen.block.BlockLootSpawn;
import ru.xlv.itemgen.util.LootSpawnConfig;

import javax.annotation.Nonnull;
import java.util.Random;

@Getter
public class TileEntityLootSpawn extends TileEntityChest implements IMatrixInventoryProvider {

    private static final String SPAWN_TIME_KEY = "spawnTime";

    private LootSpawnConfig.LootSpawnModel config;

    private long nextSpawnTimeMills;

    private MatrixInventory matrixInventory;

    public TileEntityLootSpawn() {}

    public TileEntityLootSpawn(LootSpawnConfig.LootSpawnModel config) {
        this.config = config;
        matrixInventory = MatrixInventoryFactory.create();
    }

    public void interact() {
        if (matrixInventory == null) {
            return;
        }
        if(System.currentTimeMillis() >= nextSpawnTimeMills) {
            if(config == null) {
                Block block = getWorldObj().getBlock(xCoord, yCoord, zCoord);
                if (block instanceof BlockLootSpawn) {
                    config = XlvsItemGenMod.INSTANCE.getConfig().getLootSpawnConfig(((BlockLootSpawn) block).getType());
                }
            }
            nextSpawnTimeMills = System.currentTimeMillis() + config.getSpawnPeriodSecs() * 1000L;
            Random random = new Random();
            matrixInventory.clear();
            matrixInventory.setMatrixSize(10, 30);
            config.getItems().forEach(itemStackModel -> {
                float f = random.nextFloat() * 100;
                if(itemStackModel.getChance() >= f) {
                    ItemStack itemStack = ItemStackFactory.create(itemStackModel.getUnlocalizedName(), itemStackModel.getAmount(), itemStackModel.getMetadata(), itemStackModel.getNbtJson());
                    if(itemStack == null || itemStack.getItem() == null) {
                        try {
                            throw new RuntimeException("Bad generated item found: " + config);
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                    matrixInventory.addItem(itemStack);
//                    while (matrixInventory.addItem(itemStack).isFailure()) {
//                        matrixInventory.resizeMatrix(matrixInventory.getWidth(), matrixInventory.getHeight() + 1);
//                    }
                }
            });
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        if(matrixInventory == null) {
            matrixInventory = MatrixInventoryFactory.create(10, 30);
        }
        matrixInventory.readFromNBT(p_145839_1_, new NBTLoader(p_145839_1_));
        if (p_145839_1_.hasKey(SPAWN_TIME_KEY)) {
            nextSpawnTimeMills = p_145839_1_.getLong(SPAWN_TIME_KEY);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        if (matrixInventory != null) {
            matrixInventory.writeToNBT(p_145841_1_, new NBTLoader(p_145841_1_));
            p_145841_1_.setLong(SPAWN_TIME_KEY, nextSpawnTimeMills);
        }
    }

    @Override
    public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
        return entityPlayer.getDistance(xCoord, yCoord, zCoord) < 4;
    }
}
