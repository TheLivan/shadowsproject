package ru.xlv.post.handle;

import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.achievement.AchievementType;
import ru.xlv.core.common.inventory.result.AddItemResult;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Maps;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachment;
import ru.xlv.post.common.PostAttachmentCredits;
import ru.xlv.post.common.PostAttachmentItemStack;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.result.PostDeleteResult;
import ru.xlv.post.handle.result.PostSendResult;
import ru.xlv.post.handle.result.PostTakeAttachmentsResult;
import ru.xlv.post.network.PacketPostNew;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class PostHandler {

    @Getter
    private final Map<String, List<PostObject>> postObjectMap = new HashMap<>();

    public void init() {
        postObjectMap.putAll(XlvsPostMod.INSTANCE.getDatabaseManager().getAllPostSync());
    }

    public void shutdown() {}

    public void markPostViewed(@Nonnull String recipient, @Nonnull String postUUID) {
        UUID uuid = UUID.fromString(postUUID);
        List<PostObject> allPost = getAllPost(recipient);
        if (allPost != null) {
            for (PostObject postObject : allPost) {
                if(postObject.getUuid().equals(uuid)) {
                    postObject.setViewed(true);
                    XlvsPostMod.INSTANCE.getDatabaseManager().updatePostObject(recipient, postObject).thenAccept(aBoolean -> {
                        if(!aBoolean) {
                            throw new RuntimeException("An error has occurred during updating a PostObject " + postObject + ". No object has updated.");
                        }
                    });
                    break;
                }
            }
        }
    }

    public PostAttachmentItemStack getPostAttachmentItemStack(@Nonnull EntityPlayer entityPlayer, int matrixX, int matrixY) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer != null) {
            ItemStack itemStack = serverPlayer.getSelectedCharacter().getMatrixInventory().getMatrixItem(matrixX, matrixY);
            if (itemStack != null) {
                return new PostAttachmentItemStack(itemStack);
            }
        }
        return null;
    }

    public PostSendResult sendPostSync(@Nonnull PostObject postObject) {
        postObject.setCreationTimeMills(System.currentTimeMillis());
        return sendPost0(postObject);
    }

    public CompletableFuture<PostSendResult> sendPost(@Nonnull PostObject postObject) {
        postObject.setCreationTimeMills(System.currentTimeMillis());
        return CompletableFuture.supplyAsync(() -> sendPost0(postObject));
    }

    private PostSendResult sendPost0(PostObject postObject) {
        try {
            XlvsPostMod.INSTANCE.getDatabaseManager().sendPostObjectSync(postObject);
        } catch (Exception e) {
            e.printStackTrace();
            return PostSendResult.DATABASE_ERROR;
        }
        Maps.addElemToMappedList(postObjectMap, postObject.getSender(), postObject);
        Maps.addElemToMappedList(postObjectMap, postObject.getRecipient(), postObject);
        processSendPost(postObject);
        return PostSendResult.SUCCESS;
    }

    private void processSendPost(PostObject postObject) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(postObject.getSender());
        if (serverPlayer != null && serverPlayer.isOnline()) {
            StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
            statManager.getStatProvider(StatType.TOTAL_POST_SENT).increment();
            XlvsCore.INSTANCE.getAchievementHandler().updateAchievement(serverPlayer, AchievementType.SEND_POST);
        }
        serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(postObject.getRecipient());
        if (serverPlayer != null && serverPlayer.isOnline()) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketPostNew(postObject));
        }
    }

    public CompletableFuture<PostTakeAttachmentsResult> takePostAttachments(@Nonnull EntityPlayer entityPlayer, @Nonnull String postUUID) {
        return takePostAttachments(entityPlayer, getPostObject(entityPlayer.getCommandSenderName(), postUUID));
    }

    public CompletableFuture<PostTakeAttachmentsResult> takePostAttachments(@Nonnull EntityPlayer entityPlayer, @Nullable PostObject postObject) {
        if (postObject == null) {
            return CompletableFuture.completedFuture(PostTakeAttachmentsResult.POST_OBJECT_NOT_FOUND);
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer == null) {
            return CompletableFuture.completedFuture(PostTakeAttachmentsResult.UNKNOWN_ERROR);
        }
        if(!entityPlayer.getCommandSenderName().equals(postObject.getRecipient())) {
            return CompletableFuture.completedFuture(PostTakeAttachmentsResult.PLAYER_NOT_RECIPIENT);
        }
        Iterator<PostAttachment<?>> iterator = postObject.getAttachments().iterator();
        PostTakeAttachmentsResult postTakeAttachmentsResult = PostTakeAttachmentsResult.SUCCESS;
        while (iterator.hasNext()) {
            PostAttachment<?> next = iterator.next();
            if(next instanceof PostAttachmentItemStack) {
                AddItemResult addItemResult = serverPlayer.getSelectedCharacter().getMatrixInventory().addItem(((PostAttachmentItemStack) next).getAttachment());
                if(addItemResult != AddItemResult.SUCCESS) {
                    postTakeAttachmentsResult = PostTakeAttachmentsResult.NO_FREE_INV_SPACE;
                    continue;
                }
            } else if(next instanceof PostAttachmentCredits) {
                serverPlayer.getSelectedCharacter().getWallet().addCredits(((PostAttachmentCredits) next).getAttachment());
            }
            iterator.remove();
        }
        if(postTakeAttachmentsResult == PostTakeAttachmentsResult.SUCCESS) {
            return XlvsPostMod.INSTANCE.getDatabaseManager().updatePostObject(entityPlayer.getCommandSenderName(), postObject).thenApply(aBoolean -> aBoolean ? PostTakeAttachmentsResult.SUCCESS : PostTakeAttachmentsResult.UNKNOWN_ERROR);
        } else {
            return CompletableFuture.completedFuture(postTakeAttachmentsResult);
        }
    }

    public CompletableFuture<PostDeleteResult> deletePostObject(@Nonnull EntityPlayer entityPlayer, @Nonnull String postUuid) {
        PostObject postObject = getPostObject(entityPlayer.getCommandSenderName(), postUuid);
        if (postObject == null) {
            return CompletableFuture.completedFuture(PostDeleteResult.POST_NOT_FOUND);
        }
        return XlvsPostMod.INSTANCE.getDatabaseManager().removePostObject(entityPlayer.getCommandSenderName(), postObject)
                .thenApply(aBoolean -> aBoolean ? PostDeleteResult.SUCCESS : PostDeleteResult.DATABASE_ERROR)
                .thenApply(postDeleteResult -> {
                    if(postDeleteResult == PostDeleteResult.SUCCESS) {
                        Maps.removeElemFromMappedList(postObjectMap, entityPlayer.getCommandSenderName(), postObject);
                    }
                    return postDeleteResult;
                });
    }

    @Nullable
    public PostObject getPostObject(@Nonnull String username, @Nonnull String uuid) {
        UUID uuid1 = UUID.fromString(uuid);
        return Maps.getElemFromMappedList(postObjectMap, username, postObject -> postObject.getUuid().equals(uuid1));
    }

    @Nullable
    public List<PostObject> getAllPost(@Nonnull String username) {
        return postObjectMap.get(username);
    }
}
