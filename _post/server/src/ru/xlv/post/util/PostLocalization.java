package ru.xlv.post.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class PostLocalization extends Localization {

    private final String responseSendResultSuccessMessage = "responseSendResultSuccessMessage";
    private final String responseSendResultUnknownErrorMessage = "responseSendResultUnknownErrorMessage";
    private final String responseSendResultDatabaseErrorMessage = "responseSendResultDatabaseErrorMessage";

    private final String responseTakeAttResultSuccessMessage = "responseTakeAttResultSuccessMessage";
    private final String responseTakeAttResultPostObjectNotFoundMessage = "responseTakeAttResultPostObjectNotFoundMessage";
    private final String responseTakeAttResultNotFreeInvSpaceMessage = "responseTakeAttResultNotFreeInvSpaceMessage";
    private final String responseTakeAttResultPlayerNotRecipientMessage = "responseTakeAttResultPlayerNotRecipientMessage";
    private final String responseTakeAttResultUnknownErrorMessage = "responseTakeAttResultUnknownErrorMessage";

    private final String responseDeleteResultSuccessMessage = "responseDeleteResultSuccessMessage";
    private final String responseDeleteResultPostNotFoundMessage = "responseDeleteResultPostNotFoundMessage";
    private final String responseDeleteResultDatabaseErrorMessage = "responseDeleteResultDatabaseErrorMessage";

    @Override
    public File getConfigFile() {
        return new File("config/post/localization.json");
    }
}
