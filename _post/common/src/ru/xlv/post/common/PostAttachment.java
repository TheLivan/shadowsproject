package ru.xlv.post.common;

import lombok.Data;

@Data
public abstract class PostAttachment<T> {

    private final T attachment;
    private final PostAttachmentType type;
}
